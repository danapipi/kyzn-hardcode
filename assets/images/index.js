const pp_image = require("./ic_dummy_pp.png");
const xp_icon = require("./ic_xp.png");
const carouselBackground_1 = require("./carousel_background_1.png");
const landingFooter = require("./landingFooter.png");
const fadedRect = require("./fading_rect.png");
const bookingIcon = require("./booking_ic.png");
const educationIcon = require("./education_ic.png");
const fitnessIcon = require("./fitness_ic.png");
const nutritionIcon = require("./nutrition_ic.png");
const sportIcon = require("./sport_ic.png");
const wellnessIcon = require("./wellnes_ic.png");
const bookingBG = require("./booking_bg.png");
const educationBG = require("./education_bg.png");
const fitnessBG = require("./fitness_bg.png");
const nutritionBG = require("./nutrition_bg.png");
const sportBG = require("./sport_bg.png");
const wellnessBG = require("./wellness_bg.png");
const exp = require("./xp.png");
const coin = require("./coin.png");
const profile = require("./profile.png");
const level = require("./level.png");
const background = require("./background.png");
const home_active = require("./home_active.png");
const home_inactive = require("./home_inactive.png");
const mail_inactive = require("./mail_inactive.png");
const mail_active = require("./mail_active.png");
const profile_active = require("./profile_active.png");
const profile_inactive = require("./profile_inactive.png");
const activity_active = require("./activity_active.png");
const activity_inactive = require("./activity_inactive.png");
const beauty_icon = require("./beauty_icon.png");
const fitness1_icon = require("./fitness1_icon.png");
const fitness2_icon = require("./fitness2_icon.png");
const fitness3_icon = require("./fitness3_icon.png");
const fitness4_icon = require("./fitness4_icon.png");
const fitness5_icon = require("./fitness5_icon.png");
const healing_icon = require("./healing_icon.png");
const medicine_icon = require("./healing_icon.png");
const meditation_icon = require("./meditation_icon.png");
const pilates_icon = require("./pilates_icon.png");
const recovery_icon = require("./recovery_icon.png");
const sports_icon = require("./sports_icon.png");
const yoga_icon = require("./yoga_icon.png");
const search = require("./search.png");
const scan_qr = require("./scan_qr.png");
const logo = require("./logo.png");
const premium_ic = require("./premium_ic.png");
const bg_level = require("./bg_level.png");
const bg_actionsSheet = require("./bg-actionSheet.png");
const rentcount = require("./rentcount_ic.png");
const groupClass = require("./classgroup_ic.png");
const loginLogo = require("./login_logo.png");
const pass_ic = require("./pass_ic.png");
const username_ic = require("./username_ic.png");
const topup_bg = require("./topup_bg.png");
const gopay_ic = require("./gopay_ic.png");
const placeholder = require("./placeholder.png");
const successCorner = require("./success_corner.png");
const successLogo = require("./success_logo.png");
const time_history = require("./ic_time_history.png");
const bgCard = require("./bgcard.png");
const emptyActivity = require("./emptyActivity.png");
const bcaLogo = require("./bca-logo.png");
const succes_ic = require("./succes_ic.png");
const edit_ic = require("./edit_ic.png");
const change_pass_ic = require("./change_pass_ic.png");
const loyalty_ic = require("./loyalty_ic.png");
const qr_ic = require("./qr_ic.png");
const faq = require("./faq.png");
const ic_profile_placeholder = require("./ic_profile_placeholder.png");
const add_photo = require("./add_photo.png");
const camera_ic = require("./camera_ic.png");
const premium_logo = require("./premium_logo.png");
const payment_background = require("./payment_background.png");
const dummy_member_card = require("./dummy_member_card.png");
const qrBackground = require("./qr_background.png");
const qrLogo = require("./qr_logo.png");
const star = require("./star.png");
const banner_dummy = require("./dummy-img.png");
const not_premium_ic = require("./not_premium_ic.png");
const bg_noPremium_actionsheet = require("./bg_noPremium_actionsheet.png");
const ic_coupon = require("./ic_coupon.png");
const ic_active_coupon = require("./ic_active_coupon.png");
const coupon_bg = require("./coupon_bg.png");
const coupon_gradient = require("./coupon_gradient.png");
const zeniImage = require("./ZeniImage.png");
const panoramaImage = require("./PanoramaImage.png");
const scheduleImage = require("./ScheduleImage.png");
const coachImg = require("./coachImg.png");
const ballboyImg = require("./ballboyImg.png");
const checkmark = require("./checkmark.png");
const in_progress = require("./in_progress.png");
const waitinglist = require("./waitinglist.png");
const no_member_img = require("./no_member_img.png");
const img_memberhip_lscape = require("./img_memberhip_lscape.png");
const img_memberhip_potrait = require("./img_memberhip_potrait.png");
const img_product_placeholder = require("./img_product_placeholder.png");
const ic_logout = require("./ic_logout.png");
const ic_privacy = require("./ic_privacy.png");
const ic_referral = require("./ic_referral.png");
const ic_set_preference = require("./ic_set_preference.png");
const ic_term  = require("./ic_term.png");
const referral_img = require("./referral_img.png");
const ic_copy = require("./ic_copy.png");
const ic_dates = require('./ic-dates.png')
const funnel_ic = require('./funnel_ic.png')


export const image = {
  pp_image,
  xp_icon,
  carouselBackground_1,
  landingFooter,
  fadedRect,
  bookingIcon,
  educationIcon,
  fitnessIcon,
  nutritionIcon,
  sportIcon,
  wellnessIcon,
  bookingBG,
  educationBG,
  fitnessBG,
  nutritionBG,
  sportBG,
  wellnessBG,
  exp,
  coin,
  profile,
  level,
  background,
  home_active,
  home_inactive,
  mail_active,
  mail_inactive,
  profile_active,
  profile_inactive,
  activity_active,
  activity_inactive,
  beauty_icon,
  fitness1_icon,
  fitness2_icon,
  fitness3_icon,
  fitness4_icon,
  fitness5_icon,
  healing_icon,
  medicine_icon,
  meditation_icon,
  pilates_icon,
  recovery_icon,
  sports_icon,
  yoga_icon,
  search,
  scan_qr,
  logo,
  premium_ic,
  bg_level,
  bg_actionsSheet,
  rentcount,
  groupClass,
  loginLogo,
  pass_ic,
  username_ic,
  topup_bg,
  gopay_ic,
  placeholder,
  successCorner,
  successLogo,
  time_history,
  bgCard,
  emptyActivity,
  bcaLogo,
  succes_ic,
  edit_ic,
  loyalty_ic,
  change_pass_ic,
  qr_ic,
  faq,
  ic_profile_placeholder,
  add_photo,
  camera_ic,
  premium_logo,
  payment_background,
  dummy_member_card,
  star,
  banner_dummy,
  qrBackground,
  qrLogo,
  not_premium_ic,
  bg_noPremium_actionsheet,
  ic_coupon,
  ic_active_coupon,
  coupon_bg,
  coupon_gradient,
  zeniImage,
  panoramaImage,
  scheduleImage,
  coachImg,
  ballboyImg,
  checkmark,
  in_progress,
  waitinglist,
  no_member_img,
  img_memberhip_lscape,
  img_memberhip_potrait,
  img_product_placeholder,
  ic_logout,
  ic_privacy,
  ic_referral,
  ic_set_preference,
  ic_term,
  referral_img,
  ic_copy,
  ic_dates,
  funnel_ic
};

const dummyMembership = require("./dummy/dummy_membership.png");

export const dummyImages = {
  dummyMembership,
};
