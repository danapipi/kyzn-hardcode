module.exports = {
  root: true,
  extends: "@react-native-community",
  rules: {
    "object-curly-spacing": ["error", "always"],
    quotes: ["error", "double"],
    "react-hooks/exhaustive-deps": "off",
    "react-native/no-inline-styles": "off",
  },
};
