import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, ScrollView } from 'react-native';
import styles from './Times.styles';
import { getTimeLabelHeight } from '../utils';
import { Dimensions } from 'react-native';
import moment from 'moment';

const { width } = Dimensions.get('window');

const Times = ({ times, hoursInDisplay, timeStep, textStyle, data }) => {
  const height = getTimeLabelHeight(hoursInDisplay, timeStep);
  console.log("asjdoquwoiepqowu[ time", times);
  return (
  <View style={{flex:1, flexDirection:"row"}}>
    <ScrollView style={{width:"100%", flexDirection:"row", flex:1}}>
      {/* <View style={[styles.columnContainer]}> */}
        {times.map((item, index) => {
          if(index >= 8 && index <= 21){
            let dataEvent = item.eventDate
            console.log("asjdoquwoiepqowu[ time time ", dataEvent)

            return(
              <View style={{flexDirection:"row"}}>
                <View key={item.time} style={[styles.label, { height: height, width: 60,backgroundColor: index % 2 == 0 ? "#F5F5F5" : "#FFF", alignItems:"center", justifyContent:"center", borderColor:"#E0E0E0", borderWidth:1}]}>
                  <Text style={[styles.text, textStyle]}>{item.time}</Text>
                </View>
                <ScrollView 
                  style={{flex:1, height: 500, width: width, position:"absolute", left: 60}} 
                  horizontal 
                  showsHorizontalScrollIndicator={false}
                  scrollEnabled={true}
                >
                  {dataEvent.length !== 0 && dataEvent.map((item, index) => {
                    let duration = moment.duration(item.endDate.diff(item.startDate));
                    let multiple = duration.asHours();
                    return(
                      <View key={item.time} style={[styles.label, 
                        { 
                            height: 50 * multiple, 
                            width: 96, 
                            backgroundColor: "transparent", 
                            alignItems:"center",
                            justifyContent:"center", 
                            borderColor:"#E0E0E0", 
                            borderWidth:1,
                            marginRight: (dataEvent.length - 1) == index ? 70 : 0,
                            marginTop:5, 
                            marginLeft: 16 
                        }]}>
                        <Text style={[styles.text, textStyle]}>{item.description}</Text>
                        <Text style={[styles.text, textStyle]}>{item.subDescription}</Text>
                      </View>
                    )
                  })}
                </ScrollView>
              </View>
            )
          }
        })}
      {/* </View> */}
    </ScrollView>
  </View>
  );
};

Times.propTypes = {
  times: PropTypes.arrayOf(PropTypes.string).isRequired,
  hoursInDisplay: PropTypes.number.isRequired,
  timeStep: PropTypes.number.isRequired,
  textStyle: Text.propTypes.style,
};

export default React.memo(Times);
