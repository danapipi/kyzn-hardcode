# kyzn-mobile

## Installation steps
- yarn
- cmd/adjust-lib.sh
- cd ios && pod install


## Official guide to running on device
- https://reactnative.dev/docs/running-on-device

## Test run for iOS (Mac only)
- Install XCode
- npm run ios
- Alternatively, open ios/kyzn.xcworkspace on XCode and run from XCode

## Test run for Android
- Install Android Studio
- Connect your device or run simulator
- react-native run-android

## Uploading to Apple Store (Mac only)
- Open ios/kyzn.xcworkspace on XCode
- Follow the XCode guide on uploading
https://instabug.com/blog/how-to-submit-app-to-app-store/

## Generating AAB/APK
- Update the version in file (android/app/build.gradle, search for versionName)
- cd android
- ./gradlew assembleRelease
- AAB and APK will appear in android/build/output
