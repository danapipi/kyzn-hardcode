import React, { useEffect } from "react";
import SplashScreen from "react-native-splash-screen";
import { NativeBaseProvider } from "native-base";
import AppNavigator from "./navigators/app";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import configureStore from "store/configureStore";
import theme from "utils/theme";
import RemotePushController from "./utils/pushNotificationController";
import DynamicLinkController from "./utils/dynamicLinkController";

const App = () => {
  useEffect(() => {
    SplashScreen.hide();
  }, []);

  const { persistor, store } = configureStore();

  return (
    <Provider store={store}>
      <RemotePushController />
      <DynamicLinkController />
      <PersistGate persistor={persistor}>
        <NativeBaseProvider theme={theme}>
          <AppNavigator />
        </NativeBaseProvider>
      </PersistGate>
    </Provider>
  );
};

export default App;
