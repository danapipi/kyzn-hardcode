const NAME = "FAMILY";

export const getUserFamily = store => store[NAME].user.data;
export const getIsLoadingUserFamily = store => store[NAME].user.isLoading;
