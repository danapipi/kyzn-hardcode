const NAME = "FAMILY";

export const getAllFamilyDetails = store =>
  store[NAME].all.data[0]?.user_family_details || [];
export const getAllFamilies = store => store[NAME].all.data || [];
export const getFamilyOne = store => store[NAME].all.dataFamilyOne || [];
export const getIsLoadingAllFamilies = store => store[NAME].all.isLoading;
