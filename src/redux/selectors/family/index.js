import * as user from "./user";
import * as all from "./all";

export default {
  ...user,
  ...all,
};
