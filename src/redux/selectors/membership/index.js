import * as categories from "./categories";
import * as packages from "./packages";
import * as types from "./types";

export default {
  ...categories,
  ...packages,
  ...types,
};
