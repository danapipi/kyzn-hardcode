const NAME = "MEMBERSHIP";

export const getMembershipTypes = store => store[NAME].types;
