const NAME = "MEMBERSHIP";

export const getMembershipPackages = store => store[NAME].packages;
export const getMembershipById = store => store[NAME].membershipById;
export const isLoadingMembershipById = store => store[NAME].membershipById.isLoading;
