const NAME = "MEMBERSHIP";

export const getMembershipCategories = store => store[NAME].categories.data;
export const getIsMembershipCategoriesLoading = store =>
  store[NAME].categories.isLoading;
