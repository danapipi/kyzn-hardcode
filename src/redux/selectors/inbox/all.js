const NAME = "INBOX";

export const getInbox = store => store[NAME].all.data || [];
