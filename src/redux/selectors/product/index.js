import * as featuredActivity from "./featuredActivity";
import * as featuredMembership from "./featuredMembership";

export default {
  ...featuredActivity,
  ...featuredMembership
};
