const NAME = "PRODUCT";

export const getFeaturedMembership = store => store[NAME].featuredMembership.data;
export const getFeaturedMembershipIsLoading = store => store[NAME].featuredMembership.isLoading;
