import isEmpty from 'lodash/isEmpty';

const NAME = 'PERSIST';

export const getToken = store => {
  const {token} = store[NAME];
  return !isEmpty(token) ? token : null;
};
