import isEmpty from 'lodash/isEmpty';

const NAME = 'AUTH';

export const getFirstTime = store => {
  const {firstTime} = store[NAME];
//   const dataFirst = useSelector((state) => state.AUTH.firstTime);

  return !isEmpty(firstTime) ? firstTime : null;
};
