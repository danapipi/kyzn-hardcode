import persist from "./persist";
import membership from "./membership";
import auth from "./auth";
import family from "./family";
import product from "./product";
import inbox from './inbox';

export default {
  ...persist,
  ...membership,
  ...auth,
  ...family,
  ...product,
  ...inbox,
};
