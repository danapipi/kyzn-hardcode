const NAME = "AUTH";

export const getProfile = store => store[NAME].profile.data;
export const getIsLoadingProfile = store => store[NAME].profile.isLoading;
