import {applyMiddleware, createStore} from 'redux';
import createSagaMiddleware from 'redux-saga';
import {persistStore, persistCombineReducers} from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';
import reducers from 'reducers';
import sagas from 'sagas';

let middlewares;
let store;
const sagaMiddleware = createSagaMiddleware();
middlewares = applyMiddleware(sagaMiddleware);

const config = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: ['PERSIST', 'AUTH'],
};

const reducer = persistCombineReducers(config, reducers);

export const getStore = () => store;

const configureStore = () => {
  store = createStore(reducer, middlewares);
  sagaMiddleware.run(sagas);
  const persistor = persistStore(store);
  return {persistor, store};
};

export default configureStore;
