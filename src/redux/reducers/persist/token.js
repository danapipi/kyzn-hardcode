import Actions from "actions";

export const getDefaultState = () => ({});

const token = (state, action) => {
  if (typeof state === "undefined") {
    return getDefaultState();
  }
  switch (action.type) {
    case Actions.LOGIN_SUCCESS:
      return action.data;
    case Actions.LOGIN_FAIL:
    case Actions.LOGOUT:
      return getDefaultState();
    default:
      return state;
  }
};

export default token;
