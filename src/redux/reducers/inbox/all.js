import Actions from "actions";

export const getDefaultState = () => ({});

const initialState = {
  data: [
    {
      id: 78,
      user_id: 2,
      type: "family_invite",
      activity_booking_id: null,
      user_family_detail_id: 245,
      title: "Family Invitation",
      message: "Ed19 has invited you to join My fan family",
      status: "expired",
      expired_at: "2021-11-23T00:43:52.000Z",
      created_at: "2021-11-20T00:43:52.000Z",
      updated_at: "2021-11-24T22:32:47.000Z",
      deleted_at: null,
      ActivityBookingId: null,
      userId: 2,
      activityBookingId: null,
      userFamilyDetailId: 245
    },
    {
      id: 75,
      user_id: 2,
      type: "family_invite",
      activity_booking_id: null,
      user_family_detail_id: 236,
      title: "Family Invitation",
      message: "Ed13 has invited you to join Best family ever family",
      status: "accepted",
      expired_at: "2021-11-21T10:17:53.000Z",
      created_at: "2021-11-18T10:17:53.000Z",
      updated_at: "2021-11-18T10:19:08.000Z",
      deleted_at: null,
      ActivityBookingId: null,
      userId: 2,
      activityBookingId: null,
      userFamilyDetailId: 236
    },
    {
      id: 74,
      user_id: 2,
      type: "activity_invite",
      activity_booking_id: 324,
      user_family_detail_id: null,
      title: "Activity Invitation",
      message: "Ed13 has invited you to join activity",
      status: "accepted",
      expired_at: "2021-11-18T12:00:00.000Z",
      created_at: "2021-11-18T10:15:04.000Z",
      updated_at: "2021-11-18T10:16:09.000Z",
      deleted_at: null,
      ActivityBookingId: 324,
      userId: 2,
      activityBookingId: 324,
      userFamilyDetailId: null
    },
    {
      id: 73,
      user_id: 2,
      type: "family_invite",
      activity_booking_id: null,
      user_family_detail_id: 234,
      title: "Family Invitation",
      message: "Ed12 has invited you to join Ed12 family family",
      status: "accepted",
      expired_at: "2021-11-21T09:54:53.000Z",
      created_at: "2021-11-18T09:54:53.000Z",
      updated_at: "2021-11-18T09:57:01.000Z",
      deleted_at: null,
      ActivityBookingId: null,
      userId: 2,
      activityBookingId: null,
      userFamilyDetailId: 234
    },
    {
      id: 69,
      user_id: 2,
      type: "activity_invite",
      activity_booking_id: 318,
      user_family_detail_id: null,
      title: "Activity Invitation",
      message: "Ed12 has invited you to join activity",
      status: "accepted",
      expired_at: "2021-11-18T22:00:00.000Z",
      created_at: "2021-11-18T09:36:11.000Z",
      updated_at: "2021-11-18T09:38:45.000Z",
      deleted_at: null,
      ActivityBookingId: 318,
      userId: 2,
      activityBookingId: 318,
      userFamilyDetailId: null
    },
    {
      id: 57,
      user_id: 2,
      type: "activity_invite",
      activity_booking_id: 291,
      user_family_detail_id: null,
      title: "Activity Invitation",
      message: "Ed10 has invited you to join activity",
      status: "expired",
      expired_at: "2021-11-17T22:00:00.000Z",
      created_at: "2021-11-17T15:58:30.000Z",
      updated_at: "2021-11-18T01:36:37.000Z",
      deleted_at: null,
      ActivityBookingId: 291,
      userId: 2,
      activityBookingId: 291,
      userFamilyDetailId: null
    },
    {
      id: 40,
      user_id: 2,
      type: "family_invite",
      activity_booking_id: null,
      user_family_detail_id: 203,
      title: "Family Invitation",
      message: "Hey has invited you to join Heyfamily family",
      status: "pending_invite",
      expired_at: null,
      created_at: "2021-11-14T06:12:04.000Z",
      updated_at: "2021-11-14T06:12:04.000Z",
      deleted_at: null,
      ActivityBookingId: null,
      userId: 2,
      activityBookingId: null,
      userFamilyDetailId: 203
    },
    {
      id: 1,
      user_id: 2,
      type: "message",
      activity_booking_id: null,
      user_family_detail_id: null,
      title: "Welcome",
      message: "Welcome to sports app",
      status: "pending_invite",
      expired_at: null,
      created_at: "2021-11-08T17:00:00.000Z",
      updated_at: null,
      deleted_at: null,
      ActivityBookingId: null,
      userId: 2,
      activityBookingId: null,
      userFamilyDetailId: null
    },

  ],
  isLoading: false,
};

const allInbox = (state = initialState, action) => {
  if (typeof state === "undefined") {
    return getDefaultState();
  }
  switch (action.type) {
    case Actions.FETCH_INBOX_REQUEST_SUCCESS:
      return {
        data: action.data,
        isLoading: false,
      };
    case Actions.FETCH_INBOX_REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case Actions.FETCH_INBOX_REQUEST_FAIL:
      return {
        ...state,
        isLoading: false,
      };
    default:
      return state;
  }
};

export default allInbox;
