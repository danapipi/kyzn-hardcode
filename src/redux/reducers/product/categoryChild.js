import Actions from "actions";

export const getDefaultState = () => ({});

const initialState = {
  categoryChild: [
    {
      wellness: [
        {
          id: 7,
          category_id: 8,
          name: "Yoga",
          description: "Practice your body and mind with us.",
          image:
            "https://kyzn-api-staging.tbtft.com/api/uploads/categoryChildImage-1639746239044.jpg",
          created_at: "2021-10-08T10:21:52.000Z",
          updated_at: "2021-12-17T13:03:59.000Z",
          deleted_at: null,
          activity_types: [
            {
              id: 1,
              name: "Rent Court",
              description: "Rent Court at BSD",
              image_url:
                "http://localhost:5000/api/uploads/activityTypeImage-1634101059390.jpeg",
            },
            {
              id: 2,
              name: "Group Class",
              description: "Group Class at BSD",
              image_url:
                "http://localhost:5000/api/uploads/activityTypeImage-1633687072424.png",
            },
          ],
        },
        {
          id: 8,
          category_id: 8,
          name: "Meditation",
          description: "Stay focused and calm your mind with meditation.",
          image:
            "https://kyzn-api-staging.tbtft.com/api/uploads/categoryChildImage-1639746322882.webp",
          created_at: "2021-10-08T10:21:52.000Z",
          updated_at: "2021-12-17T13:05:22.000Z",
          deleted_at: null,
          activity_types: [],
        },
        {
          id: 9,
          category_id: 8,
          name: "Pilates",
          description:
            "Improve your postural alignment and flexibility with us.",
          image:
            "http://localhost:5000/api/uploads/categoryChildImage-1633688512280.png",
          created_at: "2021-10-08T10:21:52.000Z",
          updated_at: "2021-10-08T10:21:52.000Z",
          deleted_at: null,
          activity_types: [],
        },
        {
          id: 10,
          category_id: 8,
          name: "Recovery",
          description: "We will help you to recovery fast.",
          image:
            "http://localhost:5000/api/uploads/categoryChildImage-1633688512280.png",
          created_at: "2021-10-08T10:21:52.000Z",
          updated_at: "2021-10-08T10:21:52.000Z",
          deleted_at: null,
          activity_types: [
            {
              id: 1,
              name: "Rent Court",
              description: "Rent Court at BSD",
              image_url:
                "http://localhost:5000/api/uploads/activityTypeImage-1634101059390.jpeg",
            },
          ],
        },
        {
          id: 11,
          category_id: 8,
          name: "Functional Medicine",
          description: "Functional medicine description.",
          image:
            "http://localhost:5000/api/uploads/categoryChildImage-1633688512280.png",
          created_at: "2021-10-08T10:21:52.000Z",
          updated_at: "2021-10-08T10:21:52.000Z",
          deleted_at: null,
          activity_types: [],
        },
      ],
    },
    {
      sport: [
        {
          id: 14,
          category_id: 9,
          name: "Basketball",
          description: "basketball yo",
          image:
            "http://localhost:5000/api/uploads/categoryChildImage-1634135817209.jpg",
          created_at: "2021-10-13T14:36:57.000Z",
          updated_at: "2021-10-13T14:36:57.000Z",
          deleted_at: null,
          activity_types: [
            {
              id: 1,
              name: "Rent Court",
              description: "Rent Court at BSD",
              image_url:
                "http://localhost:5000/api/uploads/activityTypeImage-1634101059390.jpeg",
            },
            {
              id: 14,
              name: "CRT Palugada 2",
              description: ".",
              image_url: "https://api.kyzn.life/api/uploads/kyzn-logo.png",
            },
            {
              id: 6,
              name: "Private Training",
              description: "Private Training at BSD",
              image_url: "https://kyzn-api.tbtft.com/api/uploads/kyzn-logo.png",
            },
            {
              id: 2,
              name: "Group Class",
              description: "Group Class at BSD",
              image_url:
                "http://localhost:5000/api/uploads/activityTypeImage-1633687072424.png",
            },
          ],
        },
        {
          id: 15,
          category_id: 9,
          name: "Badminton",
          description: "badminton yo",
          image:
            "http://localhost:5000/api/uploads/categoryChildImage-1634135866815.jfif",
          created_at: "2021-10-13T14:37:46.000Z",
          updated_at: "2021-10-13T14:37:46.000Z",
          deleted_at: null,
          activity_types: [
            {
              id: 2,
              name: "Group Class",
              description: "Group Class at BSD",
              image_url:
                "http://localhost:5000/api/uploads/activityTypeImage-1633687072424.png",
            },
            {
              id: 1,
              name: "Rent Court",
              description: "Rent Court at BSD",
              image_url:
                "http://localhost:5000/api/uploads/activityTypeImage-1634101059390.jpeg",
            },
          ],
        },
        {
          id: 16,
          category_id: 9,
          name: "Swimming",
          description: "Swimming Yo",
          image: "https://via.placeholder.com/1000",
          created_at: "2021-10-13T14:38:35.000Z",
          updated_at: "2021-10-13T14:38:35.000Z",
          deleted_at: null,
          activity_types: [
            {
              id: 2,
              name: "Group Class",
              description: "Group Class at BSD",
              image_url:
                "http://localhost:5000/api/uploads/activityTypeImage-1633687072424.png",
            },
          ],
        },
        {
          id: 25,
          category_id: 9,
          name: "Tennis",
          description: "Tennis",
          image: "https://api.kyzn.life/api/uploads/kyzn-logo.png",
          created_at: "2021-11-23T03:50:23.000Z",
          updated_at: "2021-11-23T03:50:23.000Z",
          deleted_at: null,
          activity_types: [
            {
              id: 2,
              name: "Group Class",
              description: "Group Class at BSD",
              image_url:
                "http://localhost:5000/api/uploads/activityTypeImage-1633687072424.png",
            },
            {
              id: 6,
              name: "Private Training",
              description: "Private Training at BSD",
              image_url: "https://kyzn-api.tbtft.com/api/uploads/kyzn-logo.png",
            },
            {
              id: 1,
              name: "Rent Court",
              description: "Rent Court at BSD",
              image_url:
                "http://localhost:5000/api/uploads/activityTypeImage-1634101059390.jpeg",
            },
          ],
        },
        {
          id: 26,
          category_id: 9,
          name: "Gymnastics",
          description: "Gymnastics",
          image: "https://api.kyzn.life/api/uploads/kyzn-logo.png",
          created_at: "2021-11-23T06:42:37.000Z",
          updated_at: "2021-11-23T06:42:37.000Z",
          deleted_at: null,
          activity_types: [
            {
              id: 2,
              name: "Group Class",
              description: "Group Class at BSD",
              image_url:
                "http://localhost:5000/api/uploads/activityTypeImage-1633687072424.png",
            },
          ],
        },
        {
          id: 27,
          category_id: 9,
          name: "Dance",
          description: "Dance",
          image: "https://api.kyzn.life/api/uploads/kyzn-logo.png",
          created_at: "2021-11-23T06:43:05.000Z",
          updated_at: "2021-11-23T06:43:05.000Z",
          deleted_at: null,
          activity_types: [
            {
              id: 2,
              name: "Group Class",
              description: "Group Class at BSD",
              image_url:
                "http://localhost:5000/api/uploads/activityTypeImage-1633687072424.png",
            },
          ],
        },
        {
          id: 28,
          category_id: 9,
          name: "First Step",
          description: "First Step",
          image: "https://api.kyzn.life/api/uploads/kyzn-logo.png",
          created_at: "2021-11-23T06:43:21.000Z",
          updated_at: "2021-11-23T06:43:21.000Z",
          deleted_at: null,
          activity_types: [
            {
              id: 2,
              name: "Group Class",
              description: "Group Class at BSD",
              image_url:
                "http://localhost:5000/api/uploads/activityTypeImage-1633687072424.png",
            },
          ],
        },
        {
          id: 29,
          category_id: 9,
          name: "Cycling",
          description: "Cycling",
          image: "https://api.kyzn.life/api/uploads/kyzn-logo.png",
          created_at: "2021-11-25T17:40:11.000Z",
          updated_at: "2021-11-25T17:40:11.000Z",
          deleted_at: null,
          activity_types: [
            {
              id: 2,
              name: "Group Class",
              description: "Group Class at BSD",
              image_url:
                "http://localhost:5000/api/uploads/activityTypeImage-1633687072424.png",
            },
          ],
        },
      ],
    },
    {
      fitness: [
        {
          id: 17,
          category_id: 10,
          name: "Cardio",
          description:
            "Ready to get moving and burn some calories as well as improve your aerobic health? Check out our Cardio classes!",
          image: "https://via.placeholder.com/1000",
          created_at: "2021-10-13T14:39:22.000Z",
          updated_at: "2021-12-13T06:55:29.000Z",
          deleted_at: null,
          activity_types: [
            {
              id: 2,
              name: "Group Class",
              description: "Group Class at BSD",
              image_url:
                "http://localhost:5000/api/uploads/activityTypeImage-1633687072424.png",
            },
            {
              id: 6,
              name: "Private Training",
              description: "Private Training at BSD",
              image_url: "https://kyzn-api.tbtft.com/api/uploads/kyzn-logo.png",
            },
          ],
        },
        {
          id: 18,
          category_id: 10,
          name: "Strength",
          description: "Building strength",
          image: "https://via.placeholder.com/1000",
          created_at: "2021-10-13T14:39:52.000Z",
          updated_at: "2021-11-22T16:07:55.000Z",
          deleted_at: null,
          activity_types: [
            {
              id: 6,
              name: "Private Training",
              description: "Private Training at BSD",
              image_url: "https://kyzn-api.tbtft.com/api/uploads/kyzn-logo.png",
            },
            {
              id: 2,
              name: "Group Class",
              description: "Group Class at BSD",
              image_url:
                "http://localhost:5000/api/uploads/activityTypeImage-1633687072424.png",
            },
          ],
        },
      ],
    },
  ],
  isLoading: false,
};

const categoryChild = (state = initialState, action) => {
  if (typeof state === "undefined") {
    return getDefaultState();
  }
  switch (action.type) {
    case Actions.CATEGORY_CHILD_REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case Actions.CATEGORY_CHILD_SUCCESS:
      return {
        ...state,
        categoryChild: action.data,
        isLoading: false,
      };
    case Actions.CATEGORY_CHILD_FAIL:
      return {
        ...state,
        isLoading: false,
      };
    default:
      return state;
  }
};

export default categoryChild;
