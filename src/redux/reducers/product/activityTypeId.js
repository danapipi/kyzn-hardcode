import Actions from "actions";

export const getDefaultState = () => ({});

const initialState = {
  activityTypeId:{
    "default_membership_ids": [
        1
    ],
    "rent_court_activity_type_id": 1,
    "pt_activity_type_id": 6,
    "class_activity_type_id": 2,
    "panorama_activity_type_id": 7,
    "wellness_category_id": 8,
    "fitness_category_id": 10,
    "sport_category_id": 9
  },
  isLoading: false,
};

const activityTypeId = (state = initialState, action) => {
  if (typeof state === "undefined") {
    return getDefaultState();
  }
  switch (action.type) {
    case Actions.ACTIVITY_TYPE_ID_REQUEST:
      return {
        ...state,
        isLoading: true,
      };

    case Actions.ACTIVITY_TYPE_ID_SUCCESS:
      return {
        ...state,
        activityTypeId: action.data,
        isLoading: false,
      };
    case Actions.ACTIVITY_TYPE_ID_FAIL:
      return {
        ...state,
        isLoading: false,
      };

    default:
      return state;
  }
};

export default activityTypeId;
