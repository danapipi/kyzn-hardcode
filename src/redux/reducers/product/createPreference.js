import Actions from "actions";

export const getDefaultState = () => ({});

const initialState = {
  createPreference: [],
  isLoading: false,
};

const createPreference = (state = initialState, action) => {
  if (typeof state === "undefined") {
    return getDefaultState();
  }
  switch (action.type) {
    case Actions.CREATE_PREFERENCE_REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case Actions.CREATE_PREFERENCE_SUCCESS:
      return {
        ...state,
        preference: action.data,
        isLoading: false,
      };
    case Actions.CREATE_PREFERENCE__FAIL:
      return {
        ...state,
        isLoading: false,
      };
    default:
      return state;
  }
};

export default createPreference;
