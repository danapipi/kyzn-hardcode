import Actions from "actions";

export const getDefaultState = () => ({});

const initialState = {
  data: [
    {
      category_id: 9,
      category_child_id: null,
      activity_id: 19,
      created_at: "2021-11-25T20:30:30.000Z",
      updated_at: "2021-12-08T05:15:17.000Z",
      deleted_at: null,
      category: {
        id: 9,
        name: "Sports",
        description: "Move your body",
        image:
          "https://kyzn-api.tbtft.com/api/uploads/categoryImage-1634198459071.jfif",
        created_at: "2021-10-13T14:36:04.000Z",
        updated_at: "2021-10-14T08:00:59.000Z",
        deleted_at: null,
      },
      category_child: null,
      activities: [
        {
          id: 19,
          activity_type_id: 2,
          venue_id: 1,
          name: "PULSE Badminton",
          description:
            "An intense badminton class with a background of uplifting beats! It doesn't matter if you are a beginner or a pro, you can get a workout here! ",
          image_thumbnail:
            "https://mmc.tirto.id/image/2020/01/29/badminton-istockphoto-1_ratio-16x9.jpg",
          image_landscape:
            "https://mmc.tirto.id/image/2020/01/29/badminton-istockphoto-1_ratio-16x9.jpg",
          rate_type: "hour",
          prep_duration: 5,
          min_duration: 1,
          step_duration: 60,
          step_duration_price: 0,
          publish_date: "2021-11-24T15:46:13.000Z",
          available_until: null,
          min_participants: 1,
          max_participants: 8,
          age_unit: "years",
          min_age: 18,
          max_age: 99,
          gender: "all",
          rating: 4.7,
          preset_rating: 0,
          queue_priority: "first come first serve",
          xp_reward: 50,
          coin_reward: 25,
          is_selling: true,
          is_show_preset_rating: true,
          created_at: "2021-11-24T16:16:03.000Z",
          updated_at: "2021-12-13T14:10:18.000Z",
          deleted_at: null,
        },
        {
          id: 30,
          activity_type_id: 2,
          venue_id: 1,
          name: "GoldStar Badminton - PRACTICE",
          description: "GoldStar Badminton ",
          image_thumbnail:
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSeNeQJZ0uIcxmgXdHNC6C_7VzMRIOdFlQe1f58FBLBsq4oli6fDUy4InF7Z5pb_k_Lkus&usqp=CAU",
          image_landscape:
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSeNeQJZ0uIcxmgXdHNC6C_7VzMRIOdFlQe1f58FBLBsq4oli6fDUy4InF7Z5pb_k_Lkus&usqp=CAU",
          rate_type: "hour",
          prep_duration: 5,
          min_duration: 1,
          step_duration: 60,
          step_duration_price: 0,
          publish_date: "2021-11-24T15:46:13.000Z",
          available_until: null,
          min_participants: 1,
          max_participants: 8,
          age_unit: "years",
          min_age: 11,
          max_age: 13,
          gender: "all",
          rating: 4.7,
          preset_rating: 4.8,
          queue_priority: "first come first serve",
          xp_reward: 50,
          coin_reward: 25,
          is_selling: true,
          is_show_preset_rating: true,
          created_at: "2021-11-25T17:26:53.000Z",
          updated_at: "2021-12-07T08:47:11.000Z",
          deleted_at: null,
        },
        {
          id: 18,
          activity_type_id: 2,
          venue_id: 1,
          name: "PULSE Basketball",
          description:
            "An explosive, fast-paced basketball class set to hip-hop sounds! It doesn't matter if you are a beginner or a pro, you can get a workout here! ",
          image_thumbnail:
            "https://images.foxtv.com/static.fox9.com/www.fox9.com/content/uploads/2021/08/764/432/NCAA-bball.jpg?ve=1&tl=1",
          image_landscape:
            "https://images.foxtv.com/static.fox9.com/www.fox9.com/content/uploads/2021/08/764/432/NCAA-bball.jpg?ve=1&tl=1",
          rate_type: "hour",
          prep_duration: 5,
          min_duration: 1,
          step_duration: 60,
          step_duration_price: 0,
          publish_date: "2021-11-24T15:46:13.000Z",
          available_until: null,
          min_participants: 1,
          max_participants: 12,
          age_unit: "years",
          min_age: 18,
          max_age: 99,
          gender: "all",
          rating: 4.7,
          preset_rating: 0,
          queue_priority: "first come first serve",
          xp_reward: 50,
          coin_reward: 25,
          is_selling: true,
          is_show_preset_rating: true,
          created_at: "2021-11-24T15:54:46.000Z",
          updated_at: "2021-12-07T09:21:53.000Z",
          deleted_at: null,
        },
      ],
    },
    {
      category_id: 10,
      category_child_id: null,
      activity_id: 1,
      created_at: "2021-12-08T05:14:49.000Z",
      updated_at: "2021-12-08T05:15:17.000Z",
      deleted_at: null,
      category: {
        id: 10,
        name: "Fitness",
        description: "Fitness yo",
        image: "https://via.placeholder.com/1000",
        created_at: "2021-10-13T14:38:53.000Z",
        updated_at: "2021-10-13T14:38:53.000Z",
        deleted_at: null,
      },
      category_child: null,
      activities: [
        {
          id: 1,
          activity_type_id: 2,
          venue_id: 1,
          name: "BEAT",
          description:
            "An intense rhythm group cycling class which works the whole body with innovative movements.",
          image_thumbnail:
            "https://st2.depositphotos.com/4366957/6625/i/600/depositphotos_66253287-stock-photo-muscular-bodybuilder-guy-close-up.jpg",
          image_landscape:
            "https://st2.depositphotos.com/4366957/6625/i/600/depositphotos_66253287-stock-photo-muscular-bodybuilder-guy-close-up.jpg",
          rate_type: "hour",
          prep_duration: 5,
          min_duration: 60,
          step_duration: 60,
          step_duration_price: 0,
          publish_date: "2021-11-22T15:57:27.000Z",
          available_until: null,
          min_participants: 1,
          max_participants: 15,
          age_unit: "years",
          min_age: 18,
          max_age: 99,
          gender: "all",
          rating: 4.8,
          preset_rating: 0,
          queue_priority: "first come first serve",
          xp_reward: 50,
          coin_reward: 25,
          is_selling: true,
          is_show_preset_rating: true,
          created_at: "2021-11-22T16:05:56.000Z",
          updated_at: "2021-12-07T04:40:51.000Z",
          deleted_at: null,
        },
      ],
    },
  ],
  isLoading: false,
};

const featuredActivity = (state = initialState, action) => {
  if (typeof state === "undefined") {
    return getDefaultState();
  }
  switch (action.type) {
    case Actions.FEATURED_ACTIVITY_REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case Actions.FEATURED_ACTIVITY_SUCCESS:
      return {
        ...state,
        // loading: true,
        data: action.data,
        isLoading: false,
      };
    case Actions.FEATURED_ACTIVITY_FAIL:
      return {
        ...state,
        isLoading: false,
      };
    default:
      return state;
  }
};

export default featuredActivity;
