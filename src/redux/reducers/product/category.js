import Actions from "actions";

export const getDefaultState = () => ({});

const initialState = {
  category: [
    {
      id: 8,
      name: "Wellness",
      description: "Stay healthy with us.",
      image:
        "https://kyzn-api-staging.tbtft.com/api/uploads/categoryImage-1639746143835.jpg",
      created_at: "2021-10-08T10:21:52.000Z",
      updated_at: "2021-12-17T13:02:23.000Z",
      deleted_at: null,
    },
    {
      id: 9,
      name: "Sports",
      description: "Move your body",
      image:
        "https://kyzn-api.tbtft.com/api/uploads/categoryImage-1634198459071.jfif",
      created_at: "2021-10-13T14:36:04.000Z",
      updated_at: "2021-10-14T08:00:59.000Z",
      deleted_at: null,
    },
    {
      id: 10,
      name: "Fitness",
      description: "Fitness yo",
      image: "https://via.placeholder.com/1000",
      created_at: "2021-10-13T14:38:53.000Z",
      updated_at: "2021-10-13T14:38:53.000Z",
      deleted_at: null,
    },
    {
      id: 12,
      name: "Panorama",
      description: "Panorama at BSD",
      image: "https://kyzn-api.tbtft.com/api/uploads/kyzn-logo.png",
      created_at: "2021-11-06T06:05:52.000Z",
      updated_at: "2021-11-06T06:05:52.000Z",
      deleted_at: null,
    },
    {
      id: 17,
      name: "Aerobic Category",
      description: "Aerobic Category",
      image: "https://kyzn-api.tbtft.com/api/uploads/kyzn-logo.png",
      created_at: "2021-11-17T07:19:49.000Z",
      updated_at: "2021-11-17T07:19:49.000Z",
      deleted_at: null,
    },
  ],
  isLoading: false,
};

const category = (state = initialState, action) => {
  if (typeof state === "undefined") {
    return getDefaultState();
  }
  switch (action.type) {
    case Actions.CATEGORY_REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case Actions.CATEGORY_SUCCESS:
      return {
        ...state,
        category: action.data,
        isLoading: false,
      };
    case Actions.CATEGORY_FAIL:
      return {
        ...state,
        isLoading: false,
      };
    default:
      return state;
  }
};

export default category;
