import {combineReducers} from 'redux';
import category from './category';
import categoryChild from './categoryChild'
import activity from './activity'
import activityType from './activityType'
import activitySchedule from './activitySchedule'
import activityScheduleAll from './activityScheduleAll'
import activityScheduleAllWithoutUserId from './activityScheduleAllWithoutUserId'
import pastSchedule from './pastSchedule'
import upcomingSchedule from './upcomingSchedule'
import featuredActivity from './featuredActivity'
import featuredMembership from './featuredMembership'
import activityById from './activityById'
import activityTypeId from './activityTypeId'
import preference from "./preference";
import createPreference from "./createPreference";


export default combineReducers({
  category,
  categoryChild,
  activity,
  activityType,
  activitySchedule,
  upcomingSchedule,
  featuredActivity,
  featuredMembership,
  activityById,
  pastSchedule,
  activityScheduleAll,
  activityTypeId,
  preference,
  createPreference,
  activityScheduleAllWithoutUserId
});
