import Actions from "actions";

export const getDefaultState = () => ({});

const initialState = {
  pastSchedule: [
    {
      "user_id": 2,
      "activity_schedule_id": 4058,
      "user_coupon_id": null,
      "booked_price": 700000,
      "paid_amount": 700000,
      "payment_method": "balance",
      "paid_by": "online",
      "paying_user_id": 2,
      "paying_staff_id": null,
      "payment_status": "paid",
      "attendance_status": "pending",
      "expired_at": null,
      "invite_status": "invited",
      "booking_status": "success",
      "cancel_reason": null,
      "created_at": "2021-12-22T08:47:26.000Z",
      "updated_at": "2021-12-22T08:47:26.000Z",
      "deleted_at": null,
      "ActivityScheduleId": 4058,
      "UserCouponId": null,
      "activity_booking_id": 25,
      "activity_schedule": {
        "id": 4058,
        "activity_court_id": 383,
        "start_time": "2021-12-23T11:00:00.000Z",
        "end_time": "2021-12-23T11:55:00.000Z",
        "internal_start_time": "2021-12-23T11:00:00.000Z",
        "internal_end_time": "2021-12-23T12:00:00.000Z",
        "duration": 60,
        "status": "active",
        "rrule_preset_id": null,
        "created_at": "2021-12-22T08:47:26.000Z",
        "updated_at": "2021-12-22T08:47:26.000Z",
        "deleted_at": null,
        "RrulePresetId": null,
        "activity_court": {
          "id": 383,
          "activity_id": 94,
          "court_id": 14,
          "priority": 0,
          "required_weight": 0,
          "created_at": "2021-12-16T04:41:52.000Z",
          "updated_at": "2021-12-16T04:41:52.000Z",
          "deleted_at": null
        }
      },
      "activity": {
        "id": 94,
        "activity_type_id": 6,
        "venue_id": 1,
        "name": "Private Tennis Coaching",
        "description": "Hone your skills with our expert tennis coach, choose what you need to work on and get the extra attention needed to get your game to the next level! ",
        "image_thumbnail": "https://api.kyzn.life/api/uploads/kyzn-logo.png",
        "image_landscape": "https://api.kyzn.life/api/uploads/kyzn-logo.png",
        "rate_type": "hour",
        "prep_duration": 5,
        "min_duration": 1,
        "step_duration": 60,
        "step_duration_price": 0,
        "publish_date": "2021-12-16T04:29:54.000Z",
        "available_until": null,
        "min_participants": 1,
        "max_participants": 1,
        "age_unit": "years",
        "min_age": 3,
        "max_age": 99,
        "gender": "all",
        "rating": 0,
        "preset_rating": 0,
        "queue_priority": "first come first serve",
        "xp_reward": 300,
        "coin_reward": 150,
        "is_selling": false,
        "is_show_preset_rating": true,
        "created_at": "2021-12-16T04:41:52.000Z",
        "updated_at": "2021-12-16T04:41:52.000Z",
        "deleted_at": null,
        "activity_workers": [
          {
            "id": 304,
            "activity_id": 94,
            "worker_id": 17,
            "worker_classification_id": 3,
            "price": 350000,
            "step_price": 350000,
            "commission_type": "amount",
            "commission_amount": 125000,
            "step_commission_type": "amount",
            "step_commission_amount": 125000,
            "created_at": "2021-12-16T04:41:52.000Z",
            "updated_at": "2021-12-16T04:41:52.000Z",
            "deleted_at": null,
            "worker": {
              "id": 17,
              "worker_type_id": 1,
              "name": "Indra",
              "email": "johanistolip07@gmail.com",
              "phone_number": "081381962602",
              "status": "single",
              "birthdate": "1987-12-06T17:00:00.000Z",
              "gender": "male",
              "salary": 4500000,
              "id_card_number": "3174060712870010",
              "bpjs_number": "1",
              "photo": "https://api.kyzn.life/api/uploads/default-avatar.png",
              "bank_name": "BCA",
              "bank_account_number": "7330350568",
              "bank_beneficiary": "Johanis Indra Tolip",
              "type": "Contract",
              "address": "Jl. Meninjo No. 52A Rt 004 Rw 005 Ciganjur Jagakarsa, Jakarta Selatan",
              "province": "DKI Jakarta",
              "district": "Ciganjur",
              "city": "Jakarta Selatan",
              "postal_code": "1",
              "rating": 4.7,
              "preset_rating": 0,
              "is_active": true,
              "start_working_time": "07:00:00",
              "end_working_time": "20:00:00",
              "created_at": "2021-11-24T15:08:02.000Z",
              "updated_at": "2021-11-24T15:08:02.000Z",
              "deleted_at": null
            },
            "worker_classification": {
              "id": 3,
              "worker_type_id": 1,
              "name": "Senior Coach",
              "description": "Senior Coach",
              "expected_price": 350000,
              "expected_step_price": 350000,
              "expected_commission_type": "amount",
              "expected_commission_amount": 125000,
              "expected_step_commission_type": "amount",
              "expected_step_commission_amount": 125000,
              "created_at": "2021-11-05T17:33:48.000Z",
              "updated_at": "2021-11-24T14:17:22.000Z",
              "deleted_at": null
            }
          }
        ]
      },
      "court": {
        "id": 14,
        "venue_id": 1,
        "name": "Tennis Court 1",
        "description": "Our indoor tennis space.",
        "picture_url": "https://api.kyzn.life/api/uploads/pictureUrl-1639385181217.jpg",
        "floor": "1",
        "open_time": "07:00:00",
        "close_time": "20:00:00",
        "max_weight": 1,
        "rating": 4.7,
        "preset_rating": 0,
        "is_active": true,
        "is_show_preset_rating": true,
        "created_at": "2021-11-22T15:30:29.000Z",
        "updated_at": "2021-12-13T08:46:23.000Z",
        "deleted_at": null
      },
      "venue": {
        "id": 1,
        "name": "Court BSD",
        "address": "BSD",
        "pos_lat": 1,
        "pos_long": 1,
        "open_time": "10:00:00",
        "close_time": "22:00:00",
        "is_open": true,
        "created_at": "2021-09-24T17:00:00.000Z",
        "updated_at": null,
        "deleted_at": null
      },
      "activity_rating": {},
      "worker_rating": {},
      "court_rating": {},
      "participants": [
        {
          "id": 25,
          "user_id": 2,
          "activity_schedule_id": 4058,
          "user_coupon_id": null,
          "booked_price": 700000,
          "paid_amount": 700000,
          "payment_method": "balance",
          "paid_by": "online",
          "paying_user_id": 2,
          "paying_staff_id": null,
          "payment_status": "paid",
          "attendance_status": "pending",
          "expired_at": null,
          "invite_status": "invited",
          "booking_status": "success",
          "cancel_reason": null,
          "created_at": "2021-12-22T08:47:26.000Z",
          "updated_at": "2021-12-22T08:47:26.000Z",
          "deleted_at": null,
          "UserCouponId": null,
          "user": {
            "id": 2,
            "name": "user",
            "username": null,
            "phone_number": "+62851616543211",
            "xp": 200,
            "coin": 500,
            "balance": 83808500,
            "status": "Registered",
            "birthdate": "2001-11-30T16:00:00.000Z",
            "gender": "male",
            "address": "Jl. Asd",
            "photo": "https://api.kyzn.life/api/uploads/default-avatar.png",
            "nationality": "Indonesia",
            "postal_code": "213321",
            "province": "Bali",
            "district": "Nusa dua",
            "city": "Bali",
            "referral_code": "Grand Opening",
            "id_card_no": null,
            "instagram_username": null,
            "company_name": null,
            "physical_limitation": null,
            "emergency_contact_name": null,
            "emergency_contact_relationship": null,
            "emergency_phone_number": null,
            "emergency_home_number": null,
            "email": null,
            "rfid_card_no": null,
            "created_at": "2021-09-25T01:00:00.000Z",
            "updated_at": "2021-12-22T08:47:26.000Z"
          }
        }
      ]
    }
  ],
  isLoading: false,
};

const pastSchedule = (state = initialState, action) => {
  if (typeof state === "undefined") {
    return getDefaultState();
  }
  switch (action.type) {
    case Actions.PAST_SCHEDULE_REQUEST:
      return {
        ...state,
        isLoading: true,
      };

    case Actions.PAST_SCHEDULE_SUCCESS:
      console.log("activity here", action);
      return {
        ...state,
        pastSchedule: action.data,
        isLoading: false,
      };
    case Actions.PAST_SCHEDULE_FAIL:
      return {
        ...state,
        isLoading: false,
      };

    default:
      return state;
  }
};

export default pastSchedule;
