import Actions from "actions";

export const getDefaultState = () => ({});

const initialState = {
  preference: [],
  isLoading: false,
};

const preference = (state = initialState, action) => {
  if (typeof state === "undefined") {
    return getDefaultState();
  }
  switch (action.type) {
    case Actions.PREFERENCE_REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case Actions.PREFERENCE_SUCCESS:
      return {
        ...state,
        preference: action.data,
        isLoading: false,
      };
    case Actions.PREFERENCE__FAIL:
      return {
        ...state,
        isLoading: false,
      };
    default:
      return state;
  }
};

export default preference;
