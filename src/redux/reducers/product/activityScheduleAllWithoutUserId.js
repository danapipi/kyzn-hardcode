import Actions from "actions";

export const getDefaultState = () => ({});

const initialState = {
  activityScheduleAllWithoutUserId: {},
  isLoading: false,
};

const activityScheduleAllWithoutUserId = (state = initialState, action) => {
  if (typeof state === "undefined") {
    return getDefaultState();
  }
  switch (action.type) {
    case Actions.ACTIVITY_SCHEDULE_ALL_NO_USER_ID_REQUEST:
      return {
        ...state,
        isLoading: true,
      };

    case Actions.ACTIVITY_SCHEDULE_ALL_NO_USER_ID_SUCCESS:
      return {
        ...state,
        activityScheduleAll: action.data,
        isLoading: false,
      };
    case Actions.ACTIVITY_SCHEDULE_ALL_NO_USER_ID_FAIL:
      return {
        ...state,
        isLoading: false,
      };

    default:
      return state;
  }
};

export default activityScheduleAllWithoutUserId;
