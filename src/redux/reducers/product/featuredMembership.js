import Actions from "actions";

export const getDefaultState = () => ({});

const initialState = {
  data: [
    {
      id: 6,
      age_group_id: 1,
      membership_category_id: null,
      membership_id: 8,
      membership_package_category_id: null,
      membership_package_id: null,
      order_no: 2,
      created_at: "2021-12-06T09:33:01.000Z",
      updated_at: "2021-12-06T09:33:01.000Z",
      deleted_at: null,
      membership: {
        id: 8,
        venue_id: 1,
        membership_type_id: 1,
        membership_category_id: 4,
        name: "Academy Pass 3 Months",
        image_url:
          "https://api.kyzn.life/api/uploads/imageUrl-1639387920980.jpg",
        portrait_image_url:
          "https://api.kyzn.life/api/uploads/portraitImageUrl-1639710147285.jpg",
        perks_description:
          "For our junior athletes. Sign up for 3 different activities included per week + our holiday program outside of term-time. ",
        duration_unit: "month",
        duration: 3,
        price: 4200000,
        age_unit: "years",
        min_age: 1,
        max_age: 18,
        available_quota: 100,
        quota_left: 100,
        publish_date: "2021-12-17T03:02:26.000Z",
        available_until: null,
        is_limited: false,
        is_selling: true,
        is_preset: false,
        is_recurring_allowed: false,
        deposit_amount: null,
        recurring_amount: null,
        interval_unit: null,
        interval: null,
        min_recurrence_progress: null,
        created_at: "2021-11-23T06:01:30.000Z",
        updated_at: "2021-12-17T03:02:28.000Z",
        deleted_at: null,
        membership_category: {
          id: 4,
          name: "Academy",
          image_url:
            "https://api.kyzn.life/api/uploads/imageUrl-1639128697382.jpg",
          portrait_image_url:
            "https://api.kyzn.life/api/uploads/portraitImageUrl-1639128697407.jpg",
          description: "For the budding athlete.",
          order_no: 3,
          is_showing: true,
          created_at: "2021-10-14T08:26:40.000Z",
          updated_at: "2021-12-10T09:31:37.000Z",
          deleted_at: null,
        },
      },
      membership_category: null,
      membership_package: null,
      membership_package_category: null,
    },
    {
      id: 7,
      age_group_id: 1,
      membership_category_id: null,
      membership_id: 17,
      membership_package_category_id: null,
      membership_package_id: null,
      order_no: 3,
      created_at: "2021-12-13T10:54:42.000Z",
      updated_at: "2021-12-13T10:54:42.000Z",
      deleted_at: null,
      membership: {
        id: 17,
        venue_id: 1,
        membership_type_id: 1,
        membership_category_id: 1,
        name: "dtmembership Platinum - 12 Months",
        image_url: "https://api.kyzn.life/api/uploads/kyzn-logo.png",
        portrait_image_url:
          "https://api.kyzn.life/api/uploads/portraitImageUrl-1639707325544.jpg",
        perks_description: "Test Platinum - 12 Months",
        duration_unit: "month",
        duration: 1,
        price: 60000,
        age_unit: "years",
        min_age: 18,
        max_age: 99,
        available_quota: 100,
        quota_left: 97,
        publish_date: "2021-12-17T02:15:24.000Z",
        available_until: null,
        is_limited: true,
        is_selling: true,
        is_preset: false,
        is_recurring_allowed: true,
        deposit_amount: 5000,
        recurring_amount: 5000,
        interval_unit: "MONTH",
        interval: 1,
        min_recurrence_progress: 12,
        created_at: "2021-11-26T06:22:58.000Z",
        updated_at: "2021-12-17T02:15:25.000Z",
        deleted_at: null,
        membership_category: {
          id: 1,
          name: "Platinum",
          image_url:
            "https://api.kyzn.life/api/uploads/imageUrl-1639128101046.jpg",
          portrait_image_url:
            "https://api.kyzn.life/api/uploads/portraitImageUrl-1639128126827.jpg",
          description: "Access Free classes",
          order_no: 1,
          is_showing: true,
          created_at: "2021-10-14T08:26:40.000Z",
          updated_at: "2021-12-10T09:22:06.000Z",
          deleted_at: null,
        },
      },
      membership_category: null,
      membership_package: null,
      membership_package_category: null,
    },
  ],
  isLoading: false,
};

const featuredMembership = (state = initialState, action) => {
  if (typeof state === "undefined") {
    return getDefaultState();
  }
  switch (action.type) {
    case Actions.FEATURED_MEMBERSHIP_REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case Actions.FEATURED_MEMBERSHIP_SUCCESS:
      return {
        ...state,
        // loading: true,
        data: action.data,
        isLoading: false,
      };
    case Actions.FEATURED_MEMBERSHIP_FAIL:
      return {
        ...state,
        isLoading: false,
      };
    default:
      return state;
  }
};

export default featuredMembership;
