import Actions from "actions";

export const getDefaultState = () => ({});

const initialState = {
  activityType: {},
  isLoading: false,
};

const activityType = (state = initialState, action) => {
  if (typeof state === "undefined") {
    return getDefaultState();
  }
  switch (action.type) {
    case Actions.ACTIVITY_TYPE_REQUEST:
      return {
        ...state,
        isLoading: true,
      };

    case Actions.ACTIVITY_TYPE_SUCCESS:
      return {
        ...state,
        activityType: action.data,
        isLoading: false,
      };
    case Actions.ACTIVITY_TYPE_FAIL:
      return {
        ...state,
        isLoading: false,
      };

    default:
      return state;
  }
};

export default activityType;
