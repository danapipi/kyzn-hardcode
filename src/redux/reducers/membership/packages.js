import Actions from "actions";

export const getDefaultState = () => ({});

const packages = (state, action) => {
  console.log("action action action", action);
  if (typeof state === "undefined") {
    return getDefaultState();
  }
  switch (action.type) {
    case Actions.FETCH_MEMBERSHIP_PACKAGES_SUCCESS:
      return action.data;
    default:
      return state;
  }
};

export default packages;
