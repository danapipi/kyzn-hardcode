import Actions from "actions";

export const getDefaultState = () => ({});

const types = (state, action) => {
  if (typeof state === "undefined") {
    return getDefaultState();
  }
  switch (action.type) {
    case Actions.FETCH_MEMBERSHIP_TYPES_SUCCCESS:
      return action.data;
    default:
      return state;
  }
};

export default types;
