import { combineReducers } from "redux";
import packages from "./packages";
import types from "./types";
import categories from "./categories";
import membershipById from "./membershipById";

export default combineReducers({
  packages,
  types,
  categories,
  membershipById
});
