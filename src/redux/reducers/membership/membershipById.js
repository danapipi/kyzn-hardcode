
import Actions from "actions";

export const getDefaultState = () => ({});

const initialState = {
  membershipData: {},
  isLoading: false,
};

const membershipById = (state = initialState, action) => {
  if (typeof state === "undefined") {
    return getDefaultState();
  }
  switch (action.type) {
    case Actions.MEMBERSHIP_BY_ID_REQUEST:
      return {
        ...state,
        isLoading: true,
      };

    case Actions.MEMBERSHIP_BY_ID_SUCCESS:
      return {
        ...state,
        // loading: true,
        membershipData: action.data,
        isLoading: false,
      };
    case Actions.MEMBERSHIP_BY_ID_FAIL:
      return {
        ...state,
        isLoading: false,
      };
    default:
      return state;
  }
};

export default membershipById;
