import Actions from "actions";

export const getDefaultState = () => ({});

const initialState = {
  data: {},
  activityParticipant: [
    {
      id: 2,
      user_family_id: 2,
      user_id: 2,
      role: "adult",
      invite_status: "invited",
      user: {
        id: 2,
        name: "user",
        username: null,
        phone_number: "+62851616543211",
        xp: 200,
        coin: 500,
        balance: 84508500,
        status: "Registered",
        birthdate: "2001-11-30T16:00:00.000Z",
        gender: "male",
        address: "Jl. Asd",
        photo: "https://api.kyzn.life/api/uploads/default-avatar.png",
        nationality: "Indonesia",
        postal_code: "213321",
        province: "Bali",
        district: "Nusa Dua",
        city: "Bali",
        referral_code: "Grand Opening",
        id_card_no: null,
        instagram_username: null,
        company_name: null,
        physical_limitation: null,
        emergency_contact_name: null,
        emergency_contact_relationship: null,
        emergency_phone_number: null,
        emergency_home_number: null,
        email: null,
        rfid_card_no: null,
      },
    },
    {
      id: 3,
      user_family_id: 2,
      user_id: 3,
      role: "adult",
      invite_status: "invited",
      user: {
        id: 3,
        name: "Edward",
        username: null,
        phone_number: "+62812345678910",
        xp: 200,
        coin: 500,
        balance: 84508500,
        status: "Registered",
        birthdate: "2001-11-30T16:00:00.000Z",
        gender: "male",
        address: "Jl. Asd",
        photo: "https://api.kyzn.life/api/uploads/default-avatar.png",
        nationality: "Indonesia",
        postal_code: "213321",
        province: "Jawa Timur",
        district: "Jombang",
        city: "Kediri",
        referral_code: null,
        id_card_no: null,
        instagram_username: null,
        company_name: null,
        physical_limitation: null,
        emergency_contact_name: null,
        emergency_contact_relationship: null,
        emergency_phone_number: null,
        emergency_home_number: null,
        email: null,
        rfid_card_no: null,
      },
    },
    {
      id: 4,
      user_family_id: 2,
      user_id: 4,
      role: "adult",
      invite_status: "invited",
      user: {
        id: 4,
        name: "Alfred",
        username: null,
        phone_number: "+628981234568",
        xp: 200,
        coin: 500,
        balance: 84508500,
        status: "Registered",
        birthdate: "2001-11-30T16:00:00.000Z",
        gender: "male",
        address: "Jl. Asd",
        photo: "https://api.kyzn.life/api/uploads/default-avatar.png",
        nationality: "Indonesia",
        postal_code: "213321",
        province: "Middle Java",
        district: "Tegal",
        city: "Solo",
        referral_code: "Sporty",
        id_card_no: null,
        instagram_username: null,
        company_name: null,
        physical_limitation: null,
        emergency_contact_name: null,
        emergency_contact_relationship: null,
        emergency_phone_number: null,
        emergency_home_number: null,
        email: "Alfred@ironman.com",
        rfid_card_no: null,
      },
    },
  ],
  isLoading: false,
};

const user = (state = initialState, action) => {
  if (typeof state === "undefined") {
    return getDefaultState();
  }
  switch (action.type) {
    case Actions.FETCH_USER_FAMILY_SUCCESS:
      return {
        data: action.data,
        isLoading: false,
      };
    case Actions.FETCH_USER_FAMILY_REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case Actions.FETCH_USER_FAMILY_FAIL:
      return {
        ...state,
        isLoading: false,
      };
    default:
      return state;
  }
};

export default user;
