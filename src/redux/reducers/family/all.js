import Actions from "actions";

export const getDefaultState = () => ({});

const initialState = {
  data: [
    {
      id: 2,
      user_id: 2,
      name: "qwe fams",
      // user: {
      //   id: 2,
      //   name: "user",
      //   username: null,
      //   phone_number: "+62851616543211",
      //   balance: 84508500,
      //   birthdate: "2001-11-30T16:00:00.000Z",
      //   gender: "male",
      //   "address": "Jl. Asd",
      //   "photo": "https://api.kyzn.life/api/uploads/default-avatar.png",
      //   "nationality": "Indonesia",
      //   "postal_code": "213321",
      //   "province": "Bali",
      //   "district": "Nusa dua",
      //   "city": "Bali",
      //   "referral_code": "Grand Opening",
      //   "id_card_no": null,
      //   "instagram_username": null,
      //   "company_name": null,
      //   "physical_limitation": null,
      //   "emergency_contact_name": null,
      //   "emergency_contact_relationship": null,
      //   "emergency_phone_number": null,
      //   "emergency_home_number": null,
      //   "email": null,
      //   "rfid_card_no": null,
      //   "created_at": "2021-09-25T01:00:00.000Z",
      //   "updated_at": "2021-11-22T18:25:52.000Z"
      // },
      user_family_details: [
        {
          id: 2,
          user_family_id: 2,
          user_id: 2,
          role: "adult",
          invite_status: "invited",
          user: {
            id: 2,
            name: "user",
            username: null,
            phone_number: "+62851616543211",
            xp: 200,
            coin: 500,
            balance: 84508500,
            status: "Registered",
            birthdate: "2001-11-30T16:00:00.000Z",
            gender: "male",
            address: "Jl. Asd",
            photo: "https://api.kyzn.life/api/uploads/default-avatar.png",
            nationality: "Indonesia",
            postal_code: "213321",
            province: "Bali",
            district: "Nusa Dua",
            city: "Bali",
            referral_code: "Grand Opening",
            id_card_no: null,
            instagram_username: null,
            company_name: null,
            physical_limitation: null,
            emergency_contact_name: null,
            emergency_contact_relationship: null,
            emergency_phone_number: null,
            emergency_home_number: null,
            email: null,
            rfid_card_no: null,
          },
        },
        {
          id: 54,
          user_family_id: 2,
          user_id: 11,
          role: "adult",
          invite_status: "invited",
          user: {
            id: 11,
            name: "Edward",
            username: null,
            phone_number: "0811326491",
            xp: 200,
            coin: 500,
            balance: 5840000,
            birthdate: "2021-11-07T16:00:00.000Z",
            gender: "male",
            address: "",
            photo: "https://api.kyzn.life/api/uploads/default-avatar.png",
            nationality: "Indonesia",
            postal_code: null,
            province: null,
            district: null,
            city: null,
            referral_code: "379UGS",
            id_card_no: null,
            instagram_username: null,
            company_name: null,
            physical_limitation: null,
            emergency_contact_name: null,
            emergency_contact_relationship: null,
            emergency_phone_number: null,
            emergency_home_number: null,
            email: null,
            rfid_card_no: null,
          },
        },
        {
          id: 167,
          user_family_id: 2,
          user_id: 1012,
          role: "adult",
          invite_status: "invited",
          user: {
            id: 1012,
            name: "Ed1",
            username: null,
            phone_number: "+6285161654321",
            xp: 200,
            coin: 500,
            balance: 3680000,
            birthdate: "2021-11-07T16:00:00.000Z",
            gender: "male",
            address: "",
            photo: "https://api.kyzn.life/api/uploads/default-avatar.png",
            nationality: "Indonesia",
            postal_code: "9999998",
            province: null,
            district: null,
            city: null,
            referral_code: "dA8i1Z",
            id_card_no: null,
            instagram_username: null,
            company_name: null,
            physical_limitation: null,
            emergency_contact_name: null,
            emergency_contact_relationship: null,
            emergency_phone_number: null,
            emergency_home_number: null,
            email: null,
            rfid_card_no: null,
          },
        },
        {
          id: 171,
          user_family_id: 2,
          user_id: 39,
          role: "adult",
          invite_status: "pending",
          user: {
            id: 39,
            name: "Novi3",
            username: null,
            phone_number: "081318965453",
            xp: 200,
            coin: 500,
            balance: 300000,
            birthdate: "1994-10-31T16:00:00.000Z",
            gender: "male",
            address: null,
            photo: "https://api.kyzn.life/api/uploads/default-avatar.png",
            nationality: "Indonesia",
            postal_code: null,
            province: null,
            district: null,
            city: null,
            referral_code: "ABENUo",
            id_card_no: null,
            instagram_username: null,
            company_name: null,
            physical_limitation: null,
            emergency_contact_name: null,
            emergency_contact_relationship: null,
            emergency_phone_number: null,
            emergency_home_number: null,
            email: null,
            rfid_card_no: null,
          },
        },
      ],
    },
  ],
  dataFamilyOne: {
    user_family_details: [
      {
        id: 54,
        user_family_id: 2,
        user_id: 11,
        role: "adult",
        invite_status: "invited",
        user: {
          id: 11,
          name: "Edward",
          username: null,
          phone_number: "0811326491",
          xp: 0,
          coin: 0,
          balance: 5840000,
          status: "Registered",
          birthdate: "2001-11-30T16:00:00.000Z",
          gender: "male",
          address: null,
          photo: "https://api.kyzn.life/api/uploads/default-avatar.png",
          nationality: null,
          postal_code: null,
          province: null,
          district: null,
          city: null,
          referral_code: "379UGS",
          id_card_no: null,
          instagram_username: null,
          company_name: null,
          physical_limitation: null,
          emergency_contact_name: null,
          emergency_contact_relationship: null,
          emergency_phone_number: null,
          emergency_home_number: null,
          email: null,
          rfid_card_no: null,
          qr_string:
            "U2FsdGVkX18OJvu+1CIegujk4GGfmwM8rVMBG7Kziz3zKnAeMcXRLqb4bWAlRnKPNlH8WQtmGThlUwP0HhV4yMfIzwovloD7jEgDGjtW+6fypvomyZ8Vdby7v2AC/xM4VgtTaSk0HxcdzEZLIEX1Tji+NKiv8HNMqhqs4Kvruh4=",
        },
      },
      {
        id: 167,
        user_family_id: 2,
        user_id: 1012,
        role: "adult",
        invite_status: "invited",
        user: {
          id: 1012,
          name: "Ed1",
          username: null,
          phone_number: "+6285161654321",
          xp: 0,
          coin: 0,
          balance: 3680000,
          status: "Registered",
          birthdate: "2021-11-07T16:00:00.000Z",
          gender: "male",
          address: "",
          photo: "https://api.kyzn.life/api/uploads/default-avatar.png",
          nationality: "Indonesia",
          postal_code: "9999998",
          province: "",
          district: "",
          city: "",
          referral_code: "dA8i1Z",
          id_card_no: null,
          instagram_username: null,
          company_name: null,
          physical_limitation: null,
          emergency_contact_name: null,
          emergency_contact_relationship: null,
          emergency_phone_number: null,
          emergency_home_number: null,
          email: null,
          rfid_card_no: null,
          qr_string:
            "U2FsdGVkX19/hAqyzod2mkMtdnsUrxX/YxP/1hkJXNQW/xGrDgBA+3QtM90BY9WhrMVms7Wxp25xN+mDvJfbali5fXywTMv8uZSN5bYLWWAiCQK1vuJyqtzoFCbaMtLnba7DrDBpEOuCIGyDjZ8n1eDULRQ0inaHtNNqttYy35c=",
        },
      },
      {
        id: 167,
        user_family_id: 2,
        user_id: 1012,
        role: "adult",
        invite_status: "invited",
        user: {
          id: 39,
          name: "Novi3",
          username: null,
          phone_number: "081318965453",
          xp: 200,
          coin: 500,
          balance: 300000,
          birthdate: "1994-10-31T16:00:00.000Z",
          gender: "male",
          address: null,
          photo: "https://api.kyzn.life/api/uploads/default-avatar.png",
          nationality: "Indonesia",
          postal_code: null,
          province: null,
          district: null,
          city: null,
          referral_code: "ABENUo",
          id_card_no: null,
          instagram_username: null,
          company_name: null,
          physical_limitation: null,
          emergency_contact_name: null,
          emergency_contact_relationship: null,
          emergency_phone_number: null,
          emergency_home_number: null,
          email: null,
          rfid_card_no: null,
          qr_string:
            "U2FsdGVkX19/hAqyzod2mkMtdnsUrxX/YxP/1hkJXNQW/xGrDgBA+3QtM90BY9WhrMVms7Wxp25xN+mDvJfbali5fXywTMv8uZSN5bYLWWAiCQK1vuJyqtzoFCbaMtLnba7DrDBpEOuCIGyDjZ8n1eDULRQ0inaHtNNqttYy35c=",
        },
      },
    ],
  },
  couponsData: [
    {
      id: 2,
      user_id: 2,
      name: "Baksket ball coupon",
      coupon_type_id: 1,
      discount: 5000,
      discount_type: "amount",
      effective_from: "2021-10-28T10:12:34.000Z",
      expired_at: "2050-12-15T17:00:00.000Z",
      status: "unused",
      created_at: "2021-10-28T10:13:06.000Z",
      updated_at: "2021-11-06T08:01:28.000Z",
      deleted_at: null,
      coupon_type: {
        id: 1,
        name: "Limited Promo",
        created_at: "2021-10-27T19:25:29.000Z",
        updated_at: "2021-10-27T19:25:29.000Z",
        deleted_at: null,
      },
      user_coupon_details: [],
    },
    {
      id: 6,
      user_id: 2,
      name: "Bball Court Half Price",
      coupon_type_id: 1,
      discount: 50,
      discount_type: "percentage",
      effective_from: "2021-10-28T10:12:34.000Z",
      expired_at: "2050-12-15T17:00:00.000Z",
      status: "unused",
      created_at: "2021-10-28T10:13:06.000Z",
      updated_at: "2021-11-06T08:01:28.000Z",
      deleted_at: null,
      coupon_type: {
        id: 1,
        name: "Limited Promo",
        created_at: "2021-10-27T19:25:29.000Z",
        updated_at: "2021-10-27T19:25:29.000Z",
        deleted_at: null,
      },
      user_coupon_details: [],
    },
    {
      id: 8,
      user_id: 2,
      name: "Bball Court Half Price",
      coupon_type_id: 1,
      discount: 25,
      discount_type: "percentage",
      effective_from: "2021-10-28T10:12:34.000Z",
      expired_at: "2050-12-15T17:00:00.000Z",
      status: "unused",
      created_at: "2021-10-28T10:13:06.000Z",
      updated_at: "2021-11-06T08:01:28.000Z",
      deleted_at: null,
      coupon_type: {
        id: 1,
        name: "Limited Promo",
        created_at: "2021-10-27T19:25:29.000Z",
        updated_at: "2021-10-27T19:25:29.000Z",
        deleted_at: null,
      },
      user_coupon_details: [
        {
          id: 1,
          user_coupon_id: 8,
          category_id: 9,
          category_child_id: 14,
          activity_id: 30,
          created_at: "2021-11-06T03:26:20.000Z",
          updated_at: "2021-11-06T03:26:20.000Z",
          deleted_at: null,
          ActivityId: 30,
          CategoryId: 9,
          CategoryChildId: 14,
          UserCouponId: 8,
        },
      ],
    },
    {
      id: 10,
      user_id: 2,
      name: "Basket ball coupon",
      coupon_type_id: 1,
      discount: 5000,
      discount_type: "amount",
      effective_from: "2021-10-28T10:12:34.000Z",
      expired_at: "2050-12-15T17:00:00.000Z",
      status: "unused",
      created_at: "2021-10-28T10:13:06.000Z",
      updated_at: "2021-11-06T08:01:28.000Z",
      deleted_at: null,
      coupon_type: {
        id: 1,
        name: "Limited Promo",
        created_at: "2021-10-27T19:25:29.000Z",
        updated_at: "2021-10-27T19:25:29.000Z",
        deleted_at: null,
      },
      user_coupon_details: [],
    },
    {
      id: 11,
      user_id: 2,
      name: "Basket ball coupon",
      coupon_type_id: 1,
      discount: 50,
      discount_type: "percentage",
      effective_from: "2021-10-28T10:12:34.000Z",
      expired_at: "2050-12-15T17:00:00.000Z",
      status: "unused",
      created_at: "2021-10-28T10:13:06.000Z",
      updated_at: "2021-11-06T08:01:28.000Z",
      deleted_at: null,
      coupon_type: {
        id: 1,
        name: "Limited Promo",
        created_at: "2021-10-27T19:25:29.000Z",
        updated_at: "2021-10-27T19:25:29.000Z",
        deleted_at: null,
      },
      user_coupon_details: [],
    },
    {
      id: 12,
      user_id: 2,
      name: "Bball Court Half Price",
      coupon_type_id: 1,
      discount: 50,
      discount_type: "percentage",
      effective_from: "2021-10-28T10:12:34.000Z",
      expired_at: "2050-12-15T17:00:00.000Z",
      status: "unused",
      created_at: "2021-10-28T10:13:06.000Z",
      updated_at: "2021-11-06T08:01:28.000Z",
      deleted_at: null,
      coupon_type: {
        id: 1,
        name: "Limited Promo",
        created_at: "2021-10-27T19:25:29.000Z",
        updated_at: "2021-10-27T19:25:29.000Z",
        deleted_at: null,
      },
      user_coupon_details: [
        {
          id: 5,
          user_coupon_id: 12,
          category_id: 9,
          category_child_id: 14,
          activity_id: 30,
          created_at: "2021-11-07T11:33:19.000Z",
          updated_at: "2021-11-07T11:33:19.000Z",
          deleted_at: null,
          ActivityId: 30,
          CategoryId: 9,
          CategoryChildId: 14,
          UserCouponId: 12,
        },
      ],
    },
  ],
  isLoading: false,
};

const all = (state = initialState, action) => {
  if (typeof state === "undefined") {
    return getDefaultState();
  }
  switch (action.type) {
    case Actions.FETCH_ALL_FAMILIES_SUCCESS:
      return {
        data: action.data,
        isLoading: false,
      };
    case Actions.FETCH_ALL_FAMILIES_REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case Actions.FETCH_ALL_FAMILIES_FAIL:
      return {
        ...state,
        isLoading: false,
      };
    default:
      return state;
  }
};

export default all;
