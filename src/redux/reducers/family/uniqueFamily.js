import Actions from "actions";

export const getDefaultState = () => ({});

const initialState = {
  data: [
    {
      id: 2,
      phone_number: "+62851616543211",
      username: null,
      name: "user",
      photo: "https://api.kyzn.life/api/uploads/default-avatar.png",
      xp: 200,
      coin: 500,
      balance: 84508500,
      qr_string: "U2FsdGVkX18EE3HrIMaLJvwFubiNELtPZcaPK78CRzL0rkjElBbaMJJWUERfVsYP34+QQE/iVz+0i8P8m3IhNSteyONXfS8mD40rE7yph5A8IoKN5N+pcljjobufmP10frI2ipOQAu9/Si7tmSbWNE1nBPVsn4Q33L+fWXo75eg="
    },
    {
      id: 11,
      phone_number: "0811326491",
      username: null,
      name: "Edward",
      photo: "https://api.kyzn.life/api/uploads/default-avatar.png",
      xp: 0,
      coin: 0,
      balance: 5840000,
      qr_string: "U2FsdGVkX1+mSE5zx0Npx2qxamh00Tf+1T7U/h/M1trZW7z2z6n6JO4d586AnH5bLfFNGGKHvMTqpRRgYfJaQjYFMJcJp+hINEY0rZMQkdKJBTwVUMaAbnDF6p4sZKk+gnQzMqRv/+3oC6qEknNpdxINz4PAYOPIuTDW1fvmlHY="
    },
    {
      id: 1012,
      phone_number: "+6285161654321",
      username: null,
      name: "Ed1",
      photo: "https://api.kyzn.life/api/uploads/default-avatar.png",
      xp: 0,
      coin: 0,
      balance: 3680000,
      qr_string: "U2FsdGVkX1/22W9nUkSZFGWswx+HSvsiQnuk+rB52p+WH9r/1hHLRwQ01mhq0Q+xev96+rDxxXFdEftsKAX+GkYfiXI9fQiEocROKIR9C87nUfc1cL62JhlpnIh6FRKZW5M+8x5AB0BN9/ltdjc4Y4eMJXt9C5TpDuYpNdZ7nyc="
    },
    {
      id: 38,
      phone_number: "081318965453",
      username: null,
      name: "Novi3",
      photo: "https://api.kyzn.life/api/uploads/default-avatar.png",
      xp: 0,
      coin: 0,
      balance: 0,
      qr_string: "U2FsdGVkX190NZTip6Z/1h9TVRvy87JuD6XBcu/6FmqiCw3Nqapa0tyD0flE4pKX3YKlWT/o8jXzzxTFImRUyU9hkJaApwZ3J0spMuC1LJ+efV2QkdMJYt03YQxek9UUqpjsIJBoq4mGorh1qnyOHwt+9tesvG/XBS4LJk+8M0w="
    },
  ],
  isLoading: false,
};

const uniqueFamily = (state = initialState, action) => {
  if (typeof state === "undefined") {
    return getDefaultState();
  }
  switch (action.type) {
    case Actions.FETCH_FAMILY_UNIQUE_SUCCESS:
      console.log("action.data >>>> ", action.data);
      return {
        data: action.data,
        isLoading: false,
      };
    case Actions.FETCH_FAMILY_UNIQUE_REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case Actions.FETCH_FAMILY_UNIQUE_FAIL:
      return {
        ...state,
        isLoading: false,
      };
    default:
      return state;
  }
};

export default uniqueFamily;
