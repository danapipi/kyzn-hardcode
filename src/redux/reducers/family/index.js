import { combineReducers } from "redux";
import user from "./user";
import all from "./all";
import uniqueFamily from "./uniqueFamily";

export default combineReducers({
  user,
  all,
  uniqueFamily,
});
