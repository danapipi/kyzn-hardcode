import persist from "./persist";
import product from "./product";
import auth from "./auth";
import topUpBalance from "./topUpBalance";
import membership from "./membership";
import family from "./family";
import inbox from './inbox';
import worker from './worker';

export default {
  PERSIST: persist,
  PRODUCT: product,
  AUTH: auth,
  TOPUP: topUpBalance,
  MEMBERSHIP: membership,
  FAMILY: family,
  INBOX: inbox,
  WORKER: worker
};
