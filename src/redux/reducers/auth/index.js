import { combineReducers } from "redux";
import profile from "./profile";
import updateProfile from "./updateProfile";
import updateProfileChild from "./updateProfileChild";
import userMembershipPackage from "./userMembershipPackage";
import login from "./login"
import register from './register';
import loginUserName from "./loginUsername";
import firstTime from "./firstTime";

export default combineReducers({
  profile,
  updateProfile,
  updateProfileChild,
  userMembershipPackage,
  login,
  register,
  loginUserName,
  firstTime
});
