import Actions from "actions";

export const getDefaultState = () => ({});

const initialState = {
  isLoading: false,
  error: ""
};

const login = (state = initialState, action) => {
  if (typeof state === "undefined") {
    return getDefaultState();
  }
  switch (action.type) {
    case Actions.LOGIN_REQUEST:
      return {
        ...state,
        isLoading: true,
        error: ""
      };
    case Actions.LOGIN_SUCCESS:
      return {
        ...state,
        data: action.data,
        isLoading: false,
        error: ""
      };
    case Actions.LOGIN_FAIL:
      return {
        ...state,
        isLoading: false,
        error: action.error
      };
    case Actions.RESET:
      return {
        state
      }
    default:
      return state;
  }
};

export default login;
