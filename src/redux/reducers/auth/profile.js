import Actions from "actions";

export const getDefaultState = () => ({});

const initialState = {
  data: {},
  isLoading: false,
};

const profile = (state = initialState, action) => {
  if (typeof state === "undefined") {
    return getDefaultState();
  }
  switch (action.type) {
    case Actions.PROFILE_REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case Actions.PROFILE_SUCCESS:
      console.log("hooola", action.data)
      return {
        ...state,
        data: action.data,
        isLoading: false,
      };
    case Actions.PROFILE_FAIL:
      return {
        ...state,
        isLoading: false,
      };
    case Actions.LOGOUT:
      return initialState;
    default:
      return state;
  }
};

export default profile;
