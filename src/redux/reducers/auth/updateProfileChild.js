import Actions from "actions";

export const getDefaultState = () => ({});

const initialState = {
  data: {},
  isLoading: false,
};

const updateProfileChild = (state = initialState, action) => {
  if (typeof state === "undefined") {
    return getDefaultState();
  }
  switch (action.type) {
    case Actions.UPDATE_PROFILE_CHILD_REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case Actions.UPDATE_PROFILE_CHILD_SUCCESS:
      return {
        ...state,
        data: action.data,
        isLoading: false,
      };
    case Actions.UPDATE_PROFILE_CHILD_FAIL:
      return {
        ...state,
        isLoading: false,
      };
    case Actions.LOGOUT:
      return initialState;
    default:
      return state;
  }
};

export default updateProfileChild;
