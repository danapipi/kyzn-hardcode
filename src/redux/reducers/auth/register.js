import Actions from "actions";

export const getDefaultState = () => ({});

const initialState = {
  isLoading: false,
  error: "",
};

const register = (state = initialState, action) => {
  if (typeof state === "undefined") {
    return getDefaultState();
  }
  switch (action.type) {
    case Actions.REGISTER_REQUEST:
      return {
        ...state,
        isLoading: true,
        error: "",
      };
    case Actions.REGISTER_SUCCESS:
      return {
        ...state,
        isLoading: false,
        error: "",
      };
    case Actions.REGISTER_FAIL:
      return {
        ...state,
        isLoading: false,
        error: action.error,
      };
    case Actions.RESET:
      return {
        state,
      };
    default:
      return state;
  }
};

export default register;
