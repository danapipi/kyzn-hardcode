import Actions from "actions";

export const getDefaultState = () => ({});

const initialState = {
  firstTime: false
};

const firstTime = (state = initialState, action) => {
  if (typeof state === "undefined") {
    return getDefaultState();
  }
  switch (action.type) {
    case Actions.FISRT_TIME_REQUEST:
      console.log("action.data", action.data, state);
      return {
        ...state,
        firstTime: action.data,
      };
    default:
      return state;
  }
};

export default firstTime;
