import Actions from "actions";

export const getDefaultState = () => ({});

const initialState = {
  isLoading: false,
  error: "",
};

const loginUserName = (state = initialState, action) => {
  if (typeof state === "undefined") {
    return getDefaultState();
  }
  switch (action.type) {
    case Actions.LOGIN_USERNAME_REQUEST:
      return {
        ...state,
        isLoading: true,
        error: "",
      };
    case Actions.LOGIN_USERNAME_SUCCESS:
      return {
        ...state,
        data: action.data,
        isLoading: false,
        error: "",
      };
    case Actions.LOGIN_USERNAME_FAIL:
      return {
        ...state,
        isLoading: false,
        error: action.error,
      };
    case Actions.RESET:
      return {
        state,
      };
    default:
      return state;
  }
};

export default loginUserName;
