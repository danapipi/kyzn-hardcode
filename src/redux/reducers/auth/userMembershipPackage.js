import Actions from "actions";

export const getDefaultState = () => ({});

const initialState = {
  data: {},
  isLoading: false,
};

const userMembershipPackage = (state = initialState, action) => {
  if (typeof state === "undefined") {
    return getDefaultState();
  }
  switch (action.type) {
    case Actions.USER_MEMBERSHIP_PACKAGE_REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case Actions.USER_MEMBERSHIP_PACKAGE_SUCCESS:
      return {
        ...state,
        data: action.data,
        isLoading: false,
      };
    case Actions.USER_MEMBERSHIP_PACKAGE_FAIL:
      return {
        ...state,
        isLoading: false,
      };
    default:
      return state;
  }
};

export default userMembershipPackage;
