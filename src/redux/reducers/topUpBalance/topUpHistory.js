import Actions from "actions";

export const getDefaultState = () => ({});

const initialState = {
  topUpHistory: [
    {
      id: 431,
      user_id: 2,
      activity_id: null,
      type: "deposit",
      amount: 5000,
      balance_before: 84503500,
      balance_after: 84508500,
      membership_id: null,
      membership_package_id: null,
      created_at: "2021-11-22T18:25:52.000Z",
      updated_at: "2021-11-22T18:25:52.000Z",
      deleted_at: null,
      UserId: 2,
    },
    {
      id: 430,
      user_id: 2,
      activity_id: null,
      type: "deposit",
      amount: 6000,
      balance_before: 84497500,
      balance_after: 84503500,
      membership_id: null,
      membership_package_id: null,
      created_at: "2021-11-22T18:22:41.000Z",
      updated_at: "2021-11-22T18:22:41.000Z",
      deleted_at: null,
      UserId: 2,
    },
    {
      id: 418,
      user_id: 2,
      activity_id: null,
      type: "deposit",
      amount: 47500,
      balance_before: 84450000,
      balance_after: 84497500,
      membership_id: null,
      membership_package_id: null,
      created_at: "2021-11-20T14:30:40.000Z",
      updated_at: "2021-11-20T14:30:40.000Z",
      deleted_at: null,
      UserId: 2,
    },
    {
      id: 393,
      user_id: 2,
      activity_id: 32,
      type: "purchase",
      amount: 130000,
      balance_before: 84450000,
      balance_after: 84320000,
      membership_id: null,
      membership_package_id: null,
      created_at: "2021-11-19T18:48:42.000Z",
      updated_at: "2021-11-19T18:48:42.000Z",
      deleted_at: null,
      UserId: 2,
    },
    {
      id: 392,
      user_id: 2,
      activity_id: 32,
      type: "purchase",
      amount: 130000,
      balance_before: 84580000,
      balance_after: 84450000,
      membership_id: null,
      membership_package_id: null,
      created_at: "2021-11-19T18:47:24.000Z",
      updated_at: "2021-11-19T18:47:24.000Z",
      deleted_at: null,
      UserId: 2,
    },
    {
      id: 325,
      user_id: 2,
      activity_id: null,
      type: "deposit",
      amount: 10000,
      balance_before: 85700000,
      balance_after: 85710000,
      membership_id: null,
      membership_package_id: null,
      created_at: "2021-11-17T10:09:52.000Z",
      updated_at: "2021-11-17T10:09:52.000Z",
      deleted_at: null,
      UserId: 2,
    },
    {
      id: 304,
      user_id: 2,
      activity_id: 30,
      type: "purchase",
      amount: 50000,
      balance_before: 85700000,
      balance_after: 85650000,
      membership_id: null,
      membership_package_id: null,
      created_at: "2021-11-15T15:20:48.000Z",
      updated_at: "2021-11-15T15:20:48.000Z",
      deleted_at: null,
      UserId: 2,
    },
    {
      id: 303,
      user_id: 2,
      activity_id: 30,
      type: "purchase",
      amount: 50000,
      balance_before: 85750000,
      balance_after: 85700000,
      membership_id: null,
      membership_package_id: null,
      created_at: "2021-11-15T15:13:19.000Z",
      updated_at: "2021-11-15T15:13:19.000Z",
      deleted_at: null,
      UserId: 2,
    },
    {
      id: 301,
      user_id: 2,
      activity_id: 30,
      type: "purchase",
      amount: 50000,
      balance_before: 85700000,
      balance_after: 85650000,
      membership_id: null,
      membership_package_id: null,
      created_at: "2021-11-15T10:41:39.000Z",
      updated_at: "2021-11-15T10:41:39.000Z",
      deleted_at: null,
      UserId: 2,
    },
    {
      id: 251,
      user_id: 2,
      activity_id: null,
      type: "deposit",
      amount: 0,
      balance_before: 85750000,
      balance_after: 85750000,
      membership_id: null,
      membership_package_id: null,
      created_at: "2021-11-14T05:33:02.000Z",
      updated_at: "2021-11-14T05:33:02.000Z",
      deleted_at: null,
      UserId: 2,
    },
  ],
  isLoading: false,
};

const topUpHistory = (state = initialState, action) => {
  if (typeof state === "undefined") {
    return getDefaultState();
  }
  switch (action.type) {
    case Actions.TOP_UP_HISTORY_REQUEST:
      return {
        ...state,
        isLoading: true,
      };

    case Actions.TOP_UP_HISTORY_SUCCESS:
      return {
        ...state,
        topUpHistory: action.data,
        isLoading: false,
      };
    case Actions.TOP_UP_HISTORY_FAIL:
      return {
        ...state,
        isLoading: false,
      };
    default:
      return state;
  }
};

export default topUpHistory;
