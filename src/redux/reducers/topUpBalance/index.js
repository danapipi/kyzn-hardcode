import {combineReducers} from 'redux';
import topUpHistory from './topUpHistory';
import topUp from './topUp'

export default combineReducers({
  topUpHistory,
  topUp
});
