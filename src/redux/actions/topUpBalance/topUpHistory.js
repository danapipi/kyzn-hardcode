export const NAME = 'TOP_UP_HISTORY';

export const TOP_UP_HISTORY_REQUEST = `${NAME}/TOP_UP_HISTORY_REQUEST`;
export const TOP_UP_HISTORY_SUCCESS = `${NAME}/TOP_UP_HISTORY_SUCCESS`;
export const TOP_UP_HISTORY_FAIL = `${NAME}/TOP_UP_HISTORY_FAIL`;

export const topupHistory = (limit, offset) => ({
  type: TOP_UP_HISTORY_REQUEST,
  limit,
  offset
});
export const topupHistorySuccess = data => ({
  type: TOP_UP_HISTORY_SUCCESS,
  data,
});

export const topupHistoryFail = data => ({
  type: TOP_UP_HISTORY_FAIL,
  data,
});

