import * as topUpBalance from './topUp';
import * as topUpHistory from './topUpHistory';

export default {
  ...topUpBalance,
  ...topUpHistory,
};
