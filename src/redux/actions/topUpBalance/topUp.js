export const NAME = 'TOP_UP';

export const TOP_UP_REQUEST = `${NAME}/TOP_UP_REQUEST`;
export const TOP_UP_SUCCESS = `${NAME}/TOP_UP_SUCCESS`;
export const TOP_UP_FAIL = `${NAME}/TOP_UP_FAIL`;
export const RESET = `${NAME}/RESET`;


export const topup = (amount, userId) => ({
  type: TOP_UP_REQUEST,
  payload: {
    amount, userId
  },
});

export const topupSuccess = data => ({
  type: TOP_UP_SUCCESS,
  data,
});

export const topupFail = error => ({
  type: TOP_UP_FAIL,
  error,
});

export const TopUpReset = () => ({
  type: RESET,
})

