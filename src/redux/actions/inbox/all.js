export const NAME = "INBOX";

export const FETCH_INBOX_REQUEST = `${NAME}/FETCH_INBOX_REQUEST`;
export const FETCH_INBOX_REQUEST_SUCCESS = `${NAME}/FETCH_INBOX_REQUEST_SUCCESS`;
export const FETCH_INBOX_REQUEST_FAIL = `${NAME}/FETCH_INBOX_REQUEST_FAIL`;

export const fetchInbox = (userId) => ({
  type: FETCH_INBOX_REQUEST,
  userId,
});

export const fetchInboxSuccess = data => ({
  type: FETCH_INBOX_REQUEST_SUCCESS,
  data,
});

export const fetchInboxFail = data => ({
  type: FETCH_INBOX_REQUEST_FAIL,
  data,
});
