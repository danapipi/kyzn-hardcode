export const NAME = "PREFERENCE";

export const PREFERENCE_REQUEST = `${NAME}/PREFERENCE_REQUEST`;
export const PREFERENCE_SUCCESS = `${NAME}/PREFERENCE_SUCCESS`;
export const PREFERENCE_FAIL = `${NAME}/PREFERENCE_FAIL`;

export const preference = () => ({
  type: PREFERENCE_REQUEST,
});

export const preferenceSuccess = (data) => ({
  type: PREFERENCE_SUCCESS,
  data,
});

export const preferenceFail = (data) => ({
  type: PREFERENCE_FAIL,
  data,
});
