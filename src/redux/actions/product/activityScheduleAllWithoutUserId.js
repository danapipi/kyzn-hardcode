export const NAME = 'ACTIVITY_SCHEDULE_ALL_NO_USER_ID';

export const ACTIVITY_SCHEDULE_ALL_NO_USER_ID_REQUEST = `${NAME}/ACTIVITY_SCHEDULE_ALL_NO_USER_ID_REQUEST`;
export const ACTIVITY_SCHEDULE_ALL_NO_USER_ID_SUCCESS = `${NAME}/ACTIVITY_SCHEDULE_ALL_NO_USER_ID_SUCCESS`;
export const ACTIVITY_SCHEDULE_ALL_NO_USER_ID_FAIL = `${NAME}/ACTIVITY_SCHEDULE_ALL_NO_USER_ID_FAIL`;

export const activityScheduleAllWithoutUserId = () => ({
  type: ACTIVITY_SCHEDULE_ALL_NO_USER_ID_REQUEST,
});

export const activityScheduleAllWithoutUserIdSuccess = data => ({
  type: ACTIVITY_SCHEDULE_ALL_NO_USER_ID_SUCCESS,
  data,
});

export const activityScheduleAllWithoutUserIdFail = data => ({
  type: ACTIVITY_SCHEDULE_ALL_NO_USER_ID_FAIL,
  data,
});

