export const NAME = 'ACTIVITY';

export const ACTIVITY_REQUEST = `${NAME}/ACTIVITY_REQUEST`;
export const ACTIVITY_SUCCESS = `${NAME}/ACTIVITY_SUCCESS`;
export const ACTIVITY_FAIL = `${NAME}/ACTIVITY_FAIL`;

export const activity = (activityTypeId, categoryId, categoryChildId) => ({
  type: ACTIVITY_REQUEST,
  activityTypeId,
  categoryId,
  categoryChildId
});

export const activitySuccess = data => ({
  type: ACTIVITY_SUCCESS,
  data,
});

export const activityFail = data => ({
  type: ACTIVITY_FAIL,
  data,
});

