export const NAME = 'CATEGORY_CHILD';

export const CATEGORY_CHILD_REQUEST = `${NAME}/CATEGORY_CHILD_REQUEST`;
export const CATEGORY_CHILD_SUCCESS = `${NAME}/CATEGORY_CHILD_SUCCESS`;
export const CATEGORY_CHILD_FAIL = `${NAME}/CATEGORY_CHILD_FAIL`;

export const categoryChild = (id) => ({
  type: CATEGORY_CHILD_REQUEST,
  id
});

export const categoryChildSuccess = data => ({
  type: CATEGORY_CHILD_SUCCESS,
  data,
});

export const categoryChildFail = data => ({
  type: CATEGORY_CHILD_FAIL,
  data,
});

