export const NAME = 'BOOKING';

export const ACTIVITY_BOOK_REQUEST = `${NAME}/ACTIVITY_BOOK_REQUEST`;
export const ACTIVITY_BOOK_SUCCESS = `${NAME}/ACTIVITY_BOOK_SUCCESS`;
export const ACTIVITY_BOOK_FAIL = `${NAME}/ACTIVITY_BOOK_FAIL`;

export const activityBooking = (activityScheduleId, userId) => ({
  type: ACTIVITY_BOOK_REQUEST,
  activityScheduleId, 
  userId
});

export const activityBookingSuccess = data => ({
  type: ACTIVITY_BOOK_SUCCESS,
  data,
});

export const activityBookingFail = data => ({
  type: ACTIVITY_BOOK_FAIL,
  data,
});
