export const NAME = 'ACTIVITY_TYPE';

export const ACTIVITY_TYPE_REQUEST = `${NAME}/ACTIVITY_TYPE_REQUEST`;
export const ACTIVITY_TYPE_SUCCESS = `${NAME}/ACTIVITY_TYPE_SUCCESS`;
export const ACTIVITY_TYPE_FAIL = `${NAME}/ACTIVITY_TYPE_FAIL`;

export const activityType = (categoryChildId) => ({
  type: ACTIVITY_TYPE_REQUEST,
  categoryChildId
});

export const activityTypeSuccess = data => ({
  type: ACTIVITY_TYPE_SUCCESS,
  data,
});

export const activityTypeFail = data => ({
  type: ACTIVITY_TYPE_FAIL,
  data,
});

