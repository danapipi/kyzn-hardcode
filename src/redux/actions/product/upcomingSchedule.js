export const NAME = 'UPCOMING_SCHEDULE';

export const UPCOMING_SCHEDULE_REQUEST = `${NAME}/UPCOMING_SCHEDULE_REQUEST`;
export const UPCOMING_SCHEDULE_SUCCESS = `${NAME}/UPCOMING_SCHEDULE_SUCCESS`;
export const UPCOMING_SCHEDULE_FAIL = `${NAME}/UPCOMING_SCHEDULE_FAIL`;

export const upcomingSchedule = (userID) => ({
  type: UPCOMING_SCHEDULE_REQUEST,
  userID
});

export const upcomingScheduleSuccess = data => ({
  type: UPCOMING_SCHEDULE_SUCCESS,
  data,
});

export const upcomingScheduleFail = data => ({
  type: UPCOMING_SCHEDULE_FAIL,
  data,
});

