import * as category from "./category";
import * as categoryChild from "./categoryChild";
import * as activity from "./activity";
import * as activityType from "./activityType";
import * as activitySchedule from "./activitySchedule";
import * as activityScheduleAll from "./activityScheduleAll";
import * as activityScheduleAllWithoutUserId from "./activityScheduleAllWithoutUserId";
import * as activityBooking from "./activityBook";
import * as upcomingSchedule from "./upcomingSchedule";
import * as pastSchedule from "./pastSchedule";
import * as featuredActivity from "./featuredActivity";
import * as featuredMembership from "./featuredMembership";
import * as activityById from "./activityById";
import * as activityTypeId from "./activityTypeId";
import * as preference from "./preference";
import * as createPreference from "./createPreference";

export default {
  ...category,
  ...categoryChild,
  ...activity,
  ...activityType,
  ...activitySchedule,
  ...activityBooking,
  ...upcomingSchedule,
  ...featuredActivity,
  ...featuredMembership,
  ...activityById,
  ...pastSchedule,
  ...activityScheduleAll,
  ...activityTypeId,
  ...activityScheduleAllWithoutUserId,
  ...preference,
  ...createPreference,
};
