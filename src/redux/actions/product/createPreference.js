export const NAME = "CREATE_PREFERENCE";

export const CREATE_PREFERENCE_REQUEST = `${NAME}/PREFERENCE_REQUEST`;
export const CREATE_PREFERENCE_SUCCESS = `${NAME}/PREFERENCE_SUCCESS`;
export const CREATE_PREFERENCE_FAIL = `${NAME}/PREFERENCE_FAIL`;

export const createPreference = (payload) => ({
  type: CREATE_PREFERENCE_REQUEST,
  payload,
});

export const createPreferenceSuccess = (data) => ({
  type: CREATE_PREFERENCE_SUCCESS,
  data,
});

export const createPreferenceFail = (data) => ({
  type: CREATE_PREFERENCE_FAIL,
  data,
});
