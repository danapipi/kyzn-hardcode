export const NAME = 'ACTIVITY_SCHEDULE_ALL';

export const ACTIVITY_SCHEDULE_ALL_REQUEST = `${NAME}/ACTIVITY_SCHEDULE_ALL_REQUEST`;
export const ACTIVITY_SCHEDULE_ALL_SUCCESS = `${NAME}/ACTIVITY_SCHEDULE_ALL_SUCCESS`;
export const ACTIVITY_SCHEDULE_ALL_FAIL = `${NAME}/ACTIVITY_SCHEDULE_ALL_FAIL`;

export const activityScheduleAll = (userId) => ({
  type: ACTIVITY_SCHEDULE_ALL_REQUEST,
  userId
});

export const activityScheduleAllSuccess = data => ({
  type: ACTIVITY_SCHEDULE_ALL_SUCCESS,
  data,
});

export const activityScheduleAllFail = data => ({
  type: ACTIVITY_SCHEDULE_ALL_FAIL,
  data,
});

