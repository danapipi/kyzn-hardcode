export const NAME = 'ACTIVITY_TYPE_ID';

export const ACTIVITY_TYPE_ID_REQUEST = `${NAME}/ACTIVITY_TYPE_ID_REQUEST`;
export const ACTIVITY_TYPE_ID_SUCCESS = `${NAME}/ACTIVITY_TYPE_ID_SUCCESS`;
export const ACTIVITY_TYPE_ID_FAIL = `${NAME}/ACTIVITY_TYPE_ID_FAIL`;

export const activityTypeById = () => ({
  type: ACTIVITY_TYPE_ID_REQUEST
});

export const activityTypeIdSuccess = data => ({
  type: ACTIVITY_TYPE_ID_SUCCESS,
  data,
});

export const activityTypeIdFail = data => ({
  type: ACTIVITY_TYPE_ID_FAIL,
  data,
});

