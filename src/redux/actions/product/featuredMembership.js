export const NAME = 'FEATURED_MEMBERSHIP';

export const FEATURED_MEMBERSHIP_REQUEST = `${NAME}/FEATURED_MEMBERSHIP_REQUEST`;
export const FEATURED_MEMBERSHIP_SUCCESS = `${NAME}/FEATURED_MEMBERSHIP_SUCCESS`;
export const FEATURED_MEMBERSHIP_FAIL = `${NAME}/FEATURED_MEMBERSHIP_FAIL`;

export const featuredMembership = () => ({
  type: FEATURED_MEMBERSHIP_REQUEST, 
});

export const featuredMembershipSuccess = data => ({
  type: FEATURED_MEMBERSHIP_SUCCESS,
  data,
});

export const featuredMembershipFail = data => ({
  type: FEATURED_MEMBERSHIP_FAIL,
  data,
});

