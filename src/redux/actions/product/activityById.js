export const NAME = 'ACTIVITY';

export const ACTIVITY_BY_ID_REQUEST = `${NAME}/ACTIVITY_BY_ID_REQUEST`;
export const ACTIVITY_BY_ID_SUCCESS = `${NAME}/ACTIVITY_BY_ID_SUCCESS`;
export const ACTIVITY_BY_ID_FAIL = `${NAME}/ACTIVITY_BY_ID_FAIL`;

export const activityById = (activityTypeId, activityId, date, stepMultiplier, userId) => ({
  type: ACTIVITY_BY_ID_REQUEST,
  activityTypeId,
  activityId,
  date,
  stepMultiplier,
  userId
});

export const activityByIdSuccess = data => ({
  type: ACTIVITY_BY_ID_SUCCESS,
  data,
});

export const activityByIdFail = data => ({
  type: ACTIVITY_BY_ID_FAIL,
  data,
});

