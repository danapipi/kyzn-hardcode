export const NAME = 'PAST_SCHEDULE';

export const PAST_SCHEDULE_REQUEST = `${NAME}/PAST_SCHEDULE_REQUEST`;
export const PAST_SCHEDULE_SUCCESS = `${NAME}/PAST_SCHEDULE_SUCCESS`;
export const PAST_SCHEDULE_FAIL = `${NAME}/PAST_SCHEDULE_FAIL`;

export const pastSchedule = (userID) => ({
  type: PAST_SCHEDULE_REQUEST,
  userID
});

export const pastScheduleSuccess = data => ({
  type: PAST_SCHEDULE_SUCCESS,
  data,
});

export const pastScheduleFail = data => ({
  type: PAST_SCHEDULE_FAIL,
  data,
});

