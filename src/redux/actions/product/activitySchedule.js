export const NAME = 'ACTIVITY_SCHEDULE';

export const ACTIVITY_SCHEDULE_REQUEST = `${NAME}/ACTIVITY_SCHEDULE_REQUEST`;
export const ACTIVITY_SCHEDULE_SUCCESS = `${NAME}/ACTIVITY_SCHEDULE_SUCCESS`;
export const ACTIVITY_SCHEDULE_FAIL = `${NAME}/ACTIVITY_SCHEDULE_FAIL`;

export const activitySchedule = (activity_id, userId) => ({
  type: ACTIVITY_SCHEDULE_REQUEST,
  activity_id,
  userId
});

export const activityScheduleSuccess = data => ({
  type: ACTIVITY_SCHEDULE_SUCCESS,
  data,
});

export const activityScheduleFail = data => ({
  type: ACTIVITY_SCHEDULE_FAIL,
  data,
});

