export const NAME = 'FEATURED_ACTIVITY';

export const FEATURED_ACTIVITY_REQUEST = `${NAME}/FEATURED_ACTIVITY_REQUEST`;
export const FEATURED_ACTIVITY_SUCCESS = `${NAME}/FEATURED_ACTIVITY_SUCCESS`;
export const FEATURED_ACTIVITY_FAIL = `${NAME}/FEATURED_ACTIVITY_FAIL`;

export const featuredActivity = () => ({
  type: FEATURED_ACTIVITY_REQUEST, 
});

export const featuredActivitySuccess = data => ({
  type: FEATURED_ACTIVITY_SUCCESS,
  data,
});

export const featuredActivityFail = data => ({
  type: FEATURED_ACTIVITY_FAIL,
  data,
});

