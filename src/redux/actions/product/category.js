export const NAME = 'CATEGORY';

export const CATEGORY_REQUEST = `${NAME}/CATEGORY_REQUEST`;
export const CATEGORY_SUCCESS = `${NAME}/CATEGORY_SUCCESS`;
export const CATEGORY_FAIL = `${NAME}/CATEGORY_FAIL`;

export const category = () => ({
  type: CATEGORY_REQUEST,
});

export const categorySuccess = data => ({
  type: CATEGORY_SUCCESS,
  data,
});

export const categoryFail = data => ({
  type: CATEGORY_FAIL,
  data,
});

