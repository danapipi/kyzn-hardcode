export const NAME = 'FEATURED_WORKER';

export const FEATURED_WORKER_REQUEST = `${NAME}/FEATURED_WORKER_REQUEST`;
export const FEATURED_WORKER_SUCCESS = `${NAME}/FEATURED_WORKER_SUCCESS`;
export const FEATURED_WORKER_FAIL = `${NAME}/FEATURED_WORKER_FAIL`;

export const featuredWorker = () => ({
  type: FEATURED_WORKER_REQUEST, 
});

export const featuredWorkerSuccess = data => ({
  type: FEATURED_WORKER_SUCCESS,
  data,
});

export const featuredWorkerFail = data => ({
  type: FEATURED_WORKER_FAIL,
  data,
});

