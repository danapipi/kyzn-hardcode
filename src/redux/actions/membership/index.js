import * as categories from "./categories";
import * as packages from "./packages";
import * as types from "./types";
import * as payment from "./payment";
import * as membershipById from "./membershipById";

export default {
  ...categories,
  ...packages,
  ...types,
  ...payment,
  ...membershipById
};
