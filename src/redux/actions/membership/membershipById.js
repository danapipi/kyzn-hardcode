export const NAME = 'MEMBERSHIP';

export const MEMBERSHIP_BY_ID_REQUEST = `${NAME}/MEMBERSHIP_BY_ID_REQUEST`;
export const MEMBERSHIP_BY_ID_SUCCESS = `${NAME}/MEMBERSHIP_BY_ID_SUCCESS`;
export const MEMBERSHIP_BY_ID_FAIL = `${NAME}/MEMBERSHIP_BY_ID_FAIL`;

export const fetchMembershipById = (id) => ({
  type: MEMBERSHIP_BY_ID_REQUEST,
  id,
});

export const fetchMembershipByIdSuccess = data => ({
  type: MEMBERSHIP_BY_ID_SUCCESS,
  data,
});

export const fetchMembershipByIdFail = data => ({
  type: MEMBERSHIP_BY_ID_FAIL,
  data,
});

