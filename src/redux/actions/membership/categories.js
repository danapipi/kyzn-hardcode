export const NAME = "MEMBERSHIP";

export const FETCH_MEMBERSHIP_CATEGORIES_REQUEST = `${NAME}/FETCH_MEMBERSHIP_CATEGORIES_REQUEST`;
export const FETCH_MEMBERSHIP_CATEGORIES_SUCCESS = `${NAME}/FETCH_MEMBERSHIP_CATEGORIES_SUCCESS`;
export const FETCH_MEMBERSHIP_CATEGORIES_FAIL = `${NAME}/FETCH_MEMBERSHIP_CATEGORIES_FAIL`;

export const fetchMembershipCategories = () => ({
  type: FETCH_MEMBERSHIP_CATEGORIES_REQUEST,
});

export const fetchMembershipCategoriesSuccess = data => ({
  type: FETCH_MEMBERSHIP_CATEGORIES_SUCCESS,
  data,
});

export const fetchMembershipCategoriesFail = data => ({
  type: FETCH_MEMBERSHIP_CATEGORIES_FAIL,
  data,
});
