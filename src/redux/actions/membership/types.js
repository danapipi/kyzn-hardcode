export const NAME = "MEMBERSHIP";

export const FETCH_MEMBERSHIP_TYPES_REQUEST = `${NAME}/FETCH_MEMBERSHIP_TYPES_REQUEST`;
export const FETCH_MEMBERSHIP_TYPES_SUCCESS = `${NAME}/FETCH_MEMBERSHIP_TYPES_SUCCESS`;
export const FETCH_MEMBERSHIP_TYPES_FAIL = `${NAME}/FETCH_MEMBERSHIP_TYPES_FAIL`;

export const fetchMembershipTypes = () => ({
  type: FETCH_MEMBERSHIP_TYPES_REQUEST,
});

export const fetchMembershipTypesSuccess = data => ({
  type: FETCH_MEMBERSHIP_TYPES_SUCCESS,
  data,
});

export const fetchMembershipTypesFail = data => ({
  type: FETCH_MEMBERSHIP_TYPES_FAIL,
  data,
});
