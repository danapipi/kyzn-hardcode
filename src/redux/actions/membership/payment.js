export const NAME = "MEMBERSHIP";

export const MEMBERSHIP_PAYMENT_REQUEST = `${NAME}/MEMBERSHIP_PAYMENT_REQUEST`;
export const MEMBERSHIP_PAYMENT_SUCCESS = `${NAME}/MEMBERSHIP_PAYMENT_SUCCESS`;
export const MEMBERSHIP_PAYMENT_FAIL = `${NAME}/MEMBERSHIP_PAYMENT_FAIL`;
export const RESET = `${NAME}/RESET`;

export const makeMembershipPayment = ({
  data,
  successCallback,
  errorCallback,
}) => ({
  type: MEMBERSHIP_PAYMENT_REQUEST,
  data,
  successCallback,
  errorCallback,
});

export const makeMembershipPaymentSuccess = data => ({
  type: MEMBERSHIP_PAYMENT_SUCCESS,
  data,
});

export const makeMembershipPaymentFail = error => ({
  type: MEMBERSHIP_PAYMENT_FAIL,
  error,
});

export const membershipPaymentReset = () => ({
  type: RESET,
})
