export const NAME = "MEMBERSHIP";

export const FETCH_MEMBERSHIP_PACKAGES_REQUEST = `${NAME}/FETCH_MEMBERSHIP_PACKAGES_REQUEST`;
export const FETCH_MEMBERSHIP_PACKAGES_SUCCESS = `${NAME}/FETCH_MEMBERSHIP_PACKAGES_SUCCESS`;
export const FETCH_MEMBERSHIP_PACKAGES_FAIL = `${NAME}/FETCH_MEMBERSHIP_PACKAGES_FAIL`;

export const fetchMembershipPackages = () => ({
  type: FETCH_MEMBERSHIP_PACKAGES_REQUEST,
});

export const fetchMembershipPackagesSuccess = data => ({
  type: FETCH_MEMBERSHIP_PACKAGES_SUCCESS,
  data,
});

export const fetchMembershipPackagesFail = data => ({
  type: FETCH_MEMBERSHIP_PACKAGES_FAIL,
  data,
});
