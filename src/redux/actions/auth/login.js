export const NAME = "AUTH";

export const LOGIN_REQUEST = `${NAME}/LOGIN_REQUEST`;
export const LOGIN_SUCCESS = `${NAME}/LOGIN_SUCCESS`;
export const LOGIN_FAIL = `${NAME}/LOGIN_FAIL`;
export const LOGOUT = `${NAME}/LOGOUT`;
export const RESET = `${NAME}/RESET`;

export const login = (
  phoneNumber,
  password,
) => ({
  type: LOGIN_REQUEST,
  payload: { phoneNumber, password },
});

export const loginSuccess = (data) => ({
  type: LOGIN_SUCCESS,
  data,
});

export const loginFail = (error) => ({
  type: LOGIN_FAIL,
  error,
});

export const logout = () => ({
  type: LOGOUT,
});

export const reset = () => ({
  type: RESET,
})
