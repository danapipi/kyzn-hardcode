export const NAME = "PROFILE";

export const UPDATE_PROFILE_CHILD_REQUEST = `${NAME}/UPDATE_PROFILE_CHILD_REQUEST`;
export const UPDATE_PROFILE_CHILD_SUCCESS = `${NAME}/UPDATE_PROFILE_CHILD_SUCCESS`;
export const UPDATE_PROFILE_CHILD_FAIL = `${NAME}/UPDATE_PROFILE_CHILD_FAIL`;

export const updateProfileChild = ({
  id,
  name,
  email,
  birthdate,
  gender,
  address,
  nationality,
  postalCode,
  province,
  district,
  photo
}) => ({
  type: UPDATE_PROFILE_CHILD_REQUEST,
  payload: {
    id,
    name,
    email,
    birthdate,
    gender,
    address,
    nationality,
    postalCode,
    province,
    district,
    photo,
  },
});

export const updateProfileChildSuccess = (data) => ({
  type: UPDATE_PROFILE_CHILD_SUCCESS,
  data,
});

export const updateProfileChildFail = (data) => ({
  type: UPDATE_PROFILE_CHILD_FAIL,
  data,
});
