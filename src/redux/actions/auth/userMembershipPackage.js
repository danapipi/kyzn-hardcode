export const NAME = 'USER_MEMBERSHIP_PACKAGE';

export const USER_MEMBERSHIP_PACKAGE_REQUEST = `${NAME}/USER_MEMBERSHIP_PACKAGE_REQUEST`;
export const USER_MEMBERSHIP_PACKAGE_SUCCESS = `${NAME}/USER_MEMBERSHIP_PACKAGE_SUCCESS`;
export const USER_MEMBERSHIP_PACKAGE_FAIL = `${NAME}/USER_MEMBERSHIP_PACKAGE_FAIL`;

export const userMembershipPackage = (userId) => ({
  type: USER_MEMBERSHIP_PACKAGE_REQUEST,
  userId
});

export const userMembershipPackageSuccess = data => ({
  type: USER_MEMBERSHIP_PACKAGE_SUCCESS,
  data,
});

export const userMembershipPackageFail = data => ({
  type: USER_MEMBERSHIP_PACKAGE_FAIL,
  data,
});

