export const NAME = 'PROFILE';

export const PROFILE_REQUEST = `${NAME}/PROFILE_REQUEST`;
export const PROFILE_SUCCESS = `${NAME}/PROFILE_SUCCESS`;
export const PROFILE_FAIL = `${NAME}/PROFILE_FAIL`;

export const profile = () => ({
  type: PROFILE_REQUEST,
});

export const profileSuccess = data => ({
  type: PROFILE_SUCCESS,
  data,
});

export const profileFail = data => ({
  type: PROFILE_FAIL,
  data,
});

