export const NAME = "REGISTER";

export const REGISTER_REQUEST = `${NAME}/REGISTER_REQUEST`;
export const REGISTER_SUCCESS = `${NAME}/REGISTER_SUCCESS`;
export const REGISTER_FAIL = `${NAME}/REGISTER_FAIL`;
export const RESET = `${NAME}/RESET`;


export const register = (
  phoneNumber,
  password,
  confPassword,
  name,
  gender,
  nationality,
  birthdate,
  postalCode,
  address,
  province,
  district,
  city,
  username
) => ({
  type: REGISTER_REQUEST,
  payload: {
    phoneNumber,
    password,
    confPassword,
    name,
    gender,
    nationality,
    birthdate,
    postalCode,
    address,
    province,
    district,
    city,
    username
  },
});

export const registerSuccess = (data) => ({
  type: REGISTER_SUCCESS,
  data,
});

export const registerFail = (error) => ({
  type: REGISTER_FAIL,
  error,
});

export const signUpReset = () => ({
  type: RESET,
})


