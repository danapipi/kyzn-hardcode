export const NAME = "PROFILE";

export const UPDATE_PROFILE_REQUEST = `${NAME}/UPDATE_PROFILE_REQUEST`;
export const UPDATE_PROFILE_SUCCESS = `${NAME}/UPDATE_PROFILE_SUCCESS`;
export const UPDATE_PROFILE_FAIL = `${NAME}/UPDATE_PROFILE_FAIL`;

export const updateProfile = ({
  name,
  email,
  birthdate,
  gender,
  address,
  nationality,
  postalCode,
  province,
  district,
  photo
}) => ({
  type: UPDATE_PROFILE_REQUEST,
  payload: {
    name,
    email,
    birthdate,
    gender,
    address,
    nationality,
    postalCode,
    province,
    district,
    photo,
  },
});

export const updateProfileSuccess = (data) => ({
  type: UPDATE_PROFILE_SUCCESS,
  data,
});

export const updateProfileFail = (data) => ({
  type: UPDATE_PROFILE_FAIL,
  data,
});
