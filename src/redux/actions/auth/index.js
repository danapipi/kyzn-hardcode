import * as login from './login';
import * as register from './register';
import * as profile from './profile';
import * as updateProfile from './updateProfile';
import * as updateProfileChild from './updateProfileChild';
import * as loginUsername from './loginUsername';
import * as userMembershipPackage from './userMembershipPackage';
import * as firstTime from './firstTime';

export default {
  ...login,
  ...register,
  ...profile,
  ...updateProfile,
  ...updateProfileChild,
  ...loginUsername,
  ...userMembershipPackage,
  ...firstTime
};
