export const NAME = 'AUTH';

export const LOGIN_USERNAME_REQUEST = `${NAME}/LOGIN_USERNAME_REQUEST`;
export const LOGIN_USERNAME_SUCCESS = `${NAME}/LOGIN_USERNAME_SUCCESS`;
export const LOGIN_USERNAME_FAIL = `${NAME}/LOGIN_USERNAME_FAIL`;
export const LOGOUT = `${NAME}/LOGOUT`;
export const RESET = `${NAME}/RESET`;


export const loginUserName = (username, password) => ({
  type: LOGIN_USERNAME_REQUEST,
  payload: {
    username,
    password,
  },
});

export const loginUsernameSuccess = data => ({
  type: LOGIN_USERNAME_SUCCESS,
  data,
});

export const loginUsernameFail = error => ({
  type: LOGIN_USERNAME_FAIL,
  error,
});

export const logout = () => ({
  type: LOGOUT,
});

export const loginUserNameReset = () => ({
  type: RESET,
})
