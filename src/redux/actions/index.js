import auth from "./auth";
import product from "./product";
import topUpBalance from "./topUpBalance";
import membership from "./membership";
import family from "./family";
import inbox from './inbox';
import worker from './worker';

export default {
  ...auth,
  ...product,
  ...topUpBalance,
  ...membership,
  ...family,
  ...inbox,
  ...worker
};
