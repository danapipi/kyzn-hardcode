export const NAME = "FAMILY";

export const ADD_CHILD_REQUEST = `${NAME}/ADD_CHILD_REQUEST`;
export const ADD_CHILD_SUCCESS = `${NAME}/ADD_CHILD_SUCCESS`;
export const ADD_CHILD_FAIL = `${NAME}/ADD_CHILD_FAIL`;

export const addChild = (payload, successCallback, failCallback) => ({
  type: ADD_CHILD_REQUEST,
  payload,
  successCallback,
  failCallback,
});

export const addChildSuccess = data => ({
  type: ADD_CHILD_SUCCESS,
  data,
});

export const addChildFail = data => ({
  type: ADD_CHILD_FAIL,
  data,
});
