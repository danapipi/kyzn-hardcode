export const NAME = "FAMILY";

export const FETCH_ALL_FAMILIES_REQUEST = `${NAME}/FETCH_ALL_FAMILIES_REQUEST`;
export const FETCH_ALL_FAMILIES_SUCCESS = `${NAME}/FETCH_ALL_FAMILIES_SUCCESS`;
export const FETCH_ALL_FAMILIES_FAIL = `${NAME}/FETCH_ALL_FAMILIES_FAIL`;

export const fetchAllFamilies = filters => ({
  type: FETCH_ALL_FAMILIES_REQUEST,
  filters,
});

export const fetchAllFamiliesSuccess = data => ({
  type: FETCH_ALL_FAMILIES_SUCCESS,
  data,
});

export const fetchAllFamiliesFail = data => ({
  type: FETCH_ALL_FAMILIES_FAIL,
  data,
});
