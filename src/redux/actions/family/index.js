import * as user from "./user";
import * as all from "./all";
import * as addChild from "./addChild";
import * as uniqueFamily from './uniqueFamily';

export default {
  ...user,
  ...all,
  ...addChild,
  ...uniqueFamily,
};
