export const NAME = "FAMILY";

export const FETCH_USER_FAMILY_REQUEST = `${NAME}/FETCH_USER_FAMILY_REQUEST`;
export const FETCH_USER_FAMILY_SUCCESS = `${NAME}/FETCH_USER_FAMILY_SUCCESS`;
export const FETCH_USER_FAMILY_FAIL = `${NAME}/FETCH_USER_FAMILY_FAIL`;

export const fetchUserFamily = () => ({
  type: FETCH_USER_FAMILY_REQUEST,
});

export const fetchUserFamilySuccess = data => ({
  type: FETCH_USER_FAMILY_SUCCESS,
  data,
});

export const fetchUserFamilyFail = data => ({
  type: FETCH_USER_FAMILY_FAIL,
  data,
});
