export const NAME = "FAMILY";

export const FETCH_FAMILY_UNIQUE_REQUEST = `${NAME}/FETCH_FAMILY_UNIQUE_REQUEST`;
export const FETCH_FAMILY_UNIQUE_SUCCESS = `${NAME}/FETCH_FAMILY_UNIQUE_SUCCESS`;
export const FETCH_FAMILY_UNIQUE_FAIL = `${NAME}/FETCH_FAMILY_UNIQUE_FAIL`;

export const fetchFamilyUnique = role => ({
  type: FETCH_FAMILY_UNIQUE_REQUEST,
  role
});

export const fetchFamilyUniqueSuccess = data => ({
  type: FETCH_FAMILY_UNIQUE_SUCCESS,
  data,
});

export const fetchFamilyUniqueFail = data => ({
  type: FETCH_FAMILY_UNIQUE_FAIL,
  data,
});
