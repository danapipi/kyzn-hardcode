import { takeLatest, all, fork, put, call } from "redux-saga/effects";
import Actions from "actions";
import { membershipService } from "services";

function* fetchMembershipTypes() {
  try {
    const response = yield call(membershipService.getMembershipTypes);
    if (response.status !== 200) {
      throw new Error(`Request failed with status ${response.status}`);
    }
    yield put(Actions.fetchMembershipTypesSuccess(response?.data?.data || {}));
  } catch (error) {
    yield put(Actions.fetchMembershipTypesFail(error.response.data.message));
  }
}
function* watchFetchMembershipTypes() {
  yield takeLatest(
    Actions.FETCH_MEMBERSHIP_TYPES_REQUEST,
    fetchMembershipTypes,
  );
}

export default function* types() {
  yield all([fork(watchFetchMembershipTypes)]);
}
