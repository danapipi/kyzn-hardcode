import { takeLatest, all, fork, put, call } from "redux-saga/effects";
import Actions from "actions";
import { membershipService } from "services";

function* membershipPayment({ data, successCallback, errorCallback }) {
  try {
    const response = yield call(membershipService.makeMembershipPayment, data);
    console.log(data)
    if (response?.status !== 200) {
      throw new Error(`Request failed with status ${response.status}`);
    }
    yield put(Actions.makeMembershipPaymentSuccess());
    // yield put(Actions.profile());
    if (successCallback) {
      successCallback();
    }
  } catch (error) {
    yield put(Actions.makeMembershipPaymentFail(error.response.data.message));
    if (errorCallback) {
      errorCallback();
    }
  }
}
function* watchMembershipPaymentRequest() {
  yield takeLatest(Actions.MEMBERSHIP_PAYMENT_REQUEST, membershipPayment);
}

export default function* payment() {
  yield all([fork(watchMembershipPaymentRequest)]);
}
