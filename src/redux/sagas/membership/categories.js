import { takeLatest, all, fork, put, call } from "redux-saga/effects";
import Actions from "actions";
import { membershipService } from "services";

function* fetchMembershipCategories() {
  try {
    const response = yield call(membershipService.getMembershipCategories);
    if (response.status !== 200) {
      throw new Error(`Request failed with status ${response.status}`);
    }
    yield put(
      Actions.fetchMembershipCategoriesSuccess(response?.data?.data || {}),
    );
  } catch (error) {
    yield put(
      Actions.fetchMembershipCategoriesFail(error.response.data.message),
    );
  }
}
function* watchFetchMembershipCategories() {
  yield takeLatest(
    Actions.FETCH_MEMBERSHIP_CATEGORIES_REQUEST,
    fetchMembershipCategories,
  );
}

export default function* categories() {
  yield all([fork(watchFetchMembershipCategories)]);
}
