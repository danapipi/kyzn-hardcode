import { takeLatest, all, fork, put, call } from "redux-saga/effects";
import Actions from "actions";
import { membershipService } from "services";

function* fetchMembershipPackages() {
  try {
    const response = yield call(membershipService.getMembershipPackages);
    if (response.status !== 200) {
      throw new Error(`Request failed with status ${response.status}`);
    }
    yield put(
      Actions.fetchMembershipPackagesSuccess(response?.data?.data || {}),
    );
  } catch (error) {
    yield put(Actions.fetchMembershipPackagesFail(error.response.data.message));
  }
}
function* watchFetchMembershipPackages() {
  yield takeLatest(
    Actions.FETCH_MEMBERSHIP_PACKAGES_REQUEST,
    fetchMembershipPackages,
  );
}

export default function* packages() {
  yield all([fork(watchFetchMembershipPackages)]);
}
