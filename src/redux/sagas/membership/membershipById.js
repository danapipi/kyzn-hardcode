import { takeLatest, all, fork, put, delay, call } from "redux-saga/effects";
import Actions from "actions";
import { membershipService } from "../../../services";

function* membershipByIdRequest({ id }) {
  try {
    const { data } = yield call(
      membershipService.getMembershipPackagesById,
      id
    );
    yield put(Actions.fetchMembershipByIdSuccess(data.data));
  } catch (error) {
    console.log("error getActivity", error);
    yield put(Actions.fetchMembershipByIdFail(error));
  }
}
function* watchMembershipById() {
  yield takeLatest(Actions.MEMBERSHIP_BY_ID_REQUEST, membershipByIdRequest);
}

export default function* membershipById() {
  yield all([fork(watchMembershipById)]);
}
