import { all, fork } from "redux-saga/effects";
import categories from "./categories";
import packages from "./packages";
import types from "./types";
import payment from "./payment";
import membershipById from "./membershipById"

export default function* membership() {
  yield all([fork(categories)]);
  yield all([fork(packages)]);
  yield all([fork(types)]);
  yield all([fork(payment)]);
  yield all([fork(membershipById)]);
}
