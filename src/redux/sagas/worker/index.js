import {all, fork} from 'redux-saga/effects';
import featuredWorker from './featuredWorker'

export default function* categorySaga() {
    yield all([fork(featuredWorker)])
}