import {takeLatest, all, fork, put, delay, call} from 'redux-saga/effects';
import Actions from 'actions';
import { workerService } from "../../../services"

function* featuredWorkerRequest() {
  try {
    const { data } = yield call(workerService.getFeaturedWorker)
    console.log("data get featuredWorker", data)
    yield put(Actions.featuredWorkerSuccess(data.data))
  } catch (error) {
    console.log("error get featuredWorker", error)
    yield put(Actions.featuredWorkerFail(error));
  }
}
function* watchFeaturedWorker() {
  yield takeLatest(Actions.FEATURED_WORKER_REQUEST, featuredWorkerRequest);
}

export default function* featuredWorker() {
  yield all([fork(watchFeaturedWorker)]);
}
