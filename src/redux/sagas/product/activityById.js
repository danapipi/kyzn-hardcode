import { takeLatest, all, fork, put, delay, call } from "redux-saga/effects";
import Actions from "actions";
import { activityService } from "../../../services";

function* activityByIdRequest({ activityTypeId, activityId, date, stepMultiplier, userId }) {
  try {
    const { data } = yield call(
      activityService.getActivityById,
      activityTypeId,
      activityId,
      date,
      stepMultiplier,
      userId
    );
    yield put(Actions.activityByIdSuccess(data.data));
  } catch (error) {
    console.log("error getActivity", error);
    yield put(Actions.activityByIdFail(error));
  }
}
function* watchActivityById() {
  yield takeLatest(Actions.ACTIVITY_BY_ID_REQUEST, activityByIdRequest);
}

export default function* activityById() {
  yield all([fork(watchActivityById)]);
}
