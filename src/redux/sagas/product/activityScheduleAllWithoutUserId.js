import { takeLatest, all, fork, put, delay, call } from "redux-saga/effects";
import Actions from "actions";
import { activityService } from "../../../services";

function* activityScheduleAllWithoutUserId() {
  try {
    const { data } = yield call(
      activityService.getAllActivityScheduleWithoutUserId
    );
    yield put(Actions.activityScheduleAllWithoutUserIdSuccess(data.data));
  } catch (error) {
    console.log("error getActivitySchedule", error);
    yield put(Actions.activityScheduleAllWithoutUserIdFail(error));
  }
}
function* watchActivityScheduleAllWithoutUserId() {
  yield takeLatest(Actions.ACTIVITY_SCHEDULE_ALL_REQUEST, activityScheduleAllWithoutUserId);
}

export default function* activityScheduleAll() {
  yield all([fork(watchActivityScheduleAllWithoutUserId)]);
}
