import { takeLatest, all, fork, put, delay, call } from "redux-saga/effects";
import Actions from "actions";
import { utils } from "../../../services";

function* activityTypeByIdRequest() {
  try {
    const { data } = yield call(
      utils.getConfig,
    );
    yield put(Actions.activityTypeIdSuccess(data.data));
    console.log("callled data", data);
  } catch (error) {
    console.log("error getActivityType", error);
    yield put(Actions.activityTypeIdFail(error));
  }
}
function* watchActivityTypeById() {
  yield takeLatest(Actions.ACTIVITY_TYPE_ID_REQUEST, activityTypeByIdRequest);
}

export default function* activityTypeById() {
  yield all([fork(watchActivityTypeById)]);
}