import { takeLatest, all, fork, put, delay, call } from "redux-saga/effects";
import Actions from "actions";
import { preferenceService } from "../../../services";

function* preferenceRequest() {
  try {
    const { data } = yield call(preferenceService.getPreferences);
    console.log("data get preferenceRequest", data);
    yield put(Actions.preferenceSuccess(data.data));
  } catch (error) {
    console.log("error get preferenceRequest", error);
    yield put(Actions.preferenceFail(error));
  }
}
function* watchPreference() {
  yield takeLatest(Actions.PREFERENCE_REQUEST, preferenceRequest);
}

export default function* preference() {
  yield all([fork(watchPreference)]);
}
