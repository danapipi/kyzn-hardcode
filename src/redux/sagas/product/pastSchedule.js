import { takeLatest, all, fork, put, delay, call } from "redux-saga/effects";
import Actions from "actions";
import { activityService } from "../../../services";

function* pastScheduleRequest({ userID }) {
  try {
    const { data } = yield call(
      activityService.getPastSchedule,
      userID
    );
    console.log("PAST SCHEDULE", userID)
    console.log("PAST SCHEDULE", data.data)
    yield put(Actions.pastScheduleSuccess(data.data));
  } catch (error) {
    console.log("error getUpcomingSchedule", error);
    yield put(Actions.pastScheduleFail(error));
  }
}
function* watchPastSchedule() {
  yield takeLatest(Actions.PAST_SCHEDULE_REQUEST, pastScheduleRequest);
}

export default function* pastSchedule() {
  yield all([fork(watchPastSchedule)]);
}
