import {takeLatest, all, fork, put, delay, call} from 'redux-saga/effects';
import Actions from 'actions';
import { activityService } from "../../../services"

function* featuredActivityRequest() {
  try {
    const { data } = yield call(activityService.getFeaturedActivity)
    console.log("data get featuredActivity", data)
    yield put(Actions.featuredActivitySuccess(data.data))
  } catch (error) {
    console.log("error get featuredActivity", error)
    yield put(Actions.featuredActivityFail(error));
  }
}
function* watchFeaturedActivity() {
  yield takeLatest(Actions.FEATURED_ACTIVITY_REQUEST, featuredActivityRequest);
}

export default function* featuredActivity() {
  yield all([fork(watchFeaturedActivity)]);
}
