import { takeLatest, all, fork, put, delay, call } from "redux-saga/effects";
import Actions from "actions";
import { activityService } from "../../../services";

function* activityBookRequest({ activityScheduleId, userId }) {
  try {
    console.log("ABREQUEST", activityScheduleId, userId)
    const { data } = yield call(activityService.postActivityBooking, {
      activityScheduleId, userId
    });
    console.log("activityBookRequest res", data);
    yield put(Actions.activityBookingSuccess(data.data));
  } catch (error) {
    console.log("activityBookRequest error", error.response.data.message);
    yield put(Actions.activityBookingFail(error.response.data.message));
  }
}
function* watchActivityBook() {
  yield takeLatest(Actions.ACTIVITY_BOOK_REQUEST, activityBookRequest);
}

export default function* activityBook() {
  yield all([fork(watchActivityBook)]);
}
