import { takeLatest, all, fork, put, delay, call } from "redux-saga/effects";
import Actions from "actions";
import { activityService } from "../../../services";

function* activityScheduleRequest({ activity_id, userId }) {
  try {
    const { data } = yield call(
      activityService.getActivitySchedule,
      activity_id,
      userId
    );
    yield put(Actions.activityScheduleSuccess(data.data));
  } catch (error) {
    console.log("error getActivitySchedule", error);
    yield put(Actions.activityScheduleFail(error));
  }
}
function* watchActivitySchedule() {
  yield takeLatest(Actions.ACTIVITY_SCHEDULE_REQUEST, activityScheduleRequest);
}

export default function* activitySchedule() {
  yield all([fork(watchActivitySchedule)]);
}
