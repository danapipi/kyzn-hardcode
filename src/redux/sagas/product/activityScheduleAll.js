import { takeLatest, all, fork, put, delay, call } from "redux-saga/effects";
import Actions from "actions";
import { activityService } from "../../../services";

function* activityScheduleAllRequest({userId}) {
  try {
    const { data } = yield call(
      activityService.getAllActivitySchedule,
      userId
    );
    yield put(Actions.activityScheduleAllSuccess(data.data));
  } catch (error) {
    console.log("error getActivitySchedule", error);
    yield put(Actions.activityScheduleAllFail(error));
  }
}
function* watchActivityScheduleAll() {
  yield takeLatest(Actions.ACTIVITY_SCHEDULE_ALL_REQUEST, activityScheduleAllRequest);
}

export default function* activityScheduleAll() {
  yield all([fork(watchActivityScheduleAll)]);
}
