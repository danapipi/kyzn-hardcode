import { takeLatest, all, fork, put, delay, call } from "redux-saga/effects";
import Actions from "actions";
import { activityService } from "../../../services";

function* activityTypeRequest({ categoryChildId }) {
  try {
    const { data } = yield call(
      activityService.getActivityType,
      categoryChildId
    );
    yield put(Actions.activityTypeSuccess(data.data));
  } catch (error) {
    console.log("error getActivityType", error);
    yield put(Actions.activityTypeFail(error));
  }
}
function* watchActivityType() {
  yield takeLatest(Actions.ACTIVITY_TYPE_REQUEST, activityTypeRequest);
}

export default function* activityType() {
  yield all([fork(watchActivityType)]);
}
