import { takeLatest, all, fork, put, delay, call } from "redux-saga/effects";
import Actions from "actions";
import { activityService } from "../../../services";

function* upcomingScheduleRequest({ userID }) {
  try {
    const { data } = yield call(
      activityService.getUpcomingSchedule,
      userID
    );
    yield put(Actions.upcomingScheduleSuccess(data.data));
  } catch (error) {
    console.log("error getUpcomingSchedule", error);
    yield put(Actions.upcomingScheduleFail(error));
  }
}
function* watchUpcomingSchedule() {
  yield takeLatest(Actions.UPCOMING_SCHEDULE_REQUEST, upcomingScheduleRequest);
}

export default function* upcomingSchedule() {
  yield all([fork(watchUpcomingSchedule)]);
}
