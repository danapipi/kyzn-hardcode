import {all, fork} from 'redux-saga/effects';
import category from './category';
import categoryChild from './categoryChild'
import activity from './activity'
import activityType from './activityType'
import activityTypeById from './activityTypeById'
import activitySchedule from './activitySchedule'
import activityScheduleAll from './activityScheduleAll'
import activityBook from './activityBook';
import upcomingSchedule from './upcomingSchedule';
import pastSchedule from './pastSchedule';
import featuredActivity from './featuredActivity'
import featuredMembership from './featuredMembership'
import activityById from './activityById'
import preference from './preference';
import createPreference from './createPreference';

export default function* categorySaga() {
    yield all([fork(category)])
    yield all([fork(categoryChild)])
    yield all([fork(activity)])
    yield all([fork(activityType)])
    yield all([fork(activitySchedule)])
    yield all([fork(activityBook)])
    yield all([fork(upcomingSchedule)])
    yield all([fork(pastSchedule)])
    yield all([fork(featuredActivity)])
    yield all([fork(featuredMembership)])
    yield all([fork(activityById)])
    yield all([fork(activityScheduleAll)])
    yield all([fork(preference)])
    yield all([fork(createPreference)])
    yield all([fork(activityTypeById)])
}