import {takeLatest, all, fork, put, delay, call} from 'redux-saga/effects';
import Actions from 'actions';
import { activityService } from "../../../services"

function* featuredMembershipRequest() {
  try {
    const { data } = yield call(activityService.getFeaturedMembership)
    console.log("data get featuredMembership", data)
    yield put(Actions.featuredMembershipSuccess(data.data))
  } catch (error) {
    console.log("error get featuredMembership", error)
    yield put(Actions.featuredMembershipFail(error));
  }
}
function* watchFeaturedMembership() {
  yield takeLatest(Actions.FEATURED_MEMBERSHIP_REQUEST, featuredMembershipRequest);
}

export default function* featuredMembership() {
  yield all([fork(watchFeaturedMembership)]);
}
