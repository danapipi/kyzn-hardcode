import {takeLatest, all, fork, put, delay, call} from 'redux-saga/effects';
import Actions from 'actions';
import { categoryService } from "../../../services"

function* categoryRequest() {
  try {
    const { data } = yield call(categoryService.getCategory)
    console.log("data getCategory", data)
    yield put(Actions.categorySuccess(data.data))
  } catch (error) {
    console.log("error getCategory", error)
    yield put(Actions.categoryFail(error));
  }
}
function* watchCategory() {
  yield takeLatest(Actions.CATEGORY_REQUEST, categoryRequest);
}

export default function* category() {
  yield all([fork(watchCategory)]);
}
