import {takeLatest, all, fork, put, delay, call} from 'redux-saga/effects';
import Actions from 'actions';
import { categoryService } from "../../../services"

function* categoryChildRequest({ id  }) {
  try {
    const { data } = yield call(categoryService.getCategoryChild, id)
    yield put(Actions.categoryChildSuccess(data.data))
  } catch (error) {
    console.log("error getCategoryChild", error)
    yield put(Actions.categoryChildFail(error));
  }
}
function* watchCategoryChild() {
  yield takeLatest(Actions.CATEGORY_CHILD_REQUEST, categoryChildRequest);
}

export default function* categoryChild() {
  yield all([fork(watchCategoryChild)]);
}
