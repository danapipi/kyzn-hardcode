import { takeLatest, all, fork, put, delay, call } from "redux-saga/effects";
import Actions from "actions";
import { preferenceService } from "../../../services";

function* createPreferenceRequest(payload) {
  try {
    const { data } = yield call(preferenceService.createPreference, payload);
    console.log("data get createPreferenceRequest", data);
    yield put(Actions.createPreferenceSuccess(data.data));
  } catch (error) {
    console.log("error get createPreferenceRequest", error);
    yield put(Actions.createPreferenceFail(error));
  }
}
function* watchCreatePreference() {
  yield takeLatest(Actions.CREATE_PREFERENCE_REQUEST, createPreferenceRequest);
} 

export default function* createPreference() {
  yield all([fork(watchCreatePreference)]);
}
