import { takeLatest, all, fork, put, delay, call } from "redux-saga/effects";
import Actions from "actions";
import { activityService } from "../../../services";

function* activityRequest({ activityTypeId, categoryId, categoryChildId }) {
  try {
    const { data } = yield call(
      activityService.getActivity,
      activityTypeId,
      categoryId,
      categoryChildId
    );
    yield put(Actions.activitySuccess(data.data));
  } catch (error) {
    console.log("error getActivity", error);
    yield put(Actions.activityFail(error));
  }
}
function* watchActivity() {
  yield takeLatest(Actions.ACTIVITY_REQUEST, activityRequest);
}

export default function* activity() {
  yield all([fork(watchActivity)]);
}
