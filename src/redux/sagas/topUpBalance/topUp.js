import { takeLatest, all, fork, put, delay, call } from "redux-saga/effects";
import Actions from "actions";
import { topUpService } from "../../../services";
import { navigate } from "navigators/app/NavigationService"

function* topUpRequest({ payload: { amount, userId } }) {
  try {
    console.log("TOPUPRQUEST", amount, userId)
    const { data } = yield call(topUpService.deposit, {
      amount: amount, userId: userId
    });
    console.log("topUpRequest res", data);
    yield put(Actions.topupSuccess(data.message));
    console.log("navigation", navigate)
    // yield put(Actions.profile());
    yield put(Actions.topupHistory());
  } catch (error) {
    console.log("topUpRequest error", error.response.data.message);
    yield put(Actions.topupFail(error.response.data.message));
  }
}
function* watchTopUp() {
  yield takeLatest(Actions.TOP_UP_REQUEST, topUpRequest);
}

export default function* topUp() {
  yield all([fork(watchTopUp)]);
}
