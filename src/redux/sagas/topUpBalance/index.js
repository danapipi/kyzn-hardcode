import {all, fork} from 'redux-saga/effects';
import topUp from './topUp';
import topUpHistory from './topUpHistory'

export default function* auth() {
  yield all([fork(topUp)]);
  yield all([fork(topUpHistory)]);
}
