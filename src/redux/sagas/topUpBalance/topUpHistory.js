import { takeLatest, all, fork, put, delay, call } from "redux-saga/effects";
import Actions from "actions";
import { topUpService } from "../../../services";

function* topUpHistoryRequest({ limit, offset }) {
  try {
    const { data } = yield call(
        topUpService.depositHistory,
      limit,
      offset
    );
    console.log("data topUpHistoryRequest", data.data);
    yield put(Actions.topupHistorySuccess(data.data));
  } catch (error) {
    console.log("error getTopUpHistory", error);
    yield put(Actions.topupHistoryFail(error));
  }
}
function* watchtopUpHistory() {
  yield takeLatest(Actions.TOP_UP_HISTORY_REQUEST, topUpHistoryRequest);
}

export default function* topUpHistory() {
  yield all([fork(watchtopUpHistory)]);
}
