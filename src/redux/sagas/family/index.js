import { all as reduxAll, fork } from "redux-saga/effects";
import user from "./user";
import all from "./all";
import addChild from "./addChild";
import uniqueFamily from './uniqueFamily';

export default function* family() {
  yield reduxAll([fork(user)]);
  yield reduxAll([fork(all)]);
  yield reduxAll([fork(addChild)]);
  yield reduxAll([fork(uniqueFamily)]);
}
