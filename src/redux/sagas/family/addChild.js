import {
  takeLatest,
  all as reduxAll,
  fork,
  put,
  call,
} from "redux-saga/effects";
import Actions from "actions";
import { familyService } from "services";

function* addChild({ payload, successCallback, failCallback }) {
  try {
    console.log(payload);
    const { status, data } = yield call(familyService.addChild, payload);
    if (status !== 200) {
      throw new Error(`Response failed with status ${status}`);
    }
    yield put(Actions.addChildSuccess(data?.data));
    if (successCallback) {
      successCallback();
    }
  } catch (error) {
    yield put(Actions.addChildFail(error.message));
    if (failCallback) {
      failCallback();
    }
  }
}
function* watchAddChild() {
  yield takeLatest(Actions.ADD_CHILD_REQUEST, addChild);
}

export default function* all() {
  yield reduxAll([fork(watchAddChild)]);
}
