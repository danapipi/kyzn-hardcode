import {
  takeLatest,
  all as reduxAll,
  fork,
  put,
  call,
} from "redux-saga/effects";
import Actions from "actions";
import { familyService } from "services";

const defaultFilters = {
  role: "all",
};

function* fetchAllFamilies({ filters = defaultFilters }) {
  try {
    const { status, data } = yield call(familyService.getAllFamilies, filters);
    if (status !== 200) {
      throw new Error(`Response failed with status ${status}`);
    }
    yield put(Actions.fetchAllFamiliesSuccess(data?.data));
  } catch (error) {
    yield put(Actions.fetchAllFamiliesFail(error.message));
  }
}
function* watchFetchAllFamilies() {
  yield takeLatest(Actions.FETCH_ALL_FAMILIES_REQUEST, fetchAllFamilies);
}

// Refresh when new child is added
function* watchAddChild() {
  yield takeLatest(Actions.ADD_CHILD_SUCCESS, fetchAllFamilies);
}

export default function* all() {
  yield reduxAll([fork(watchFetchAllFamilies)]);
  yield reduxAll([fork(watchAddChild)]);
}
