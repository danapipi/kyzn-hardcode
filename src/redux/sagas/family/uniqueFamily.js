import { takeLatest, all, fork, put, call, select } from "redux-saga/effects";
import Actions from "actions";
import { familyService } from "services";
import _ from "lodash";
import Selectors from "selectors";

Array.prototype.move = function (from, to) {
  this.splice(to, 0, this.splice(from, 1)[0]);
};

const defaultRole = {
  role: "all",
};

function* fetchUniqueFamily({ role = defaultRole }) {
  try {
    console.log("SAGA ROLE: ", role);
    // const id = 2;
    const { status, data } = yield call(familyService.getUniqueFamily, role);
    console.log("DATA SAGA: ", data);
    let profile = yield select(Selectors.getProfile);
    if (status !== 200) {
      throw new Error(`Response failed with status ${status}`);
    }
    if (data.data && profile) {
      let selfIndex = _.findIndex(
        data.data,
        e => {
          return e.id === profile.id;
        },
        0,
      );
      console.log(selfIndex, "Selfindex");
      console.log(profile.id, "Selfindex profile");
      if (selfIndex > 0) {
        let newdata = _.uniq([...[data.data[selfIndex]], ...data.data]);
        yield put(Actions.fetchFamilyUniqueSuccess(newdata));
      } else {
        yield put(Actions.fetchFamilyUniqueSuccess(data.data));
      }
    }
  } catch (error) {
    yield put(Actions.fetchFamilyUniqueFail(error.response.data));
  }
}
function* watchFetchUniqueFamily() {
  yield takeLatest(Actions.FETCH_FAMILY_UNIQUE_REQUEST, fetchUniqueFamily);
}

export default function* uniqueFamily() {
  yield all([fork(watchFetchUniqueFamily)]);
}
