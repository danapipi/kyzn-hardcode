import { takeLatest, all, fork, put, call } from "redux-saga/effects";
import Actions from "actions";
import { familyService } from "services";

function* fetchUserFamily({ id }) {
  try {
    // const id = 2;
    const { status, data } = yield call(familyService.getFamily, id);
    if (status !== 200) {
      throw new Error(`Response failed with status ${status}`);
    }
    yield put(Actions.fetchUserFamilySuccess(data?.data));
  } catch (error) {
    yield put(Actions.fetchUserFamilyFail(error.message));
  }
}
function* watchFetchUserFamily() {
  yield takeLatest(Actions.FETCH_USER_FAMILY_REQUEST, fetchUserFamily);
}

export default function* user() {
  yield all([fork(watchFetchUserFamily)]);
}
