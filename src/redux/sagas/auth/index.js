import {all, fork} from 'redux-saga/effects';
import login from './login';
import register from './register';
import profile from './profile';
import updateProfile from './updateProfile'
import updateProfileChild from './updateProfileChild'
import loginUsername from './loginUsername';
import userMembershipPackage from './userMembershipPackage';

export default function* auth() {
  yield all([fork(login)]);
  yield all([fork(register)]);
  yield all([fork(profile)]);
  yield all([fork(updateProfile)]);
  yield all([fork(updateProfileChild)]);
  yield all([fork(loginUsername)]);
  yield all([fork(userMembershipPackage)]);
}
