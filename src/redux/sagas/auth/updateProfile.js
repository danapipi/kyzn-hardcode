import { takeLatest, all, fork, put, delay, call } from "redux-saga/effects";
import Actions from "actions";
import { authService } from "../../../services";
import { UserApi } from "../../../utils/Api";

function* updateProfileRequest({ payload }) {
  try {
    const { data } = yield call(authService.updateProfile, payload);
    console.log("data updateProfileRequest", data);
    // TODO Fix this return variable name lol
    yield put(Actions.updateProfileSuccess(data));
    yield put(Actions.profileSuccess(data.data));
  } catch (error) {
    console.log("error updateProfileRequest", error);
    yield put(Actions.updateProfileFail(error));
  }
}
function* watchProfile() {
  yield takeLatest(Actions.UPDATE_PROFILE_REQUEST, updateProfileRequest);
}

export default function* updateProfile() {
  yield all([fork(watchProfile)]);
}
