import { takeLatest, all, fork, put, delay, call } from "redux-saga/effects";
import Actions from "actions";
import { authService } from "../../../services";


function* userMembershipRequest({ userId }) {
  try {
    console.log("blahblabhlabha", userId);
    const { data } = yield call(authService.userMembershipPackage, userId);
    console.log("data getUserMembership", data);
    // TODO Fix this return variable name lol
    yield put(Actions.userMembershipPackageSuccess(data.data));
  } catch (error) {
    console.log("error getUserMembership", error);
    yield put(Actions.userMembershipPackageFail(error));
  }
}
function* watchUserMembershipPackage() {
  yield takeLatest(Actions.USER_MEMBERSHIP_PACKAGE_REQUEST, userMembershipRequest);
}

export default function* userMembershipPackage() {
  yield all([fork(watchUserMembershipPackage)]);
}
