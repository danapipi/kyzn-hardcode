import { takeLatest, all, fork, put, delay, call } from "redux-saga/effects";
import Actions from "actions";
import { authService } from "../../../services";
import { UserApi } from "../../../utils/Api";

function* updateProfileChildRequest({ payload }) {
  try {
    const { data } = yield call(authService.updateProfile, payload);
    console.log("data updateProfileChildRequest", data);
    // TODO Fix this return variable name lol
    yield put(Actions.updateProfileChildSuccess(data));
  } catch (error) {
    console.log("error updateProfileChildRequest", error);
    yield put(Actions.updateProfileChildFail(error));
  }
}
function* watchProfile() {
  yield takeLatest(Actions.UPDATE_PROFILE_CHILD_REQUEST, updateProfileChildRequest);
}

export default function* updateProfile() {
  yield all([fork(watchProfile)]);
}
