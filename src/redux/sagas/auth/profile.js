import { takeLatest, all, fork, put, delay, call } from "redux-saga/effects";
import Actions from "actions";
import { authService } from "../../../services";

function* profileRequest() {
  try {
    const { data } = yield call(authService.profile);
    console.log("data getProfile", data);
    // TODO Fix this return variable name lol
    // yield put(Actions.profileSuccess(data.date));
  } catch (error) {
    console.log("error getProfile", error);
    yield put(Actions.profileFail(error));
  }
}
function* watchProfile() {
  yield takeLatest(Actions.PROFILE_REQUEST, profileRequest);
  // yield takeLatest(Actions.LOGIN_SUCCESS, profileRequest);
}

export default function* profile() {
  yield all([fork(watchProfile)]);
}
