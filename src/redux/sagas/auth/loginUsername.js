import { takeLatest, all, fork, put, delay, call } from "redux-saga/effects";
import Actions from "actions";
import { authService } from "../../../services";
import { UserApi } from "utils/Api";
import RNStorage from "../../../utils/storage";

function* loginUsernameRequest({ payload: { username, password } }) {
  try {
    // yield delay(3000);
    const { data } = yield call(authService.loginUserName, {
      username,
      password,
    });
    console.log("loginUsernameRequest res", data);
    yield all([UserApi.setHeaderToken(data.token)]);
    yield call(RNStorage.setToken, data.token);
    // yield call(UserApi.setHeaderToken, data.token)
    yield put(Actions.loginSuccess(data.token));
  } catch (error) {
    console.log("loginUsernameRequest error", error.response.data.message);
    yield put(Actions.loginUsernameFail(error.response.data.message));
  }
}
function* watchLoginUsername() {
  yield takeLatest(Actions.LOGIN_USERNAME_REQUEST, loginUsernameRequest);
}

export default function* login() {
  yield all([fork(watchLoginUsername)]);
}
