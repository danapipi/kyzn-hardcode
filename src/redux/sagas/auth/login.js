import { takeLatest, all, fork, put, delay, call } from "redux-saga/effects";
import Actions from "actions";
import { authService } from "../../../services";
import { UserApi } from "utils/Api";
import RNStorage from "../../../utils/storage";

function* loginRequest({ payload: { phoneNumber, password } }) {
  try {
    // yield delay(3000);
    const { data } = yield call(authService.login, phoneNumber, password);
    console.log("loginRequest res", data);
    yield all([UserApi.setHeaderToken(data.token)]);
    yield call(RNStorage.setToken, data.token);
    // yield call(UserApi.setHeaderToken, data.token)
    yield put(Actions.loginSuccess(data.token));
  } catch (error) {
    console.log(
      "loginRequest error",
      error.response.data.message
    );
    yield put(Actions.loginFail(error.response.data.message));
  }
}
function* watchLogin() {
  yield takeLatest(Actions.LOGIN_REQUEST, loginRequest);
}

export default function* login() {
  yield all([fork(watchLogin)]);
}
