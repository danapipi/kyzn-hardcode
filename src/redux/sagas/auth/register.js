import { takeLatest, all, fork, put, delay, call } from "redux-saga/effects";
import Actions from "actions";
import { authService } from "../../../services";
import { useDispatch } from "react-redux";
import { UserApi } from "utils/Api";

function* registerRequest({
  payload: {
    phoneNumber,
    password,
    confPassword,
    name,
    gender,
    nationality,
    birthdate,
    postalCode = "",
    address = "",
    province = "",
    district = "",
    city = "",
    username = ""
  },
}) {
  try {
    const { data } = yield call(authService.register, {
      phoneNumber,
      password,
      confPassword,
      name,
      gender,
      nationality,
      birthdate,
      postalCode,
      address,
      province,
      district,
      city,
      username
    });
    console.log("registerRequest res", data);
    yield all([UserApi.setHeaderToken(data.token)]);
    // yield put(Actions.registerSuccess(data));
    yield put(Actions.loginSuccess(data.token));
    // if (data.success) {
    //   const { dataLogin } = yield call(authService.login, {
    //     phoneNumber,
    //     password,
    //   });
    //   console.log("loginRequest res", dataLogin);
    //   yield put(Actions.loginSuccess(dataLogin.token));
    // }
  } catch (error) {
    console.log("registerRequest error", error.response.data.message);
    yield put(Actions.registerFail(error.response.data.message));
  }
}

function* watchRegister() {
  yield takeLatest(Actions.REGISTER_REQUEST, registerRequest);
}

export default function* register() {
  yield all([fork(watchRegister)]);
}
