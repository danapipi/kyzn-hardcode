import { all as reduxAll, fork } from "redux-saga/effects";
import all from "./all";

export default function* inbox() {
  yield reduxAll([fork(all)]);
}
