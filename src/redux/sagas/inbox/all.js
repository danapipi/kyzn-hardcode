import {
  takeLatest,
  all as reduxAll,
  fork,
  put,
  call,
} from "redux-saga/effects";
import Actions from "actions";
import { inboxService } from "services";


function* fetchInbox({ userId }) {
  try {
    const { status, data } = yield call(inboxService.getInbox, userId);
    if (status !== 200) {
      throw new Error(`Response failed with status ${status}`);
    }
    yield put(Actions.fetchInboxSuccess(data?.data));
  } catch (error) {
    yield put(Actions.fetchInboxFail(error.response.data));
  }
}
function* watchFetchInbox() {
  yield takeLatest(Actions.FETCH_INBOX_REQUEST, fetchInbox);
}

export default function* all() {
  yield reduxAll([fork(watchFetchInbox)]);
}
