import { all, fork } from "redux-saga/effects";
import auth from "./auth";
import product from "./product";
import topUp from "./topUpBalance";
import membership from "./membership";
import family from "./family";
import inbox from './inbox';
import worker from './worker';

export default function* root() {
  yield all([fork(auth)]);
  yield all([fork(product)]);
  yield all([fork(topUp)]);
  yield all([fork(membership)]);
  yield all([fork(family)]);
  yield all([fork(inbox)]);
  yield all([fork(worker)]);
}
