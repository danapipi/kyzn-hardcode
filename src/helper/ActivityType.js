export default class ActivityType {

	static RENT_COUNT:number = 1;
  static GROUP_CLASS: number = 2;
  static PRIVATE_TRAINING: number = 6; 
  static PANORAMA:number = 7;

}