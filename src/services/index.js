import { Platform } from "react-native";
import { UserApi, UserApiFormData } from "../utils/Api";

const user = UserApi.client;
const userFormData = UserApiFormData.client;

export const utils = {
  getConfig: () => user.get("/config"),
};

export const preferenceService = {
  getPreferences: () => user.get("/user-preference"),
  createPreference: ({ payload }) => {
    const params = new URLSearchParams();
    params.append("categoryChildIds", JSON.stringify(payload));
    return user.post("/user-preference/create", params, {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
    });
  },
};

export const categoryService = {
  getCategory: () => user.get("/category"),
  getCategoryChild: categoryId =>
    user.get(`/category-child?categoryId=${categoryId}`),
};

export const activityService = {
  getActivity: (activityTypeId, categoryId, categoryChildId) => {
    if (categoryId == null) {
      return user.get(`activity?activityTypeId=${activityTypeId}`);
    } else {
      return user.get(
        `activity?activityTypeId=${activityTypeId}&categoryId=${categoryId}&categoryChildId=${categoryChildId}`,
      );
    }
  },
  getActivityType: categoryChildId =>
    user.get(`/activity-type?categoryChildId=${categoryChildId}`),
  getActivitySchedule: (activity_id, userId) =>
    user.get(`/activity-schedule?activity_id=${activity_id}&userId=${userId}`),
  getAllActivitySchedule: userId =>
    user.get(`/activity-schedule?userId=${userId}`),
  getAllActivityScheduleWithoutUserId: () => user.get("/activity-schedule"),
  postActivityBooking: ({ activityScheduleId, userId }) =>
    user.post("activity-booking", { activityScheduleId, userId }),
  getUpcomingSchedule: userID =>
    user.get(`/activity-booking/schedule?filterBy=upcoming&userId=${userID}`),
  getPastSchedule: userID =>
    user.get(`/activity-booking/schedule?filterBy=past&userId=${userID}`),
  getFeaturedActivity: () => user.get("featured-activity"),
  getFeaturedMembership: () => user.get("featured-membership"),
  getActivityById: (
    activityTypeId,
    activityId,
    date,
    stepMultiplier,
    userId,
  ) => {
    if (date == null) {
      return user.get(`activity/${activityTypeId}?userId=${userId}`);
    } else {
      return user.get(
        `activity/${activityTypeId}?date=${date}&stepMultiplier=${stepMultiplier}&userId=${userId}`,
      );
    }
  },
};

export const authService = {
  login: ({ phoneNumber, password }) =>
    user.post("/auth/login", { phoneNumber, password }),
  loginUserName: ({ username, password }) =>
    user.post("/auth/login-username", { username, password }),
  userMembershipPackage: userId =>
    user.get(`/user-membership-package/${userId}`),
  register: ({
    phoneNumber,
    password,
    confPassword,
    name,
    gender,
    nationality,
    birthdate,
    postalCode,
    address,
    province,
    district,
    city,
    username,
  }) =>
    user.post("/auth/register", {
      phoneNumber,
      password,
      confPassword,
      name,
      gender,
      nationality,
      birthdate,
      postalCode,
      address,
      province,
      district,
      city,
      username,
    }),
  registerUserName: ({}) => user.post("/auth/register-username", {}),
  profile: () => user.get("/auth/me"),
  updateProfile: payload => {
    console.log("auth Service ", payload);
    let formData = new FormData();
    formData.append("name", payload.name);
    formData.append("birthdate", payload.birthdate);
    formData.append("gender", payload.gender);
    // formData.append('address', payload.address);
    formData.append("nationality", payload.nationality);
    // formData.append('postalCode', payload.postalCode);
    // formData.append('province', payload.province);
    // formData.append('district', payload.district);
    // formData.append('city', city);
    if (payload.photo) {
      formData.append("photo", {
        uri:
          Platform.OS === "android"
            ? payload.photo.uri
            : "file://" + payload.photo.uri,
        name: payload.name,
        type: "image/jpeg",
      });
    }
    console.log("auth Service formData", formData);
    return user.put("/auth", formData);
  },
  updateProfileChild: payload => {
    console.log("auth Service ", payload);
    let formData = new FormData();
    formData.append("name", payload.name);
    formData.append("birthdate", payload.birthdate);
    formData.append("gender", payload.gender);
    // formData.append('address', payload.address);
    formData.append("nationality", payload.nationality);
    // formData.append('postalCode', payload.postalCode);
    // formData.append('province', payload.province);
    // formData.append('district', payload.district);
    // formData.append('city', city);
    if (payload.photo) {
      formData.append("photo", {
        uri:
          Platform.OS === "android"
            ? payload.photo.uri
            : "file://" + payload.photo.uri,
        name: payload.name,
        type: "image/jpeg",
      });
    }
    console.log("auth Service formData", formData);
    return user.put(`/family/edit-child/${payload.id}`, formData);
  },
  changePassword: ({ oldPassword, newPassword }) =>
    user.put("/auth/change-password", { oldPassword, newPassword }),
};

export const topUpService = {
  deposit: ({ amount, userId }) =>
    user.post("user-balance/deposit", { amount, userId }),
  depositHistory: (limit, offset) =>
    user.get(`user-balance-log?limit=${50}&offset=${offset}`),
};

export const membershipService = {
  getMembershipTypes: () => user.get("/membership-type"),
  getMembershipCategories: () => user.get("/membership-category"),
  getMembershipPackages: () => user.get("/membership-package-category"),
  makeMembershipPayment: payload => user.post("/membership-payment", payload),
  makeMembershipPackagePayment: payload =>
    user.post("/membership-package-payment", payload),
  getMembershipPackagesById: id => user.get(`/membership-package/${id}`),
};

export const familyService = {
  addChild: payload => user.post("/family/add-child", payload),
  getAllFamilies: ({ role }) => user.get(`/family/?role=${role}`),
  getFamily: id => {
    if (id !== undefined) {
      return user.get(`/family/${id}`);
    } else {
      return user.get("/family");
    }
  },
  getUniqueFamily: ({ role }) => user.get(`/family/unique?role=${role}`),
};

export const inboxService = {
  getInbox: userId => user.get(`/user-inbox?userId=${userId}`),
};

export const workerService = {
  getFeaturedWorker: () => user.get("/featured-worker"),
};
