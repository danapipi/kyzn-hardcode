import { extendTheme } from "native-base";

const colors = {
  white: "#FFFFFF",
  green: "green",
  transparent: "transparent",
  opaque: "#00000070",
  black: "#212121",
  gray: "#8D97A6",
  button: {
    blue: "#1035BB",
    black: "#212121",
    orange: "#E9BD4A",
    lightOrange: "#E2C985",
  },
  lightGreen: "#A4D3AE",
  platinumGreen: "#A5CDC8",
  blue: "#0E1BB9",
};

const fontConfig = {
  Montserrat: {
    100: {
      normal: "Montserrat-Regular",
    },
    200: {
      normal: "Montserrat-Semibold",
    },
    300: {
      normal: "Montserrat-Bold",
    },
  },
};

const fonts = {
  heading: "Montserrat",
  body: "Montserrat",
  mono: "Montserrat",
};

const components = {
  Text: {
    bold: { fontWeight: 300 }
  },
};

const theme = extendTheme({ colors, fontConfig, fonts, components });

export default theme;
