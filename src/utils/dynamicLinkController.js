/* eslint-disable react-hooks/rules-of-hooks */
import React from "react";
import { Linking } from "react-native";
import dynamicLinks from "@react-native-firebase/dynamic-links";
import axios from "axios";
import { useSelector } from "react-redux";
import Selectors from "selectors";
import NavigationService from "../navigators/app/NavigationService";

const dynamicLinkGenerator =
  "https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=AIzaSyCV5WJi94zL0b6dhdnbaGjFcEuCepQho04";

// just dummy
const referalCodeDummy = "rc_12345";

const dynamicLinkController = props => {
  console.log("DynamicLinkController Props: ", props);
  console.log("NavigationService: ", NavigationService);

  const token = useSelector(Selectors.getToken);
  console.log("Token: ", token);

  React.useEffect(() => {
    // Bg listener
    backgroundListenerDynamicLink();

    // foreground listener deeplinking
    const unsubscribe = dynamicLinks().onLink(foregroundListenerDynamicLink);

    buildDynamicLinkDummy();

    // unsubscribe
    // When the component is unmounted, remove the listener
    return () => {
      unsubscribe();
    };
  }, []);

  // just generate to test
  const buildDynamicLinkDummy = async () => {
    try {
      const params = {
        dynamicLinkInfo: {
          domainUriPrefix: "https://kyzn.page.link",
          link: `https://kyzn.page.link/app/?link=kyzn://referal?code=${referalCodeDummy}`,
          androidInfo: {
            androidPackageName: "com.kyzn",
          },
          iosInfo: {
            iosBundleId: "sg.com.lovenest.customer",
            iosAppStoreId: "1463134198",
          },
        },
      };

      const config = {
        headers: {
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": "*",
        },
      };

      const generator = await axios?.post(dynamicLinkGenerator, params, config);
      console.log("generator: ", generator);
      if (generator?.data?.shortLink) {
        return generator?.data?.shortLink;
      } else {
        return null;
      }
    } catch (error) {
      console.log("Error Create Dynamic Link: ", error);
    }
  };

  // foreground handler
  const foregroundListenerDynamicLink = link => {
    try {
      navigator(link);
    } catch (error) {
      console.log("foregroundListenerDynamicLink: ", error);
    }
  };

  // background listener
  const backgroundListenerDynamicLink = () => {
    try {
      Linking?.addEventListener("url", ({ url }) => {
        console.log("BG Process: ", url);
        if (url) {
          dynamicLinkNavigateBackground(url);
        }
      });
    } catch (error) {
      console.log("Error background dynamic link listener: ", error);
    }
  };

  // if any url will be navigate from background or kill state of app
  const dynamicLinkNavigateBackground = link => {
    try {
      navigator(link);
    } catch (error) {
      console.log("Error dynamicLinkNavigateBackground: ", error);
    }
  };

  // navigator for url
  const navigator = link => {
    try {
      console.log("Navigator url: ", link);
      if (link) {
        const trimed = link?.url
          ? link?.url.replace("https://kyzn.page.link/app/?link=")
          : link?.url?.url.replace("https://kyzn.page.link/app/?link=");

        const removeUndefined = trimed?.replace("undefined", "");

        const removeSpecialChar = removeUndefined?.replace("%3D", "=");

        const finalCleanUp = removeSpecialChar?.replace(
          "kyzn://referal?code=",
          "",
        );

        if (removeSpecialChar) {
          if (token) {
            //  user already login
            Linking.canOpenURL(
              `kyzn://BottomTabs/Home?code=${finalCleanUp}`,
            ).then(supported => {
              if (supported) {
                Linking.openURL(`kyzn://BottomTabs/Home?code=${finalCleanUp}`);
              } else {
                console.log("Don't know how to open URI: " + removeSpecialChar);
              }
            });
          } else {
            // when new user coming
            Linking.canOpenURL(`kyzn://SignUp?code=${finalCleanUp}`).then(
              supported => {
                if (supported) {
                  Linking.openURL(`kyzn://SignUp?code=${finalCleanUp}`);
                } else {
                  console.log(
                    "Don't know how to open URI: " + removeSpecialChar,
                  );
                }
              },
            );
          }
        } else {
          // do nothing;
        }
      }
    } catch (error) {
      console.log("Error navigator: ", error);
    }
  };

  return null;
};

export default dynamicLinkController;
