import _ from 'lodash';

const Commons = {
    convertToRupiah(angka) {
        let rupiah = ''

        if(_.isUndefined(angka)){
          return '0'
        }

        const angkarev = angka
          .toString()
          .split('')
          .reverse()
          .join('')
    
        // eslint-disable-next-line no-plusplus
        for (let i = 0; i < angkarev.length; i++)
          if (i % 3 === 0) rupiah += `${angkarev.substr(i, 3)}.`
        return `$ ${rupiah
          .split('', rupiah.length - 1)
          .reverse()
          .join('')}`
      },

      capitalized(word) {
        if(word) {
          const lower = word.toLowerCase();
          return word.charAt(0).toUpperCase() + lower.slice(1);
        }

        return ""
      }

      }

export default Commons