// eslint-disable-next-line no-unused-vars
import React, { useEffect } from "react";
import { Platform } from "react-native";
import PushNotificationIOS from "@react-native-community/push-notification-ios";
import PushNotification, { Importance } from "react-native-push-notification";
import NavigationService, {
  navigationAction,
} from "../navigators/app/NavigationService";
import messaging from "@react-native-firebase/messaging";

// for mode ID
// need put channel id to make pop up show on foreground
const GENERAL_NOTIFICATION_DEV_ID = "devmode";

const RemotePushController = props => {
  console.log("NavigationService: ", NavigationService);
  console.log("RemotePushController: ", props);
  useEffect(() => {
    if (Platform?.OS === "ios") {
      _config();
      _iOSRegisterFCMRemote();
      _iOSRequestPermission();
      _iOSGetFCMToken();
      _iOSInitialState();
      _iOSForegroundHandler();
    } else {
      _config();
      _iOSRequestPermission();
      _iOSGetFCMToken();
    }
  }, []);

  const _config = () => {
    PushNotification.configure({
      onRegister: function (token) {
        //  for Android t0ken
        console.log("TOKEN:", token?.token);
        PushNotification.createChannel(
          {
            channelId: GENERAL_NOTIFICATION_DEV_ID, // (required)
            channelName: GENERAL_NOTIFICATION_DEV_ID, // (required)
            channelDescription: "General Notification", // (optional) default: undefined.
            playSound: false, // (optional) default: true
            soundName: "default", // (optional) See `soundName` parameter of `localNotification` function
            importance: Importance.HIGH, // (optional) default: Importance.HIGH. Int value of the Android notification importance
            vibrate: true, // (optional) default: true. Creates the default vibration pattern if true.
          },
          created => console.log(`createChannel Common returned '${created}'`), // (optional) callback returns whether the channel was created, false means it already existed.
        );
      },

      onNotification: function (notification) {
        try {
          console.log("NOTIFICATION:", notification);
          console.log("NavigationService: ", NavigationService);

          if (notification?.foreground && !notification?.userInteraction) {
            PushNotification?.localNotification({
              channelId: GENERAL_NOTIFICATION_DEV_ID,
              title: notification?.title,
              message: notification?.message,
            });
          } else if (
            notification?.foreground &&
            notification?.userInteraction
          ) {
            // navigate and dont show local notif
            navigationAction("BottomTabs", {
              name: "BottomTabs",
              params: {
                screen: "Inbox",
                params: { id: "test" },
              },
            });
          } else if (
            !notification?.foreground &&
            notification?.userInteraction
          ) {
            // navigate from app killed
            navigationAction("BottomTabs", {
              name: "BottomTabs",
              params: {
                screen: "Inbox",
                params: { id: "test" },
              },
            });
          }

          // (required) Called when a remote is received or opened, or local notification is opened
          notification.finish(PushNotificationIOS.FetchResult.NoData);
        } catch (error) {
          //
        }
      },
      onAction: notification => {
        console.log("onAction: ", notification);
      },

      senderID: "your_fcm_sender_id_here",

      permissions: {
        alert: true,
        badge: true,
        sound: true,
      },

      popInitialNotification: true,

      requestPermissions: true,
    });
  };

  // get fcm token
  const _iOSGetFCMToken = () => {
    try {
      messaging()
        ?.getToken()
        .then(token => {
          console.log("IOS FCM Token: ", token);
          if (token) {
            // permission has granted
            PushNotification.createChannel(
              {
                channelId: GENERAL_NOTIFICATION_DEV_ID, // (required)
                channelName: GENERAL_NOTIFICATION_DEV_ID, // (required)
                channelDescription: "General Notification", // (optional) default: undefined.
                playSound: false, // (optional) default: true
                soundName: "default", // (optional) See `soundName` parameter of `localNotification` function
                importance: Importance.HIGH, // (optional) default: Importance.HIGH. Int value of the Android notification importance
                vibrate: true, // (optional) default: true. Creates the default vibration pattern if true.
              },
              created =>
                console.log(`createChannel Common returned '${created}'`), // (optional) callback returns whether the channel was created, false means it already existed.
            );
          }
        })
        .catch(error => {
          throw new Error(error);
        });
    } catch (error) {
      //
    }
  };

  // register IOS FCM
  const _iOSRegisterFCMRemote = () => {
    try {
      messaging().setAutoInitEnabled(true);
    } catch (error) {
      //
    }
  };

  // request permission IOS
  const _iOSRequestPermission = () => {
    try {
      messaging()
        ?.requestPermission()
        .then(granted => {
          console.log("FCM IOS Granted: ", granted);
        })
        .catch(error => {
          throw new Error(error);
        });
    } catch (error) {
      //
    }
  };

  // when app kill
  const _iOSInitialState = () => {
    try {
      messaging()
        ?.getInitialNotification()
        .then(notification => {
          console.log("_iOSInitialState: ", notification);
        })
        .catch(error => {
          throw new Error(error);
        });
    } catch (error) {
      // error
    }
  };

  // when app open or foreground
  const _iOSForegroundHandler = () => {
    try {
      messaging()?.onMessage(notification => {
        console.log("_iOSForegroundHandler: ", notification);
      });
    } catch (error) {
      //
    }
  };

  return null;
};

export default RemotePushController;
