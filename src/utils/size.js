import {Dimensions} from 'react-native';

const {width: SCREEN_WIDTH, height: SCREEN_HEIGHT} = Dimensions.get('window');

export const getScreenWidth = () => SCREEN_WIDTH;

export const getScreenHeight = () => SCREEN_HEIGHT;

export const getAspectRatio = WIDTH => Math.floor((4 / 3) * WIDTH);

export const isPortrait = (WIDTH, HEIGHT) => HEIGHT >= WIDTH;
