import axios from "axios";
import { BASE_URL } from "../../config";
import { getUniqueId } from "react-native-device-info";
import AsyncStorage from "@react-native-community/async-storage";

const saveFcmToken = async token => {
  console.log("TOKEN CICING: ", token);
  try {
    if (token) {
      const getTokenUser = await AsyncStorage.getItem("KYZN.TOKEN");
      console.log("Bangsat: ", getTokenUser);
      const uri = `${BASE_URL}/api/user/fcm/token`;
      console.log("API FCM: ", uri);
      const options = {
        method: "POST",
        data: {
          token,
          deviceId: token,
        },
        url: uri,
        headers: {
          "Content-Type": "application/json; charset=utf-8",
          Authorization: getTokenUser,
        },
      };
      console.log("CICING CAI: ", options);

      axios(options)
        .then(res => {
          console.log("RES AXIOS FCM: ", res);
        })
        .catch(error => {
          console.log("AXIOS ERROR FCM: ", error);
        });
    } else {
      console.log("no token");
    }
  } catch (error) {
    console.log("Error SaveFcmToken: ", error);
  }
};

export default saveFcmToken;
