import axios from "axios";
import { BASE_URL } from "../../config";
import { getUniqueId } from "react-native-device-info";
import AsyncStorage from "@react-native-community/async-storage";
import messaging from "@react-native-firebase/messaging";

const ClearFcmToken = async () => {
  return new Promise((resolve, reject) => {
    console.log("ClearFcmToken: ");
    try {
      messaging()
        ?.getToken()
        .then(async token => {
          console.log("CLEAR FCM TOKEN: ", token);
          if (token) {
            const getTokenUser = await AsyncStorage.getItem("KYZN.TOKEN");
            console.log("Bangsat: ", getTokenUser);
            const uri = `${BASE_URL}/api/user/fcm/token/revoke`;
            console.log("API FCM: ", uri);
            const options = {
              method: "delete",
              data: {
                token,
              },
              url: uri,
              headers: {
                Authorization: getTokenUser,
              },
            };
            console.log("CICING CAI: ", options);

            axios(options)
              .then(res => {
                console.log("AXIOS CLEAR FCM ERROR FCM: ", res);
                resolve(null);
              })
              .catch(error => {
                console.log("AXIOS CLEAR FCM ERROR FCM: ", error);
                resolve(null);
              });
          } else {
            console.log("no token");
            resolve(null);
          }
        })
        .catch(error => {
          console.log("error : ", error);
          resolve(null);
        });
    } catch (error) {
      console.log("Error SaveFcmToken: ", error);
      resolve(null);
    }
  });
};

export default ClearFcmToken;
