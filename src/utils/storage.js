import AsyncStorage from "@react-native-community/async-storage";
const type = {
  token: "KYZN.TOKEN",
  firstTime: "KYZN.FIRST_TIME",
};

export default class RNStorage {
  static getToken = async () => {
    try {
      const state = await AsyncStorage.getItem(type.token);
      return state;
    } catch (error) {
      return error;
    }
  };

  static setToken = async token => {
    console.log("RNStorage Set Token", token);
    try {
      await AsyncStorage.setItem(type.token, token);
    } catch (error) {
      console.log("RNStorage Set Token Error ", error);
      return error;
    }
  };

  static removeToken = async () => {
    try {
      const data = await AsyncStorage.removeItem(type.token);
      return data;
    } catch (error) {
      return error;
    }
  };

  static getFirstTime = async () => {
    try {
      const state = await AsyncStorage.getItem(type.firstTime);
      return state;
    } catch (error) {
      return error;
    }
  }

  static setFirstTime = async (value) => {
    console.log("firstTime key", value);
    try {
      await AsyncStorage.setItem(type.firstTime, value);
    } catch (error) {
      return error;
    }
  };

  static removeFirstTime = async () => {
    try {
      const data = await AsyncStorage.removeItem(type.firstTime);
      return data;
    } catch (error) {
      return error;
    }
  };
}
