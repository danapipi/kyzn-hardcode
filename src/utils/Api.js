import axios from "axios";
import { BASE_URL } from "../../config";
import Selectors from "selectors";
import { useSelector } from "react-redux";
import { getStore } from "../redux/store/configureStore";
import RNStorage from "../utils/storage";

class Api {
  constructor(path, isFormData) {
    if (isFormData) {
      this.client = axios.create({
        baseURL: `${BASE_URL}/api/${path}`,
        headers: { "Content-Type": "multipart/form-data" },
      });
    } else {
      this.client = axios.create({
        baseURL: `${BASE_URL}/api/${path}`,
        headers: { "Content-Type": "application/json" },
      });
    }
    // this.setHeaderToken();
    this.configHeaderToken();
    this.interceptResponse();
  }

  async configHeaderToken() {
    const token = await RNStorage.getToken();
    // const token = "VYkVCcr0Pkw1hW36Hze01YZW0jHLb4FSkTAlcK7Yf8d6WJoFSp53a0ManpdLzcaSP8IrCi7D9UewFdvr8hLL3WLxrWqrDop9UWAh"
    if (token) {
      this.client.defaults.headers.common["Authorization"] = token;
    }
  }

  async setHeaderToken(token) {
    try {
      // const token = getStore().getState().PERSIST.token
      console.log("cek token", token);

      if (token) {
        this.client.defaults.headers.common["Authorization"] = token;
      }
      return token;
    } catch (error) {
      return error;
    }
  }

  interceptResponse() {
    this.client.interceptors.response.use(
      function (response) {
        console.log("response api", response);

        return response;
      },
      function (error) {
        if (error) {
          // eslint-disable-next-line no-console
          console.log("response error", error.response);
        }
        return Promise.reject(error);
      }
    );
  }
}

export default new Api();
export const UserApi = new Api("user", false);
export const UserApiFormData = new Api("user", true);
