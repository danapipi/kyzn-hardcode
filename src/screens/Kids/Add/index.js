import {
  Box,
  Button,
  FormControl,
  Image,
  Input,
  Select,
  Avatar,
  ScrollView,
  Spinner,
  ChevronLeftIcon,
  ChevronRightIcon,
  Actionsheet,
  HStack,
  View,
} from "native-base";
import React, { useState } from "react";
import { isNull } from "lodash";
import { image } from "images";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import Actions from "actions";
import { useDispatch, useSelector } from "react-redux";
import { UserApi } from "utils/Api";
import { ActivityIndicator } from "react-native";

import FloatingInput from "components/ui/FloatingInput";
import NationPicker from "components/ui/NationPicker";
import GenderPicker from "components/ui/GenderPicker";
import DatePicker from "components/ui/DatePicker";
import CalendarPicker from "react-native-calendar-picker";
import InputField from "components/ui/InputField";

import moment from "moment";
import { Text, TouchableOpacity, Alert, Dimensions } from "react-native";
import { moderateScale } from "react-native-size-matters";

import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

const user = UserApi.client;

const GENDERS = [
  { label: "Male", value: "male" },
  { label: "Female", value: "female" },
];

const COUNTRIES = [
  { label: "Indonesia", value: "indonesia" },
  { label: "Malaysia", value: "malaysia" },
];

const AddKids = ({ navigation, route: { params } }) => {
  const dispatch = useDispatch();
  const [formState, setFormState] = useState({
    familyId: params?.familyId || 2,
    memberType: "regular",
    birthdate: null,
  });

  const familyData = useSelector(state => state.FAMILY.all);

  const [isLoadingSave, setIsLoadingSave] = useState(false);

  const [isOpen, setIsOpen] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const [showConfPassword, setshowConfPassword] = useState(false);

  const [isLoading, setIsLoading] = useState(false);
  const [onSelectedDate, setOnSelectedDate] = useState(
    moment().format("dddd, DD MMMM yyyy"),
  );

  const onClose = () => {
    setIsOpen(false);
  };

  const changeDate = date => {
    let dateSelect = moment(date).format("DD MMMM yyyy");
    let dateForForm = moment(date).format("yyyy-MM-DD");
    console.log("date nya: ", dateSelect);
    setOnSelectedDate(dateSelect);
    setFormState(prevState => ({ ...prevState, ["birthdate"]: dateForForm }));
  };

  const onSubmitDate = () => {
    onClose();
  };

  const setFormInputData = ({ name, value }) => {
    setFormState(prevState => ({ ...prevState, [name]: value }));
  };

  const renderTextInput = ({ title, name, inputProps = {} }) => (
    <>
      <FormControl.Label marginTop={1}>{title}</FormControl.Label>
      <Input
        backgroundColor="white"
        variant="outline"
        {...inputProps}
        onChangeText={text => {
          setFormState(prevState => ({ ...prevState, [name]: text }));
        }}
      />
    </>
  );

  const renderSelect = ({ title, name, data }) => (
    <>
      <FormControl.Label marginTop={1}>{title}</FormControl.Label>
      <Select
        backgroundColor="white"
        variant="outline"
        onValueChange={value => {
          // console.log("name: value", `${name}: ${value}`);
          setFormState(prevState => ({ ...prevState, [name]: value }));
        }}>
        {data.map(({ label, value }) => (
          <Select.Item label={label} value={value} />
        ))}
      </Select>
    </>
  );

  const handleAdd = async () => {
    // setIsLoadingSave(true);
    if (isNull(formState.birthdate) == true) {
      setIsLoadingSave(false);
      Alert.alert("Please fill in your kid's birthdate to continue");
    } else {
      // setIsLoading(true);
      const payload = {
        id: Math.floor(Math.random() * 100) + 1,
        user_family_id: 2,
        user_id: 2,
        role: "child",
        invite_status: "invited",
        user: {
          id: Math.floor(Math.random() * 100) + 1,
          name: formState.name || "",
          username: formState?.username || "",
          phone_number: null,
          xp: 200,
          coin: 500,
          balance: 200000,
          status: "Registered",
          birthdate: formState.birthdate,
          gender: formState.gender,
          address: formState.address || "",
          photo: "https://api.kyzn.life/api/uploads/default-avatar.png",
          nationality: "Indonesia",
          postal_code: formState.postalCode || "",
          province: formState.province || "",
          district: formState.district || "",
          city: formState.city || "",
          referral_code: "Grand Opening",
          id_card_no: null,
          instagram_username: null,
          company_name: null,
          physical_limitation: null,
          emergency_contact_name: null,
          emergency_contact_relationship: null,
          emergency_phone_number: null,
          emergency_home_number: null,
          email: null,
          rfid_card_no: null,
          qr_string:
            "U2FsdGVkX1/JwImmnMw2T4NSMsa0RJISRjVdX8P/1P5H0xRny+SvaRfIK6HH/N23pG+5Zkp15rjokjEQ6JhRwRGk/zc/q4QUevc0ecAuasGPCV6sqIcWYmpm9+fPgFodK8DP9LWx0zkUAoFEN2fi3QMj92HCkLSbFkguAZceYKw=",
        },
      };

      console.log("PAYLOAD DATA BRO: ", payload);

      console.log("FAMILYDATA1 BRO: ", familyData);

      console.log("FAMILYDATA2 BRO: ", familyData.data[0]);

      console.log(
        "FAMILYDATA3 BRO: ",
        familyData.dataFamilyOne.user_family_details,
      );

      const dataOfFamily = familyData.data[0];
      const dataOfFamilyOne = familyData.dataFamilyOne;

      dataOfFamily.user_family_details.push(payload);
      dataOfFamilyOne.user_family_details.push(payload);

      navigation.goBack();

      console.log("dataOfFamily: ", dataOfFamily);

      // dispatch(Actions.fetchAllFamiliesSuccess(dataOfFamily[0]));
      // dispatch(Actions.fetchAllFamiliesSuccess(dataOfFamily[0]));

      // user
      //   .post("family/add-child", formState)
      //   .then(res => {
      //     setIsLoading(false);
      //     if (res.data.success === true) {
      //       setIsLoadingSave(false);
      //       // dispatch(Actions.fetchAllFamilies({}));
      //       navigation.goBack();
      //       // navigation.navigate("AddFamily", { title: "Edit Family", familyId: res.data })
      //     } else {
      //       // Alert.alert("Failed to add family")
      //       setIsLoadingSave(false);
      //     }
      //   })
      //   .catch(err => {
      //     setIsLoadingSave(false);
      //     setIsLoading(false);
      //     setFormState(prevState => ({ ...prevState, ["birthdate"]: null }));
      //     console.log("FORM DATA PREV BRO: ", formState);
      //     let errorResponse = err.response.data.message;
      //     let errorMessage;

      //     if (errorResponse.hasOwnProperty("password")) {
      //       errorMessage = errorResponse.password;
      //     } else if (errorResponse.hasOwnProperty("confPassword")) {
      //       errorMessage = errorResponse.confPassword;
      //     } else {
      //       errorMessage = errorResponse;
      //     }

      //     Alert.alert("Error", errorMessage, [
      //       {
      //         text: "OK",
      //         onPress: () => console.log("OK Pressed"),
      //         style: "cancel",
      //       },
      //     ]);
      //     // Alert.alert("Failed to add family")
      //   });
    }
  };

  const RenderCalendar = ({ title, dateSelected }) => (
    <>
      <FormControl.Label marginTop={1}>{title}</FormControl.Label>
      <TouchableOpacity onPress={() => setIsOpen(true)}>
        <HStack
          style={{
            backgroundColor: "#fff",
            paddingHorizontal: moderateScale(10, 0.25),
            paddingVertical: moderateScale(10, 0.25),
            justifyContent: "space-between",
            alignItems: "center",
          }}>
          <Text
            style={{
              fontSize: moderateScale(10),
            }}>
            {dateSelected}
          </Text>
          <Image source={image.ic_dates} width={5} height={5} />
        </HStack>
      </TouchableOpacity>
    </>
  );

  // if (isLoading) {
  //   return <Spinner />;
  // }

  return (
    <Box flex={1} safeArea bg="white">
      <Image
        source={image.successCorner}
        position="absolute"
        top={0}
        right={0}
        zIndex={-1}
      />
      <KeyboardAwareScrollView enableOnAndroid={true}>
        <Box flex={1} padding={5}>
          <FormControl>
            <Avatar
              alignSelf="center"
              bg="black"
              source={image.ic_profile_placeholder}
              size="lg">
              <Avatar.Badge overflow="visible" borderColor="transparent">
                <Icon
                  name="pencil-circle"
                  color="blue"
                  size={20}
                  style={{ margin: -7 }}
                />
              </Avatar.Badge>
              GG
            </Avatar>
            <FloatingInput
              label="Full Name *"
              value={formState?.name ? formState?.name : ""}
              onChangeText={val => {
                setFormInputData({ name: "name", value: val });
              }}
              bgColorLabel={"white"}
            />
            {/* {renderSelect({ title: "Gender", name: "gender", data: GENDERS })} */}
            <GenderPicker
              label="Gender *"
              onSelected={val => {
                setFormInputData({ name: "gender", value: val });
              }}
              bgColorLabel={"white"}
            />
            <DatePicker
              label="Birthdate *"
              onSelected={val => {
                changeDate(val);
              }}
              bgColorLabel={"white"}
            />
            <FloatingInput
              label="Username *"
              value={formState?.username ? formState?.username : ""}
              onChangeText={val => {
                setFormInputData({ name: "username", value: val });
              }}
              bgColorLabel={"white"}
            />
            <FloatingInput
              label="Password *"
              value={formState?.password ? formState?.password : ""}
              onChangeText={val => {
                setFormInputData({ name: "password", value: val });
              }}
              isPassword={true}
              bgColorLabel={"white"}
            />
            <FloatingInput
              label="Confirm Password *"
              value={formState?.confPassword ? formState?.confPassword : ""}
              onChangeText={val => {
                setFormInputData({ name: "confPassword", value: val });
              }}
              isPassword={true}
              bgColorLabel={"white"}
            />
            <View
              style={{
                alignSelf: "center",
                height: 1,
                backgroundColor: "gray",
                width: Dimensions.get("screen").width / 1.4,
                marginVertical: 12,
              }}
            />
            <FloatingInput
              label="Address (Optional)"
              onChangeText={val => {
                setFormInputData({ name: "address", value: val });
              }}
              bgColorLabel={"white"}
            />
            <FloatingInput
              label="City (Optional)"
              onChangeText={val => {
                setFormInputData({ name: "city", value: val });
              }}
              bgColorLabel={"white"}
            />
            <FloatingInput
              label="Province (Optional)"
              onChangeText={val => {
                setFormInputData({ name: "province", value: val });
              }}
              bgColorLabel={"white"}
            />
            <FloatingInput
              label="District (Optional)"
              onChangeText={val => {
                setFormInputData({ name: "district", value: val });
              }}
              bgColorLabel={"white"}
            />
            <FloatingInput
              label="Postal Code (Optional)"
              onChangeText={val => {
                setFormInputData({ name: "postalCode", value: val });
              }}
              bgColorLabel={"white"}
            />
            <NationPicker
              label="Nationality"
              onSelected={val => {
                setFormInputData({ name: "nationality", value: val });
              }}
              bgColorLabel={"white"}
            />

            {/* {renderTextInput({ title: "Emergency Number" })} */}
          </FormControl>
          <Button
            backgroundColor="button.black"
            borderRadius={0}
            _pressed={{ backgroundColor: "button.black:alpha.20" }}
            marginBottom={5}
            marginTop={10}
            onPress={() => {
              handleAdd();
            }}>
            {isLoading ? (
              <ActivityIndicator size="small" color="white" />
            ) : (
              <Text style={{ color: "white" }}>SAVE</Text>
            )}
          </Button>
          <Button
            variant="ghost"
            onPress={() => navigation.goBack()}
            _text={{ color: "black" }}
            _pressed={{ backgroundColor: "button.black:alpha.20" }}>
            Cancel
          </Button>
        </Box>
      </KeyboardAwareScrollView>

      <Actionsheet isOpen={isOpen} onClose={onClose} hideDragIndicator={false}>
        <Actionsheet.Content h={400}>
          <CalendarPicker
            weekdays={["S", "M", "T", "W", "T", "F", "S"]}
            dayLabelsWrapper={{ borderColor: "transparent" }}
            previousComponent={<ChevronLeftIcon />}
            nextComponent={<ChevronRightIcon />}
            dayShape={{ backgroundColor: "red" }}
            selectedDayStyle={{
              backgroundColor: "transparent",
              borderColor: "black",
              borderWidth: 1,
              borderRadius: 14,
              width: 40,
            }}
            onDateChange={changeDate}
          />
          <TouchableOpacity onPress={onSubmitDate}>
            <Box
              style={{
                backgroundColor: "#212121",
                padding: 10,
                width: 320,
                alignItems: "center",
              }}>
              <Text style={{ color: "#FFF" }}>{"Close"}</Text>
            </Box>
          </TouchableOpacity>
        </Actionsheet.Content>
      </Actionsheet>
    </Box>
  );
};

export default AddKids;
