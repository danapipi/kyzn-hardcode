import React, { useState,useRef,useEffect } from "react";
import { Text, Dimensions, TouchableOpacity } from "react-native";
import { Box, FlatList, HStack, Image, ChevronLeftIcon } from "native-base";
import { SafeAreaProvider } from "react-native-safe-area-context";
import { image } from "../../../assets/images";
import { moderateScale } from "react-native-size-matters";
import CardActivity from "../../components/ui/CardActivity";
import CardOrder from "../../components/ui/CardOrder";
import Loading from "components/ui/Loading";

import { Select, CheckIcon, Flex } from 'native-base';
import dummyActivity from "./dummyData/activity.json";
import { useSelector, useDispatch } from "react-redux";
import { UserApi } from "utils/Api";
import Actions from "actions";
import _ from 'lodash';
import moment from 'moment';
import { useIsFocused } from '@react-navigation/native'

const userapi = UserApi.client;
const { height } = Dimensions.get("window");

const DetailViewAll = ({ navigation }) => {
  const dispatch = useDispatch()
  const isFocused = useIsFocused()

  const data = useSelector((state) => state.PRODUCT.upcomingSchedule);
  const data_past = useSelector((state) => state.PRODUCT.pastSchedule);
  const dataUniqueFamily = useSelector((state) => state.FAMILY.uniqueFamily)
  const [activityList, setActivityList] = useState([]);
  const [tabActive, setTabActive] = useState(1);
  const [page, setPage] = useState(0);
  const [size, setSize] = useState(20);
  const [stopInfiniteScroll, setStopInfiniteScroll] = useState(true)
  const family = useSelector((state) => state.FAMILY.user.data)
  const [selectedUser, setSelectedUser] = useState(dataUniqueFamily.data[0].id);
  const [dataUpcoming, setDataUpcoming] = useState([]);
  const [dataPast, setDataPast] = useState([]);

  const profile = useSelector((state) => state.AUTH.profile);

  const [isLoading, setIsLoading] = useState(true);
  const countRef = useRef(0);


  const { 
    class_activity_type_id, 
    panorama_activity_type_id, 
    pt_activity_type_id, 
    rent_court_activity_type_id
  } = useSelector((state) => state.PRODUCT.activityTypeId.activityTypeId);

  const changeTab = (idTab) => {
    setTabActive(idTab);
  };

  console.log("jagsiudgoqgwpeiq", family, family.user_family_details);

  const changeUser = async (user) => {
    setSelectedUser(user);
    console.log("Asdasdasd", `activity-booking/schedule?filterBy=upcoming&userId=${user}`)
    let activityData = await userapi.get(`/activity-booking/schedule?filterBy=upcoming&userId=${user}`)
    console.log("PROMISE", activityData.data)

    let dataUpcomingTemp = activityData.data.data
    let orderUpcoming = _.orderBy(dataUpcomingTemp, ['start_time'], ['asc']);
    // let filterDate =  _.filter(orderUpcoming, function(o) { return moment(o.start_time).format("DD MM YYYY HH:mm") >= moment().format("DD MM YYYY HH:mm")});
    let resizeUpcomingData = _.chunk(orderUpcoming, [size])

    setDataUpcoming(resizeUpcomingData[page]);

    console.log("Asdasdasd", `activity-booking/schedule?filterBy=upcoming&userId=${user}`)
    let activityDataPast = await userapi.get(`/activity-booking/schedule?filterBy=past&userId=${user}`)
    console.log("PROMISE", activityData.data)

    let dataPastTemp = activityDataPast.data.data
    let orderPast = _.orderBy(dataPastTemp, ['start_time'], ['asc']);
    // let filterDate =  _.filter(orderUpcoming, function(o) { return moment(o.start_time).format("DD MM YYYY HH:mm") >= moment().format("DD MM YYYY HH:mm")});
    let resizePastData = _.chunk(orderPast, [size])

    setDataPast(resizePastData[page]);
    // setActivityList(activityData.data.data)
  };
  
  const populateData = async() => {
    let dataUpcomingTemp = data.upcomingSchedule
    // let dataUpcomingTemp = await userapi.get(`/activity-booking/schedule?filterBy=upcoming&userId=${selectedUser}`)
    // console.log("logging dataUpcomingTemp: ", dataUpcomingTemp.data.data);
    // let orderUpcoming = _.orderBy(dataUpcomingTemp, ['activity_schedule.start_time'], ['asc']);
    // console.log("asdaqwuqoe heeerrrrr", dataUpcomingTemp, orderUpcoming);
    // let filterDate =  _.filter(orderUpcoming, function(o) { return moment(o.start_time).format("DD MM YYYY HH:mm") >= moment().format("DD MM YYYY HH:mm")});

    // let resizeUpcomingData = _.chunk(filterDate, [size])

    // let dataUpTemp = activityList.length !== 0 ? activityList : resizeUpcomingData[page];
    setDataUpcoming(dataUpcomingTemp);

    let dataPastTemp = data_past.pastSchedule
    // let orderUpcoming2 = _.orderBy(dataPastTemp, ['start_time'], ['asc']);
    // let filterDate2 =  _.filter(orderUpcoming2, function(o) { return moment(o.start_time).format("DD MM YYYY HH:mm") >= moment().format("DD MM YYYY HH:mm")});

    // let resizePastData = _.chunk(filterDate2, [size])

    // let dataUpTemp2 = activityList.length !== 0 ? activityList : resizePastData[page];
    setDataPast(dataPastTemp);
    setIsLoading(false)
  }

  useEffect(() => {
    // if(countRef.current == 0){
    //   populateData()
      
    //   countRef.current++;
    // }
    
    onRefresh(selectedUser)
    populateData()


    // console.log("asdaqwuqoe", data, data_past);

    return () => {
      onRefresh(_.get(profile.data, 'id', null))
      setIsLoading(true)
      countRef.current = 0
    }
  }, [isFocused])
  
  useEffect(() => {
    navigation.setOptions({
      headerLeft: () => (
        <TouchableOpacity onPress={() => 
          // navigation.goBack()
          navigation.navigate("BottomTabs", {
            screen: "Activity",
          })

          } style={{flexDirection:"row", alignItems:"center", justifyContent:"center"}}>
          <ChevronLeftIcon />
          <Text
            style={{
              fontSize: 16,
              color: "#000",
              letterSpacing: 0.2,
              fontWeight:"700",
              marginTop: 2
            }}
          >
            All Activity
          </Text>
        </TouchableOpacity>
      ),
    });
    if(countRef.current == 0){
      // populateData()
      
      countRef.current++;
    }

  }, [])

  const renderEmpty = (type) => {
    return (
      <Box
        style={{
          alignItems: "center",
          justifyContent: "center",
          flex: 1,
          alignContent: "center",
          alignSelf: "center",
          height: height - 250,
        }}
      >
        <Image
          source={image.emptyActivity}
          alt="background"
          resizeMode="cover"
          style={{ width: 250, height: 180 }}
        />
        <Text
          style={{
            color: "black",
            fontFamily: "Montserrat-Bold",
            fontSize: 16,
          }}
        >
          {/* {type == 1  */}
          {type == rent_court_activity_type_id
            ? 
            `There are no ongoing activities`
            :
            `There are no past activities`
          }
        </Text>
      </Box>
    );
  };



  const handleLoadMore = () => {
    console.log("handle more");


  }

  const onRefresh = (id) => {
    // let userId = _.get(profile.data, 'id', null)
    let userId = id || _.get(profile.data, 'id', null)
    
    // dispatch(Actions.upcomingSchedule(userId))
    // dispatch(Actions.pastSchedule(userId))
    // dispatch(Actions.topupHistory());
    setIsLoading(false)
  };

  const renderActivity = () => {

    return (
      <Box style={{ padding: 20 }}>
        {dataUpcoming && dataUpcoming.length == 0 ? (
          renderEmpty(1)
        ) : (
          <FlatList
            listKey={(index) => index}
          onRefresh={() => onRefresh(selectedUser)}
          refreshing={true}
            data={dataUpcoming}
            extraData={dataUpcoming}
            scrollEnabled={true}
            renderItem={(item) => {
              console.log("item activity", item);
              return (
                <CardActivity
                  data={item}
                  userId={selectedUser}
                  onPress={() =>
                    navigation.navigate("DetailActivity", {
                      title: "Detail Activity",
                      data: item,
                      userId: selectedUser,
                      typeDetail: 1,
                      fromHistory: false,
                      fromUpcoming: true
                    })
                  }
                />
              );
            }}
            onEndReachedThreshold={0.5}
            onEndReached={
                stopInfiniteScroll 
                ? () => handleLoadMore()
                : console.log("doing nothing")
            }
          />
        )}
      </Box>
    );
  };

  const renderHistory = () => {
    // let dataUpcomingTemp = data_past.pastSchedule
    // let orderUpcoming = _.orderBy(dataUpcomingTemp, ['start_time'], ['asc']);
    // let filterDateHistory =  _.filter(orderUpcoming, function(o) { return moment(o.start_time).format("DD MM YYYY HH:mm") < moment().format("DD MM YYYY HH:mm")});

    return (
      <Box style={{ padding: 20 }}>
        {dataPast && dataPast.length == 0 ? (
          renderEmpty(2)
        ) : (
        <FlatList
          listKey={(index) => index}
          data={dataPast}
          onRefresh={() => onRefresh(selectedUser)}
          refreshing={false}
          scrollEnabled={true}
          renderItem={(item) => {
            console.log("item activity dataUpcoming", item);
            return (
              <CardActivity
                data={item}
                userId={selectedUser}
                onPress={() =>
                  navigation.navigate("DetailActivity", {
                    title: "Detail Activity",
                    data: item,
                    typeDetail: 1,
                    userId: selectedUser,
                    fromHistory: true,
                    fromUpcoming: false,
                  })
                }
              />
            );
          }}
        />
        )}
      </Box>
    );
  };

  const component = ({ navigation }) => {
    return (
      <SafeAreaProvider
        style={{
          flex: 1,
          backgroundColor: "white",
        }}
        edges={["right", "bottom", "left"]}
      >
        {tabActive == 1 ? renderActivity() : renderHistory()}
      </SafeAreaProvider>
    );
  };

  return (
    <Box style={{ backgroundColor: "white", flex: 1 }}>
      <HStack style={{ padding: 20 }}>
        <TouchableOpacity onPress={() => changeTab(1)}>
          <Box
            style={{
              padding: 12,
              paddingVertical: 4,
              borderRadius: 16,
              backgroundColor: tabActive == 1 ? "#000" : "transparent",
              marginRight: 16,
            }}
          >
            <Text
              style={{
                fontSize: 12,
                color: tabActive == 1 ? "#FFF" : "#616161",
                fontWeight: "700",
              }}
            >
              Upcoming Activity
            </Text>
          </Box>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => changeTab(2)}>
          <Box
            style={{
              padding: 12,
              paddingVertical: 4,
              borderRadius: 16,
              backgroundColor: tabActive == 2 ? "#000" : "transparent",
              marginRight: 16,
            }}
          >
            <Text
              style={{
                fontSize: 12,
                color: tabActive == 2 ? "#FFF" : "#616161",
                fontWeight: "bold",
              }}
            >
              History
            </Text>
          </Box>
        </TouchableOpacity>
      {/* <Text>{JSON.stringify(family)}</Text> */}
      {/* <Flex
      // direction="row"
      justify="flex-end"
      style={{marginTop: -10, marginRight: 10, flexDirection: "row"}}>
      {dataUniqueFamily.data &&
      <Select
          selectedValue={selectedUser}
          width={115}
          accessibilityLabel="Choose User"
          placeholder="Choose User"
          _selectedItem={{
            endIcon: <CheckIcon size="5" />
          }}
          mt={1}
          onValueChange={(itemValue) => changeUser(itemValue)}
        >
          {dataUniqueFamily.data && dataUniqueFamily.data.map((family_member) => 
            <Select.Item label={family_member.name} value={family_member.id} />
          )}
        </Select>
        }
        </Flex> */}
      </HStack>
      {/* {JSON.stringify(activityList)} */}
      {isLoading 
        ? 
          <Loading /> 
        :
          <FlatList ListHeaderComponent={component({ navigation })} />
      }
    </Box>
  );
};

export default DetailViewAll;
