import React, { useState, useEffect } from "react";
import {
  Dimensions,
  ActivityIndicator,
  Text,
  TouchableOpacity,
  Image,
  Alert,
} from "react-native";
import {
  Box,
  Button,
  FlatList,
  ScrollView,
  HStack,
  ZStack,
  VStack,
  Actionsheet,
  ChevronLeftIcon,
  Toast,
} from "native-base";
import PropTypes from "prop-types";
import { backgroundColor } from "styled-system";
import { useRoute } from "@react-navigation/core";
import { useIsFocused } from "@react-navigation/native";
import _ from "lodash";
import moment from "moment";
import { AirbnbRating } from "react-native-ratings";
import { moderateScale } from "react-native-size-matters";
import Commons from "utils/common";
import { image, dummyImages } from "images";
import { UserApi } from "../../utils/Api";
import { useSelector } from "react-redux";

const userapi = UserApi.client;

const DetailActivity = ({ navigation, typeDetail }) => {
  const route = useRoute();
  const isFocused = useIsFocused();

  const fromHistory = route.params.fromHistory;
  const fromUpcoming = route.params.fromUpcoming;

  const activityDetail = fromUpcoming
    ? useSelector(state => state.PRODUCT.upcomingSchedule.upcomingSchedule)
    : useSelector(state => state.PRODUCT.pastSchedule.pastSchedule);

  console.log("activityDetail: ", activityDetail);

  console.log("Routeeeee: ", route);
  const data = route?.params?.data?.item;

  const activityParticipant = useSelector(
    state => state.FAMILY.user.activityParticipant,
  );

  const currentActivityDetail = activityDetail.findIndex(
    item => item.activity_schedule_id === data.activity_schedule_id,
  );

  const status = _.get(data, "status", "");
  // const status = data.status
  const attendance_status = _.get(data, "attendance_status", "");
  const booking_status = _.get(data, "booking_status", "");
  const venueTemp = _.get(data, "activity_court.venue.address", "");
  const venue = _.get(data, "venue.address", venueTemp);
  const [cancelOpen, setCancelOpen] = useState(false);
  // const courtTemp = _.get(data, 'activity_court.court.name', '')
  const courtTemp = _.get(
    data,
    "activity_schedule.activity_court.court.name",
    "",
  );
  const courtTempId = _.get(
    data,
    "activity_schedule.activity_court.court.id",
    "",
  );
  const court = _.get(data, "court.name", courtTemp);
  const courtId = _.get(data, "court.id", courtTempId);
  const ratingsActivity = _.get(data, "activity_rating.id", null);
  const ratingsCourt = _.get(data, "court_rating.id", null);
  const ratingsWorker = _.get(data, "worker_rating.id", null);
  const rateOfActivity = _.get(data, "activity_rating.rating", 3);
  const rateOfCourt = _.get(data, "court_rating.rating", 3);
  const rateOfWorker = _.get(data, "worker_rating.rating", 3);
  // const startTime = _.get(data, 'start_time', " ")
  const startTime = _.get(data, "activity_schedule.start_time", " ");
  // const endTime = _.get(data, 'end_time', " ")
  const endTime = _.get(data, "activity_schedule.end_time", " ");
  const [participants, setParticipants] = useState(activityParticipant);
  const maxParticipant = _.get(data, "activity.max_participants", 5);
  // const activityScheduleId = _.get(data, 'id', " ")
  // const activityScheduleId = _.get(data, 'activity_schedule_id', 1)
  const activityScheduleId = _.get(data, "ActivityScheduleId", 1);
  const activityId = _.get(data, "activity.id", " ");
  const activityBookingId = _.get(data, "activity_booking_id", " ");
  const isToday =
    moment(startTime).format("dddd, DD MMMM YYYY") ==
    moment().format("dddd, DD MMMM YYYY");
  const isTommorow =
    moment(startTime).format("dddd, DD MMMM YYYY") ==
    moment().add(1, "day").format("dddd, DD MMMM YYYY");
  const nameTemp = _.get(data, "activity_court.activity.name", "");
  const name = _.get(data, "activity.name", nameTemp);
  const workerName = _.get(
    data,
    "activity.activity_workers[0].worker.name",
    "",
  );
  const profile = useSelector(state => state.AUTH.profile.data);
  const userId = _.get(route.params, "userId", profile.id);
  const paying_user_id = _.get(data, "paying_user_id", null);
  const [rateData, setRateData] = useState([]);
  const [classRate, setClassRate] = useState(3);
  // console.log("sdjaiwugdoa", data, );

  const workerTemp = _.get(data, "activity.activity_workers", []);
  const workerdatas = _.get(
    data,
    "activity_schedule.activity_court.activity.activity_workers",
    workerTemp,
  );
  console.log("workerdatas: ", workerdatas);
  console.log("ratingsActivity: ", ratingsActivity);

  let workers = [];
  let courts = [];

  // let dataCourtRating = {
  //   name: court,
  //   rating: 3,
  //   id: courtId
  // }

  let dataActivityRating = {
    name: name,
    rating: 3,
    id: activityScheduleId,
  };

  const calculateWorker = () => {
    for (let i = 0; i < workerdatas.length; i++) {
      workers.push({
        id: workerdatas[i].worker.id,
        rating: 3,
      });
    }
  };

  const calculateCourt = () => {
    courts.push({
      id: courtId,
      rating: 3,
    });
  };

  const refreshParticipant = id => {
    // to retrieved the global schedule data & refresh the list
    setParticipants(activityParticipant);
  };

  const getStatus = obj => {
    if (_.upperCase(obj.attendance_status) !== "CANCELLED") {
      if (_.upperCase(obj.booking_status) == "SUCCESS") {
        if (participants[0].user.id != userId) {
          return "INVITED";
        } else {
          return "BOOKED";
        }
      } else if (_.upperCase(obj.booking_status) == "WAITING LIST") {
        return "WAITING LIST";
      } else if (_.upperCase(obj.booking_status) == "CANCELLED") {
        return "CANCELLED";
      }
    } else if (_.upperCase(obj.attendance_status) == "CANCELLED") {
      return "CANCELLED";
    }
    //WAITING LIST
  };

  useEffect(() => {
    calculateWorker();
    calculateCourt();
    console.log("workers: ", workers);

    return () => {
      workers = [];
      courts = [];
    };
  }, []);

  useEffect(() => {
    navigation.setOptions({
      headerLeft: () => (
        <TouchableOpacity
          onPress={() =>
            // navigation.goBack()
            navigation.navigate("DetailViewAll", {
              title: "All Activity",
            })
          }
          style={{
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
          }}>
          <ChevronLeftIcon />
          <Text
            style={{
              fontSize: 16,
              color: "#000",
              letterSpacing: 0.2,
              fontWeight: "700",
              marginTop: 2,
            }}>
            Detail Activity
          </Text>
        </TouchableOpacity>
      ),
    });
  }, []);

  useEffect(() => {
    refreshParticipant(userId);
  }, [isFocused]);

  console.log("halloo", data, route.params);

  const rateForClass = () => {};

  const ratingCompleted = e => {
    console.log("hello me", e);
    Alert.alert(`Are you sure you want to rate this class?`, ``, [
      {
        text: "Yes, Rate!",
        onPress: () => rateClass(e),
      },
      {
        text: "Later",
        onPress: () => null,
      },
    ]);
  };

  const rateClass = rating => {
    try {
      console.log(
        "activityScheduleId > ",
        route?.params?.data?.item?.activity_schedule_id,
      );
      let requestBody = {
        activityScheduleId: route?.params?.data?.item?.activity_schedule_id,
        rating: dataActivityRating.rating,
        workerRatings: workers,
        courtRatings: courts,
      };

      console.log("rateClass requestBody: ", requestBody);

      userapi
        .post(`/activity/rate`, requestBody)
        .then(res => {
          console.log("rateClass res: ", res);
          navigation.goBack();
          Toast.show({
            description: "Successfully rate!",
            duration: 3000,
          });
        })
        .catch(error => {
          console.log("rateClass err: >>>>", error);
          Alert.alert(
            `${
              error?.data?.message?.activityScheduleId
                ? error?.data?.message?.activityScheduleId
                : error?.message
            }`,
            error?.data?.message?.activityScheduleId
              ? error?.data?.message?.activityScheduleId
              : error?.message,
            [
              {
                text: "OK",
                onPress: () => console.log("OK Pressed"),
                style: "cancel",
              },
            ],
          );
        });
    } catch (error) {
      console.log("rateClass err: ", error);
      Alert.alert(
        `${
          error?.data?.message?.activityScheduleId
            ? error?.data?.message?.activityScheduleId
            : error?.message
        }`,
        error?.data?.message?.activityScheduleId
          ? error?.data?.message?.activityScheduleId
          : error?.message,
        [
          {
            text: "OK",
            onPress: () => console.log("OK Pressed"),
            style: "cancel",
          },
        ],
      );
    }
  };

  const openCancel = () => {
    setCancelOpen(true);
  };

  const onCloseCancel = () => {
    setCancelOpen(false);
  };

  const onHandleCancel = () => {
    // modify the reducer here
    activityDetail[currentActivityDetail].booking_status = "CANCELLED";

    Toast.show({
      description: "Activity Booking is successfully canceled",
      duration: 2000,
    });
    navigation.goBack();
    setCancelOpen(false);
  };
  return (
    <Box flex={1}>
      <ScrollView
        style={{ backgroundColor: "white", flex: 1, marginBottom: 60 }}>
        <Box style={{ padding: 20 }}>
          <HStack
            style={{
              alignItems: "center",
              paddingVertical: 9,
              justifyContent: "space-between",
            }}>
            <VStack style={{ justifyContent: "center" }}>
              <Text
                style={{
                  fontSize: 10,
                  color: "#000",
                  letterSpacing: 0.2,
                  fontWeight: "600",
                  fontFamily: "Montserrat-Regular",
                }}>
                Payment Status
              </Text>
              <Text
                style={{
                  fontSize: 8,
                  color: "#009174",
                  letterSpacing: 0.2,
                  fontFamily: "Montserrat-Regular",
                  fontWeight: "700",
                  marginTop: 2,
                }}>
                {_.upperCase(data.payment_status)}
              </Text>
            </VStack>
            <VStack style={{ justifyContent: "center" }}>
              <Text
                style={{
                  fontSize: 10,
                  color: "#000",
                  letterSpacing: 0.2,
                  fontWeight: "600",
                  fontFamily: "Montserrat-Regular",
                }}>
                Status
              </Text>
              <Text
                style={{
                  fontSize: 8,
                  color:
                    getStatus(data) == "BOOKED" || getStatus(data) == "INVITED"
                      ? "#009174"
                      : getStatus(data) == "CANCELLED"
                      ? "red"
                      : "#E2BB4B",
                  letterSpacing: 0.2,
                  fontWeight: "700",
                  fontFamily: "Montserrat-Regular",
                  marginTop: 2,
                }}>
                {_.upperCase(getStatus(data))}
              </Text>
            </VStack>
          </HStack>
        </Box>
        <Box style={{ padding: 20, paddingTop: 0 }}>
          <VStack
            style={{
              borderColor: "#DFE3EC",
              borderWidth: 1,
              borderBottomWidth: 0,
              padding: 16,
            }}>
            <Text
              style={{
                fontSize: 10,
                color: "#757575",
                letterSpacing: 0.2,
                marginBottom: 10,
                fontFamily: "Montserrat-Regular",
              }}>
              Booking for
            </Text>
            <Text
              style={{
                fontWeight: "bold",
                color: "#000",
                fontSize: 12,
                letterSpacing: 0.2,
                fontFamily: "Montserrat-Regular",
              }}>
              {name}
            </Text>
            {workerName != "" && (
              <Text
                style={{
                  fontSize: 12,
                  color: "#000",
                  letterSpacing: 0.2,
                  fontFamily: "Montserrat-Regular",
                }}>
                with
                <Text
                  style={{
                    fontWeight: "bold",
                    color: "#000",
                    fontSize: 12,
                    letterSpacing: 0.2,
                    fontFamily: "Montserrat-Regular",
                  }}>
                  {` ${workerdatas.map((item, index) => {
                    if (index == workerdatas.length - 1)
                      return item.worker.name;
                    else return item.worker.name + `, `;
                  })}`}
                </Text>
              </Text>
            )}
            <Text
              style={{
                fontSize: 10,
                color: "#757575",
                letterSpacing: 0.2,
                fontFamily: "Montserrat-Regular",
              }}>
              {isToday
                ? `Today, ${moment(startTime).format(
                    " DD MMM YYYY | HH:mm",
                  )} - ${moment(endTime).format("HH:mm")}`
                : isTommorow
                ? `Tomorrow, ${moment(startTime).format(
                    " DD MMM YYYY | HH:mm",
                  )} - ${moment(endTime).format("HH:mm")}`
                : `${moment(startTime).format(
                    "dddd, DD MMM YYYY | HH:mm",
                  )} - ${moment(endTime).format("HH:mm")}`}
            </Text>
          </VStack>
          <VStack
            style={{ borderColor: "#DFE3EC", borderWidth: 1, padding: 16 }}>
            <Text
              style={{
                fontSize: 10,
                color: "#757575",
                letterSpacing: 0.2,
                marginBottom: 10,
                fontFamily: "Montserrat-Regular",
              }}>
              Exercise at
            </Text>
            <Text
              style={{
                fontWeight: "bold",
                fontSize: 12,
                color: "#000",
                letterSpacing: 0.2,
                marginTop: 2,
                fontFamily: "Montserrat-Regular",
              }}>
              {court}
            </Text>
            <Text
              style={{
                fontSize: 10,
                color: "#757575",
                letterSpacing: 0.2,
                marginTop: 2,
                fontFamily: "Montserrat-Regular",
              }}
              numberOfLines={1}>
              {venue}
            </Text>
          </VStack>

          {/* <Box style={{marginTop:24}}>
            <Text
              style={{
                fontSize: 10,
                color: "#212121",
                letterSpacing: 0.2,
                marginTop:2,
                marginBottom:4,
                fontFamily: "Montserrat-Regular",
              }}
              numberOfLines={1}
            >
              Participant
            </Text>
            <Box style={{
              flex: 1,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'flex-start',
              // backgroundColor: 'green',
              paddingHorizontal: moderateScale(16),
              paddingVertical: moderateScale(10),
              borderColor: '#DFE3EC',
              borderWidth:1
            }}>
              <Image
                source={{uri: "https://via.placeholder.com/35x35"}}
                style={{
                  width: moderateScale(35),
                  height: moderateScale(35),
                  marginRight: moderateScale(10),
                  borderRadius:70
                }}
              />
              <Box style={{flex: 1}}>
                <Text style={{
                  fontSize: moderateScale(12, 0.25),
                  fontFamily: "Montserrat-Regular",
                  fontWeight: "700",
                  marginBottom: moderateScale(4)
                }}>
                  You
                </Text>
                <Text style={{
                  fontSize: moderateScale(10, 0.25),
                  fontFamily: "Montserrat-Regular",
                  fontWeight: "600",
                  marginBottom: moderateScale(4),
                  color:"#009174"
                }}>
                  Inisiator
                </Text>
              </Box>
              <Box style={{
                padding: moderateScale(4)
              }}>
                  <Text style={{
                    color: "#212121" ,
                    fontSize: moderateScale(10, 0.25),
                    fontFamily: "Montserrat-Regular",
                    fontWeight: "600",
                  }}>
                    081254322345
                  </Text>
              </Box>
            </Box>
          </Box> */}

          <Box style={{ marginTop: 32 }}>
            <HStack
              style={{ justifyContent: "space-between", marginBottom: 8 }}>
              <Text
                style={{
                  fontSize: 12,
                  color: "#000",
                  letterSpacing: 0.2,
                  fontFamily: "Montserrat-Regular",
                }}>
                Order ID
              </Text>
              <Text
                style={{
                  fontSize: 12,
                  color: "#000",
                  letterSpacing: 0.2,
                  fontFamily: "Montserrat-Regular",
                }}>
                -
              </Text>
            </HStack>
            <HStack
              style={{ justifyContent: "space-between", marginBottom: 8 }}>
              <Text
                style={{
                  fontSize: 12,
                  color: "#000",
                  letterSpacing: 0.2,
                  fontFamily: "Montserrat-Regular",
                }}>
                Date & Time
              </Text>
              <Text
                style={{
                  fontSize: 12,
                  color: "#000",
                  letterSpacing: 0.2,
                  fontFamily: "Montserrat-Regular",
                }}>
                -
              </Text>
            </HStack>
            <HStack
              style={{ justifyContent: "space-between", marginBottom: 8 }}>
              <Text
                style={{
                  fontSize: 12,
                  letterSpacing: 0.2,
                  color: "#000",
                  fontFamily: "Montserrat-Regular",
                }}>
                Payment Method
              </Text>
              <Text
                style={{
                  fontSize: 12,
                  color: "#000",
                  letterSpacing: 0.2,
                  fontFamily: "Montserrat-Regular",
                }}>
                -
              </Text>
            </HStack>
            <HStack
              style={{ justifyContent: "space-between", marginBottom: 8 }}>
              <Text
                style={{
                  color: "#000",
                  fontSize: 12,
                  letterSpacing: 0.2,
                  fontFamily: "Montserrat-Regular",
                }}>
                VA Number
              </Text>
              <Text
                style={{
                  color: "#000",
                  fontSize: 12,
                  letterSpacing: 0.2,
                  fontFamily: "Montserrat-Regular",
                }}>
                -
              </Text>
            </HStack>
          </Box>
          {
            // payingUserId == userId && (
            attendance_status != "cancelled" &&
              fromUpcoming == true &&
              // (data.activity_schedule.activity_court.activity.activity_type_id == 1 ||
              //   data.activity_schedule.activity_court.activity.activity_type_id == 6) &&(
              (data.activity.activity_type_id == 1 ||
                data.activity.activity_type_id == 6) && (
                <Box style={{ marginTop: moderateScale(25, 0.25) }}>
                  <HStack
                    style={{
                      paddingHorizontal: 16,
                      justifyContent: "space-between",
                    }}>
                    <Text
                      style={{
                        fontFamily: "Montserrat-Regular",
                        fontWeight: "700",
                        color: "#000",
                        fontSize: 12,
                      }}>
                      {/* {`Participant (Max ${maxParticipant} persons)`} */}
                      {`Participant (Max 3 persons)`}
                    </Text>
                  </HStack>
                  <HStack style={{ padding: 16 }}>
                    <FlatList
                      data={participants}
                      extraData={participants}
                      showsHorizontalScrollIndicator={false}
                      horizontal
                      renderItem={({ item }) => {
                        return (
                          <VStack
                            style={{
                              width: 60,
                              alignItems: "center",
                              justifyContent: "center",
                              marginRight: 10,
                            }}>
                            <Image
                              resizeMode="contain"
                              source={{ uri: item.user.photo }}
                              alt="image"
                              style={{
                                height: 50,
                                width: 50,
                                borderRadius: 100,
                                marginBottom: 5,
                              }}
                            />
                            <Text
                              style={{
                                fontFamily: "Montserrat-Regular",
                                fontWeight: "600",
                                color: "#000",
                                fontSize: 10,
                              }}>
                              {item.user.id === profile.id
                                ? `You`
                                : item.user.name}
                            </Text>
                          </VStack>
                        );
                      }}
                      ListFooterComponent={() => {
                        if (participants[0].user.id == profile.id) {
                          return (
                            <TouchableOpacity
                              onPress={() =>
                                navigation.navigate("Participant", {
                                  activityBookingId: activityBookingId,
                                  activityScheduleId: activityScheduleId,
                                  userId: userId,
                                })
                              }>
                              <VStack
                                style={{
                                  width: 60,
                                  alignItems: "center",
                                  justifyContent: "center",
                                  marginRight: 10,
                                }}>
                                <Box
                                  style={{
                                    height: 50,
                                    width: 50,
                                    borderRadius: 100,
                                    backgroundColor: "#EEE",
                                    marginBottom: 5,
                                    alignItems: "center",
                                    justifyContent: "center",
                                  }}>
                                  <Text
                                    style={{
                                      fontFamily: "Montserrat-Regular",
                                      fontWeight: "600",
                                      fontSize: 10,
                                      color: "#000",
                                    }}>
                                    +
                                  </Text>
                                </Box>
                                <Text
                                  style={{
                                    fontFamily: "Montserrat-Regular",
                                    fontWeight: "600",
                                    color: "#000",
                                    fontSize: 10,
                                  }}>
                                  Add More
                                </Text>
                              </VStack>
                            </TouchableOpacity>
                          );
                        } else {
                          return null;
                        }
                      }}
                      keyExtractor={(item, index) => String(item + index)}
                    />
                  </HStack>
                </Box>
              )
          }
          {fromUpcoming &&
            userId == profile.id &&
            attendance_status != "cancelled" &&
            participants[0].user.id == profile.id && (
              <TouchableOpacity
                onPress={() => openCancel()}
                style={{
                  backgroundColor: "#F8F9FC",
                  height: moderateScale(70, 0.25),
                  alignItems: "center",
                  justifyContent: "center",
                  marginTop: 100,
                }}>
                <Text
                  style={{
                    fontSize: 14,
                    letterSpacing: 0.2,
                    fontWeight: "600",
                    fontFamily: "Montserrat-Regular",
                    color: "red",
                  }}>
                  Cancel Activity
                </Text>
              </TouchableOpacity>
            )}
          {attendance_status == "cancelled" && (
            <Box
              style={{
                backgroundColor: "transparent",
                height: moderateScale(70, 0.25),
                alignItems: "center",
                justifyContent: "center",
                marginTop: 100,
              }}>
              <Text
                style={{
                  fontSize: 14,
                  letterSpacing: 0.2,
                  fontWeight: "600",
                  fontFamily: "Montserrat-Regular",
                  color: "red",
                }}>
                {`This booking has been canceled`}
              </Text>
            </Box>
          )}
        </Box>
      </ScrollView>

      <Box
        style={{
          position: "absolute",
          bottom: 0,
          right: 0,
          width: "100%",
          padding: 20,
          paddingBottom: 40,
          backgroundColor: "#FFF",
          alignItems: "center",
        }}>
        {/* <HStack style={{ justifyContent: "space-between" }}>
          {fromHistory 
            ? 
            <Button
              style={{width:"100%"}}
              bgColor={"#000"}
              onPress={() => navigation.goBack()}
            >
              Submit
            </Button>
            :
            <Button
              style={{width:"100%"}}
              bgColor={"#000"}
              onPress={() => navigation.goBack()}
            >
              OK
            </Button>
          }
        </HStack> */}

        <Actionsheet isOpen={cancelOpen} onClose={onCloseCancel}>
          <Actionsheet.Content h={365}>
            <Image
              source={image.bg_noPremium_actionsheet}
              style={{
                width: 150,
                height: 150,
                resizeMode: "contain",
                position: "absolute",
                left: 0,
                top: -22,
                transform: [{ rotate: "0deg" }],
              }}
              alt="image"
            />
            <Image
              source={image.not_premium_ic}
              style={{ width: 170, height: 100 }}
              resizeMode={"contain"}
              alt="image"
            />
            <Box style={{ width: 270, alignItems: "center", marginTop: 8 }}>
              <Text
                style={{
                  fontSize: 14,
                  color: "#000",
                  fontWeight: "bold",
                  fontFamily: "Montserrat-Regular",
                }}
                alignItems="flex-start">
                {`Cancel Booking`}
              </Text>
              <Text
                style={{
                  fontSize: 12,
                  color: "#000",
                  fontWeight: "500",
                  fontFamily: "Montserrat-Regular",
                  textAlign: "center",
                  marginTop: 10,
                }}
                alignItems="flex-start">
                {`Are you sure you want to cancel this activity? You might receive a penalty `}
              </Text>
            </Box>
            <Button
              w={"90%"}
              bgColor={"#212121"}
              onPress={onHandleCancel}
              style={{ marginTop: 50, borderRadius: 0 }}>
              <Text
                style={{
                  color: "#fff",
                  fontFamily: "Montserrat-Regular",
                }}>
                Confirm Cancel
              </Text>
            </Button>
            <Button
              w={"90%"}
              bgColor={"transparent"}
              onPress={onCloseCancel}
              color={"#000"}
              style={{ marginTop: 10, borderRadius: 0 }}>
              <Text
                style={{
                  fontWeight: "bold",
                  color: "#000",
                  fontFamily: "Montserrat-Regular",
                }}>
                Close
              </Text>
            </Button>
          </Actionsheet.Content>
        </Actionsheet>
      </Box>
    </Box>
  );
};

DetailActivity.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default DetailActivity;
