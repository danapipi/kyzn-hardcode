import React, { useState, useEffect } from "react";
import { Dimensions, ActivityIndicator, Text } from "react-native";
import {
  Box,
  Button,
  FlatList,
  ScrollView,
  HStack,
  Image,
  ZStack,
  VStack,
} from "native-base";
import PropTypes from "prop-types";
import { backgroundColor } from "styled-system";
import { useRoute } from "@react-navigation/core";
import {image} from '../../../assets/images';

const DetailOrder = ({ navigation, data, typeDetail }) => {

  const route = useRoute();
  
  useEffect(() => {}, []);

  return (
    <Box flex={1}>
      <ScrollView style={{ backgroundColor: "white", flex: 1 }}>
        <Box style={{ padding: 20}}>
          <HStack
            style={{
              alignItems: "center",
              paddingVertical: 9,
              justifyContent: "space-between",
            }}
          >
            <VStack style={{ justifyContent: "center" }}>
              <Text
                style={{
                  fontSize: 10,
                  letterSpacing: 0.2,
                  fontWeight:"600",
                  fontFamily: "Montserrat-Regular",
                }}
              >
                Payment Status
              </Text>
              <Text
                style={{
                  fontSize: 8,
                  color: "#FF8D40",
                  letterSpacing: 0.2,
                  fontFamily: "Montserrat-Regular",
                  fontWeight:"700",
                  marginTop: 2
                }}
              >
                PENDING
              </Text>
            </VStack>
          </HStack>
        </Box>
        <Box style={{ padding: 20, paddingTop:0 }}>
          <HStack
            style={{
              borderColor: "#DFE3EC",
              borderWidth: 1,
              padding: 16,
              alignItems:'center',
              justifyContent:"space-between"
            }}
          >
            <VStack style={{flex:1}}>
              <Text
                style={{
                  fontSize: 10,
                  color: "#212121",
                  letterSpacing: 0.2,
                  marginBottom: 10,
                  fontFamily: "Montserrat-Regular",
                }}
              >
                BCA Virtual Account
              </Text>
              <HStack style={{alignItems:"center"}}>
                <Image
                  style={{ width: 60, height: 17, marginLeft:-5 }}
                  source={image.bcaLogo}
                  alt="image"
                  resizeMode="contain"
                />
                <Text
                  style={{
                    fontWeight: "bold",
                    fontSize: 12,
                    letterSpacing: 0.2,
                    fontFamily: "Montserrat-Regular",
                  }}
                >
                  013904293482349
                </Text>
              </HStack>
            </VStack>
            <Image
              style={{ width: 60, height: 17 }}
              source={require("../../../assets/images/arrow-fwd.png")}
              alt="image"
              resizeMode="contain"
            />
          </HStack>

          <Box style={{marginTop:32}}>
            <HStack style={{justifyContent:"space-between", marginBottom: 8}}>
              <Text
                style={{
                  fontSize: 12,
                  letterSpacing: 0.2,
                  fontFamily: "Montserrat-Regular"
                }}
              >
                Order ID
              </Text>
              <Text
                style={{
                  fontSize: 12,
                  letterSpacing: 0.2,
                  fontFamily: "Montserrat-Regular",
                }}
              >
                23456
              </Text>
            </HStack>
            <HStack style={{justifyContent:"space-between", marginBottom: 8}}>
              <Text
                style={{
                  fontSize: 12,
                  letterSpacing: 0.2,
                  fontFamily: "Montserrat-Regular",
                }}
              >
                Date & Time
              </Text>
              <Text
                style={{
                  fontSize: 12,
                  letterSpacing: 0.2,
                  fontFamily: "Montserrat-Regular",
                }}
              >
                23 Jan 2019 | 20:00
              </Text>
            </HStack>
            <HStack style={{justifyContent:"space-between", marginBottom: 8}}>
              <Text
                style={{
                  fontSize: 12,
                  letterSpacing: 0.2,
                  fontFamily: "Montserrat-Regular",
                }}
              >
                Payment Method
              </Text>
              <Text
                style={{
                  fontSize: 12,
                  letterSpacing: 0.2,
                  fontFamily: "Montserrat-Regular",
                }}
              >
                - 
              </Text>
            </HStack>
            <HStack style={{justifyContent:"space-between", marginBottom: 8}}>
              <Text
                style={{
                  fontSize: 12,
                  letterSpacing: 0.2,
                  fontFamily: "Montserrat-Regular",
                }}
              >
                VA Number
              </Text>
              <Text
                style={{
                  fontSize: 12,
                  letterSpacing: 0.2,
                  fontFamily: "Montserrat-Regular",
                }}
              >
                -
              </Text>
            </HStack>
          </Box>
        </Box>
        <Box style={{ height: 4, backgroundColor: "#F8F9FC" }} />
        <Box style={{padding:20}}>
          <HStack style={{justifyContent:"space-between", marginBottom: 8}}>
            <Box style={{flex:2}}>
              <Text
                style={{
                  fontSize: 12,
                  letterSpacing: 0.2,
                  color:"#7C858B",
                  fontFamily: "Montserrat-Regular",
                }}
              >
                PRODUCT
              </Text>
            </Box>
            <Box style={{flex:1, alignItems:"center"}}>
              <Text
                style={{
                  fontSize: 12,
                  letterSpacing: 0.2,
                  color:"#7C858B",
                  fontFamily: "Montserrat-Regular",
                }}
              >
                PRICE
              </Text>
            </Box>
          </HStack>
          <HStack style={{
            justifyContent:"space-between", 
            alignItems:"center", 
            backgroundColor:"#F8F9FC", 
            paddingLeft:12, 
            paddingVertical:10
          }}>
            <VStack style={{flex:2}}>
              <Text
                style={{
                  fontSize: 12,
                  letterSpacing: 0.2,
                  color:"#000",
                  fontFamily: "Montserrat-Regular",
                }}
              >
                -
              </Text>
              <Text
                style={{
                  fontSize: 10,
                  letterSpacing: 0.2,
                  color:"#7C858B",
                  fontFamily: "Montserrat-Regular",
                }}
              >
                -
              </Text>
            </VStack>
            <Box style={{flex:1, alignItems:"center"}}>
              <Text
                style={{
                  fontSize: 12,
                  fontWeight:"600",
                  letterSpacing: 0.2,
                  color:"#000",
                  fontFamily: "Montserrat-Regular",
                }}
              >
                -
              </Text>
            </Box>
          </HStack>
          <HStack style={{
            justifyContent:"space-between", 
            marginBottom: 8, 
            alignItems:"center", 
            backgroundColor:"#FFF", 
            paddingLeft:12,
            paddingRight:16, 
            paddingVertical:10
          }}>
            <VStack style={{flex:2}}>
              <Text
                style={{
                  fontSize: 12,
                  letterSpacing: 0.2,
                  color:"#000",
                  fontFamily: "Montserrat-Regular",
                }}
              >
                Discount
              </Text>
            </VStack>
            <Box style={{flex:1, alignItems:"flex-end"}}>
              <Text
                style={{
                  fontSize: 12,
                  fontWeight:"600",
                  letterSpacing: 0.2,
                  color:"#000",
                  fontFamily: "Montserrat-Regular",
                }}
              >
                -
              </Text>
            </Box>
          </HStack>
          <HStack style={{
            justifyContent:"space-between", 
            alignItems:"center", 
            backgroundColor:"#F8F9FC", 
            paddingLeft:12, 
            paddingVertical:10
          }}>
            <VStack style={{flex:2, alignItems:"flex-end"}}>
              <Text
                style={{
                  fontSize: 12,
                  letterSpacing: 0.2,
                  color:"#000",
                  fontFamily: "Montserrat-Regular",
                  fontWeight:"600"
                }}
              >
                Total
              </Text>
            </VStack>
            <Box style={{flex:1, alignItems:"center"}}>
              <Text
                style={{
                  fontSize: 12,
                  fontWeight:"600",
                  letterSpacing: 0.2,
                  color:"#000",
                  fontFamily: "Montserrat-Regular",
                }}
              >
                -
              </Text>
            </Box>
          </HStack>
        </Box>
      </ScrollView>
      <Box
        style={{
          position: "absolute",
          bottom: 0,
          right: 0,
          width: "100%",
          padding: 20,
          paddingBottom: 40,
          backgroundColor: "#FFF",
          alignItems:"center"
        }}
      >
        <HStack style={{ justifyContent: "space-between" }}>
          
          <Button
            style={{width:"100%"}}
            bgColor={"#000"}
            onPress={() => navigation.navigate("Payment")}
          >
            OK
          </Button>
        </HStack>
      </Box>
    </Box>
  );
};

DetailOrder.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default DetailOrder;
