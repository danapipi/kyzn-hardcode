import React, { useEffect, useRef } from 'react'
import { View, Text, ImageBackground, TouchableOpacity } from 'react-native'
import {
  Box,
  Button,
  FlatList,
  ScrollView,
  HStack,
  Image,
  ZStack,
  VStack,
} from "native-base";
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { image } from "../../../assets/images";
import { moderateScale } from "react-native-size-matters";
import CardActivity from "../../components/ui/CardActivity"
import CardOrder from "../../components/ui/CardOrder"
import Loading from "components/ui/Loading";
import TopUpHistoryList from "components/ui/TopUpHistoryList";

import dummyActivity from "./dummyData/activity.json"
import { useDispatch, useSelector } from 'react-redux';
import Actions from "actions";
import _ from 'lodash';
import moment from 'moment';

const Activity = ({navigation}) => {

  const dispatch = useDispatch()
  const data = useSelector((state) => state.PRODUCT.upcomingSchedule)
  const dataTopUp = useSelector((state) => state.TOPUP.topUpHistory);
  const profile = useSelector((state) => state.AUTH.profile);
  const countRef = useRef(0);
  const isLoading = data.isLoading;
  const dataTopUpTemp = dataTopUp.topUpHistory
  const resizeArrayTopUp = _.chunk(dataTopUpTemp, [3])

  useEffect(() => {
    let userId = _.get(profile.data, 'id', null)
    
    if(!_.isNull(userId)){
      if(countRef.current == 0){
        // dispatch(Actions.upcomingSchedule(userId))
        // dispatch(Actions.pastSchedule(userId))
        // dispatch(Actions.topupHistory());
        countRef.current++;
      }
    }
  })

  console.log("ajsdgqw", data);

  const onRefresh = () => {

    let userId = _.get(profile.data, 'id', null)
    
    // dispatch(Actions.upcomingSchedule(userId))
    // dispatch(Actions.pastSchedule(userId))
    // dispatch(Actions.topupHistory());
  }

  console.log("Activity data", data, profile, );

    const component = ({ navigation }) => {
      let dataUpcoming = data.upcomingSchedule

      let orderUpcoming = _.orderBy(dataUpcoming, ['start_time'], ['asc']);
      let filterDate =  _.filter(orderUpcoming, function(o) { return moment(o.start_time) >= moment()});
      
      let resizeUpcomingData = _.chunk(filterDate, [3])

      return (
        <SafeAreaProvider
          style={{
            flex: 1,
            backgroundColor: "white",
          }}
          edges={["right", "bottom", "left"]}
        >
          <Box style={{padding:20}}>
            <Box style={{paddingTop:16 }}>
                <HStack style={{justifyContent:"space-between"}}>
                  <Text style={{color: 'black', fontFamily: "Montserrat-Bold"}}>Activity</Text>
                  <TouchableOpacity onPress={() => onRefresh()}>
                    <Image
                      source={image.time_history}
                      alt="background"
                      resizeMode="cover"
                      style={{ width: 20, height:20}}
                    />
                  </TouchableOpacity>
                </HStack>
            </Box>
            <Box style={{marginTop:24}}>
              {dataUpcoming.length === 0 
                ? 
                  <Box style={{alignItems:"center"}}>
                    <Image
                      source={image.emptyActivity}
                      alt="background"
                      resizeMode="cover"
                      style={{ width: 250, height:180}}
                    />
                    <Text style={{color: 'black', fontFamily: "Montserrat-Bold", fontSize:16}}>There are no upcoming activities</Text>
                  </Box>
                :
                  <FlatList
                    listKey={index => index}
                    data={resizeUpcomingData[0]}
                    scrollEnabled={true}
                    
                    renderItem={(item) => {
                      console.log("item activity", item);
                      return(
                        <CardActivity
                          data={item} 
                          userId={profile.data.id}
                          onPress={() => 
                            navigation.navigate(
                              "DetailActivity", 
                              { 
                                "title": "Detail Activity",
                                "data" : item,
                                "typeDetail": 1 ,
                                userId: profile.data.id,
                                fromHistory: false,
                                fromUpcoming: true
                              }
                            )
                        }
                        />
                      )
                    }}
                  />
              }
              <TouchableOpacity  onPress={() => 
                navigation.navigate(
                  "DetailViewAll", 
                  { 
                    "title": "All Activity"
                  }
                )
              }>
                <Box style={{backgroundColor:"#000", alignItems:"center", paddingVertical:8.5, marginTop: 16}}>
                  <Text
                    style={{
                      fontSize: 10,
                      color: "#FFF",
                      letterSpacing: 0.2,
                      fontFamily: "Montserrat-Bold",
                    }}
                    numberOfLines={1}
                  >
                    VIEW ALL
                  </Text>
                </Box>
              </TouchableOpacity>
            </Box>
          </Box>

          {dummyActivity.length === 0 && 
            <Box style={{height:10, backgroundColor:"#F8F9FC"}}/>
          }
          <Box>
            <Box style={{marginTop:20, paddingHorizontal:20 }}>
                <HStack style={{justifyContent:"space-between", alignItems:'center'}}>
                  <Text style={{color: 'black', fontFamily: "Montserrat-Bold"}}>Credit History</Text>
                  <TouchableOpacity onPress={() => navigation.navigate("TopUp")} style={{width:50,alignItems:"flex-end", paddingVertical:10}}>
                    <Text style={{color: 'black', fontFamily: "Montserrat-Regular", fontSize:10}}>View All</Text>
                  </TouchableOpacity>
                </HStack>
            </Box>
            <Box style={{marginTop:12}}>
            <FlatList
                listKey={index => index}
                data={resizeArrayTopUp[0]}
                renderItem={(item) => {
                  console.log("item activity", item);
                  return(
                    <TopUpHistoryList navigation={navigation} data={item}/>
                  )
                }}
              />
            </Box>
          </Box>
      </SafeAreaProvider>
      )
    }

    return (
      <Box style={{ backgroundColor: "white", flex: 1, paddingTop:16 }}>
        {isLoading 
          ?
            <Loading /> 
          : 
            <FlatList 
              ListHeaderComponent={component({navigation})}
            />
        }
      </Box>
    )
}


export default Activity;
