import React from "react";
import {
  View,
  Text,
  Actionsheet,
  Box,
  Button,
  HStack,
  Image,
} from "native-base";
import { image } from "../../../assets/images";

export default function InboxClaimReward({ isOpen, onClose, onDone }) {
  return (
    <View>
      <Actionsheet isOpen={isOpen} onClose={onClose}>
        <Actionsheet.Content h={340}>
          <Image
            source={image.bg_actionsSheet}
            style={{ width: 300, height: 300, position: "absolute", right: 0 }}
            alt="image"
          />
          <Image
            source={image.succes_ic}
            style={{ width: 170, height: 100 }}
            resizeMode={"contain"}
            alt="image"
          />
          <Box style={{ width: 270, alignItems: "center", marginTop: 8 }}>
            <Text
              style={{
                fontSize: 14,
                color: "#000",
                fontWeight: "bold",
                fontFamily: "Montserrat-Regular",
              }}
              alignItems="flex-start"
            >
              Claim was Successful
            </Text>
            <Text
              style={{
                fontSize: 12,
                color: "#000",
                fontWeight: "500",
                fontFamily: "Montserrat-Regular",
                textAlign: "center",
                marginTop: 10,
              }}
              alignItems="flex-start"
            >
              Lorem ipsum dolor sit amet, consectetur adipiscing elit.
              <Text
                style={{
                  fontWeight: "bold",
                }}
              ></Text>
            </Text>
          </Box>
          <HStack
            paddingTop="2"
            paddingBottom="2"
            space="2"
            justifyContent="center"
            alignItems="center"
          >
            <Image source={image.xp_icon} />
            <Text
              style={{
                fontFamily: "Montserrat-Bold",
                fontSize: 12,
              }}
            >
              800
            </Text>
          </HStack>
          <Button
            w={"90%"}
            bgColor={"#212121"}
            onPress={onDone}
            color={"#fff"}
            style={{ marginTop: 10, borderRadius: 0 }}
          >
            <Text
              style={{
                fontWeight: "bold",
                color: "#fff",
                fontFamily: "Montserrat-Regular",
              }}
            >
              Done
            </Text>
          </Button>
        </Actionsheet.Content>
      </Actionsheet>
    </View>
  );
}
