import React, { useState, useEffect } from "react";
import { StyleSheet } from "react-native";
import { View, Text, ScrollView, Pressable, HStack, Button, Actionsheet, Toast, Box } from "native-base";
import InboxClaimReward from "./InboxClaimReward";
import InboxInviteConfirmation from "../Inbox/InboxInviteConfirmation";
import { UserApi } from "../../utils/Api";
import { moderateScale } from "react-native-size-matters";

import { useDispatch, useSelector } from "react-redux";
import Selectors from "selectors";

const user = UserApi.client;

export default function InboxDetail({navigation, route}) {
  const [isPromoOpen, setPromoOpen] = useState(false);
  const [openInvitationSheet, setOpenInvitationSheet] = useState(false)

  const profile = useSelector(Selectors.getProfile);
  const { message, status, type, user_family_detail_id, activity_booking_id, user_id } = route.params.item
  const item = {
    message: message,
    status: status,
    type: type,
    user_family_detail_id: user_family_detail_id,
    activity_booking_id: activity_booking_id
  }

  useEffect(() => {
    console.log("params: ", route.params);

  }, [])

  const processInvite = (message) => {
    if(type == "family_invite") {
      user.post(`/family/invitation`, {
        familyId: user_family_detail_id,
        type: message
      })
        .then(res => {
          console.log("family invitation res: ", res);
          setOpenInvitationSheet(false)
          Toast.show({
            description: "You have accepted family invitation",
            duration: 4000
          })
          navigation.goBack()
        })
        .catch(err => {
          console.log("family invitation err: ", err.response.data.message);
          setOpenInvitationSheet(false)
          Toast.show({
            description: err.response.data.message,
            duration: 2000
          })
        })
    } else if(type == "activity_invite") {
      user.post(`/activity-booking/invitation`, {
        activityBookingId: activity_booking_id,
        type: message
      })
        .then(res => {
          console.log("activity invitation res: ", res);
          setOpenInvitationSheet(false)
          Toast.show({
            description: "You have accepted activity invitation",
            duration: 4000
          })
          navigation.goBack()
        })
        .catch(err => {
          console.log("activity invitation err: ", err.response.data.message);
          setOpenInvitationSheet(false)
          Toast.show({
            description: err.response.data.message,
            duration: 2000
          })
        })
    }
  }

  const renderPromo = () => {
    console.log('RENDER PROMO: ', route)
    return (
      <View style={styles.container}>
        <ScrollView contentContainerStyle={styles.scrollView}>
          <View style={styles.contentScrollView}>
            {/* <Text style={styles.headerTitle}>Description</Text> */}
            <Text style={styles.fillDescription}>{message}</Text>
            {/* <Text style={styles.headerTitle}>How to Use</Text>
            <Text style={styles.fillDescription}>{MOCK_FILL_DESCRIPTION}</Text>
            <Text style={styles.headerTitle}>Terms & Conditions</Text>
            <Text style={styles.fillDescription}>{MOCK_FILL_DESCRIPTION}</Text> */}
          </View>
        </ScrollView>
        {
          type == "family_invite" || type == "activity_invite" ? 
          status == "pending_invite" && (
            <View style={styles.claimConfirmation}>

            <Box marginBottom={moderateScale(10, .25)}>
              {type == "activity_invite" && <Text
                style={{
                  fontSize: 12,
                  // color: "#606060",
                  color: "#000",
                  marginBottom: 5,
                  fontFamily: "Montserrat-Regular",
                }}
              >
                {`Please be reminded that you will need a membership or day pass to enter premises`}
              </Text>}
            </Box>
              {user_id == profile.id &&<Pressable
                // disabled={true}
                style={styles.claimButton}
                onPress={() => setOpenInvitationSheet(true)}
              >
                <Text style={styles.claimText}>
                  {`View Invitation`}
                </Text>
              </Pressable>}
            </View>
          ) : (
            <View style={styles.claimConfirmation}>
              <Pressable
                style={styles.claimButton}
                onPress={() => setPromoOpen(true)}
              >
                <Text style={styles.claimText}>Claim</Text>
              </Pressable>
              <Pressable style={styles.cancelButton}>
                <Text style={styles.cancelText}>Cancel</Text>
              </Pressable>
            </View>
          )
        }

        <InboxClaimReward
          isOpen={isPromoOpen}
          onDone={() => setPromoOpen(false)}
        />

        <InboxInviteConfirmation 
          isOpen={openInvitationSheet}
          item={item}
          onClose={() => setOpenInvitationSheet(false)}
          onAccept={() => processInvite("accept")}
          onDecline={() => processInvite("reject")}
          status={status}
        /> 

        {/* <Actionsheet isOpen={openInvitationSheet} onClose={setOpenInvitationSheet(false)}>
          <Actionsheet.Content h={340}>
            <Image
              source={image.bg_actionsSheet}
              style={{ width: 300, height: 300, position: "absolute", right: 0 }}
              alt="image"
            />
            <Image
              source={image.succes_ic}
              style={{ width: 170, height: 100 }}
              resizeMode={"contain"}
              alt="image"
            />
            <Box style={{ width: 270, alignItems: "center", marginTop: 8 }}>
              <Text
                style={{
                  fontSize: 14,
                  color: "#000",
                  fontWeight: "bold",
                  fontFamily: "Montserrat-Regular",
                }}
                alignItems="flex-start"
              >
                Claim was Successful
              </Text>
              <Text
                style={{
                  fontSize: 12,
                  color: "#000",
                  fontWeight: "500",
                  fontFamily: "Montserrat-Regular",
                  textAlign: "center",
                  marginTop: 10,
                }}
                alignItems="flex-start"
              >
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                <Text
                  style={{
                    fontWeight: "bold",
                  }}
                ></Text>
              </Text>
            </Box>
            <HStack
              paddingTop="2"
              paddingBottom="2"
              space="2"
              justifyContent="center"
              alignItems="center"
            >
              <Image source={image.xp_icon} />
              <Text
                style={{
                  fontFamily: "Montserrat-Bold",
                  fontSize: 12,
                }}
              >
                800
              </Text>
            </HStack>
            <Button
              w={"90%"}
              bgColor={"#212121"}
              onPress={() => setPromoOpen(false)}
              color={"#fff"}
              style={{ marginTop: 10, borderRadius: 0 }}
            >
              <Text
                style={{
                  fontWeight: "bold",
                  color: "#fff",
                  fontFamily: "Montserrat-Regular",
                }}
              >
                Done
              </Text>
            </Button>
          </Actionsheet.Content>
        </Actionsheet> */}
      </View>
    );
  };

  return <View style={styles.container}>{renderPromo()}</View>;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  scrollView: {
    // flexGrow: 1,
    // padding: 16,
  },
  contentScrollView: {
    flexGrow: 1,
    padding: 16,
  },
  claimConfirmation: {
    justifyContent: "center",
    // alignItems: "center",
    padding: 16,
  },
  headerTitle: {
    color: "black",
    fontFamily: "Montserrat-Bold",
    fontSize: 12,
  },
  fillDescription: {
    color: "black",
    fontFamily: "Montserrat-Regular",
    fontSize: 12,
    paddingBottom: 16,
  },
  claimButton: {
    backgroundColor: "#1035BB",
    height: 42,
    marginBottom: moderateScale(4, .25),
    justifyContent: "center",
    alignItems: "center",
  },
  claimText: {
    fontFamily: "Montserrat-Regular",
    fontSize: 14,
    color: "white",
  },
  cancelButton: {
    justifyContent: "center",
    alignItems: "center",
    height: 42,
  },
  cancelText: {
    fontFamily: "Montserrat-Regular",
    fontSize: 14,
    color: "black",
  },
});

const MOCK_FILL_DESCRIPTION =
  "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).";
