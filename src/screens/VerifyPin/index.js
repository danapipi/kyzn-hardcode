import React, { useState, useRef } from "react";
import { View, Text } from "react-native";
import { moderateScale, scale } from "react-native-size-matters";
import CodeInput from "react-native-confirmation-code-input";

const VerifyPin = ({ navigation }) => {
  const [pin, setPin] = useState("");
  const codeInputRef = useRef(null);

  const verifyPin = (pin) => {
    console.log("PIN", pin);
    // call api in here
  };

  // clearInput = () => {
  //     this.refs.codeInputRef.clear();
  //   };
  return (
    <View style={{ backgroundColor: "white", flex: 1 }}>
      <Text
        style={{
          color: "black",
          fontSize: moderateScale(16, 0.25),
          fontFamily: "Montserrat-Regular",
          fontWeight: "600",
          textAlign: "center",
          marginBottom: moderateScale(29),
          marginTop: moderateScale(62)
        }}
      >
        Please enter your PIN number
      </Text>
      <CodeInput
        ref={codeInputRef}
        activeColor="transparent"
        inactiveColor="transparent"
        autoFocus={false}
        ignoreCase={true}
        inputPosition="center"
        size={50}
        space={15}
        secureTextEntry={true}
        onCodeChange={(value) => setPin(value)}
        onFulfill={(pin) => {
          if (pin.length === 4) {
            verifyPin(pin);
          }
        }}
        containerStyle={{
          marginHorizontal: moderateScale(16, 0, 25),
          // backgroundColor: 'red',
          flex: 0,
          marginTop: moderateScale(0, 0, 25),
          // height: moderateScale(24, 0.25)
        }}
        codeInputStyle={{
          borderWidth: 1,
        //   backgroundColor: "rgba(69,83,95, 0.99)",
          borderRadius: 1,
          borderColor: "#7C858B",
          fontWeight: "bold",
          letterSpacing: 0.5,
          textAlign: "center",
          fontSize: moderateScale(21, 0.25),
          color: "black",
        }}
        codeLength={4}
        keyboardType="number-pad"
        selectionColor={"#1F93FF"}
      />
    </View>
  );
};

export default VerifyPin;
