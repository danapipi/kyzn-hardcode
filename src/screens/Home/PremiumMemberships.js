import React from "react";
import { StyleSheet } from "react-native";
import { Box, Button, FlatList, Text } from "native-base";
import { useNavigation } from "@react-navigation/core";
import Commons from "../../utils/common";

const styles = StyleSheet.create({
  container: {
    width: "40%",
    height: 200,
    flex: 1,
    marginTop: 10,
  },
  flex: {
    flex: 1,
    alignItems: "center",
    justifyContent: "flex-end",
    paddingBottom: 15,
  },
});

const MOCK_DATA = [
  {
    price: "499.99",
    description: "3 Months Platinum Pass",
    type: "platinum",
  },
  {
    price: "499.99",
    description: "3 Months Platinum Pass",
    type: "gold",
  },
  {
    price: "499.99",
    description: "3 Months Platinum Pass",
    type: "silver",
  },
];

const PremiumMemberships = () => {
  const navigation = useNavigation();
  return (
    <FlatList
      ItemSeparatorComponent={() => <Box width="5" />}
      layout="default"
      data={MOCK_DATA}
      renderItem={({ item: { price, description, type } }) => (
        <Button
          style={styles.container}
          backgroundColor="button.blue"
          _pressed={{
            backgroundColor: "button.blue:alpha.20",
          }}
          onPress={() =>
            navigation.navigate("Memberships", {
              screen: "MembershipView",
              params: { membershipType: type },
            })
          }>
          <Box style={styles.flex}>
            <Text color="white" fontWeight="bold">
              {`${Commons.convertToRupiah(price)}`}
            </Text>
            <Text color="white">{description}</Text>
          </Box>
        </Button>
      )}
      horizontal={true}
      showsHorizontalScrollIndicator={false}
    />
  );
};

export default PremiumMemberships;
