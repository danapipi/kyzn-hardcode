import React, { useState, useEffect, useRef } from "react";
import {
  Dimensions,
  ActivityIndicator,
  StyleSheet,
  Platform,
} from "react-native";
import {
  Box,
  Text,
  Button,
  FlatList,
  Image,
  ZStack,
  VStack,
  Row,
} from "native-base";
import PropTypes from "prop-types";
import CategoryCard from "components/ui/CategoryCard";
import UserInfo from "components/ui/UserInfo";
import UserTracking from "components/ui/UserTracking";
import ProgressLevel from "components/ui/ProgressLevel";
import { image } from "images";
import { moderateScale } from "react-native-size-matters";
import { connect, useDispatch, useSelector } from "react-redux";
import Selectors from "selectors";
import Actions from "actions";
import { category } from "actions/product";
import Loading from "components/ui/Loading";
import TopUpCard from "components/ui/TopUpCard";
import PanoramaCard from "components/ui/PanoramaCard";
import Memberships from "./Memberships";
import Commons from "../../utils/common";
import _ from "lodash";
import HomeActivityCard from "components/ui/HomeActivityCard";
import HomeActivityCard1 from "components/ui/HomeActivityCard1";
import PersonalTrainerCard from "components/ui/PersonalTrainerCard";
import HomeBanner from "components/ui/HomeBanner";
import HomeMappingActivityCard from "components/ui/HomeMappingActivityCard";
import { useIsFocused } from "@react-navigation/core";
import RNStorage from "../../utils/storage";
import messaging from "@react-native-firebase/messaging";
import saveFcmToken from "../../utils/SaveFCMToken";

const styles = StyleSheet.create({
  main: {
    backgroundColor: "white",
    flex: 1,
  },
  list: {
    marginHorizontal: 0,
  },
});

const Home = ({ navigation }) => {
  const dispatch = useDispatch();
  const data = useSelector(state => state.PRODUCT.category);
  const profile = useSelector(state => state.AUTH.profile);
  const dataUpcoming = useSelector(state => state.PRODUCT.upcomingSchedule);
  const dataFeaturedWorker = useSelector(state => state.WORKER.featuredWorker);
  let dataWorker =
    dataFeaturedWorker.featuredWorker.length > 0
      ? dataFeaturedWorker.featuredWorker
      : [];
  console.log("data featuredWorker :", dataFeaturedWorker, dataWorker);
  const dataSchedule = useSelector(state => state.PRODUCT.activityScheduleAll);
  const dataUniqueFamily = useSelector(state => state.FAMILY.uniqueFamily);
  // const dataAllSchedule = useSelector((state) => state.PRODUCT.activityScheduleAllWithoutUserId);
  const isMember =
    profile.data.user_memberships &&
    !(
      profile.data.user_memberships.length == 1 &&
      _.get(profile, "data.user_memberships.0.membership.name", "") ==
        "Non-Member"
    );

  const isLoading = profile.isLoading;
  let balance = _.get(profile, "data.balance", "");
  let name = _.get(profile, "data.name", "");
  let photo = _.get(profile, "data.photo", "");
  let coin = _.get(profile, "data.coin", "");
  let xp = _.get(profile, "data.xp", "");
  console.log("balance", profile);
  const {
    class_activity_type_id,
    panorama_activity_type_id,
    pt_activity_type_id,
    rent_court_activity_type_id,
    wellness_category_id,
    fitness_category_id,
    sport_category_id,
  } = useSelector(state => state.PRODUCT.activityTypeId.activityTypeId);

  const [dataDummyCategory, setDataDummyCategory] = useState([
    {
      id: wellness_category_id,
      name: "WELLNESS",
      image: image.wellnessIcon,
      background_image: image.wellnessBG,
      active: true,
    },
    {
      id: sport_category_id,
      name: "SPORT",
      image: image.sportIcon,
      background_image: image.sportBG,
      active: true,
    },
    {
      id: fitness_category_id,
      name: "FITNESS",
      image: image.fitnessIcon,
      background_image: image.fitnessBG,
      active: true,
    },
    {
      id: 4,
      name: "BOOK COURTS",
      image: image.bookingIcon,
      background_image: image.bookingBG,
      active: true,
    },
    {
      id: 5,
      name: "NUTRITION",
      image: image.nutritionIcon,
      background_image: image.nutritionBG,
      active: false,
    },
    {
      id: 6,
      name: "EDUCATION",
      image: image.educationIcon,
      background_image: image.educationBG,
      active: true,
    },
  ]);
  const [numCol, setNumCol] = useState(2);
  const countRef = useRef(0);
  const [dataInfo, setDataInfo] = useState([
    {
      id: 1,
      title: "Schedule",
      image: image.scheduleImage,
    },
    // {
    //   id: 2,
    //   title: "Zeni Store",
    //   image: image.zeniImage,
    // },
    // {
    //   id: 3,
    //   title: "Panorama",
    //   image: image.panoramaImage,
    // },
  ]);

  const onRefresh = () => {
    let userId = _.get(profile.data, "id", null);

    // dispatch(Actions.profile());
    // dispatch(Actions.category());
    // dispatch(Actions.featuredActivity());
    // dispatch(Actions.featuredMembership());
    // dispatch(Actions.fetchAllFamilies());
    // dispatch(Actions.fetchUserFamily());
    // dispatch(Actions.topupHistory());
    // dispatch(Actions.activityScheduleAll());
    // // dispatch(Actions.upcomingSchedule(userId));
    // dispatch(Actions.fetchInbox(userId));
    // dispatch(Actions.fetchFamilyUnique());
    // dispatch(Actions.activityScheduleAllWithoutUserId());
    // dispatch(Actions.featuredWorker());
  };

  const token = useSelector(Selectors.getToken);
  const dataActivityBanner = useSelector(Selectors.getFeaturedActivity);
  console.log("dataActivityBanner >>>> : ", dataActivityBanner);

  const dataFirst = useSelector(state => state.AUTH.firstTime);

  useEffect(() => {
    // generateFCMToken();
    console.log("cek token HOME PAGE", dataFirst);
    console.log("dataUniqueFamily: ", dataUniqueFamily);
    // console.log("dataAllSchedule: ", dataAllSchedule);

    let userId = _.get(profile, "data.id", null);

    // dispatch(Actions.profile());
    // dispatch(Actions.category());
    // dispatch(Actions.featuredActivity());
    // dispatch(Actions.fetchAllFamilies());
    // dispatch(Actions.fetchUserFamily());
    // dispatch(Actions.topupHistory());
    // dispatch(Actions.activityScheduleAll());
    // dispatch(Actions.upcomingSchedule(userId));
    // dispatch(Actions.fetchInbox(userId));
    // dispatch(Actions.featuredMembership());
    // dispatch(Actions.fetchFamilyUnique());
    dispatch(Actions.firstTime("true"));
    // dispatch(Actions.activityTypeById());

    RNStorage.setFirstTime("true");
    // dispatch(Actions.featuredWorker());
    // dispatch(Actions.activityScheduleAllWithoutUserId());

    const willFocusSubscription = navigation.addListener("focus", () => {
      // dispatch(Actions.topupHistory());
      // dispatch(Actions.profile());
      // dispatch(Actions.upcomingSchedule(userId));
      dispatch(Actions.firstTime(true));
      RNStorage.setFirstTime("true");
    });

    return willFocusSubscription;
  }, [token]);

  const generateFCMToken = () => {
    try {
      messaging()
        ?.getToken()
        .then(fcmToken => {
          console.log("HOME FCM TOKEN: ", fcmToken);
          if (token) {
            saveFcmToken(fcmToken);
          }
        })
        .catch(error => {
          throw new Error(error);
        });
    } catch (error) {
      console.log("GENERATE FCM TOKEN AT HOME: ", error);
    }
  };

  // flag for adjust UI if panorama disable
  let disablePanorama = true;

  const Header = () => (
    <VStack
      style={{
        marginTop: disablePanorama
          ? Platform.OS === "android"
            ? moderateScale(20, 0.25)
            : moderateScale(0, 0.25)
          : moderateScale(20, 0.25),
      }}>
      {!disablePanorama && (
        <Image
          source={image.background}
          // size="xl"
          alt="background"
          resizeMode="cover"
          style={{
            width: "100%",
            height: moderateScale(150, 0.25),
            marginTop: moderateScale(-70, 0.25),
          }}
          position="absolute"
        />
      )}

      <UserTracking
        userphoto={photo}
        disablePanorama={disablePanorama}
        name={name}
        exp={xp}
        coin={coin}
      />
      {!disablePanorama && <UserInfo name={name} exp={xp} coin={coin} />}
      <ProgressLevel res={profile} />
      {/* <Box style={{ height: 100 }}>
        <Image
          resizeMode="contain"
          source={require("images/dummyStrip.png")}
          style={{
            width: "100%",
            position: "absolute",
            top: -120,
            right: -20,
          }}
          alt="image"
        />
      </Box> */}
      <TopUpCard
        creditValue={Commons.convertToRupiah(balance)}
        navigation={navigation}
        cardStyle={2}
      />
      <FlatList
        data={dataDummyCategory}
        numColumns={numCol}
        scrollEnabled={false}
        renderItem={_item => {
          return (
            <CategoryCard
              navigation={navigation}
              data={_item}
              numCol={numCol}
            />
          );
        }}
      />
      <Box>
        <PanoramaCard data={dataInfo} navigation={navigation} cardStyle={2} />
      </Box>
      <Box>{/* <HomeBanner label={"Info & Promo"} /> */}</Box>
      {/* <Box  style={{ marginBottom: 32 }}>
        <HomeActivityCard1
          data={dataSchedule}
          dataDiff={dataUpcoming}
          label={"Available Schedule"}
          navigation={navigation}
          onPressViewAll={() => navigation.navigate("DetailSchedule")}
        />
      </Box> */}
    </VStack>
  );

  const items = [
    {
      title: "Premium",
      component: [
        <Memberships />,
        <HomeMappingActivityCard
          dataAllBanner={dataActivityBanner}
          navigation={navigation}
        />,
        <PersonalTrainerCard
          label={"Personal Trainer"}
          data={dataWorker}
          navigation={navigation}
          onPressViewAll={() => navigation.navigate("PersonalTrainers")}
        />,
        // <HomeActivityCard label={"Personal Trainer"} />,
        // <HomeActivityCard label={"Wellness"} />,
        // <HomeActivityCard label={"Sport"} />,
        // <HomeActivityCard label={"Fitness"} />,
        // <HomeActivityCard label={"Workout Video"} />,
      ],
      onViewAll: () => {
        navigation.navigate("Memberships");
      },
    },
  ];

  return (
    <Box style={styles.main} safeArea>
      {isLoading ? (
        <Loading />
      ) : (
        <FlatList
          style={[
            styles.list,
            {
              marginTop: disablePanorama
                ? moderateScale(-10, 0.25)
                : moderateScale(0, 0.25),
            },
          ]}
          refreshing={false}
          onRefresh={() => onRefresh()}
          ListHeaderComponent={Header()}
          data={items}
          renderItem={({ item: { title, component, onViewAll } }) => {
            // console.log('Components: ', component)
            return (
              <Box marginLeft={3}>
                <Row alignItems="center" justifyContent="space-between">
                  <Text fontWeight={300}>{title}</Text>
                  <Button
                    onPress={onViewAll}
                    variant="ghost"
                    _text={{
                      fontSize: "sm",
                      color: "black",
                    }}
                    _pressed={{
                      _text: {
                        color: "black:alpha.20",
                      },
                      backgroundColor: "transparent",
                    }}>
                    View All
                  </Button>
                </Row>
                {component.map(item => item)}
              </Box>
            );
          }}
        />
      )}
    </Box>
  );
};

Home.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default Home;
