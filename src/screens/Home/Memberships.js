import React, { useEffect } from "react";
import { StyleSheet, ImageBackground, Dimensions } from "react-native";
import { Box, Button, FlatList, Spinner, Text } from "native-base";
import { useNavigation } from "@react-navigation/core";
import { useDispatch, useSelector } from "react-redux";
import Selectors from "selectors";
import Actions from "actions";
import { getScreenWidth } from "utils/size";
import { dummyImages, image } from "images";
import Commons from "../../utils/common";
import _ from "lodash";

const { width } = Dimensions?.get("screen");

const styles = StyleSheet.create({
  container: {
    width: width * 0.45,
    height: width * 0.55,
    marginTop: 10,
  },
  flex: {
    flex: 1,
    alignItems: "center",
    justifyContent: "flex-end",
    paddingBottom: 15,
  },
});

const Memberships = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const membershipCategories = useSelector(Selectors.getMembershipCategories);
  const isLoading = useSelector(Selectors.getIsMembershipCategoriesLoading);
  // const dataFeaturedMemberships = useSelector(Selectors.getFeaturedMemberships);
  // const featuredMembershipIsLoading = useSelector(Selectors.getFeaturedMembershipIsLoading);
  const dataFeaturedMemberships = useSelector(
    state => state.PRODUCT.featuredMembership.data,
  );
  console.log("dataFeaturedMemberships >>> ", dataFeaturedMemberships);

  const featuredMembershipIsLoading = useSelector(
    state => state.PRODUCT.featuredMembership.isLoading,
  );

  // useEffect(() => {
  //   dispatch(Actions.fetchMembershipCategories());
  // }, [dispatch]);
  console.log(
    "membershipCategories",
    membershipCategories,
    dataFeaturedMemberships,
    featuredMembershipIsLoading,
  );
  // if (isLoading) {
  if (featuredMembershipIsLoading) {
    return <Spinner accessibilityLabel="Loading posts" />;
  }

  const handlePress = item => {
    console.log("membershipppp  ", item);
    let membershipId = _.get(item, "membership_id", null);
    let membership_package_id = _.get(item, "membership_package_id", null);
    let membership_package_category_id = _.get(
      item,
      "membership_package_category_id",
      null,
    );
    let membership_category_id = _.get(item, "membership_category_id", null);

    if (!_.isNull(membershipId)) {
      // console.log("item item ini heree");
      let categoryId = _.get(item, "membership.membership_category_id", "");
      navigation.navigate("MembershipView", {
        categoryId,
        membershipId,
      });
    } else if (!_.isNull(membership_package_id)) {
      let dataMembershipPackage = _.get(item, "membership_packages", null);
      if (!_.isNull(dataMembershipPackage)) {
        navigation.navigate("MembershipViewGroup", {
          data: dataMembershipPackage,
        });
      }
    } else if (!_.isNull(membership_package_category_id)) {
      let categoryPackageId = _.get(
        item,
        "membership.membership_package_category_id",
        membership_package_category_id,
      );
      navigation.navigate("MembershipPackagesList", {
        id: categoryPackageId,
        type: "family",
      });
    } else if (!_.isNull(membership_category_id)) {
      let categoryId = _.get(
        item,
        "membership.membership_category_id",
        membership_category_id,
      );
      navigation.navigate("MembershipPackagesList", {
        id: categoryId,
        type: "personal",
      });
    }
  };

  return (
    <FlatList
      ItemSeparatorComponent={() => <Box width="5" />}
      layout="default"
      // data={membershipCategories}
      data={dataFeaturedMemberships}
      // renderItem={({ item: { id, name, description, type } }) => (
      // renderItem={({ item: { id, image_url, membership: {name, price} } }) => (
      renderItem={({ item }) => (
        <Button
          style={styles.container}
          backgroundColor="#808080"
          _pressed={{
            backgroundColor: "button.blue:alpha.20",
          }}
          onPress={
            () => handlePress(item)
            // navigation.navigate("Memberships", {
            //   screen: "MembershipPackagesList",
            //   params: { id },
            // })
          }>
          <ImageBackground
            source={
              item?.membership?.portrait_image_url
                ? { uri: item?.membership?.portrait_image_url }
                : image.img_memberhip_potrait
            }
            style={[
              styles.container,
              { marginTop: 0, paddingLeft: 10, paddingRight: 10 },
            ]}>
            <Box style={styles.flex}>
              <Text color="white" textAlign="center">
                {Commons.convertToRupiah(
                  item?.membership?.price ? item?.membership?.price : 0,
                )}
              </Text>
              <Text
                numberOfLines={2}
                color="white"
                fontWeight={300}
                textAlign="center">
                {item?.membership?.name ? item?.membership?.name : "N/A"}
              </Text>
              {/* <Text color="white" fontWeight={300} textAlign="center">
                {name}
              </Text>
              <Text color="white" textAlign="center">
                {description}
              </Text> */}
            </Box>
          </ImageBackground>
        </Button>
      )}
      horizontal={true}
      showsHorizontalScrollIndicator={false}
    />
  );
};

export default Memberships;
