import React, { useEffect } from "react";
import Loading from "components/ui/Loading";
import AsyncStorage from "@react-native-community/async-storage";
import { useNavigation } from "@react-navigation/core";
import { useDispatch, useSelector } from "react-redux";
import _ from 'lodash';

const LoadingScreen = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const type = {
    firstTime: "KYZN.FIRST",
  };

  useEffect(() => {
    let nav = navigation.getState()

  }, []);

  return (
    <Loading />
  );
};

export default LoadingScreen;
