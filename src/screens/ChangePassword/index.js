import React, { useState } from "react";
import { View, Text, Alert } from "react-native";
import { SafeAreaProvider } from "react-native-safe-area-context";
import { moderateScale } from "react-native-size-matters";
import ButtonCustom from "components/ui/ButtonCustom";
import InputField from "components/ui/InputField";
import { useDispatch } from "react-redux";
import { UserApi } from "../../utils/Api";



const user = UserApi.client

const ChangePassword = ({navigation}) => {
  const dispatch = useDispatch();

  const [oldPass, setOldPass] = useState("");
  const [showOldPassword, setShowOldPassword] = useState(false);
  const [oldPasswordError, setOldPasswordError] = useState(false);
  const [oldPasswordErrorValue, setOldPasswordErrorValue] = useState("");

  const [pass, setPass] = useState("");
  const [showPassword, setShowPassword] = useState(false);
  const [passwordError, setPasswordError] = useState(false);
  const [passwordErrorValue, setPasswordErrorValue] = useState("");

  const [confirmPass, setConfirmPass] = useState("");
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);
  const [confirmPasswordError, setConfirmPasswordError] = useState(false);
  const [confirmPasswordErrorValue, setConfirmPasswordErrorValue] =
    useState("");

  const [loading, setIsLoading] = useState(false);

  const changePasswordHide = (val) => {
    if (val === "pass") {
      setShowPassword(!showPassword);
    } else if (val === "confirmPass") {
      setShowConfirmPassword(!showConfirmPassword);
    } else {
      setShowOldPassword(!showOldPassword);
    }
  };

  const checkPassword = (key, value) => {
    if (value.length < 6) {
      setPasswordError(true);
      setPasswordErrorValue("Password must be minimal 6 characters");
    } else {
      setPasswordError(false);
      setPasswordErrorValue("");
    }
    if (confirmPass !== "") {
      if (value === confirmPass) {
        setConfirmPasswordError(false);
        setConfirmPasswordErrorValue("");
      } else {
        setConfirmPasswordError(true);
        setConfirmPasswordErrorValue("Confirm passwords do not match");
      }
    }
  };

  const checkOldPassword = (key, value) => {
    if (value.length < 6) {
      setOldPasswordError(true);
      setOldPasswordErrorValue("Password must be minimal 6 characters");
    } else {
      setOldPasswordError(false);
      setOldPasswordErrorValue("");
    }
  };

  const confirmPassword = (key, value) => {
    if (value.length < 6) {
      setConfirmPasswordError(true);
      setConfirmPasswordErrorValue(
        "Confirm Password must be minimal 6 characters"
      );
    } else {
      setConfirmPasswordError(false);
      setConfirmPasswordErrorValue("");
    }
    if (pass !== value) {
      setConfirmPasswordError(true);
      setConfirmPasswordErrorValue("Confirm passwords do not match");
    } else {
      setConfirmPasswordError(false);
      setConfirmPasswordErrorValue("");
    }
  };

  const changePass = () => {
    setIsLoading(true);
    setIsLoading(false);
    navigation.pop()
    // user
    //   .put(`auth/change-password`, {
    //     oldPassword: oldPass,
    //     newPassword: pass,
    //   })
    //   .then((res) => {
    //     console.log("change password response", res);
    //     setIsLoading(false);
    //     navigation.pop()
    //   })
    //   .catch((err) => {
    //     Alert.alert("Error", err.response.data.message, [
    //       {
    //         text: "OK",
    //         onPress: () => console.log("OK Pressed"),
    //         style: "cancel",
    //       },
    //     ]);
    //     setIsLoading(false);
    //   });
  };

  const validation =
    pass === confirmPass &&
    !passwordError &&
    !confirmPasswordError &&
    pass !== "" &&
    confirmPass !== "" &&
    oldPass !== "" &&
    !oldPasswordError
      ? true
      : false;

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: "white",
        paddingVertical: moderateScale(20),
      }}
    >
      <InputField
        placeholder={"Old Password"}
        isPassword={true}
        onChangeText={(val) => {
          setOldPass(val);
          checkOldPassword("oldPassword", val);
        }}
        value={oldPass}
        changeShowPassword={() => changePasswordHide("oldPass")}
        showPassword={showOldPassword}
        secureTextEntry={!showOldPassword}
        error={oldPasswordError}
        errorMessage={oldPasswordErrorValue}
        showIcon={true}
        iconValue={"password"}
        // containerBgcolor={"blue"}
        containerMarginBottom={0}
      ></InputField>
      <InputField
        placeholder={"New Password"}
        isPassword={true}
        onChangeText={(val) => {
          setPass(val);
          checkPassword("password", val);
        }}
        value={pass}
        changeShowPassword={() => changePasswordHide("pass")}
        showPassword={showPassword}
        secureTextEntry={!showPassword}
        error={passwordError}
        errorMessage={passwordErrorValue}
        showIcon={true}
        iconValue={"password"}
        // containerBgcolor={"blue"}
        containerMarginBottom={0}
      ></InputField>
      <InputField
        placeholder={"Confirm New Password"}
        isPassword={true}
        onChangeText={(val) => {
          setConfirmPass(val);
          confirmPassword("confirmPassword", val);
        }}
        value={confirmPass}
        changeShowPassword={() => changePasswordHide("confirmPass")}
        showPassword={showConfirmPassword}
        secureTextEntry={!showConfirmPassword}
        error={confirmPasswordError}
        errorMessage={confirmPasswordErrorValue}
        showIcon={true}
        iconValue={"password"}
        // containerBgcolor={"blue"}
      ></InputField>
      <View style={{ flex: 2, justifyContent: "flex-end" }}>
        {validation ? (
          <ButtonCustom
            label={"Submit"}
            onPressButton={() => changePass()}
            // buttonMarginHorizontal={24}
            buttonMarginBottom={0}
            buttonMarginTop={14}
            isLoading={loading}
          ></ButtonCustom>
        ) : (
          <ButtonCustom
            label={"Submit"}
            disableButton={true}
            buttonColor={"#838586"}
            // buttonMarginHorizontal={24}
            buttonMarginBottom={0}
            buttonMarginTop={14}
            isLoading={loading}
          ></ButtonCustom>
        )}
      </View>
    </View>
  );
};

export default ChangePassword;
