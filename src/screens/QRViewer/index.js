import {
  Box,
  Button,
  CloseIcon,
  Icon,
  IconButton,
  Image,
  Spinner,
  Text,
  useTheme,
} from "native-base";
import React, { useState, useEffect, useCallback, useRef } from "react";
import {
  ImageBackground,
  StyleSheet,
  View,
  Animated,
  Dimensions,
  AppState,
} from "react-native";
import { image } from "images";
import QRCode from "react-native-qrcode-svg";
import { getScreenWidth, getScreenHeight } from "utils/size";
import Carousel, { Pagination } from "react-native-snap-carousel";
import { useDispatch, useSelector } from "react-redux";
import Actions from "actions";
import Selectors from "selectors";
import { familyService } from "../../services";
import { CountdownCircleTimer } from "react-native-countdown-circle-timer";
import BackgroundTimer from "react-native-background-timer";

const styles = StyleSheet.create({
  background: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  carousel: {
    justifyContent: "center",
    alignItems: "center",
  },
  paginationDot: {
    width: 10,
    height: 10,
    borderRadius: 5,
  },
});

const QRViewer = ({ navigation }) => {
  const dispatch = useDispatch();

  const appState = useRef(AppState.currentState);
  const [appStateVisible, setAppStateVisible] = useState(appState.current);

  const { user_family_details: familyMembers = [] } = useSelector(
    Selectors.getUserFamily,
  );
  const dataFamilies = useSelector(state => state.FAMILY);
  const dataUniqueFamily = useSelector(state => state.FAMILY.uniqueFamily);
  const profile = useSelector(state => state.AUTH.profile.data);
  console.log("dataUniqueFamily: >>> ", dataUniqueFamily?.data[0]?.qr_string);
  // const dataQR = dataFamilies !== undefined ? dataFamilies.user.data : []
  const dataQR = dataUniqueFamily?.data;
  console.log("dataQR >>>: ", dataQR[0]?.qr_string);

  const isLoading = useSelector(Selectors.getIsLoadingUserFamily);
  console.log(
    "family members",
    familyMembers,
    dataFamilies,
    dataQR,
    useSelector(state => state.FAMILY),
  );
  const size = getScreenWidth() / 2.2;
  const logoSize = size / 3;
  const [currentIndex, setCurrentIndex] = useState(0);
  const { colors } = useTheme();

  const [countdownTimer, setCountDownTime] = useState(0);

  useEffect(() => {
    let interval = null;

    if (countdownTimer === 0) {
      dispatch(Actions.fetchFamilyUnique({ role: "all" }));
      setCountDownTime(30);
    } else {
      interval = setInterval(() => {
        setCountDownTime(seconds => seconds - 1);
      }, 1000);
    }

    const subscription = navigation?.addListener("blur", () => {
      clearInterval(interval);
      BackgroundTimer.stopBackgroundTimer();
    });

    const listenerForeGround = AppState.addEventListener(
      "change",
      nextAppState => {
        if (
          appState.current.match(/inactive|background/) &&
          nextAppState === "active"
        ) {
          console.log("AppState Active", appState?.current);
          BackgroundTimer.stopBackgroundTimer();
          clearInterval(interval);
          if (countdownTimer === 0) {
            dispatch(Actions.fetchFamilyUnique({ role: "all" }));
            setCountDownTime(30);
          } else {
            interval = setInterval(() => {
              setCountDownTime(seconds => seconds - 1);
            }, 1000);
          }
        }

        appState.current = nextAppState;
        setAppStateVisible(appState.current);
        console.log("AppState Background", appState.current);
        if (appState?.current === "background") {
          clearInterval(interval);
          BackgroundTimer.runBackgroundTimer(() => {
            //code that will be called every 1 seconds
            if (countdownTimer === 0) {
              dispatch(Actions.fetchFamilyUnique({ role: "all" }));
              setCountDownTime(30);
            } else {
              setCountDownTime(seconds => seconds - 1);
            }
          }, 1000);
        }
      },
    );

    profile.qr_string =
      "U2FsdGVkX1/JwImmnMw2T4NSMsa0RJISRjVdX8P/1P5H0xRny+SvaRfIK6HH/N23pG+5Zkp15rjokjEQ6JhRwRGk/zc/q4QUevc0ecAuasGPCV6sqIcWYmpm9+fPgFodK8DP9LWx0zkUAoFEN2fi3QMj92HCkLSbFkguAZceYKw=";

    return () => {
      subscription;
      clearInterval(interval);
      listenerForeGround.remove();
    };
  }, [countdownTimer, appState]);

  // useEffect(() => {
  //   const callEveryThirtySeconds = setInterval(async () => {
  //     console.log("Call Every 30s");
  //     dispatch(Actions.fetchFamilyUnique({ role: "all" })); // -------------> MY DISPATCH
  //   }, 30000);
  //   const subscription = navigation?.addListener("blur", () => {
  //     clearInterval(callEveryThirtySeconds);
  //   });
  //   return () => {
  //     subscription;
  //     clearInterval(callEveryThirtySeconds);
  //   };
  // }, []);

  const renderLoading = () => <Spinner />;
  const renderContent = () => (
    <>
      <IconButton
        icon={<CloseIcon size={6} />}
        position="absolute"
        top={3}
        left={3}
        onPress={() => navigation.goBack()}
        zIndex={20}
      />
      <View
        style={{
          width: Dimensions?.get("screen")?.width * 0.09,
          height: Dimensions?.get("screen")?.width * 0.09,
          position: "absolute",
          top: 15,
          right: 15,
          justifyContent: "center",
          alignItems: "center",
          borderRadius: (Dimensions?.get("screen")?.width * 0.09) / 2,
          borderWidth: 2,
        }}>
        <Text style={{ fontWeight: "500" }}>{countdownTimer}</Text>
      </View>
      <Box flex={1} alignItems="center" justifyContent={"center"}>
        <Box backgroundColor="white" padding={10} top={45}>
          <QRCode
            value={profile.qr_string}
            logo={image.qrLogo}
            logoSize={logoSize}
            size={size}
          />
          <Text marginTop={5} textAlign="center" fontWeight={300}>
            {profile.name}
          </Text>
        </Box>
      </Box>
      <Box flex={0.35} />
      {/* <Button
        width="80%"
        height="7%"
        position="absolute"
        bottom={5}
        backgroundColor="button.black"
        onPress={() => navigation.navigate("ScanQR")}
        // TODO: Replace this with white scan QR icon
        leftIcon={<Image source={image.scan_qr} size={5} />}>
        Scan QR Code
      </Button> */}
    </>
  );

  return (
    <Box flex={1} safeArea>
      <ImageBackground style={styles.background} source={image.qrBackground}>
        {isLoading ? renderLoading() : renderContent()}
      </ImageBackground>
    </Box>
  );
};

export default QRViewer;
