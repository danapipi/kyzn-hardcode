import React, { useState, useEffect, useReducer } from "react";
import {
  Dimensions,
  ActivityIndicator,
  Text,
  TouchableOpacity,
  Alert,
} from "react-native";
import {
  Box,
  Button,
  ScrollView,
  HStack,
  Image,
  VStack,
  Actionsheet,
  WarningIcon,
} from "native-base";
import _, { toInteger, isString, isNull, find } from "lodash";
import PropTypes from "prop-types";
import { useIsFocused } from "@react-navigation/native";
import { backgroundColor } from "styled-system";
import { useRoute } from "@react-navigation/core";
import moment from "moment";
import Actions from "actions";
import { image } from "../../../assets/images";
import { useDispatch, useSelector } from "react-redux";
import Commons from "../../utils/common";
import { UserApi } from "../../utils/Api";
import { moderateScale } from "react-native-size-matters";

const userapi = UserApi.client;

const Payment = ({ navigation }) => {
  const [membership_status, setStatusMember] = useState(1);
  const dispatch = useDispatch();
  const [isOpen, setIsOpen] = useState(false);
  const [dataSucces, setDataSucces] = useState("");
  const [differenceBalance, setDifferenceBalance] = useState(0);
  const [openInsufficientBalanceAS, setOpenInsufficientBalanceAS] =
    useState(false);
  const [bookLoading, setBookLoading] = useState(false)

  const [_update, forceUpdate] = useReducer((x) => x + 1, 0); // force the screen to update
  const isFocused = useIsFocused();

  // TODO: make the screen refresh the profile data
  // this includes re-calling the profile API so the balance value will be updated
  const profile = useSelector((state) => state.AUTH.profile);
  const dataUpcoming = useSelector((state) => state.PRODUCT.upcomingSchedule)
  const dataPastSchedule = useSelector((state) => state.PRODUCT.pastSchedule)
  const balance = profile.data.balance;
  const userId = profile.data.id;


  const { 
    class_activity_type_id, 
    panorama_activity_type_id, 
    pt_activity_type_id, 
    rent_court_activity_type_id
  } = useSelector((state) => state.PRODUCT.activityTypeId.activityTypeId);

  const [couponApplied, setCouponApplied] = useState("");
  const [isCouponValid, setIsCouponValid] = useState(null);
  const [discountFromCoupon, setDiscountFromCoupon] = useState(0);
  const [discountType, setDiscountType] = useState("amount");
  const [couponId, setCouponId] = useState(null); // only for synchronizing on clearing coupon
  const [newPrice, setNewPrice] = useState(0);

  let route = useRoute();
  let data = route.params.data;
  let type = route.params.type;
  // let realPrice = _.get(route.params, "realPrice", 0);;
  let coupons = _.get(route.params, "coupons", []);
  let best_coupon_id = _.get(route.params, "best_coupon_id", null)
  let priceParams = _.get(route.params, "price", null);
  let timeDuration = _.get(route.params, "timeDuration", null);
  let startTime = _.get(route.params, "startTime", null);
  let activity = route.params.activity;
  let selectedUser = _.get(route, "params.selectedUser", profile.data.id);
  let stepMultiplier = _.get(route, "params.multipler", 1)
  let activityWorkerId = _.get(route, "params.activityWorkerId", 0)

  let isDiscounted = _.get(activity, "is_discounted", null);
  let realPrice = _.get(activity, "price", null);
  let priceDiscount = isDiscounted
    ? activity.discounted_price
    : realPrice
    ? realPrice
    : activity.step_duration_price;

  // this one can automatically refresh and mutate whatever data on this screen
  const onRefresh = (data) => {
    console.log("onRefresh: ", data);
    setCouponApplied(data.couponApplied);
    setIsCouponValid(data.isCouponValid);
    setDiscountFromCoupon(data.discountFromCoupon);
    setDiscountType(data.discountType);
    setCouponId(data.couponId);

    if (data.isCouponValid == true) {
      if (isString(priceParams) == true) {
        priceParams = toInteger(priceParams) * 1000;
        console.log("priceParams new: ", priceParams);
      }
      // setNewPrice(data.discountFromCoupon)

      if (data.discountType === "amount") {
        setNewPrice((priceParams || priceDiscount) - data.discountFromCoupon);
      } else if (data.discountType === "percentage") {
        setNewPrice(
          (priceParams || priceDiscount) -
            (priceParams || priceDiscount) * (data.discountFromCoupon / 100)
        );
      }
    }
  };

  const applyCoupon = (activity_id, couponId, couponName, stepMultiplier, activityWorkerId) => {
    let requestBody 

    if (type == pt_activity_type_id) {
      requestBody = {
        userCouponId: couponId,
        activityId: activity_id,
        userId: selectedUser,
        stepMultiplier: stepMultiplier,
        activityWorkerId: activityWorkerId
      }
    } else if(type == rent_court_activity_type_id) {
      requestBody = {
        userCouponId: couponId,
        activityId: activity_id,
        userId: selectedUser,
        stepMultiplier: stepMultiplier
      }
    } else {
      requestBody = {
        userCouponId: couponId,
        activityId: activity_id,
        userId: selectedUser
      }
    }

    userapi.post(`/user-coupon/apply`, requestBody)
      .then(res => {
        // when success, pop back to previous screen, while refreshing it
        console.log("applied! ", res);
        setIsCouponValid(true)
        const data = res.data.data

        setNewPrice(data.discountedPrice)
        
        setCouponApplied(couponName)
        setCouponId(couponId)
      })
      .catch(err => {
        setCouponApplied(couponName)
        setIsCouponValid(false)
        Alert.alert("Error", err.response.data.message, [
          {
            text: "OK",
            onPress: () => console.log("OK Pressed"),
            style: "cancel",
          },
        ]);
        // setIsCouponValid(null)
      })
  }

  const automaticallyApplyCoupons = () => {
    if(isNull(best_coupon_id) == false) {
      const the_best_coupon_id = find(coupons, {"id": best_coupon_id})
      console.log("the_best_coupon_id: ", the_best_coupon_id);

      applyCoupon(activity.id, the_best_coupon_id.id, the_best_coupon_id.name, stepMultiplier, activityWorkerId)
    }
  }

  useEffect(() => {
    console.log("AKTIVITAS", activity);
    console.log("PROFILE", profile);
    console.log("ROUTE", route);
    console.log("Balance", balance);
    console.log("priceDiscount", priceDiscount);
    console.log("priceParams", toInteger(priceParams));
    setDifferenceBalance((priceDiscount || toInteger(priceParams)) - balance);
    if(coupons.length > 0) {
      automaticallyApplyCoupons()
    }
  }, []);

  // component that listens the focus of the screen
  useEffect(() => {
    console.log("force update");
    forceUpdate();
  }, [isFocused]); // isFocused is used to listen focus of this very class / component

  // useEffect(() => {
  //   dispatch(Actions.topupHistory());
  //   dispatch(Actions.profile());
  // }, [isFocused]);

  useEffect(() => {
    const membershipName = _.get(memberName, "membership.name", "");
    if (membershipName === "Non-Member" || membershipName == "") setStatusMember(2);
  }, []);

  const onClose = () => {
    setIsOpen(false);
  };

  const onInsufficientBalanceClose = () => {
    setOpenInsufficientBalanceAS(false);
  };

  const goToTopUpDetails = () => {
    // console.log("goToTopUpDetails pressed");
    onInsufficientBalanceClose();
    navigation.navigate("TopUpDetails", {
      userId: userId,
      amount: differenceBalance,
      isFromPayment: true,
    });
  };

  console.log("data data active", data);

  const onHandleBook = async () => {
    setBookLoading(true)
    console.log("askdkauwofwoih book", data);
    let courtID =
      type == pt_activity_type_id
        ? _.get(data, "activity_workers[0].activity_id", null)
        : _.get(data, "activity_court[0].id", null);
    let activSch = _.get(data, "actvitiy_court[0].activity_id", null);
    let userId = selectedUser == "" ? profile.data.id : selectedUser;
    let requestBody;
    let name = _.get(activity, 'name', "Sample Activity")

    if(type == pt_activity_type_id && (priceParams) > balance){
      setBookLoading(false)
      setOpenInsufficientBalanceAS(true);

    }
    else if ((priceDiscount || priceParams) > balance) {
      // insufficient balance
      setOpenInsufficientBalanceAS(true);
      setBookLoading(false)
    } else {

      setIsOpen(true);
      setBookLoading(false)

      let data = {
        "user_id": 2,
        "activity_schedule_id": 4058,
        "user_coupon_id": null,
        "booked_price": 700000,
        "paid_amount": 700000,
        "payment_method": "balance",
        "paid_by": "online",
        "paying_user_id": 2,
        "paying_staff_id": null,
        "payment_status": "paid",
        "attendance_status": "pending",
        "expired_at": null,
        "invite_status": "invited",
        "booking_status": "success",
        "cancel_reason": null,
        "created_at": "2022-01-01T08:47:26.000Z",
        "updated_at": "2022-01-01T08:47:26.000Z",
        "deleted_at": null,
        "ActivityScheduleId": 4058,
        "UserCouponId": null,
        "activity_booking_id": 25,
        "activity_schedule": {
          "id": 4058,
          "activity_court_id": 383,
          "start_time": "2022-01-01T11:00:00.000Z",
          "end_time": "2022-01-01T11:55:00.000Z",
          "internal_start_time": "2022-01-01T11:00:00.000Z",
          "internal_end_time": "2022-01-01T12:00:00.000Z",
          "duration": 60,
          "status": "active",
          "rrule_preset_id": null,
          "created_at": "2022-01-01T08:47:26.000Z",
          "updated_at": "2022-01-01T08:47:26.000Z",
          "deleted_at": null,
          "RrulePresetId": null,
          "activity_court": {
            "id": 383,
            "activity_id": 94,
            "court_id": 14,
            "priority": 0,
            "required_weight": 0,
            "created_at": "2022-01-01T04:41:52.000Z",
            "updated_at": "2022-01-01T04:41:52.000Z",
            "deleted_at": null
          }
        },
        "activity": {
          "id": 94,
          "activity_type_id": 6,
          "venue_id": 1,
          "name": name,
          "description": "Hone your skills with our expert tennis coach, choose what you need to work on and get the extra attention needed to get your game to the next level! ",
          "image_thumbnail": "https://api.kyzn.life/api/uploads/kyzn-logo.png",
          "image_landscape": "https://api.kyzn.life/api/uploads/kyzn-logo.png",
          "rate_type": "hour",
          "prep_duration": 5,
          "min_duration": 1,
          "step_duration": 60,
          "step_duration_price": 0,
          "publish_date": "2021-12-16T04:29:54.000Z",
          "available_until": null,
          "min_participants": 1,
          "max_participants": 1,
          "age_unit": "years",
          "min_age": 3,
          "max_age": 99,
          "gender": "all",
          "rating": 0,
          "preset_rating": 0,
          "queue_priority": "first come first serve",
          "xp_reward": 300,
          "coin_reward": 150,
          "is_selling": false,
          "is_show_preset_rating": true,
          "created_at": "2021-12-16T04:41:52.000Z",
          "updated_at": "2021-12-16T04:41:52.000Z",
          "deleted_at": null,
          "activity_workers": [
            {
              "id": 304,
              "activity_id": 94,
              "worker_id": 17,
              "worker_classification_id": 3,
              "price": 350000,
              "step_price": 350000,
              "commission_type": "amount",
              "commission_amount": 125000,
              "step_commission_type": "amount",
              "step_commission_amount": 125000,
              "created_at": "2021-12-16T04:41:52.000Z",
              "updated_at": "2021-12-16T04:41:52.000Z",
              "deleted_at": null,
              "worker": {
                "id": 17,
                "worker_type_id": 1,
                "name": "Indra",
                "email": "johanistolip07@gmail.com",
                "phone_number": "081381962602",
                "status": "single",
                "birthdate": "1987-12-06T17:00:00.000Z",
                "gender": "male",
                "salary": 4500000,
                "id_card_number": "3174060712870010",
                "bpjs_number": "1",
                "photo": "https://api.kyzn.life/api/uploads/default-avatar.png",
                "bank_name": "BCA",
                "bank_account_number": "7330350568",
                "bank_beneficiary": "Johanis Indra Tolip",
                "type": "Contract",
                "address": "Jl. Meninjo No. 52A Rt 004 Rw 005 Ciganjur Jagakarsa, Jakarta Selatan",
                "province": "DKI Jakarta",
                "district": "Ciganjur",
                "city": "Jakarta Selatan",
                "postal_code": "1",
                "rating": 4.7,
                "preset_rating": 0,
                "is_active": true,
                "start_working_time": "07:00:00",
                "end_working_time": "20:00:00",
                "created_at": "2021-11-24T15:08:02.000Z",
                "updated_at": "2021-11-24T15:08:02.000Z",
                "deleted_at": null
              },
              "worker_classification": {
                "id": 3,
                "worker_type_id": 1,
                "name": "Senior Coach",
                "description": "Senior Coach",
                "expected_price": 350000,
                "expected_step_price": 350000,
                "expected_commission_type": "amount",
                "expected_commission_amount": 125000,
                "expected_step_commission_type": "amount",
                "expected_step_commission_amount": 125000,
                "created_at": "2021-11-05T17:33:48.000Z",
                "updated_at": "2021-11-24T14:17:22.000Z",
                "deleted_at": null
              }
            }
          ]
        },
        "court": {
          "id": 14,
          "venue_id": 1,
          "name": "Tennis Court 1",
          "description": "Our indoor tennis space.",
          "picture_url": "https://api.kyzn.life/api/uploads/pictureUrl-1639385181217.jpg",
          "floor": "1",
          "open_time": "07:00:00",
          "close_time": "20:00:00",
          "max_weight": 1,
          "rating": 4.7,
          "preset_rating": 0,
          "is_active": true,
          "is_show_preset_rating": true,
          "created_at": "2021-11-22T15:30:29.000Z",
          "updated_at": "2021-12-13T08:46:23.000Z",
          "deleted_at": null
        },
        "venue": {
          "id": 1,
          "name": "Court BSD",
          "address": "BSD",
          "pos_lat": 1,
          "pos_long": 1,
          "open_time": "10:00:00",
          "close_time": "22:00:00",
          "is_open": true,
          "created_at": "2021-09-24T17:00:00.000Z",
          "updated_at": null,
          "deleted_at": null
        },
        "activity_rating": {},
        "worker_rating": {},
        "court_rating": {},
        "participants": [
          {
            "id": 25,
            "user_id": 2,
            "activity_schedule_id": 4058,
            "user_coupon_id": null,
            "booked_price": 700000,
            "paid_amount": 700000,
            "payment_method": "balance",
            "paid_by": "online",
            "paying_user_id": 2,
            "paying_staff_id": null,
            "payment_status": "paid",
            "attendance_status": "pending",
            "expired_at": null,
            "invite_status": "invited",
            "booking_status": "success",
            "cancel_reason": null,
            "created_at": "2021-12-22T08:47:26.000Z",
            "updated_at": "2021-12-22T08:47:26.000Z",
            "deleted_at": null,
            "UserCouponId": null,
            "user": {
              "id": 2,
              "name": "user",
              "username": null,
              "phone_number": "+62851616543211",
              "xp": 200,
              "coin": 500,
              "balance": 83808500,
              "status": "Registered",
              "birthdate": "2001-11-30T16:00:00.000Z",
              "gender": "male",
              "address": "Jl. Asd",
              "photo": "https://api.kyzn.life/api/uploads/default-avatar.png",
              "nationality": "Indonesia",
              "postal_code": "213321",
              "province": "Bali",
              "district": "Nusa dua",
              "city": "Bali",
              "referral_code": "Grand Opening",
              "id_card_no": null,
              "instagram_username": null,
              "company_name": null,
              "physical_limitation": null,
              "emergency_contact_name": null,
              "emergency_contact_relationship": null,
              "emergency_phone_number": null,
              "emergency_home_number": null,
              "email": null,
              "rfid_card_no": null,
              "created_at": "2021-09-25T01:00:00.000Z",
              "updated_at": "2021-12-22T08:47:26.000Z"
            }
          }
        ]
      }

      dispatch(Actions.upcomingScheduleSuccess(data));

      profile.data.balance = balance - priceParams;

      // if (type == rent_court_activity_type_id) {
      //   if (isNull(couponId) == false) {
      //     requestBody = {
      //       userId: userId,
      //       activityCourtId: courtID,
      //       startTime: `${moment(startTime.onSelectedDate).format(
      //         "YYYY-MM-DD"
      //       )} ${startTime.rangeHours}`,
      //       duration: timeDuration,
      //       status: "active",
      //       userCouponId: couponId,
      //     };
      //   } else {
      //     requestBody = {
      //       userId: userId,
      //       activityCourtId: courtID,
      //       startTime: `${moment(startTime.onSelectedDate).format(
      //         "YYYY-MM-DD"
      //       )} ${startTime.rangeHours}`,
      //       duration: timeDuration,
      //       status: "active",
      //     };
      //   }
      //   await userapi
      //     .post(`/activity-booking/custom-time`, requestBody)
      //     .then((res) => {
      //       console.log("success booked", res);
      //       setIsOpen(true);
      //       setDataSucces(res.data)
      //       dispatch(Actions.upcomingSchedule(userId))
      //       dispatch(Actions.pastSchedule(userId))
      //       setBookLoading(false)
      //     })
      //     .catch((err) => {
      //       console.log("error booked", err);
      //       let dataError = err.response;
      //       let errorCode = _.get(dataError, "status", " ");
      //       let errorMassage = _.get(dataError, "data.message", " ");

      //       Alert.alert("Error", `${errorMassage}`, [
      //         { text: "OK", onPress: () => console.log("OK Pressed") },
      //       ]);
      //       setBookLoading(false)
      //     });
      // } else if (type == pt_activity_type_id) {
      //   // console.log("route.params here: ", route.params);
      //   const activityID = activity.id;
      //   const userID = userId;
      //   const workerId = data.id;
      //   const duration = timeDuration;
      //   const start_time = moment(startTime).format("yyyy-MM-DD HH:mm");
      //   const status = "active";

      //   let requestBody = {
      //     userId: userID,
      //     activityId: activityID,
      //     workerId: workerId,
      //     startTime: start_time,
      //     duration: duration,
      //     status: status,
      //   };
      //   if (isNull(couponId) == false) {
      //     requestBody = {
      //       userId: userID,
      //       activityId: activityID,
      //       workerId: workerId,
      //       startTime: start_time,
      //       duration: duration,
      //       status: status,
      //       userCouponId: couponId,
      //     };
      //   }

      //   console.log("requestBody: ", requestBody);

      //   userapi
      //     .post(`/activity-booking/coach`, requestBody)
      //     .then((res) => {
      //       console.log("/activity-booking/coach res: ", res);
      //       setIsOpen(true);
      //       setDataSucces(res.data)
      //       dispatch(Actions.upcomingSchedule(userId))
      //       dispatch(Actions.pastSchedule(userId))
      //       setBookLoading(false)
      //       // navigation.navigate("Home")
      //     })
      //     .catch((err) => {
      //       console.log("/activity-booking/coach err: ", err);
      //       let dataError = err.response;
      //       let errorCode = _.get(dataError, "status", " ");
      //       let errorMassage = _.get(dataError, "data.message", " ");
      //       setBookLoading(false)

      //       Alert.alert("Error", `${errorMassage}`, [
      //         { text: "OK", onPress: () => console.log("OK Pressed") },
      //       ]);
      //     });
     
      //   }
      //   else {
      //   // if (data.quota_left <= 0) {
      //   //   //come to waiting list

      //   //   setIsOpen(true);
      //   // } else {
      //     if (isNull(couponId) == false) {
      //       requestBody = {
      //         activityScheduleId: data.id,
      //         userId: userId,
      //         userCouponId: couponId,
      //       };
      //     } else {
      //       requestBody = {
      //         activityScheduleId: data.id,
      //         userId: userId,
      //       };
      //     }
      //     console.log("gaserror", requestBody)
      //     await userapi
      //       .post("activity-booking", requestBody)
      //       .then((res) => {
      //         console.log("success booked", res);
      //         setDataSucces(res.data)
      //         dispatch(Actions.upcomingSchedule(userId))
      //         dispatch(Actions.pastSchedule(userId))
      //         setIsOpen(true);
      //         setBookLoading(false)
      //       })
      //       .catch((err) => {
      //         let dataError = err.response;
      //         let errorCode = _.get(dataError, "status", " ");
      //         let errorMassage = _.get(dataError, "data.message", " ");
      //         console.log("gaserror", err.response)
      //         setBookLoading(false)
      //         Alert.alert("Error", `${errorMassage}`, [
      //           { text: "OK", onPress: () => console.log("OK Pressed") },
      //         ]);
      //       });
      //   // }
      //   // dispatch(Actions.activityBooking(data.id, (selectedUser)))
      //   // setIsOpen(true);
      // }
    }
  };

  const onDone = () => {
    let userId = _.get(profile.data, 'id', null)
    // dispatch(Actions.upcomingSchedule(userId))
    navigation.navigate("BottomTabs", { refresh: true });
    setIsOpen(false);
  };

  const onViewDetail = () => {
    console.log("arryTemp view detail", dataUpcoming, dataSucces, dataPastSchedule);

    let data = dataSucces.data
    let activScheduleId = _.get(data, 'activity_schedule_id' , '')

    let arryTemp = _.filter(dataUpcoming.upcomingSchedule, function(o) { 
      return o.activity_schedule.id == activScheduleId
    });

    let arryTempPast = _.filter(dataPastSchedule.pastSchedule, function(o) { 
      return o.activity_schedule.id == activScheduleId 
    });

    console.log("arryTemp", arryTemp,activScheduleId, dataUpcoming.upcomingSchedule, arryTempPast);

    console.log("arryTemp data", arryTemp, arryTempPast);
    let dataTemp = arryTemp.length !== 0 ? arryTemp : arryTempPast

    navigation.navigate(
      "DetailActivity", 
      { 
        "title": "Detail Activity",
        "data" : {"item": dataTemp[0]},
        "typeDetail": 1,
        fromUpcoming: true
      })
    setIsOpen(false);
  };

  let memberName = _.minBy(
    _.get(activity, `activity_membership_pricings`, {}),
    function (o) {
      if (o.is_discounted) {
        return o.discounted_price;
      } else {
        return o.price;
      }
    }
  );

  const renderImageDone = () => {
    if (data.quota_left <= 0) {
      return (
        <Box style={{ alignItems: "center" }}>
          <Image
            source={image.waitinglist}
            style={{ width: 170, height: 100 }}
            resizeMode={"contain"}
            alt="image"
          />
          <Box style={{ width: 270, alignItems: "center", marginTop: 8 }}>
            <Text
              style={{
                fontSize: 14,
                color: "#000",
                fontWeight: "bold",
                fontFamily: "Montserrat-Regular",
              }}
              alignItems="flex-start"
            >
              You’re in Wating List
            </Text>
            <Text
              style={{
                fontSize: 12,
                color: "#000",
                fontWeight: "500",
                fontFamily: "Montserrat-Regular",
                textAlign: "center",
                marginTop: 10,
              }}
              alignItems="flex-start"
            >
              Yay! your booking was in wating list.
              {/* <Text
                style={{
                  fontWeight: "bold",
                }}
              >
                KY2324346
              </Text> */}
            </Text>
          </Box>
        </Box>
      );
    } else {
      return (
        <Box style={{ alignItems: "center" }}>
          <Image
            source={image.succes_ic}
            style={{ width: 170, height: 100 }}
            resizeMode={"contain"}
            alt="image"
          />
          <Box style={{ width: 270, alignItems: "center", marginTop: 8 }}>
            <Text
              style={{
                fontSize: 14,
                color: "#000",
                fontWeight: "bold",
                fontFamily: "Montserrat-Regular",
              }}
              alignItems="flex-start"
            >
              Booking was Successful
            </Text>
            <Text
              style={{
                fontSize: 12,
                color: "#000",
                fontWeight: "500",
                fontFamily: "Montserrat-Regular",
                textAlign: "center",
                marginTop: 10,
              }}
              alignItems="flex-start"
            >
              Yay! your booking was successful.
              <Text
                style={{
                  fontWeight: "bold",
                  color: "#000",
                }}
              ></Text>
            </Text>
          </Box>
        </Box>
      );
    }
  };
  console.log("memberName: ", memberName);

  return (
    <Box flex={1}>
      <ScrollView style={{ backgroundColor: "white", flex: 1 }}>
        {membership_status == 1 ? (
          <HStack
            style={{ alignItems: "center", padding: 20, paddingVertical: 27 }}
          >
            {_.get(memberName, "membership.name", "") !== "Non-Member" && (
              <Image
                style={{ width: 32, height: 32 }}
                source={require("../../../assets/images/crown.png")}
                alt="image"
                resizeMode="cover"
              />
            )}
            <VStack style={{ justifyContent: "center", marginLeft: 8 }}>
              <Text
                style={{
                  fontWeight: "bold",
                  fontSize: 12,
                  letterSpacing: 0.2,
                  color: "#000",
                  fontFamily: "Montserrat-Regular",
                }}
              >
                {_.get(memberName, "membership.name", "") !== "Non-Member" &&
                  "MEMBERSHIP APPLIED"}
              </Text>
              <Text
                style={{
                  fontWeight: "bold",
                  fontSize: 12,
                  color: "#757575",
                  letterSpacing: 0.2,
                  fontFamily: "Montserrat-Regular",
                }}
              >
                {_.get(memberName, "membership.name", "") !== "Non-Member" &&
                  _.get(memberName, "membership.name", "")}
              </Text>
            </VStack>
          </HStack>
        ) : (
          <HStack
            style={{
              alignItems: "center",
              padding: 20,
              paddingVertical: 32,
              justifyContent: "space-between",
            }}
          >
            <Text
              style={{
                fontWeight: "bold",
                fontSize: 12,
                color: "#000",
                letterSpacing: 0.2,
                fontFamily: "Montserrat-Regular",
              }}
            >
              PAY WITH MY CREDIT
            </Text>
            {/* <Text
              style={{
                fontWeight: "bold",
                fontSize: 12,
                letterSpacing: 0.2,
                fontFamily: "Montserrat-Regular",
              }}
            >
              {`${Commons.convertToRupiah(toInteger(_.get(memberName, 'membership.price', 0)))} CREDIT`}
            </Text> */}
          </HStack>
        )}
        <Box style={{ height: 8, backgroundColor: "#F8F9FC" }} />
        {coupons.length > 0 && (
          <TouchableOpacity
            onPress={() => {
              navigation.navigate("Coupons", {
                // filter: activity.name,
                onRefresh: onRefresh, // part of onRefresh
                activityId: activity.id,
                type: type,
                stepMultiplier: stepMultiplier,
                activityWorkerId: activityWorkerId,
                userId: selectedUser,
                isFromPayment: true,
                couponApplied: couponApplied,
                couponId: couponId,
                couponAvailable: coupons,
              });
            }}
          >
            <HStack
              style={{
                alignItems: "center",
                paddingVertical: moderateScale(12, 0.25),
                paddingHorizontal: moderateScale(16, 0.25),
                marginHorizontal: moderateScale(20, 0.25),
                // justifyContent: "space-between",
                backgroundColor: "#FFFEEA",
                borderColor: "#FFCA52",
                borderWidth: 1,
              }}
            >
              {isCouponValid == false && (
                <WarningIcon
                  size={4}
                  color={"#ef4444"}
                  marginRight={moderateScale(12, 0.25)}
                />
              )}
              <Text
                style={{
                  flex: 1,
                  fontSize: moderateScale(12, 0.25),
                  fontWeight: "600",
                  color: "#212121",
                  fontFamily: "Montserrat-Regular",
                }}
              >
                {couponApplied === ""
                  ? `${coupons.length} Available Coupon(s)`
                  : couponApplied}
              </Text>
              <Image
                source={image.ic_coupon}
                style={{
                  width: moderateScale(16, 0.25),
                  height: moderateScale(12, 0.25),
                }}
                alt={"imageAlt"}
              />
            </HStack>
          </TouchableOpacity>
        )}
        <Box style={{ padding: 20, paddingBottom: 32 }}>
          <HStack
            style={{
              alignItems: "center",
              paddingVertical: 9,
              justifyContent: "space-between",
              backgroundColor: "#F8F9FC",
            }}
          >
            <VStack style={{ justifyContent: "center", marginLeft: 10 }}>
              <Text
                style={{
                  fontSize: 12,
                  color: "#000",
                  letterSpacing: 0.2,
                  fontFamily: "Montserrat-Regular",
                }}
              >
                {type == rent_court_activity_type_id
                  ? _.get(data, "name", " ")
                  : type == pt_activity_type_id
                  ? _.get(activity, "name", " ")
                  : _.get(activity, "name", " ")}
              </Text>
              <Text
                style={{
                  fontSize: 12,
                  color: "#757575",
                  letterSpacing: 0.2,
                  fontFamily: "Montserrat-Regular",
                }}
              >
                Adults
              </Text>
            </VStack>
            <VStack>
              <Text
                style={{
                  fontSize: 11,
                  letterSpacing: 0.2,
                  marginRight: 10,
                  textAlign: "right",
                  textDecorationStyle: "solid",
                  textDecorationLine: "line-through",
                  color: "gray",
                  fontFamily: "Montserrat-Regular",
                }}
              >
                {activity.price_with_member < activity.price_without_member &&
                  Commons.convertToRupiah(activity.price_without_member)}
              </Text>
              <Text
                style={{
                  fontWeight: "bold",
                  fontSize: 12,
                  textAlign: "right",
                  color: "#000",
                  letterSpacing: 0.2,
                  marginRight: 10,
                  fontFamily: "Montserrat-Regular",
                }}
              >
                {type == rent_court_activity_type_id
                  ? Commons.convertToRupiah(priceParams)
                  : type == pt_activity_type_id
                  ? Commons.convertToRupiah(priceParams)
                  : Commons.convertToRupiah(priceParams)}
                {/* {type == pt_activity_type_id && ` /hr`} */}
              </Text>
            </VStack>
          </HStack>
        </Box>
        <Box style={{ padding: 20 }}>
          <VStack
            style={{
              borderColor: "#DFE3EC",
              borderWidth: 1,
              borderBottomWidth: 0,
              padding: 16,
            }}
          >
            <Text
              style={{
                fontSize: 12,
                color: "#757575",
                letterSpacing: 0.2,
                marginBottom: 10,
                fontFamily: "Montserrat-Regular",
              }}
            >
              Booking for
            </Text>
            <Text
              style={{
                fontWeight: "bold",
                fontSize: 12,
                color: "#000",
                letterSpacing: 0.2,
                fontFamily: "Montserrat-Regular",
              }}
            >
              {type == rent_court_activity_type_id
                ? _.get(data, "name", " ")
                : type == pt_activity_type_id
                ? _.get(activity, "name", " ")
                : _.get(activity, "name", " ")}
            </Text>
            {type == pt_activity_type_id && (
              <Text
                style={{
                  fontSize: 12,
                  letterSpacing: 0.2,
                  fontFamily: "Montserrat-Regular",
                  color: "#000",
                  marginBottom: moderateScale(10, 0.25),
                }}
              >
                {`with `}
                <Text
                  style={{
                    fontWeight: "bold",
                    fontSize: 12,
                    color: "#000",
                    letterSpacing: 0.2,
                    fontFamily: "Montserrat-Regular",
                  }}
                >
                  {_.get(data, "name", " ")}
                </Text>
              </Text>
            )}
            <Text
              style={{
                fontSize: 12,
                color: "#757575",
                letterSpacing: 0.2,
                fontFamily: "Montserrat-Regular",
              }}
            >
              {/* {moment(data.start_time).format("DD MM yy") ==
              moment().format("DD MM yy")
                ? "Today , "
                : moment(data.start_time).format("DD MM yy") ==
                  moment().add(1, "day").format("DD MM yy")
                ? "Tomorrow , "
                : moment(data.start_time).format("dddd")}
              {moment(data.start_time).format(" DD MMMM yyyy")} */}
            </Text>
            {type == pt_activity_type_id && (
              <Text
                style={{
                  fontSize: 12,
                  color: "#757575",
                  letterSpacing: 0.2,
                  fontFamily: "Montserrat-Regular",
                }}
              >
                {_.get(route.params, "hours", " ")}
              </Text>
            )}
          </VStack>
          {type == class_activity_type_id && (
            <VStack
              style={{ borderColor: "#DFE3EC", borderWidth: 1, padding: 16 }}
            >
              <Text
                style={{
                  fontSize: 12,
                  color: "#757575",
                  letterSpacing: 0.2,
                  marginBottom: 10,
                  fontFamily: "Montserrat-Regular",
                }}
              >
                Exercise at
              </Text>
              <Text
                style={{
                  fontWeight: "bold",
                  color: "#000",
                  fontSize: 12,
                  letterSpacing: 0.2,
                  fontFamily: "Montserrat-Regular",
                }}
              >
                {type == rent_court_activity_type_id
                  ? _.get(data, `actvitiy_courts[0].court.name`, " ")
                  : _.get(data, "activity_court[0].court.name", " ")}
              </Text>
              <Text
                style={{
                  fontSize: 12,
                  color: "#757575",
                  letterSpacing: 0.2,
                  fontFamily: "Montserrat-Regular",
                }}
                numberOfLines={1}
              >
                {/* Menara Kuningan - Jln Rasuna Sahid Level 1 */}
              </Text>
            </VStack>
          )}
          {
            // type == 6 &&
            //   <VStack
            //     style={{ borderColor: "#DFE3EC", borderWidth: 1, padding: 16 }}
            //   >
            //     <Text
            //       style={{
            //         fontSize: 12,
            //         color: "#757575",
            //         letterSpacing: 0.2,
            //         marginBottom: 10,
            //         fontFamily: "Montserrat-Regular",
            //       }}
            //     >
            //       Exercise at
            //     </Text>
            //     <Text
            //       style={{
            //         fontWeight: "bold",
            //         fontSize: 12,
            //         letterSpacing: 0.2,
            //         fontFamily: "Montserrat-Regular",
            //       }}
            //     >
            //       {
            //         type == 6
            //           ? _.get(data, `activity_workers[0].activity.name`, ' ')
            //           : _.get(data, 'activity_court.court.name' , ' ')
            //       }
            //     </Text>
            //     <Text
            //       style={{
            //         fontSize: 12,
            //         color: "#757575",
            //         letterSpacing: 0.2,
            //         fontFamily: "Montserrat-Regular",
            //       }}
            //       numberOfLines={1}
            //     >
            //       {
            //         type == 6
            //           ? _.get(data, `activity_workers[0].activity.description`, ' ')
            //           : _.get(data, 'activity_court.court.name' , ' ')
            //       }
            //       {/* Menara Kuningan - Jln Rasuna Sahid Level 1 */}
            //     </Text>
            //   </VStack>
          }
        </Box>
      </ScrollView>
      <Box
        style={{
          position: "absolute",
          bottom: 0,
          right: 0,
          width: "100%",
          padding: 20,
          paddingBottom: 40,
          borderTopWidth: 1,
          borderColor: "#ECEDEF",
          backgroundColor: "#FFF",
        }}
      >
        {
          membership_status == 2 && (
            <Box marginBottom={moderateScale(10, .25)}>
              <Text
                style={{
                  fontSize: 12,
                  // color: "#606060",
                  color: "#000",
                  marginBottom: 5,
                  fontFamily: "Montserrat-Regular",
                }}
              >
                {`Please be reminded that you will need a membership or day pass to enter premises`}
              </Text>
            </Box>
          )
        }
        <HStack style={{ justifyContent: "space-between" }}>
          <VStack>
            <HStack>
              <Text
                style={{
                  fontSize: 10,
                  color: "#606060",
                  marginBottom: 5,
                  fontFamily: "Montserrat-Regular",
                }}
              >
                Credit Used
              </Text>
            </HStack>
            {isCouponValid == true ? (
              <HStack>
                <Text
                  style={{
                    fontSize: 14,
                    color: "#000",
                    fontWeight: "bold",
                    fontFamily: "Montserrat-Regular",
                    textDecorationLine: "line-through",
                    textDecorationStyle: "solid",
                  }}
                  alignItems="flex-start"
                >
                  {type == rent_court_activity_type_id || type == pt_activity_type_id
                    ? Commons.convertToRupiah(priceParams)
                    : Commons.convertToRupiah(priceParams)}
                </Text>
                <Text
                  style={{
                    fontSize: 14,
                    color: "#000",
                    fontWeight: "bold",
                    fontFamily: "Montserrat-Regular",
                    marginLeft: moderateScale(6, 0.25),
                    // textDecorationLine: "line-through",
                    // textDecorationStyle: "solid"
                  }}
                  alignItems="flex-start"
                >
                  {
                    // TODO: type == 1 is not set yet on coupon
                    Commons.convertToRupiah(newPrice)
                  }
                </Text>
              </HStack>
            ) : (
              <Text
                style={{
                  fontSize: 14,
                  color: "#000",
                  fontWeight: "bold",
                  fontFamily: "Montserrat-Regular",
                }}
                alignItems="flex-start"
              >
                {type == rent_court_activity_type_id
                  ? Commons.convertToRupiah(priceParams)
                  : type == pt_activity_type_id
                  ? Commons.convertToRupiah(priceParams)
                  : Commons.convertToRupiah(priceParams)}
              </Text>
            )}
          </VStack>
          <Button w={130} bgColor={"#1035BB"} onPress={() => onHandleBook()} isLoading={bookLoading}>
            BOOK NOW
          </Button>
        </HStack>
      </Box>
      <Actionsheet isOpen={isOpen}>
        <Actionsheet.Content h={365}>
          <Image
            source={image.bg_actionsSheet}
            style={{ width: 300, height: 300, position: "absolute", right: 0 }}
            alt="image"
          />
          {renderImageDone()}
          {/* <Button
            w={"90%"}
            bgColor={"#fff"}
            onPress={() => onViewDetail()}
            style={{ marginTop: 50, borderRadius: 0 }}
          >
            <Text
              style={{
                color: "#000",
                fontFamily: "Montserrat-Regular",
              }}
            >
              {`View details`}{(type == rent_court_activity_type_id || type == pt_activity_type_id) && ` and invite your friends`}
            </Text>
          </Button>  */}
          <Button
            w={"90%"}
            bgColor={"#212121"}
            onPress={onDone}
            color={"#fff"}
            // style={{ marginTop: 10, borderRadius: 0 }}
            style={{ marginTop: 50, borderRadius: 0 }}
          >
            <Text
              style={{
                fontWeight: "bold",
                color: "#fff",
                fontFamily: "Montserrat-Regular",
              }}
            >
              Done
            </Text>
          </Button>
        </Actionsheet.Content>
      </Actionsheet>
      <Actionsheet
        isOpen={openInsufficientBalanceAS}
        onClose={onInsufficientBalanceClose}
      >
        <Actionsheet.Content h={365}>
          <Image
            source={image.bg_noPremium_actionsheet}
            style={{
              width: 150,
              height: 150,
              resizeMode: "contain",
              position: "absolute",
              left: 0,
              top: -22,
              transform: [{ rotate: "0deg" }],
            }}
            alt="image"
          />
          <Image
            source={image.not_premium_ic}
            style={{ width: 170, height: 100 }}
            resizeMode={"contain"}
            alt="image"
          />
          <Box style={{ width: 270, alignItems: "center", marginTop: 8 }}>
            <Text
              style={{
                fontSize: 14,
                color: "#000",
                fontWeight: "bold",
                fontFamily: "Montserrat-Regular",
              }}
              alignItems="flex-start"
            >
              {`Sorry, You have insufficient balance`}
            </Text>
            <Text
              style={{
                fontSize: 12,
                color: "#000",
                fontWeight: "500",
                fontFamily: "Montserrat-Regular",
                textAlign: "center",
                marginTop: 10,
              }}
              alignItems="flex-start"
            >
              {`Please top up your balance. Your current balance is `}
              <Text
                style={{
                  fontWeight: "bold",
                  color: "#000",
                }}
              >
                {`${Commons.convertToRupiah(balance)}`}
              </Text>
            </Text>
          </Box>
          <Button
            w={"90%"}
            bgColor={"#212121"}
            onPress={goToTopUpDetails}
            style={{ marginTop: 50, borderRadius: 0 }}
          >
            <Text
              style={{
                color: "#fff",
                fontFamily: "Montserrat-Regular",
              }}
            >
              Top Up now
            </Text>
          </Button>
          <Button
            w={"90%"}
            bgColor={"transparent"}
            onPress={onInsufficientBalanceClose}
            color={"#000"}
            style={{ marginTop: 10, borderRadius: 0 }}
          >
            <Text
              style={{
                fontWeight: "bold",
                color: "#000",
                fontFamily: "Montserrat-Regular",
              }}
            >
              Close
            </Text>
          </Button>
        </Actionsheet.Content>
      </Actionsheet>
    </Box>
  );
};

Payment.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default Payment;
