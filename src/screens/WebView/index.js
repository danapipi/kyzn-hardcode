import React, {useState} from 'react';
import _get from 'lodash/get';
import { Text, View, StatusBar, NativeModules, Platform, TouchableOpacity } from 'react-native';
import { moderateScale } from 'react-native-size-matters';
import { WebView } from 'react-native-webview';

const WebViewDisplay = ({
    navigation,
    route
}) => {
  const [topUpAmount, setTopUpAmount] = useState(_get(route.params, 'amount', 0))
  const [statusbarIOSHeight, setStatusbarIOSHeight] = useState(20)

  const { StatusBarManager } = NativeModules

  if(Platform?.OS === 'ios') {
    StatusBarManager?.getHeight((statusBarHeight)=>{
      console.log("StatusBar.currentHeight for ios: ", statusBarHeight);
      console.log("topUpAmount: ", topUpAmount);
      setStatusbarIOSHeight(statusBarHeight?.height)
    })
  }

  const goBack = () => {
    const isFromPayment = _get(route.params, "isFromPayment", false)
    
    if(isFromPayment == true) {
      navigation.pop(2)
    } else {
      navigation.pop()
    }
  }

  return(
    <View style={{flex: 1, marginTop: Platform.OS == "ios" ? moderateScale(statusbarIOSHeight) : moderateScale(StatusBar.currentHeight)}}>
      <TouchableOpacity 
        onPress={() => goBack()}
        style={{
          marginBottom: moderateScale(6),
          marginLeft: moderateScale(10)
        }}
      >
        <Text
          style={{
            fontSize: moderateScale(16)
          }}
        >
          {`Close`}
        </Text>
      </TouchableOpacity>
      <WebView 
        source={{
          uri: route.params.url,
          // uri: "https://checkout-staging.xendit.co/web/61823d74d91d5bfaaa199a9a",
        }}
      />
    </View>
  )
};

export default WebViewDisplay;
