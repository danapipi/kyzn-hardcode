import React, { useState, useEffect, useRef } from "react";
import {
  Dimensions,
  ActivityIndicator,
  View,
  StyleSheet,
  Alert,
} from "react-native";
import {
  Box,
  Text,
  Button,
  FlatList,
  ScrollView,
  HStack,
  Image,
  ZStack,
  VStack,
  useTheme,
} from "native-base";
import PropTypes from "prop-types";
import _get from "lodash/get";
import { image } from "../../../../assets/images";
import { moderateScale } from "react-native-size-matters";
import { connect, useDispatch, useSelector } from "react-redux";
import Actions from "actions";
import TopUpHistoryList from "components/ui/TopUpHistoryList";
import Selectors from "selectors";
import TopUpCard from "components/ui/TopUpCard";
import Carousel, { Pagination } from "react-native-snap-carousel";
import dummyData from "./dummyData";
import Commons from "../../utils/common";
import { getScreenWidth, getScreenHeight } from "utils/size";
import { UserApi } from "../../utils/Api";
import { useIsFocused } from "@react-navigation/native";

import _ from "lodash";

const userapi = UserApi.client;

const TopUp = ({ navigation }) => {
  const dispatch = useDispatch();
  const isFocused = useIsFocused();
  const data = useSelector(state => state.TOPUP.topUpHistory);
  const profile = useSelector(state => state.AUTH.profile.data);
  const families = useSelector(Selectors.getAllFamilies);
  const dataUniqueFamily = useSelector(state => state.FAMILY.uniqueFamily);
  const sortedDataOfUniqueFamily = []
    .concat(
      dataUniqueFamily.data.filter(item => {
        return item.id == profile.id;
      }),
    )
    .concat(
      dataUniqueFamily.data.filter(item => {
        return item.id != profile.id;
      }),
    );

  const [currentIndex, setCurrentIndex] = useState(0);
  const [loadingFetchHistory, setLoadingFetchHistory] = useState(false);
  // const [populateFamilyMembers, setPopulateFamilyMembers] = useState([
  //   profileObjectforPopulating
  // ])
  const [populateFamilyMembers, setPopulateFamilyMembers] = useState(
    sortedDataOfUniqueFamily,
  );
  const dataTopUp = useSelector(state => state.TOPUP.topUpHistory);
  const dataTopUpTemp = dataTopUp.topUpHistory;
  const resizeArrayTopUp = _.chunk(dataTopUpTemp, [10]);
  console.log("dataTopUpTemp", dataTopUpTemp, resizeArrayTopUp);
  const [topUpHistories, setTopUpHistories] = useState(resizeArrayTopUp[0]);

  const memberRef = useRef(null);
  const countRef = useRef(0);

  const { colors } = useTheme();

  const populate = async () => {
    let arr = [];

    // await families.map((item) => {
    //   // console.log("item: ", item);
    //   if(item.user_id == profile.id) {
    //     item.user_family_details.filter((x) => {
    //       const member = x
    //       // member.user_family_id is familyId
    //       // member.user_id is userId instead of member.id
    //       // member.user_id === member.user.id
    //       if(member.user_id != profile.id) {
    //         console.log("member: ", member);
    //         arr.push(member)
    //       }
    //     })
    //   }
    // })

    console.log("newArr: ", arr);

    // setPopulateFamilyMembers((data) => {
    //   return data.concat(arr)
    // })
  };

  const fetchTopUpHistory = index => {
    setLoadingFetchHistory(true);
    const userId = _get(populateFamilyMembers[index], "id", profile.id);
    console.log("fetchTopUpHistory user: ", userId);
    // dispatch(Actions.fetchAllFamilies());
    // setTopUpHistories([]);
    setLoadingFetchHistory(false);
  };

  // useEffect(() => {
  //   dispatch(Actions.topupHistory());
  //   console.log("families: ", families);
  //   console.log("sortedDataOfUniqueFamily: ", sortedDataOfUniqueFamily);
  //   console.log("profile: ", profile);

  //   // dispatch(Actions.fetchFamilyUnique())
  //   // populate()
  //   // fetchTopUpHistory(0)
  // }, []);

  useEffect(() => {
    console.log("mounting ", countRef.current);
    // dispatch(Actions.profile());
    // if(countRef.current <= 1) {
    console.log("isFocused: ", isFocused);
    // dispatch(Actions.fetchFamilyUnique());
    // dispatch(Actions.fetchAllFamilies());
    fetchTopUpHistory(currentIndex);
    // setTimeout(() => {
    // setPopulateFamilyMembers(sortedDataOfUniqueFamily)
    // }, 1000);
    // setPopulateFamilyMembers([
    //   profileObjectforPopulating
    // ])
    populate();
    countRef.current++;
    // }

    return () => {
      // cleaning up
      countRef.current = 0;
      console.log("unmounting ", countRef.current);
    };
  }, [isFocused]);

  useEffect(() => {
    // listen to any changes on dataUniqueFamily
    // console.log("sortedDataOfUniqueFamily: ", sortedDataOfUniqueFamily);
    setPopulateFamilyMembers(sortedDataOfUniqueFamily);
  }, [dataUniqueFamily]);

  console.log("data History", data);

  const component = () => {
    return (
      <Box flex={1} bg="white">
        {populateFamilyMembers.length > 0 ? (
          <Box>
            <TopUpCard
              creditValue={Commons.convertToRupiah(profile.balance)}
              navigation={navigation}
              cardStyle={1}
              nameOfUser={profile.name}
              // userId={item.id}
            />
          </Box>
        ) : (
          <TopUpCard
            creditValue={Commons.convertToRupiah(profile.balance)}
            navigation={navigation}
            cardStyle={1}
            userId={profile.id}
          />
        )}
        <View
          style={{
            backgroundColor: "#F8F9FC",
            height: moderateScale(38, 0.25),
            justifyContent: "center",
            marginBottom: moderateScale(16, 0.25),
          }}>

        <Text
          style={{
            fontSize: moderateScale(17, 0.25),
            color: "black",
            fontWeight: "700",
            marginLeft: moderateScale(16, 0.25),
            fontFamily: "Rubik-Regular",
          }}>
          Credit History
        </Text>
        </View>
        {/* <View
          style={{
            backgroundColor: "#F8F9FC",
            height: moderateScale(38, 0.25),
            justifyContent: "center",
          }}>
          <Text
            style={{
              color: "#757575",
              fontSize: moderateScale(10, 0.25),
              fontWeight: "600",
              marginHorizontal: moderateScale(16, 0.25),
              fontFamily: "Montserrat-Regular",
            }}>
            {{data.item.date}}
          </Text>
        </View> */}
        {loadingFetchHistory == true ? (
          <Box
            style={{
              height: "50%",
              justifyContent: "center",
              alignItems: "center",
            }}>
            <ActivityIndicator size={"large"} />
          </Box>
        ) : (
          <FlatList
            // data={dummyData}
            // data={data.topUpHistory}
            data={topUpHistories}
            renderItem={_item => {
              return <TopUpHistoryList navigation={navigation} data={_item} />;
            }}
          />
        )}
      </Box>
    );
  };

  return (
    <Box style={{ backgroundColor: "white", flex: 1 }}>
      {component()}
      <Box style={{ marginBottom: moderateScale(20, 0.25) }} />
      {/* <FlatList
        ListHeaderComponent={component}
        style={{ marginBottom: moderateScale(20, 0.25) }}
      /> */}
    </Box>
  );
};

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },
  imageContainer: {
    width: "100%",
    height: "90%",
    alignItems: "center",
    marginTop: 50,
  },
  footerImageContainer: {
    width: "100%",
    alignItems: "flex-end",
    position: "absolute",
    zIndex: 2,
    bottom: 0,
  },
  fadeContainer: {
    width: "100%",
    height: getScreenHeight() * 0.5,
    position: "absolute",
    zIndex: 2,
    bottom: 0,
  },
  contentContainer: {
    width: "100%",
    position: "absolute",
    alignItems: "center",
    bottom: 0,
  },
  content: {
    flex: 1,
    marginBottom: 80,
    zIndex: 3,
  },
  textContainer: {
    marginHorizontal: 30,
    width: "100%",
  },
  paginationContainer: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
  },
  pagination: {
    flex: 1,
  },
  paginationDot: {
    width: 6,
    height: 6,
    borderRadius: 5,
    borderWidth: 1,
  },
  paginationButtons: {
    marginHorizontal: 15,
  },
});

TopUp.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default TopUp;
