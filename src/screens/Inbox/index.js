import React, { useState, useEffect } from "react";
import { StyleSheet, SafeAreaView, Dimensions } from "react-native";
import {
  View,
  Pressable,
  FlatList,
  Text,
  Box,
  HStack,
  IconButton,
  Icon,
  Actionsheet,
  CheckIcon,
  Select,
} from "native-base";
import { useSelector, useDispatch } from 'react-redux'
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import moment from "moment";
import { moderateScale } from 'react-native-size-matters';
import { useIsFocused } from '@react-navigation/native';

import Actions from "actions";
import Selectors from "selectors";
import InboxInviteConfirmation from "./InboxInviteConfirmation";
import { UserApi } from "../../utils/Api";

const userapi = UserApi.client;

export default function Inbox({ route, navigation }) {
  const dispatch = useDispatch()
  const isFocused = useIsFocused()

  const dataUniqueFamily = useSelector((state) => state.FAMILY.uniqueFamily);

  const [isInviteOpen, setInviteOpen] = useState(false);
  const [itemSelected, setItemSelected] = useState({});

  const profile = useSelector(Selectors.getProfile)
  const [selecteduserId, setSelectedUserId] = useState(dataUniqueFamily.data[0].id)
  const inboxData = useSelector((state) => state.INBOX.all)

  const handleInvite = (answer) => {
    userapi.post(`/family/invitation`, {
      familyId: itemSelected.user_family_detail_id,
      type: answer
    })
    setInviteOpen(false)
  }

  const fetchInboxByUserID = (userId) => {
    // dispatch(Actions.fetchInbox(userId))
    setSelectedUserId(userId)
  }

  useEffect(() => {
    fetchInboxByUserID(selecteduserId)
    // dispatch(Actions.fetchFamilyUnique());
    console.log("inbox: ", inboxData);
  }, [isFocused])

  const renderTopNavigation = () => {
    return (
      <>
        <Box
          safeAreaTop
          backgroundColor="white"
          borderBottom="1"
          borderBottomColor="#EBEEF4"
        />

        <HStack
          bg="white"
          px="1"
          py="3"
          justifyContent="space-between"
          alignItems="center"
        >
          <HStack space="4" alignItems="center">
            <Text
              color="black"
              fontSize="16"
              fontWeight="bold"
              paddingLeft="3"
              fontFamily="Montserrat-Bold"
            >
              Messages
            </Text>
          </HStack>
          <HStack space="2">
            <IconButton
              icon={
                <Icon
                  as={<MaterialIcons name="sort" />}
                  size="sm"
                  color="black"
                />
              }
              onPress={() => {
                // do something like filter etc
              }}
            />
          </HStack>
        </HStack>
      </>
    );
  };

  const renderInboxList = ({ item }) => {
    return (
      <Pressable
        onPress={() => {
          // if (item.type === "message") {
            // navigation.navigate("InboxDetail", { title: item.message, message: item.message });
            navigation.navigate("InboxDetail", { item });
          // } else if (item.type === "family_invite") {
          //   setItemSelected(item);
          //   setInviteOpen(true)
          // }
        }}
      >
        <View
          style={item.isRead ? styles.bgMessageRead : styles.bgMessageUnread}
        >
          <HStack justifyContent={"space-between"} alignItems={"center"}>
            <Text style={styles.messageTitle}>{item.title}</Text>
            {
              item.status == 'accepted' && (
                <Text style={styles.messageStatus}>{`Accepted`}</Text>
              )
            }
            {
              item.status == 'expired' && (
                <Text style={styles.expiredStatus}>{`Expired`}</Text>
              )
            }
            {
              item.status == 'cancelled' && (
                <Text style={styles.cancelledStatus}>{`Cancelled`}</Text>
              )
            }
            {
              item.status == 'rejected' && (
                <Text style={styles.rejectedStatus}>{`Rejected`}</Text>
              )
            }
          </HStack>
          <Text style={styles.message}>{item.message}</Text>
          <Text style={styles.messageDate}>{moment(item.created_at).format("YYYY-MM-DD HH:mm")}</Text>
          <View style={styles.separatorItem} />
        </View>
      </Pressable>
    );
  };

  const renderClaimAll = () => {
    return (
      <Pressable style={styles.containerClaimAll}>
        <Text style={styles.textClaimAll}>Claim All</Text>
      </Pressable>
    );
  };

  return (
    <View style={styles.container}>
      {renderTopNavigation()}
      <FlatList
        alwaysBounceVertical
        data={inboxData.data}
        keyExtractor={(item) => item.id}
        renderItem={renderInboxList}
      />
      <InboxInviteConfirmation
        isOpen={isInviteOpen}
        item={itemSelected}
        onClose={() => setInviteOpen(false)}
        onAccept={() => handleInvite("accept")}
        onDecline={() => handleInvite("reject")}
      />
      {/* {renderClaimAll()} */}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  separatorItem: {
    backgroundColor: "#EBEEF4",
    height: 1,
  },
  bgMessageRead: {
    backgroundColor: "white",
    paddingTop: 16,
  },
  bgMessageUnread: {
    backgroundColor: "#F8F9FC",
    paddingTop: 16,
  },
  messageTitle: {
    color: "#000000",
    fontFamily: "Montserrat-Bold",
    fontSize: 12,
    paddingBottom: 3,
    paddingHorizontal: 16,
    flexWrap: "wrap",
    flexShrink: 1
  },
  messageStatus: {
    color: "#009174",
    fontFamily: "Montserrat-Bold",
    fontSize: 10,
    marginRight: moderateScale(16, .25)
    // paddingBottom: 3,
    // paddingHorizontal: 16,
  },
  expiredStatus: {
    color: "grey",
    fontFamily: "Montserrat-Bold",
    fontSize: 10,
    marginRight: moderateScale(16, .25)
    // paddingBottom: 3,
    // paddingHorizontal: 16,
  },
  rejectedStatus: {
    color: "red",
    fontFamily: "Montserrat-Bold",
    fontSize: 10,
    marginRight: moderateScale(16, .25)
    // paddingBottom: 3,
    // paddingHorizontal: 16,
  },
  cancelledStatus: {
    color: "#DD5B4F",
    fontFamily: "Montserrat-Bold",
    fontSize: 10,
    marginRight: moderateScale(16, .25)
    // paddingBottom: 3,
    // paddingHorizontal: 16,
  },
  messageDate: {
    color: "#000000",
    fontFamily: "Montserrat-Regular",
    fontSize: 10,
    paddingBottom: 3,
    paddingHorizontal: 16,
  },
  message: {
    color: "#000000",
    fontFamily: "Montserrat-Regular",
    fontSize: 10,
    paddingBottom: 8,
    paddingHorizontal: 16,
  },
  containerClaimAll: {
    backgroundColor: "#1035BB",
    height: 24,
    borderRadius: 24 / 2,
    justifyContent: "center",
    alignSelf: "center",
    alignItems: "center",
    position: "absolute",
    bottom: 16,
  },
  textClaimAll: {
    color: "white",
    paddingHorizontal: 16,
  },
});

const MOCK_DATA_INBOX = [
  {
    id: 1,
    type: 1, // example for promo
    title: "Promo 1",
    date: "20 Jan 2021",
    message:
      "Lorem ipsum, or lipsum as it is dummy text used in laying out print  delay, Rp 500.000 Lorem ipsumfaksduhfkashjdfkaj...",
    isRead: false,
  },
  {
    id: 2,
    type: 1,
    title: "Promo 2",
    date: "21 Jan 2021",
    message:
      "Lorem ipsum, or lipsum as it is dummy text used in laying out print  delay, Rp 500.000 Lorem ipsumfaksduhfkashjdfkaj...",
    isRead: true,
  },
  {
    id: 3,
    type: 2,
    title: "Invitation - Groups",
    date: "22 Jan 2021",
    message:
      "Lorem ipsum, or lipsum as it is dummy text used in laying out print  delay, Rp 500.000 Lorem ipsumfaksduhfkashjdfkaj...",
    isRead: false,
  },
];
