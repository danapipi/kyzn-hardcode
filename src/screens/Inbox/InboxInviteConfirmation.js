import React from "react";
import {
  View,
  Text,
  Actionsheet,
  Box,
  Button,
  HStack,
  Image,
} from "native-base";
import { image } from "../../../assets/images";
import { moderateScale } from "react-native-size-matters";

export default function InboxInviteConfirmation({
  isOpen,
  onClose,
  onDecline,
  onAccept,
  item,
  status = "pending_invite"
}) {
  return (
    <View>
      <Actionsheet isOpen={isOpen} onClose={onClose}>
        <Actionsheet.Content h={340}>
          <Image
            source={image.bg_actionsSheet}
            style={{ width: 300, height: 300, position: "absolute", right: 0 }}
            alt="image"
          />
          {/* <Image
            source={image.pp_image}
            style={{ width: 60, height: 60, borderRadius: 60 / 2 }}
            resizeMode={"contain"}
            alt="image"
          /> */}
          <Box style={{ width: 270, alignItems: "center", marginTop: 8 }}>
            <Text
              style={{
                fontSize: 14,
                color: "#000",
                fontWeight: "bold",
                fontFamily: "Montserrat-Regular",
                textAlign: "center",
              }}
              alignItems="flex-start"
            >
              {item.title}
            </Text>
            <Text
              style={{
                fontSize: 12,
                color: "#000",
                fontWeight: "500",
                fontFamily: "Montserrat-Regular",
                textAlign: "center",
                marginTop: 10,
              }}
              alignItems="flex-start"
            >
              {item.message}
              <Text
                style={{
                  fontWeight: "normal",
                }}
              ></Text>
            </Text>
          </Box>
          {
            status == "accepted" ? (
              <Box position={"absolute"} bottom={moderateScale(20, .25)} width={'100%'}> 
                <Text style={{
                  fontSize: moderateScale(14, .25),
                  color: "#000",
                  fontWeight: "500",
                  fontFamily: "Montserrat-Regular",
                  textAlign: "center",
                  marginTop: 10,
                }}> 
                  {`You already Accepted this invitation`}
                </Text>
                <Button
                  w={"100%"}
                  bgColor={"white"}
                  onPress={onClose}
                  color={"#fff"}
                  style={{ marginTop: moderateScale(50, .25), borderRadius: 0 }}
                >
                  <Text
                    style={{
                      fontWeight: "bold",
                      color: "#000",
                      fontFamily: "Montserrat-Regular",
                    }}
                  >
                    Close
                  </Text>
                </Button>
              </Box>
            ) : (
              <Box position={"absolute"} justifyContent={"center"} alignItems={"center"} bottom={moderateScale(12, .25)} width={'100%'}> 
                <Button
                  w={"100%"}
                  bgColor={"#1035BB"}
                  onPress={onAccept}
                  color={"#fff"}
                  style={{ marginTop: 16, borderRadius: 0 }}
                >
                  <Text
                    style={{
                      fontWeight: "bold",
                      color: "#fff",
                      fontFamily: "Montserrat-Regular",
                    }}
                  >
                    Accept
                  </Text>
                </Button>
                <Button
                  w={"100%"}
                  bgColor={"white"}
                  onPress={onDecline}
                  color={"#fff"}
                  style={{ marginTop: 10, borderRadius: 0 }}
                >
                  <Text
                    style={{
                      fontWeight: "bold",
                      color: "#FF3F22",
                      fontFamily: "Montserrat-Regular",
                    }}
                  >
                    Decline
                  </Text>
                </Button>
              </Box>
            )
          }
        </Actionsheet.Content>
      </Actionsheet>
    </View>
  );
}
