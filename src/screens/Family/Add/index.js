import React, { useCallback, useEffect, useState } from "react";
import {
  Box,
  Button,
  Text,
  ScrollView,
  Image,
  ChevronDownIcon,
  FlatList,
  Actionsheet,
  Toast,
} from "native-base";
import _get from "lodash/get";
import _ from "lodash";
import {
  TouchableOpacity,
  ImageBackground,
  Dimensions,
  View,
  Alert,
} from "react-native";
import PropTypes from "prop-types";
import { useDispatch, useSelector } from "react-redux";
import Selectors from "selectors";
import Actions from "actions";
import AccountsMenu from "components/ui/AccountsMenu";
import AboutMenu from "components/ui/About";
import AddKidsCard from "components/ui/AddKidsCard";
import PremiumCard from "components/ui/PremiumCard";
import { getScreenWidth } from "utils/size";
import DividerHorizontal from "components/ui/DividerHorizontal";
import { moderateScale } from "react-native-size-matters";
import SearchBar from "components/ui/SearchBar";
import { image } from "images";
import FamilyCard from "../../../components/ui/FamilyCard";
import { UserApi } from "../../../utils/Api";
import { isNull } from "lodash";
import { useIsFocused } from "@react-navigation/native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

const user = UserApi.client;

const AddFamily = ({ navigation, route }) => {
  const [searchText, setSearchText] = useState("");
  const [openSheet, setOpenSheet] = useState(false);
  const [openKickSheet, setOpenKickSheet] = useState(false);
  const [openCancelInviteSheet, setOpenCancelInviteSheet] = useState(false);
  const [userIdToAdd, setUserIdToAdd] = useState(null);
  const [userIdToKick, setUserIdToKick] = useState(null);
  const [userIdToCancelInvite, setUserIdToCancelInvite] = useState(null);
  const [userFamilyIdToCancelInvite, setUserFamilyIdToCancelInvite] =
    useState(null);
  const [addFamilyMemberLoading, setAddFamilyMemberLoading] = useState(false);
  const [loadingDisband, setLoadingDisband] = useState(false);
  const [loadingCancelInvite, setLoadingCancelInvite] = useState(false);

  const dispatch = useDispatch();
  const isFocused = useIsFocused();
  // const family = dispatch(Actions.fetchAllFamilies());
  const profile = useSelector(Selectors.getProfile);
  const familyOne = useSelector(Selectors.getFamilyOne)

  // const membersOfTheFamily = _get(route.params, "familyMembers", [])

  const [dataUsers, setDataKids] = useState(familyOne.user_family_details);
  const [familyMembers, setFamilyembers] = useState(familyOne.user_family_details);
  console.log("familyMembers: ", familyMembers);

  const familyMembs = useSelector(Selectors.getUserFamily);
  console.log("family members: ", familyMembs);

  const familyId = _get(route.params, "familyId", 1);

  const closeSheet = () => {
    setOpenSheet(false);
  };

  const closeKickSheet = () => {
    setOpenKickSheet(false);
  };

  const closeCancelInviteSheet = () => {
    setOpenCancelInviteSheet(false);
  };

  const textOnChange = useCallback(text => {
    console.log("AddFamily text: ", text);
    if (text === "") {
      setDataKids(familyMembers);
      // setDataKids(familyMembs.user_family_details)
    }
    setSearchText(text);
  });

  const onSubmit = useCallback(e => {
    searchPhoneNumber(searchText);
  });

  const searchPhoneNumber = async text => {
    console.log("the text: ", text);
    if (text !== "") {
      await user
        .get(`/user?q=${encodeURIComponent(text)}`)
        .then(res => {
          console.log("the res: ", res);
          setDataKids(res.data.data);
        })
        .catch(err => {
          console.log("the err: ", err);
        });
    }
    // setDataKids(result)
  };

  const saveAddedPeople = userId => {
    const requestBody = {
      familyId: familyId,
      userId: userId,
    };
    // closeSheet()
    setAddFamilyMemberLoading(true);
    user
      .post("/family/add-member", requestBody)
      .then(res => {
        setAddFamilyMemberLoading(false);
        console.log("add Family res: ", res);
        Toast.show({
          description: "User has been invited to the family",
          duration: 3500,
        });
        // fetchFamily();
      })
      .catch(err => {
        setAddFamilyMemberLoading(false);
        console.log("add Family err: ", err);
        let dataError = err.response;
        let errorCode = _.get(dataError, "status", " ");
        let errorMassage = _.get(dataError, "data.message", " ");

        Alert.alert("Error", `${errorMassage}`, [
          { text: "OK", onPress: () => console.log("OK Pressed") },
        ]);
      });

    console.log("saveAddedPeople: userIdToAdd: ", userIdToAdd);
    console.log("saveAddedPeople: requestBody: ", requestBody);
  };

  const removeFamilyMember = id => {
    user
      .delete(`family/${familyId}/kick`, {
        data: {
          userId: id,
        },
      })
      .then(res => {
        console.log("res kick user: ", res);
        // fetchFamily();
        closeKickSheet();
      })
      .catch(err => {
        console.log("err cannot kick user: ", err);
        closeKickSheet();
      });
  };

  const cancelInvitingFamilyMember = (id, ufid) => {
    // TODO
    setLoadingCancelInvite(true);
    user
      .post("/family/cancel-invitation", {
        familyId: ufid,
        userId: id,
      })
      .then(res => {
        console.log("res cancel the invitation to user: ", res);
        // fetchFamily();
        closeCancelInviteSheet();
        setLoadingCancelInvite(false);
      })
      .catch(err => {
        console.log("err cancel the invitation to user: ", err.response.data);
        Toast.show({
          description: err.response.data.message,
        });
        closeCancelInviteSheet();
        setLoadingCancelInvite(false);
      });
  };

  const disbandFamily = () => {
    setLoadingDisband(true);
    user
      .delete(`/family/${familyId}/disband`)
      .then(async res => {
        // then disband the family
        console.log("disband family res: ", res);
        // await dispatch(Actions.fetchAllFamilies());
        setTimeout(() => {
          // set for 2 seconds in order to async the family fetching
          navigation.goBack();
        }, 2000);
      })
      .catch(err => {});
  };

  const fetchFamily = () => {
    user
      .get(`/family/${familyId}`)
      .then(res => {
        console.log("family: ", res);
        // if(dataUsers.length == 0) {
        setDataKids(res.data.data.user_family_details);
        // }
        setFamilyembers(res.data.data.user_family_details);
      })
      .catch(err => {
        console.log("family err: ", err);
      });
  };

  useEffect(() => {
    // fetchFamily()
    console.log("profile on edit family: ", route.params);
  }, []);

  useEffect(() => {
    dispatch(Actions.fetchUserFamily(familyId));
  }, [userIdToAdd]);

  useEffect(() => {
    // fetchFamily();
  }, [isFocused]);

  return (
    <KeyboardAwareScrollView
      // alignItems="center"
      // justifyContent="center"
      // bg={"white"}
      style={
        {
          // flex: 1,
        }
      }
      contentContainerStyle={{
        alignItems: "center",
        height: "100%",
        justifyContent: "center",
        backgroundColor: "white",
      }}>
      <Box
        style={{
          flex: 0.15,
          // height: moderateScale(8, 0.25),
          width: "100%",
          flexDirection: "row",
          alignItems: "center",
          paddingHorizontal: moderateScale(16, 0.25),
          justifyContent: "space-between",
        }}>
        <SearchBar
          placeholder="Search username or phone number"
          // contentContainerStyle={{width: "90%"}}
          onChangeText={textOnChange}
          contentContainerStyle={{
            width: "75%",
          }}
          // onSubmit={onSubmit}
        />
        <TouchableOpacity onPress={() => onSubmit()}>
          <Box
            style={{
              backgroundColor: "rgba(224, 224, 224, 1)",
              padding: moderateScale(14, 0.25),
              // paddingVertical: moderateScale(16, .25),
              // paddingHorizontal: moderateScale(14, .25),
              // width: moderateScale(100, .25),
              justifyContent: "center",
              alignItems: "center",
            }}>
            <Text
              style={{
                color: "#000",
              }}>
              {"Search"}
            </Text>
          </Box>
        </TouchableOpacity>
        {/* </ImageBackground> */}
      </Box>
      {/* <Text>{JSON.stringify(dataUsers[0].user_family_id)}</Text> */}
      <Box
        style={{ flex: 1, height: "100%" }}
        showsVerticalScrollIndicator={false}>
        {dataUsers.length > 0 ? (
          <FlatList
            data={dataUsers}
            extraData={dataUsers}
            renderItem={({ item, index }) => {
              return (
                <FamilyCard
                  familyMembers={familyMembers}
                  data={item}
                  onPress={userId => {
                    console.log("userId nya: ", userId);
                    // setUserIdToAdd(userId)
                    saveAddedPeople(userId);
                  }}
                  onRemoveMember={userId => {
                    console.log("userId for remove: ", userId);
                    setUserIdToKick(userId);
                    setOpenKickSheet(true);
                  }}
                  onCancelInvite={userId => {
                    console.log(
                      "userId for cancel invite: ",
                      item.user_family_id,
                      userId,
                    );
                    setOpenCancelInviteSheet(true);
                    setUserIdToCancelInvite(userId);
                    setUserFamilyIdToCancelInvite(item.user_family_id);
                  }}
                  selfUserId={profile.id}
                />
              );
            }}
            style={{
              width: Dimensions.get("screen").width,
            }}
            keyExtractor={(item, index) => String(item + index)}
            ListFooterComponent={() => (
              <View>
                <View
                  style={{
                    width: "100%",
                    borderColor: "#DFE3EC",
                    borderTopWidth: 0.5,
                    marginBottom: moderateScale(45),
                  }}
                />
                <TouchableOpacity
                  // style={{backgroundColor: 'red', width: "100%"}}
                  onPress={() =>
                    navigation.navigate("AddKids", {
                      familyId: route.params.familyId,
                    })
                  }>
                  <Text
                    style={{
                      color: "#212121",
                      fontSize: moderateScale(14, 0.25),
                      fontFamily: "Montserrat-Regular",
                      fontWeight: "700",
                      marginRight: moderateScale(17, 0.25),
                      alignSelf: "center",
                    }}>
                    + Add Kids
                  </Text>
                </TouchableOpacity>
              </View>
            )}
          />
        ) : (
          <View>
            <View
              style={{
                width: "100%",
                borderColor: "#DFE3EC",
                borderTopWidth: 0.5,
                marginBottom: moderateScale(45),
              }}
            />
            <TouchableOpacity
              // style={{backgroundColor: 'red', width: "100%"}}
              onPress={() =>
                navigation.navigate("AddKids", {
                  familyId: route.params.familyId,
                })
              }>
              <Text
                style={{
                  color: "#212121",
                  fontSize: moderateScale(14, 0.25),
                  fontFamily: "Montserrat-Regular",
                  fontWeight: "700",
                  marginRight: moderateScale(17, 0.25),
                  alignSelf: "center",
                }}>
                + Add Kids
              </Text>
            </TouchableOpacity>
          </View>
        )}
      </Box>
      <Button
        onPress={() => {
          navigation.goBack();
        }}
        isLoading={false}
        style={{
          backgroundColor: "#1035BB",
          marginVertical: moderateScale(16),
          width: "90%",
        }}>
        <Text
          style={{
            fontSize: moderateScale(18, 0.25),
            fontFamily: "Montserrat-Regular",
            fontWeight: "700",
            color: "#fff",
          }}>
          DONE
        </Text>
      </Button>
      {/* <Button
        onPress={() => {
          // show alert
          // quit family first
          // user.delete(`/family/${familyId}/quit`)
          setOpenSheet(true);
        }}
        isLoading={false}
        style={{
          backgroundColor: "#DD5B4F",
          marginVertical: moderateScale(16),
          width: "90%",
        }}>
        <Text
          style={{
            fontSize: moderateScale(18, 0.25),
            fontFamily: "Montserrat-Regular",
            fontWeight: "700",
            color: "#fff",
          }}>
          DISBAND
        </Text>
      </Button> */}

      {/* DISBAND FAMILY ACTION SHEET  */}
      <Actionsheet isOpen={openSheet} onClose={closeSheet}>
        <Actionsheet.Content h={245}>
          <Image
            source={image.bg_actionsSheet}
            style={{ width: 300, height: 300, position: "absolute", right: 0 }}
            alt="image"
          />

          <Box
            style={{
              width: 270,
              marginTop: 8,
              alignItems: "center",
              justifyContent: "center",
            }}>
            <Text
              style={{
                fontSize: moderateScale(16, 0.25),
                color: "#000",
                fontWeight: "bold",
                fontFamily: "Montserrat-Regular",
                marginBottom: moderateScale(12, 0.25),
                textAlign: "center",
              }}>
              {"Are you sure you want DISBAND this family?"}
            </Text>
            <Button
              onPress={() => {
                disbandFamily();
              }}
              // isLoading={addFamilyMemberLoading}
              style={{
                backgroundColor: "#1035BB",
                marginVertical: moderateScale(16),
                width: Dimensions.get("screen").width * 0.9,
              }}
              isLoading={loadingDisband}>
              <Text
                style={{
                  fontSize: moderateScale(18, 0.25),
                  fontFamily: "Montserrat-Regular",
                  fontWeight: "700",
                  color: "#fff",
                }}>
                {"YES"}
              </Text>
            </Button>
            <TouchableOpacity onPress={() => closeSheet()}>
              <Text
                style={{
                  fontSize: moderateScale(18, 0.25),
                  fontFamily: "Montserrat-Regular",
                  fontWeight: "700",
                  color: "#000",
                }}>
                {"Cancel"}
              </Text>
            </TouchableOpacity>
          </Box>
        </Actionsheet.Content>
      </Actionsheet>

      {/* MEMBER KICK ACTION SHEET */}
      <Actionsheet isOpen={openKickSheet} onClose={closeKickSheet}>
        <Actionsheet.Content h={245}>
          <Image
            source={image.bg_actionsSheet}
            style={{ width: 300, height: 300, position: "absolute", right: 0 }}
            alt="image"
          />

          <Box
            style={{
              width: 270,
              marginTop: 8,
              alignItems: "center",
              justifyContent: "center",
            }}>
            <Text
              style={{
                fontSize: moderateScale(16, 0.25),
                color: "#000",
                fontWeight: "bold",
                fontFamily: "Montserrat-Regular",
                marginBottom: moderateScale(12, 0.25),
                textAlign: "center",
              }}>
              {"Are you sure you want to KICK this member from your family?"}
            </Text>
            <Button
              onPress={() => {
                removeFamilyMember(userIdToKick);
              }}
              // isLoading={addFamilyMemberLoading}
              style={{
                backgroundColor: "#1035BB",
                marginVertical: moderateScale(16),
                width: Dimensions.get("screen").width * 0.9,
              }}
              isLoading={loadingDisband}>
              <Text
                style={{
                  fontSize: moderateScale(18, 0.25),
                  fontFamily: "Montserrat-Regular",
                  fontWeight: "700",
                  color: "#fff",
                }}>
                {"YES"}
              </Text>
            </Button>
            <TouchableOpacity
              onPress={() => {
                setUserIdToKick(null);
                closeKickSheet();
              }}>
              <Text
                style={{
                  fontSize: moderateScale(18, 0.25),
                  fontFamily: "Montserrat-Regular",
                  fontWeight: "700",
                  color: "#000",
                }}>
                {"Cancel"}
              </Text>
            </TouchableOpacity>
          </Box>
        </Actionsheet.Content>
      </Actionsheet>

      {/* CANCEL INVITE ACTION SHEET */}
      <Actionsheet
        isOpen={openCancelInviteSheet}
        onClose={closeCancelInviteSheet}>
        <Actionsheet.Content h={245}>
          <Image
            source={image.bg_actionsSheet}
            style={{ width: 300, height: 300, position: "absolute", right: 0 }}
            alt="image"
          />

          <Box
            style={{
              width: 270,
              marginTop: 8,
              alignItems: "center",
              justifyContent: "center",
            }}>
            <Text
              style={{
                fontSize: moderateScale(16, 0.25),
                color: "#000",
                fontWeight: "bold",
                fontFamily: "Montserrat-Regular",
                marginBottom: moderateScale(12, 0.25),
                textAlign: "center",
              }}>
              {"Are you sure you want to CANCEL INVITATION of this member?"}
            </Text>
            <Button
              onPress={() => {
                cancelInvitingFamilyMember(
                  userIdToCancelInvite,
                  userFamilyIdToCancelInvite,
                );
              }}
              // isLoading={addFamilyMemberLoading}
              style={{
                backgroundColor: "#1035BB",
                marginVertical: moderateScale(16),
                width: Dimensions.get("screen").width * 0.9,
              }}
              isLoading={loadingCancelInvite}>
              <Text
                style={{
                  fontSize: moderateScale(18, 0.25),
                  fontFamily: "Montserrat-Regular",
                  fontWeight: "700",
                  color: "#fff",
                }}>
                {"YES"}
              </Text>
            </Button>
            <TouchableOpacity
              onPress={() => {
                setUserIdToCancelInvite(null);
                closeCancelInviteSheet();
              }}>
              <Text
                style={{
                  fontSize: moderateScale(18, 0.25),
                  fontFamily: "Montserrat-Regular",
                  fontWeight: "700",
                  color: "#000",
                }}>
                {"Cancel"}
              </Text>
            </TouchableOpacity>
          </Box>
        </Actionsheet.Content>
      </Actionsheet>
    </KeyboardAwareScrollView>
  );
};

AddFamily.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default AddFamily;
