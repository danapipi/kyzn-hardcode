import React, { useState, useCallback, useEffect } from 'react';
import {
  Box,
  Button,
  Column,
  FlatList,
  ScrollView,
  HStack,
  Image,
  ZStack,
  Modal,
  VStack,
  Actionsheet
} from "native-base";
import _get from 'lodash/get';
import { ActivityIndicator, Dimensions, ImageBackground, Text, TouchableOpacity, View, Alert } from 'react-native';
import _filter from 'lodash/filter';
import _sortBy from 'lodash/sortBy';

import { moderateScale } from 'react-native-size-matters';
import moment from 'moment';
import { image } from '../../../assets/images';
import ButtonCustom from '../../components/ui/ButtonCustom';
import Commons from '../../utils/common';
import parseErrorStack from 'react-native/Libraries/Core/Devtools/parseErrorStack';
import { UserApi } from "../../utils/Api";
import { useSelector } from 'react-redux';
import Selectors from "selectors";

const userapi = UserApi.client;

const CouponList = ({
    navigation,
    route
}) => {
  const couponsData = useSelector(state => state.FAMILY.all.couponsData)
  const [couponData, setCouponData] = useState(couponsData)
  const [isLoading, setIsLoading] = useState(true)

  const profile = useSelector((state) => state.AUTH.profile);

  console.log("coupons: ", couponsData);

  const { 
    class_activity_type_id, 
    panorama_activity_type_id, 
    pt_activity_type_id, 
    rent_court_activity_type_id
  } = useSelector((state) => state.PRODUCT.activityTypeId.activityTypeId);

  const couponFilter= _get(route.params, "filter", "")
  // const couponFilter= "Baksket ball coupon"

  const activityId = _get(route.params, "activityId", 0)

  const couponsAvailableFromActivity = _get(route.params, 'couponAvailable', [])

  const isFromPayment = _get(route.params, "isFromPayment", false)

  const type = _get(route.params, "type", 1)

  const stepMultiplier = _get(route.params, "stepMultiplier", 1)
  const activityWorkerId = _get(route.params, "activityWorkerId", 0)
  const userId = _get(route.params, "userId", profile.data.id)

  // const filteredCoupon = _filter(couponData, ["name", couponFilter])
  const filteredCoupon = couponData.filter((x) => {
    // return x.name.toLowerCase().includes(couponFilter.toLowerCase())
    return x.status == "unused" && (moment(x.expired_at).format('x') > moment().format('x'))
  })

  const convertToDate = (date) => {
    return moment(date).format("ddd, DD MMM yyyy")
  }

  const determineDiscount = (disc, price) => {
    if(disc == 'amount') {
      return `${Commons.convertToRupiah(price)}`
    } else if(disc == 'percentage') {
      return `${price}%`
    }
  }

  const goBackAndRefresh = (couponId, nameOfCoupon, isValid, discount, discount_type) => {
    console.log("goBackAndRefresh params:  ", route.params);
    const properDiscount = isValid === true ? discount : 0
    navigation.goBack()
    route.params.onRefresh({ // part of onRefresh from payment page
      couponId: couponId,
      couponApplied: nameOfCoupon,
      isCouponValid: isValid,
      discountFromCoupon: properDiscount,
      discountType: discount_type
    })
  }

  const clearCouponAndGoBack = () => {
    navigation.goBack()
    route.params.onRefresh({ // part of onRefresh from payment page
      couponId: null,
      couponApplied: "",
      isCouponValid: null,
      discountFromCoupon: 0,
      discountType: 'amount' // just make it default 
    })
  }

  const isCouponExpired = (date) => {
    return moment().format('x') > moment(date).format('x')
  }

  const applyCoupon = (couponId, couponName, discount, discountType) => {
    let requestBody
    // console.log("couponName:  ", couponName);
    // console.log("couponId:  ", couponId);
    // console.log("activityId:  ", activityId);

    if(type == pt_activity_type_id) {
      requestBody = {
        userCouponId: couponId,
        activityId: activityId,
        userId: userId,
        stepMultiplier: stepMultiplier,
        activityWorkerId: activityWorkerId,
      }
    } else if(type == rent_court_activity_type_id) {
      requestBody = {
        userCouponId: couponId,
        activityId: activityId,
        userId: userId,
        stepMultiplier: stepMultiplier,
      }
    } else {
      requestBody = {
        userCouponId: couponId,
        activityId: activityId,
        userId: userId
      }
    }

    userapi.post(`/user-coupon/apply`, requestBody)
      .then(res => {
        // when success, pop back to previous screen, while refreshing it
        console.log("res: ", res);
        console.log("discount: ", discount);
        // const discount = res.data.data.discountedPrice
        // console.log("discount: ", discount);
      goBackAndRefresh(couponId, couponName, true, discount, discountType)
        
      })
      .catch(err => {
        Alert.alert("Error", err.response.data.message, [
          {
            text: "OK",
            onPress: () => console.log("OK Pressed"),
            style: "cancel",
          },
        ]);
        goBackAndRefresh(couponId, couponName, false, 0, discountType)
      })
  }

  useEffect(() => {
    if(couponsAvailableFromActivity.length > 0) {
      setCouponData(couponsAvailableFromActivity)
      setIsLoading(false)
    } else {
      // const sortedCoupon = _sortBy(couponsData, ["expired_at"])
      // setCouponData(sortedCoupon)
      // userapi.get(`/user-coupon/`)
      //   .then(res => {
      //     console.log("coupon datas: ", res.data.data);
      //     const sortedCoupon = _sortBy(res.data.data, ["expired_at"])
      //     console.log("coupon sorted: ", sortedCoupon);
      //     // setCouponData(res.data.data)
      //     // setCouponData(sortedCoupon)
          setIsLoading(false)
      //   })
      //   .catch(err => {
      //     console.log("coupons err: ",err);
      //     setIsLoading(false)
      //   })
    }
    console.log("filtered coupons: ", filteredCoupon);
    console.log("couponsAvailableFromActivity: ", couponsAvailableFromActivity);
    console.log("route.params: ", route.params);
    console.log("CouponData: ", couponData);
  }, [])

  return(
    <Box flex={1}>
      {
        isLoading == true ? (
          <Box flex={1} backgroundColor={'white'} alignItems={"center"} justifyContent={"center"}>
            <ActivityIndicator 
              size={"large"}
              color={'blue'}
            />
          </Box>
        ) : (
          <Box style={{ backgroundColor: "white", flex: 1 }}>
            <FlatList 
              data={filteredCoupon}
              showsVerticalScrollIndicator={false}
              contentContainerStyle={{
                alignItems: 'center',
                // justifyContent: 'center',
              }}
              renderItem={({item: {
                id,
                name, 
                discount, 
                discount_type, 
                effective_from, 
                expired_at, 
                status
              }}) => {
                let isExpired = isCouponExpired(expired_at)
                return(
                  <Box style={{
                    width: Dimensions.get('screen').width * .92,
                    height: moderateScale(164, .25),
                    justifyContent: 'flex-start',
                    alignItems: 'flex-start',
                    borderColor: '#EBEEF4',
                    borderWidth: 1,
                    marginBottom: moderateScale(25, .25),
                  }}>
                    <ImageBackground 
                      style={{
                        width: '100%',
                        height: isFromPayment == true ? "85%" : "100%",
                      }}
                    >
                    {/* <Image 
                      source={image.coupon_gradient}
                      style={{
                        position: 'absolute',
                        left: 0,
                        // top: 0,
                        width: "85%",
                        height: isFromPayment == true ? "85%" : "100%",
                        resizeMode: 'stretch'
                      }}
                      alt="images"
                    /> */}
                    <Box style={{
                      flex: 1,
                      padding: moderateScale(16, .25)
                    }}>
                      <Text style={{
                        fontWeight: "700",
                        fontSize: moderateScale(12, .25),
                        letterSpacing: 0.2,
                        color: "#000",
                        fontFamily: "Montserrat-Regular",
                        marginBottom: isFromPayment == true ? moderateScale(4, .25) : moderateScale(10, .25)
                      }}>
                        {`${name}`}
                      </Text>
                      <Text style={{
                        fontWeight: "700",
                        fontSize: moderateScale(12, .25),
                        letterSpacing: 0.2,
                        color: "#000",
                        fontFamily: "Montserrat-Regular",
                      }}>
                        {`Discount ${determineDiscount(discount_type, discount)}`}
                      </Text>
                      {
                        isFromPayment == false && (
                          <Text style={{
                            fontWeight: "700",
                            fontSize: moderateScale(12, .25),
                            position: 'absolute',
                            bottom: moderateScale(10, .25),
                            left: moderateScale(16, .25),
                            letterSpacing: 0.2,
                            color: "#000",
                            fontFamily: "Montserrat-Regular",
                          }}>
                            {`Effective until: ${convertToDate(expired_at)}`}
                          </Text>
                        )
                      }
                    </Box>
                    </ImageBackground>
                    {
                      isFromPayment == true && (
                        <Box style={{
                          flex: 1,
                          marginTop: moderateScale(-21, .25),
                          width: '100%',
                          height: '100%',
                          // backgroundColor: 'red',
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                          alignItems: 'center',
                          paddingHorizontal: moderateScale(16, .25)
                        }}>
                          <Text style={{
                            fontWeight: "700",
                            fontSize: moderateScale(12, .25),
                            letterSpacing: 0.2,
                            color: "#000",
                            fontFamily: "Montserrat-Regular",
                          }}>
                            {`Discount ${determineDiscount(discount_type, discount)}`}
                          </Text>
                          <TouchableOpacity 
                            onPress={() => {
                              if(route.params.couponApplied !== "" && id === route.params.couponId) {
                                clearCouponAndGoBack()
                              } else {
                                // applyCoupon(id, name, discount_type)
                                applyCoupon(id, name, discount, discount_type)
                              }
                            }}
                            disabled={
                              status == 'used' 
                                ? true 
                                : isExpired == true 
                                  ? true 
                                  : false
                            }
                          >
                            <Box style={{
                              justifyContent: 'center',
                              alignItems: 'center',
                              paddingVertical: moderateScale(8, .25),
                              width: Dimensions.get("screen").width * .25,
                              backgroundColor: route.params.couponApplied !== "" && id === route.params.couponId
                                ? "#DD5B4F"
                                : status == 'unused' ? 
                                  isExpired == true ?  
                                    "#C7CBCD" : 
                                    "#1035BB" : 
                                  "#C7CBCD"
                            }}> 
                              <Text style={{
                                fontWeight: "700",
                                fontSize: moderateScale(14, .25),
                                color: "#fff",
                                fontFamily: "Montserrat-Regular",
                              }}> 
                                {
                                  route.params.couponApplied !== "" && id === route.params.couponId
                                    ?  'Clear'
                                    : status == 'unused' 
                                      ? isExpired == true 
                                        ? "Expired" 
                                        : 'Apply'
                                      : 'Applied'
                                }
                              </Text>
                            </Box>
                          </TouchableOpacity>
                        </Box>
                      )
                    }
                  </Box>
                )
              }}
            />
          </Box>
        )
      }
    </Box>
  )
};

export default CouponList;
