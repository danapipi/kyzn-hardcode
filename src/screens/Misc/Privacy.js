import React, { useState, useEffect } from "react";
import { Dimensions, ActivityIndicator, Text } from "react-native";
import {
  Box,
  Button,
  FlatList,
  ScrollView,
  HStack,
  Image,
  ZStack,
  VStack,
  Actionsheet,
} from "native-base";
import _ from "lodash";
import PropTypes from "prop-types";
import { backgroundColor } from "styled-system";
import { useRoute } from "@react-navigation/core";
import moment from "moment";
import Actions from "actions";
import { image } from "../../../assets/images";
import { useDispatch, useSelector } from "react-redux";
import Commons from "../../utils/common";
import { UserApi } from "utils/Api";
import RenderHtml from "react-native-render-html";

const Privacy = ({ navigation }) => {
  let route = useRoute();
  const privacyPolicy = {
    html: `
  <h2>Privacy Policy</h2>

  <p>Updated at 2021-11-01</p>
  
  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sit amet posuere diam, vel vestibulum libero. Sed auctor viverra metus id rutrum. Suspendisse odio lectus, sollicitudin et egestas nec, rhoncus congue purus. Donec quis dapibus quam. Mauris eget vehicula velit, et ullamcorper turpis. Proin vitae urna a neque auctor efficitur. Suspendisse in odio ac augue dapibus feugiat ut non sem. Aenean non urna vestibulum, iaculis lectus at, porttitor dui. Cras porta mollis lectus, accumsan gravida justo iaculis at. Mauris id faucibus justo, vitae volutpat risus. Pellentesque vel aliquam neque. Vestibulum pulvinar, urna eu auctor viverra, mauris eros mattis nisl, sed bibendum nulla tortor congue diam. Praesent ultricies felis sem, quis malesuada dui sodales quis.</p>
  <p>Praesent sit amet condimentum sem. Proin ultricies dui vel metus semper, in fringilla massa ornare. Nunc facilisis orci vel dignissim ullamcorper. Vestibulum imperdiet finibus tellus, malesuada lobortis nisi posuere at. Aliquam nec semper lorem, vel scelerisque felis. Proin eleifend sit amet libero eu tincidunt. In at lorem eleifend, posuere tortor eget, dictum odio. In accumsan ante lacus, vitae ornare turpis laoreet id. Mauris eget semper leo. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>


  `,
  };

  return (
    <Box flex={1}>
      <ScrollView style={{ backgroundColor: "white", flex: 1 }}>
        <HStack
          style={{ alignItems: "center", padding: 20, paddingVertical: 27 }}>
          <RenderHtml source={privacyPolicy} />
        </HStack>
      </ScrollView>
    </Box>
  );
};

export default Privacy;
