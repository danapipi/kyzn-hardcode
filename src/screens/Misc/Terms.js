import React, { useState, useEffect } from "react";
import { Dimensions, ActivityIndicator, Text } from "react-native";
import {
  Box,
  Button,
  FlatList,
  ScrollView,
  HStack,
  Image,
  ZStack,
  VStack,
  Actionsheet,
} from "native-base";
import _ from "lodash";
import PropTypes from "prop-types";
import { backgroundColor } from "styled-system";
import { useRoute } from "@react-navigation/core";
import moment from 'moment';
import Actions from "actions";
import { image } from "../../../assets/images";
import { useDispatch, useSelector } from "react-redux";
import Commons from "../../utils/common";
import { UserApi } from "utils/Api";
import RenderHtml from "react-native-render-html";
import TermsAndCondition from '../../utils/TermsAndCondition'


const Terms = ({ navigation }) => {

  let route = useRoute()

  return (
    <Box flex={1}>
      <ScrollView contentContainerStyle={{paddingLeft: 15, paddingRight: 15, backgroundColor: 'white', justifyContent: 'center'}}>
        <RenderHtml source={TermsAndCondition} />
      </ScrollView>
    </Box>
  );
};

export default Terms;
