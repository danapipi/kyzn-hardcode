import React, { useState, useEffect } from "react";
import { Dimensions, ActivityIndicator, Text } from "react-native";
import {
  Box,
  Button,
  FlatList,
  ScrollView,
  HStack,
  Image,
  ZStack,
  VStack,
  Actionsheet,
} from "native-base";
import _ from "lodash";
import PropTypes from "prop-types";
import { backgroundColor } from "styled-system";
import { useRoute } from "@react-navigation/core";
import moment from 'moment';
import Actions from "actions";
import { image } from "../../../assets/images";
import { useDispatch, useSelector } from "react-redux";
import Commons from "../../utils/common";
import { UserApi } from "utils/Api";
import RenderHtml from "react-native-render-html";


const FAQ = ({ navigation }) => {

  let route = useRoute()

  const faqText = {
    html: `
    <h1>FAQ App</h1>

<h2> Adult Passes</h2>

<h3>Does the Platinum Pass entitle me to unlimited classes?</h3>

<p>The Platinum Pass entitles the holder to unlimited classes on our Core Program. Non Core-Program classes may include group coaching for sports (not Pulse or Social Sports) and will be marked on the schedule. The Platinum Pass does not entitle the holder to private training or court rentals.</p>

<h2> Bookings</h2>

<h3>How far in advance can I book classes or court rentals?</h3>

<p>Platinum Pass &amp; Academy Pass holders may book up to 8 days in advance. All other users may book up to 7 days in advance.</p>

<h3>What if I cancel?</h3>

<p>Cancelling any product over 24 hours in advance to the start time will be refunded to KYZN money. Cancellations within 24 hours or no-shows will not be refunded.</p>

<p>Platinum Pass holders who cancel or no-show to their free inclusive classes within 24hrs more than 2x in a 30 day period will receive a 250.000 IDR fine. This is to prevent abuse of the unlimited class system &amp; to keep slots available for other guests.</p>

<h3>Can I join an activity or book a class if I don&rsquo;t have any Day Pass, Club Pass, Platinum Pass or Academy Pass?</h3>

<p>No, you will need to purchase a Day Pass + the activity to enter KYZN BSD.</p>

<h2>Junior Program</h2>

<h3>What is the difference between PRACTICE and ACADEMY sessions?</h3>

<p>ACADEMY classes are structured in a 10 week term, with 4 terms in a year and a Holiday Program runs outside of these times with altered timings.</p>

<p>PRACTICE classes are designed for drop-in students (For extra practice in the week, for those waiting to join a new term or for those who want a flexible schedule). Some activities at certain ages may only have PRACTICE sessions due to the nature of the program.</p>

<h3>What does my child get with the Academy Pass?</h3>

<p>Academy Pass holders are allowed to join 3 different activities which they must sign up to for the remainder of the term time. I.E if they join in Week 2 and pick Tennis on 4pm Friday, Badminton on 4pm Tuesday and Swimming 9am Sunday those dates will remain until the next term. Usually these will be ACADEMY classes when available.</p>

<p>Academy Pass holders will have access to the KYZN facilities and be able to make bookings and use of appropriate facilities. If their guardian is not a pass-holder they will be gifted a Guardian Pass which can be used to enter the space at the time of any classes or bookings.</p>

<h3>How often can I change these activities associated with my Academy Pass?</h3>

<p>We enroll our Academy Pass holders when they join with us &amp; at the beginning of each 10 week term. Changes in schedule after this time will be made only with full agreement from the Coach, Academy Coordinator and management. This is only in exceptional circumstances and if there are available slots in other activities/times.</p>

<h3>What happens outside of the 10 week terms with my child&rsquo;s Academy Pass?</h3>

<p>Outside of term-time we hold a &lsquo;&rsquo;Holiday Schedule&rsquo;&rsquo; with an amended schedule of classes and the addition of &lsquo;&rsquo;Camp Products&rsquo;&rsquo;. In these weeks the Academy Pass will entitle your child to join 3 sessions or the camp product (which may be valued at more than 1 session)</p>

<h3>Does my child have to be an Academy Pass holder to join the program?</h3>

<p>No! Your child can also join by purchasing a term pass or drop-in. However the Academy Pass will represent the best value for your child if they want to join multiple activities.</p>

<h3>My child missed a class, can I arrange a make-up class?</h3>

<p>Make-Up classes are only available in the event of a weather-related cancellation, or in exceptional circumstances if there are available slots in a suitable make-up class. This is not in any way guaranteed.</p>

<p>If your child has a medical reason for missing several sessions in the Academy you can contact our team and we will attempt to resolve on a case-by-case basis.</p>

<p></p>
  `,
  };

  return (
    <Box flex={1}>
      <ScrollView style={{ backgroundColor: "white", flex: 1 }}>
          <HStack
            style={{ alignItems: "center", padding: 20, paddingVertical: 27, color: "#000" }}
          >
          <RenderHtml source={faqText} />
          </HStack>
      </ScrollView>
    </Box>
  );
};

export default FAQ;
