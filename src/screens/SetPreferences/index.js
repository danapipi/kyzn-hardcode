import React, { useEffect, useState, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  View,
  Text,
  FlatList,
  TouchableOpacity,
  SectionList,
  Alert,
} from "react-native";
import { moderateScale } from "react-native-size-matters";
import { image } from "images";
import Actions from "actions";
import _ from "lodash";
import ButtonCustom from "components/ui/ButtonCustom";
import { UserApi } from "../../utils/Api";
import RNStorage from "../../utils/storage";



const userapi = UserApi.client;

const preferencesTemp = [
  {
    id: 8,
    name: "Wellness",
    description: "Stay healthy with us",
    child: [],
  },
  {
    id: 9,
    name: "Sports",
    description: "Stay healthy with us",
    child: [],
  },
  {
    id: 10,
    name: "Fitness",
    description: "Stay healthy with us",
    child: [],
  },
];

const SetPreferences = ({ navigation }) => {
  const dispatch = useDispatch();
  const categories = useSelector(state => state.PRODUCT.category);
  const createPreference = useSelector(state => state.PRODUCT.createPreference);
  const dataFirstTime = useSelector(state => state.AUTH.firstTime);
  const dataCategoryChild = useSelector((state) => state.PRODUCT.categoryChild);



  const [isLoading, setIsLoading] = useState(false);
  const countRef = useRef(0);

  const [selectedPreferences, setSelectedPreferences] = useState([]);
  const [preferences, setPreferencesData] = useState([]);
  // const [sportData, setSportData] = useState([
  //   { id: 1, label: "Basketball", value: "Basketball", selected: false },
  //   { id: 2, label: "Soccer", value: "Soccer", selected: false },
  //   { id: 3, label: "Tennis", value: "Tennis", selected: false },
  //   { id: 4, label: "Swim", value: "Swim", selected: false },
  //   { id: 5, label: "Combat", value: "Combat", selected: false },
  // ]);
  // const [wellnessData, setWellnessData] = useState([
  //   { id: 6, label: "Yoga", value: "Yoga", selected: false },
  //   { id: 7, label: "Pilates", value: "Pilates", selected: false },
  //   { id: 8, label: "Meditation", value: "Meditation", selected: false },
  //   { id: 9, label: "Recovery", value: "Recovery", selected: false },
  //   { id: 10, label: "Functional", value: "Functional", selected: false },
  //   { id: 11, label: "Beauty & Spa", value: "Beauty & Spa", selected: false },
  //   { id: 12, label: "Healing", value: "Healing", selected: false },
  // ]);

  // const [selectedIdSport, setSelectedIdSport] = useState([]);
  // const [selectedIdWellness, setSelectedIdWellness] = useState([]);
  // const arrayTemp = [];
  // const joinArray =
  //   selectedIdSport.length > 0 || selectedIdWellness.length > 0
  //     ? [...selectedIdWellness, ...selectedIdSport]
  //     : arrayTemp;

  useEffect(() => {
    dispatch(Actions.category());

    // if (!dataFirstTime.firstTime) {
    //   navigation.setOptions({
    //     headerRight: () => (
    //       <TouchableOpacity
    //         onPress={() =>
    //           // navigation.goBack()
    //           {
    //             navigation.navigate("BottomTabs");
    //             dispatch(Actions.firstTime(true));
    //             RNStorage.setFirstTime("true");
    //           }
    //         }
    //         style={{
    //           flexDirection: "row",
    //           alignItems: "center",
    //           justifyContent: "center",
    //         }}>
    //         <Text
    //           style={{
    //             fontSize: 16,
    //             color: "#000",
    //             letterSpacing: 0.2,
    //             fontWeight: "700",
    //             marginTop: 2,
    //             marginRight: 10,
    //           }}>
    //           SKIP
    //         </Text>
    //       </TouchableOpacity>
    //     ),
    //   });
    // }
  }, []);

  const { 
    class_activity_type_id, 
    panorama_activity_type_id, 
    pt_activity_type_id, 
    rent_court_activity_type_id,
    wellness_category_id,
    fitness_category_id,
    sport_category_id,
  } = useSelector((state) => state.PRODUCT.activityTypeId.activityTypeId);
  

  useEffect(() => {
    setIsLoading(true);
    let wellnessId = _.get(categories, "category[0].id", wellness_category_id);
    let sportId = _.get(categories, "category[1].id", fitness_category_id);
    let fitnessId = _.get(categories, "category[2].id", sport_category_id);
    let categoryChildWellness = dataCategoryChild.categoryChild[0].wellness
    let categoryChildSports = dataCategoryChild.categoryChild[1].sport
    let categoryChildFitness = dataCategoryChild.categoryChild[2].fitness;
    let dataWellnes = {
      id: 8,
      name: "Wellness",
      description: "Stay healthy with us",
      child: categoryChildWellness,
    };

    let dataSport = {
      id: 9,
      name: "Sports",
      description: "Stay healthy with us",
      child: categoryChildSports,
    };

    let dataFitness = {
      id: 10,
      name: "Fitness",
      description: "Stay healthy with us",
      child: categoryChildFitness,
    };

    setPreferencesData(state => [...state, dataWellnes, dataSport, dataFitness]);

    // dataWellnes = {
    //   id: 9,
    //   name: "Sports",
    //   description: "Stay healthy with us",
    //   child: categoryChildSports,
    // };

    // setPreferencesData(state => [...state, dataWellnes]);


    console.log("wellnessId", wellnessId);
    // // if (countRef.current == 0) {
    // //   userapi
    // //     .get(`/category-child?categoryId=${wellnessId}`)
    // //     .then(res => {
    // //       console.log("res category: ", res.data.data);
    // //       let dataWellnes = {
    // //         id: 8,
    // //         name: "Wellness",
    // //         description: "Stay healthy with us",
    // //         child: res.data.data,
    // //       };
    // //       setPreferencesData(state => [...state, dataWellnes]);
    // //     })
    // //     .catch(err => {
    // //       console.log("err: ", err);
    // //       setIsLoading(false);
    // //     });
    // //   userapi
    // //     .get(`/category-child?categoryId=${sportId}`)
    // //     .then(res => {
    // //       console.log("res category 2: ", res.data.data);
    // //       let dataWellnes = {
    // //         id: 9,
    // //         name: "Sports",
    // //         description: "Stay healthy with us",
    // //         child: res.data.data,
    // //       };
    // //       setPreferencesData(state => [...state, dataWellnes]);
    // //     })
    // //     .catch(err => {
    // //       console.log("err: ", err);
    // //       setIsLoading(false);
    // //     });
    // //   userapi
    // //     .get(`/category-child?categoryId=${fitnessId}`)
    // //     .then(res => {
    // //       console.log("res category 3: ", res.data.data);
    // //       let dataWellnes = {
    // //         id: 10,
    // //         name: "Fitness",
    // //         description: "Stay healthy with us",
    // //         child: res.data.data,
    // //       };
    // //       setPreferencesData(state => [...state, dataWellnes]);
    // //     })
    // //     .catch(err => {
    // //       console.log("err: ", err);
    // //       setIsLoading(false);
    // //     });
    // //   countRef.current++;
    //   setIsLoading(false);
    // }
  }, []);
  console.log("categories", categories);

  const proccessPreferences = (parentId, childId) => {
    if (
      selectedPreferences.some(
        item => item.parentId === parentId && item.childId === childId,
      )
    ) {
      const excludedItem = { parentId, childId };
      const selectedItem = selectedPreferences.filter(
        item => JSON.stringify(item) !== JSON.stringify(excludedItem),
      );

      setSelectedPreferences(selectedItem);
    } else {
      setSelectedPreferences(oldData => [...oldData, { parentId, childId }]);
    }
  };

  const isPreferenceSelected = (parentId, childId) => {
    return selectedPreferences.some(
      item => item.parentId === parentId && item.childId === childId,
    );
  };

  const savePreferences = () => {
    const categoryChildIds = selectedPreferences.map(item => {
      return item.childId;
    });

    const params = new URLSearchParams();
    navigation.navigate("BottomTabs"),

    params.append("categoryChildIds", JSON.stringify(categoryChildIds));
    console.log("params params", params);
    // userapi
    //   .post(`/user-preference/create`, { categoryChildIds: categoryChildIds })
    //   .then(res => {
    //     navigation.navigate("BottomTabs"),
    //       console.log("params params response: ", res);
    //   })
    //   .catch(err => {
    //     let dataError = err.response;
    //     let errorCode = _.get(dataError, "status", " ");
    //     let errorMassage = _.get(dataError, "data.message", " ");

    //     Alert.alert("Error", `${errorMassage}`, [
    //       {
    //         text: "OK",
    //         onPress: () => navigation.navigate("BottomTabs"),
    //         style: "cancel",
    //       },
    //     ]);
    //   });
    // dispatch(Actions.createPreference(categoryChildIds));
  };

  // const dataSelected = (id, label) => {
  //   if (label === "sports") {
  //     setSportData((state) => {
  //       return state.map((item) => {
  //         if (item.id === id) {
  //           item.selected = !item.selected;
  //           if (item.selected) {
  //             let filteredArray = selectedIdSport.filter((item) => item === id);
  //             if (filteredArray.length === 0) {
  //               setSelectedIdSport((state) => [...state, item.id]);
  //             }
  //           } else {
  //             removeIds(item.id, label);
  //           }
  //         }
  //         return item;
  //       });
  //     });
  //   } else {
  //     setWellnessData((state) => {
  //       return state.map((item) => {
  //         if (item.id === id) {
  //           item.selected = !item.selected;
  //           if (item.selected) {
  //             let filteredArray = selectedIdWellness.filter(
  //               (item) => item === id
  //             );
  //             if (filteredArray.length === 0) {
  //               setSelectedIdWellness((state) => [...state, item.id]);
  //             }
  //           } else {
  //             removeIds(item.id, label);
  //           }
  //         }
  //         return item;
  //       });
  //     });
  //   }
  // };

  // console.log("uhuuu", selectedIdSport, selectedIdWellness, joinArray);

  // const removeIds = (id, label) => {
  //   if (label === "sports") {
  //     let filteredArray = selectedIdSport.filter((item) => item !== id);
  //     setSelectedIdSport(filteredArray);
  //   } else {
  //     let filteredArray = selectedIdWellness.filter((item) => item !== id);
  //     setSelectedIdWellness(filteredArray);
  //   }
  // };

  const renderListData = (parentId, item, index) => {
    return (
      <TouchableOpacity onPress={() => proccessPreferences(parentId, item.id)}>
        <View
          style={{
            height: moderateScale(32),
            paddingHorizontal: 8,
            borderWidth: 1,
            borderColor: isPreferenceSelected(parentId, item.id)
              ? "#1035BB"
              : "#9E9E9E",
            backgroundColor: isPreferenceSelected(parentId, item.id)
              ? "#E8E7FF"
              : "transparent",
            alignItems: "center",
            justifyContent: "center",
            marginRight: moderateScale(8),
            marginBottom: moderateScale(8),
          }}>
          <Text
            style={{
              color: isPreferenceSelected(parentId, item.id)
                ? "#1035BB"
                : "black",
              fontSize: moderateScale(12, 0.25),
              fontFamily: "Montserrat-Regular",
              fontWeight: isPreferenceSelected(parentId, item.id)
                ? "600"
                : "500",
            }}>
            {item.name}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View
      style={{
        backgroundColor: "white",
        flex: 1,
        paddingHorizontal: moderateScale(20),
      }}>
      <View style={{ flex: 2 }}>
        <View
          style={{
            height: moderateScale(32),
            backgroundColor: "#F8F9FC",
            justifyContent: "center",
            marginTop: moderateScale(12),
          }}>
          <Text
            style={{
              color: "black",
              fontSize: moderateScale(12, 0.25),
              fontFamily: "Montserrat-Regular",
              fontWeight: "500",
              marginLeft: moderateScale(16),
            }}>
            {selectedPreferences.length === 0
              ? "Please select or unselect tags below"
              : `${selectedPreferences.length} tags selected`}
          </Text>
        </View>
        <View
          style={{
            flex: 1,
            marginBottom: moderateScale(32),
          }}>
          {preferences.map((parentItem, index) => {
            console.log("childchildchild parentItem", parentItem);
            return (
              <View>
                <Text
                  style={{
                    color: "black",
                    fontSize: moderateScale(14, 0.25),
                    fontFamily: "Montserrat-Regular",
                    fontWeight: "700",
                    marginBottom: moderateScale(10),
                    marginTop: moderateScale(16),
                  }}>
                  {parentItem.name}
                </Text>
                <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
                  {parentItem.child.map((item, index) =>
                    renderListData(parentItem.id, item, index),
                  )}
                </View>
              </View>
            );
          })}
        </View>
      </View>

      <View style={{ flex: 0.5 }}>
        <ButtonCustom
          onPressButton={() => {
            savePreferences();
          }}
          label="Save"
          isLoading={createPreference.isLoading}
          buttonMarginHorizontal={-2}
          style={{
            marginTop: moderateScale(16),
            marginBottom: moderateScale(16),
          }}
        />
        {/* {dataFirstTime.firstTime === "true" && (
          <TouchableOpacity
            onPress={() => navigation.goBack()}
            style={{ justifyContent: "center", alignItems: "center" }}>
            <Text>Cancel</Text>
          </TouchableOpacity>
        )} */}
      </View>
    </View>
  );
};

export default SetPreferences;
