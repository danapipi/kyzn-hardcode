import React, { useCallback, useState, useEffect, useRef } from "react";
import {
  TouchableOpacity,
  Alert,
  Platform,
  Image as ReactImage,
} from "react-native";
import {
  Box,
  Text,
  AspectRatio,
  Image,
  HStack,
  VStack,
  ScrollView,
  Button,
  Actionsheet,
  ChevronRightIcon,
  ChevronLeftIcon,
  Select,
  CheckIcon,
  Flex,
} from "native-base";
import { moderateScale } from "react-native-size-matters";
import { useRoute, useIsFocused } from "@react-navigation/native";
import PropTypes from "prop-types";
import { useDispatch, useSelector } from "react-redux";
import Actions from "actions";
import CalendarPicker from "react-native-calendar-picker";
import Loading from "components/ui/Loading";
import moment, { utc } from "moment";
import _ from "lodash";
import { UserApi } from "utils/Api";

const userapi = UserApi.client;
// import { Select, CheckIcon, Flex } from 'native-base';

import dummyTime from "./dummyData/dummyTime.json";
import dummyHours from "./dummyData/dummyHours.json";
import Commons from "../../utils/common";
import { image } from "../../../assets/images";

const SportDetail = props => {
  console.log("Sprot Detail Props: ", props);
  const { navigation } = props;
  const dispatch = useDispatch();

  const family = useSelector(state => state.FAMILY.user.data);
  const profile = useSelector(state => state.AUTH.profile);
  const dataActivityById = useSelector(state => state.PRODUCT.activityById);
  const dataActivitySch = useSelector(state => state.PRODUCT.activitySchedule);

  const {
    class_activity_type_id,
    panorama_activity_type_id,
    pt_activity_type_id,
    rent_court_activity_type_id,
  } = useSelector(state => state.PRODUCT.activityTypeId.activityTypeId);

  const [activityById, setActivityById] = useState({});
  const [activitySchedule, setActivitySchedule] = useState([]);
  const dataUniqueFamily = useSelector(state => state.FAMILY.uniqueFamily);
  // const [selectedUser, setSelectedUser] = useState(dataUniqueFamily.data[0].id);
  const [selectedUser, setSelectedUser] = useState("");
  const [textShown, setTextShown] = useState(false); //To show ur remaining Text
  const [lengthMore, setLengthMore] = useState(false);
  const [isOpen, setIsOpen] = useState(false);
  const [dateToGo, setDate] = useState([]);
  const [timeSelected, setTimeSelected] = useState(null);
  const [onSelectedDate, setSelectedDate] = useState(
    moment().format("dddd, DD MMMM yyyy"),
  );
  const isFocused = useIsFocused();

  const [dataTime, setDataTime] = useState([]);
  const [selectData, setSelectDate] = useState([]);
  const [selectHours, setSelectHours] = useState(1);
  const [selectListHours, setSelectListHours] = useState(null);
  const [rangeHours, setRangeHour] = useState(null);
  const [dataDetailById, setDataDetail] = useState({});
  const countRef = useRef(0);
  const [bottomHeight, setBottomHeight] = useState(0);

  const route = useRoute();
  const type = route.params.type;

  const toggleNumberOfLines = () => {
    //To toggle the show text or hide it
    setTextShown(!textShown);
  };

  const onClose = () => {
    setIsOpen(false);
  };

  const onPressAction = e => {
    setIsOpen(true);
  };

  const changeTime = (id, item, index) => {
    setTimeSelected(index);
    setSelectDate(item);
  };

  const determineAvailableTimes = range_times => {
    const one_hour = [
      "07:00 - 07:55",
      "08:00 - 08:55",
      "09:00 - 09:55",
      "10:00 - 10:55",
      "11:00 - 11:55",
      "12:00 - 12:55",
      "13:00 - 13:55",
      "14:00 - 14:55",
      "15:00 - 15:55",
      "16:00 - 16:55",
      "17:00 - 17:55",
      "18:00 - 18:55",
      "19:00 - 19:55",
    ];
    const two_hour = [
      "07:00 - 08:55",
      "09:00 - 10:55",
      "11:00 - 12:55",
      "13:00 - 14:55",
      "15:00 - 16:55",
      "17:00 - 18:55",
      "19:00 - 20:55",
    ];
    const three_hour = [
      "07:00 - 09:55",
      "10:00 - 12:55",
      "13:00 - 15:55",
      "16:00 - 18:55",
    ];
    const four_hour = ["07:00 - 10:55", "11:00 - 14:55", "15:00 - 18:55"];

    switch (range_times) {
      case 1:
        return one_hour;
      case 2:
        return two_hour;
      case 3:
        return three_hour;
      case 4:
        return four_hour;

      default:
        break;
    }
  };

  async function determineDiffDuration(hour, callback) {
    const settingDestinationTime = moment().set({
      hour: hour,
      minute: 0,
    });

    const timeDiff = moment
      .duration(moment(moment(), "HH:mm").diff(settingDestinationTime, "HH:mm"))
      .asHours();

    await callback(timeDiff);
  }

  async function populateAvailableTimesCompareToNow() {
    const hoursAvailable = determineAvailableTimes(selectHours);

    if (
      moment(onSelectedDate).format("dddd, DD MMMM yyyy") ===
      moment().format("dddd, DD MMMM yyyy")
    ) {
      // this way can run the asynchronous function very smoothly. see here https://stackoverflow.com/a/49499491
      await hoursAvailable.reduce(async (promise, item) => {
        await promise;

        const hour = item.split("-")[0].split(":")[0];

        await determineDiffDuration(hour, timeDiff => {
          if (timeDiff > 0) {
            hoursAvailable.splice(0, 1);
          }
        });
      }, Promise.resolve());

      setDataAvailable(hoursAvailable);
    } else {
      setDataAvailable(hoursAvailable);
    }
  }

  //  for court and class
  const getActivityById = async (activityId, date, step, userId) => {
    let datelink = `&date=${date}`;
    let steplink = `&stepMultiplier=${step}`;
    // let useridlink = `&userId=${userId}`
    let base = `activity/${activityId}?`;
    if (date) {
      base = base + datelink;
    }
    if (step) {
      base = base + steplink;
    }
    // if (userId){
    //   base = base + useridlink
    // }
    if (userId == "all") {
      console.log("qweasd", base);

      let test = await userapi.get(base);
      console.log("GET ALL DATA TEST: ", test);
      setActivityById(test.data.data);
    } else {
      let test = await userapi.get(base + `&userId=${userId}`);
      setActivityById(test.data.data);
      console.log("qweasd", base + `&userId=${userId}`);
      console.log("qweasd", test.data.data);
    }
  };

  const getActivitySchedule = async (activityId, userId) => {
    let activitylink = `&activity_id=${activityId}`;
    // let steplink = `&stepMultiplier=${step}`
    let base = "activity-schedule?";
    if (activityId) {
      base = base + activitylink;
    }
    if (userId == "all") {
      console.log("asdqwe", base);

      let test = await userapi.get(base);
      setActivitySchedule(test.data.data);
    } else {
      let test = await userapi.get(base + `&userId=${userId}`);
      setActivitySchedule(test.data.data);
      console.log("asdqwe", test.data.data);
      console.log("asdqwe", test.data.data);
    }
  };

  const displayDay = () => {
    let days = [];
    let m = 0;
    let end_date = moment().add(7, "days");
    console.log("END_DATE", end_date);
    let diff = 7;
    console.log("END_DATE DIFF", diff);

    while (m < diff) {
      let date = moment().add(m, "days");
      console.log("date nya: ", date);
      if (date !== null) {
        if (moment(date).format("DD MM yy") == moment().format("DD MM yy")) {
          console.log("asjaasjiu date 1", date);
          days.push({ date: "Today", actualDate: date });
        } else if (
          moment(date).format("DD MM yy") ==
          moment().add(1, "days").format("DD MM yy")
        ) {
          // console.log("asjaasjiu date 2", date);
          days.push({ date: "Tomorrow", actualDate: date });
        } else {
          // console.log("asjaasjiu date 3", date);
          days.push({
            date: moment(date).format("ddd, MMM DD"),
            actualDate: date,
          });
        }
      }
      m++;
    }

    console.log("daysdaysdaysdays", days);

    let daysTemp = _.orderBy(days, ["actualDate"], ["asc"]);

    let dateSelect =
      daysTemp.length !== 0 &&
      moment(daysTemp[0].actualDate).format("dddd, DD MMMM yyyy");

    let dayNow = moment(daysTemp[0].actualDate).format("dddd, DD MMMM yyyy");

    let dayys = _.uniqBy(daysTemp, "date");

    const shortedDay = dayys.splice(0, diff);
    console.log("aabbbbbbbccc daysTemp", daysTemp);
    console.log("aabbbbbbbccc dateSelect", dateSelect);
    console.log("aabbbbbbbccc dayys", dayys);
    console.log("aabbbbbbbccc dayNow", dayNow);

    setSelectedDate(dateSelect);
    setDataTime(dayNow);
    // setDate(dayys);
    setDate(shortedDay);
  };

  const displayDayFacility = () => {
    let days = [];
    let m = 0;
    let end_date = moment().add(7, "days");
    console.log("END_DATE", end_date);
    let diff = 7;
    console.log("END_DATE DIFF", diff);

    while (m < diff) {
      let date = moment().add(m, "days");
      console.log("date nya: ", date);
      if (date !== null) {
        if (moment(date).format("DD MM yy") == moment().format("DD MM yy")) {
          console.log("asjaasjiu date 1", date);
          days.push({ date: "Today", actualDate: date });
        } else if (
          moment(date).format("DD MM yy") ==
          moment().add(1, "days").format("DD MM yy")
        ) {
          // console.log("asjaasjiu date 2", date);
          days.push({ date: "Tomorrow", actualDate: date });
        } else {
          // console.log("asjaasjiu date 3", date);
          days.push({
            date: moment(date).format("ddd, MMM DD"),
            actualDate: date,
          });
        }
      }
      m++;
    }

    console.log("daysdaysdaysdays", days);

    let daysTemp = _.orderBy(days, ["actualDate"], ["asc"]);
    setDate(daysTemp);
  };

  const onSubmitDate = () => {
    onClose();
    if (type == rent_court_activity_type_id) {
      displayDayFacility();
    } else {
      displayDay();
    }
  };

  console.log("detail type", route.params, type);

  const onTextLayout = useCallback(e => {
    setLengthMore(e.nativeEvent.lines.length >= 3); //to check the text is more than 4 lines or not
    // console.log(e.nativeEvent);
  }, []);

  const changeDate = date => {
    if (type == rent_court_activity_type_id) {
      let dateSelect = moment(date).format("dddd, DD MMMM yyyy");
      // getActivityById(
      //   data.id,
      //   moment(date).format("YYYY-MM-DD"),
      //   selectHours,
      //   selectedUser,
      // );
      setSelectedDate(dateSelect);
    } else {
      let dateSelect = moment(date).format("dddd, DD MMMM yyyy");
      // getActivitySchedule(data.id, selectedUser)
      let dayNow = _.filter(activitySchedule, function (o) {
        return (
          moment(o.start_time).format("DD MM yy") ==
          moment(date).format("DD MM yy")
        );
      });
      let dataTime;
      console.log("askjdiquwgoeiqgwe", dayNow);
      if (dayNow.length !== 0) {
        dataTime = dayNow;
      } else {
        dataTime = [];
      }

      setSelectedDate(dateSelect);
      setDataTime(dateSelect);
    }
  };

  let data = route.params.data.item;

  const [sportImage, setSportImage] = useState({ uri: data.image_landscape });
  // const [sportImage, setSportImage] = useState(image.sport_dummy)
  const [aspectRatioForImage, setAspectRatioForImage] = useState(16 / 9);

  console.log("jaskdjowigq", selectData, data);

  let isDiscountedTemp = _.get(data, "is_discounted", null);
  let realPriceTemp = _.get(data, "price", null);
  let isDiscounted = _.get(activityById, "is_discounted", isDiscountedTemp);
  let realPrice = _.get(activityById, "price", realPriceTemp);
  let activityType = _.get(activityById, "activity_type_id", "");
  let stepPrice = _.get(activityById, "step_duration_price", "");
  // let filterPriceMembers = _.filter(member, function (o) {
  //   return (
  //     o.id === selectedUser
  //   );
  // });
  // let filterPrice = filterPriceMembers[0].price;
  // console.log("filterPrice", filterPriceMembers, filterPrice);
  let price = activityById.price;
  //  isDiscounted
  //   ? data.discounted_price
  //   :
  // realPrice
  // ? realPrice
  // : data.step_duration_price;

  useEffect(() => {
    let userId = _.get(route.params, "userId", profile.data.id);

    // for court and class
    // params:  activityId, date, step, userId
    // getActivityById(data.id, moment().format("YYYY-MM-DD"), 1, userId);
    setActivityById(dataActivityById.activityByID);

    // only for class
    // getActivitySchedule(data.id, userId);
    setActivitySchedule(dataActivitySch.activitySchedule);

    console.log("dataActivitySch asdawdaw", dataActivitySch, dataActivityById);
  }, []);

  useEffect(() => {
    if (
      !_.isEmpty(activitySchedule) &&
      (type === class_activity_type_id || type === panorama_activity_type_id)
    ) {
      displayDay();
      console.log("aabbbbbbbccc timeout here");
    } else if (
      !_.isEmpty(activityById) &&
      type === rent_court_activity_type_id
    ) {
      // console.log("daysdaysdaysdays sssss", dataActivityById.activityByID);
      displayDayFacility();
    }
    // countRef.current++;
    // }
  }, [activitySchedule, activityById]);

  useEffect(() => {
    populateAvailableTimesCompareToNow();
  }, [isFocused, onSelectedDate, selectHours]);

  const callActivityById = itemValue => {
    setSelectedUser(itemValue);
    // dispatch(Actions.activitySchedule(data.id, itemValue));
    getActivitySchedule(data.id, itemValue);

    if (type === class_activity_type_id || type === panorama_activity_type_id) {
      console.log("auauauauau");

      getActivityById(
        data.id,
        moment(onSelectedDate ? onSelectedDate : new Date()).format(
          "YYYY-MM-DD",
        ),
        selectHours,
        itemValue,
      );
      // dispatch(Actions.activityById(data.id, data.id, null, null, itemValue));
    } else if (type === rent_court_activity_type_id) {
      console.log("auauauauau 2");
      getActivityById(
        data.id,
        moment(onSelectedDate).format("YYYY-MM-DD"),
        selectHours,
        itemValue,
      );
      // dispatch(Actions.activityById(data.id, data.id, moment(onSelectedDate).format('YYYY-MM-DD'), selectHours, itemValue))
    }

    if (
      !_.isEmpty(activitySchedule) &&
      countRef.current === 1 &&
      (type === class_activity_type_id || type === panorama_activity_type_id)
    ) {
      displayDay();
    } else if (
      !_.isEmpty(activityById) &&
      countRef.current === 1 &&
      type === rent_court_activity_type_id
    ) {
      displayDayFacility();
    }
  };

  console.log("come herrree date to go", dateToGo);

  // if (dateToGo.length === 0) {
  //   return <Loading />;
  // }
  let ss = "11:00-12:00";
  let hours = ss.substring(0, 5);

  console.log("hours hours", hours);

  let quota = _.get(selectData, "quota_left", 0);
  let timeNow = _.get(selectData, "start_time", moment());
  console.log("TIME NOEW: ", timeNow);
  let listHours = [];
  let [dataAvailable, setDataAvailable] = useState([]);
  console.log("dataAvailable: ", dataAvailable);

  // if (type == 1 && !_.isNull(dataAvailable)) {
  //   let listHoursTemp = _.filter(dataAvailable, function (o) {
  //     return o.id == selectHours;
  //   });
  //   listHours = listHoursTemp[0].hours;
  // }

  // court only
  const handlePressHour = item => {
    setSelectHours(item.id);

    populateAvailableTimesCompareToNow();
    // getActivityById(
    //   data.id,
    //   moment(onSelectedDate).format("YYYY-MM-DD"),
    //   item.id,
    //   selectedUser,
    // );
  };

  const handlePressListHour = (item, index) => {
    console.log("ajsdnoqiwq", item, index);
    setSelectListHours(index);
    setRangeHour(item);
  };

  console.log("daysTemp daysTemp dataTime", dataTime);

  const handleButtonNext = () => {
    // if (!selectedUser) {
    //   Alert.alert("Empty", "Please select user", [
    //     { text: "OK", onPress: () => console.log("OK Pressed") },
    //   ]);
    //   return;
    // }

    if (
      (_.isNull(dataAvailable) || _.isNull(rangeHours)) &&
      type == rent_court_activity_type_id
    ) {
      Alert.alert("Empty", "Please select slot time", [
        { text: "OK", onPress: () => console.log("OK Pressed") },
      ]);
      return;
    }

    if (selectData.length === 0 && type == class_activity_type_id) {
      Alert.alert("Empty", "Please select slot time", [
        { text: "OK", onPress: () => console.log("OK Pressed") },
      ]);
      return;
    }
    

    if (type == rent_court_activity_type_id) {
      console.log("heiuiwuehroiqhw", type, activityById, data);
      // navigation.navigate("Additional", {
      //   data: dataActivityById.activityByID,
      //   activity: dataActivityById.activityByID,
      //   selectedUser: selectedUser,
      //   type: type,
      //   coupons: dataActivityById.activityByID.coupons,
      //   price: Commons.convertToRupiah(price * selectHours),
      //   timeDuration: selectHours * 60,
      //   startTime: {
      //     onSelectedDate,
      //     rangeHours: rangeHours.substring(0,5),
      //     fullRangeHours: rangeHours
      //   },
      //   realPrice: price * selectHours,
      //   multipler: selectHours,
      //   best_coupon_id: dataActivityById.activityByID.best_coupon_id,
      // })
      navigation.navigate("Payment", {
        data: data,
        activity: data,
        selectedUser: selectedUser,
        type: type,
        coupons: activityById.coupons,
        price: price,
        timeDuration: selectHours * 60,
        startTime: {
          onSelectedDate,
          rangeHours: rangeHours.substring(0, 5),
          fullRangeHours: rangeHours,
        },
        realPrice: price,
        multipler: selectHours,
        best_coupon_id: activityById.best_coupon_id,
      });
    } else if (
      type == class_activity_type_id ||
      type == panorama_activity_type_id
    ) {
      navigation.navigate("Payment", {
        data: activityById,
        activity: data,
        selectedUser: selectedUser,
        coupons: activityById.coupons,
        best_coupon_id: activityById.best_coupon_id,
        type: type,
        realPrice: price,
        price: price,
      });
    }
  };

  return (
    <Box flex={1}>
      <ScrollView
        contentContainerStyle={{ paddingBottom: 100 }}
        style={{
          backgroundColor: "white",
          marginBottom: Platform.OS == "ios" ? 100 : bottomHeight,
          // marginBottom: Platform.OS == 'ios' ? 100 : 200,
        }}>
        <Box flex={1} borderWidth={0}>
          <AspectRatio ratio={aspectRatioForImage}>
            <ReactImage
              resizeMode="stretch"
              source={sportImage}
              // alt="image"
              style={{ height: moderateScale(100, 0.25) }}
              onError={() => {
                setAspectRatioForImage(3 / 2);
                setSportImage(image.img_product_placeholder);
              }}
            />
          </AspectRatio>
          {/* <Text>{JSON.stringify(activitySchedule)}</Text> */}
          <Box w={"100%"}>
            <VStack
              style={{
                padding: 20,
                borderBottomWidth: 1,
                borderColor: "#ECEDEF",
              }}>
              <Text
                alignItems="flex-start"
                fontWeight="bold"
                style={{
                  fontFamily: "Montserrat-Regular",
                  fontWeight: "700",
                  fontSize: 16,
                }}>
                {data.name}
              </Text>
              <HStack style={{ marginTop: 6 }}>
                <Text
                  style={{
                    alignItems: "flex-start",
                    fontWeight: "bold",
                    color: "#1035BB",
                    marginRight: 8,
                    fontSize: 12,
                    fontFamily: "Montserrat-Regular",
                  }}>
                  {/* Rp {data.is_discounted ? Commons.convertToRupiah(data.discounted_price) : Commons.convertToRupiah(data.price)} {type == 7 && '/hr'} */}
                  {Commons.convertToRupiah(price)}
                </Text>
                <HStack justifyItems="center" alignItems="center">
                  <Image
                    source={require("../../../assets/images/xp.png")}
                    style={{ height: 12, width: 12, marginRight: 3 }}
                    resizeMode="cover"
                    alt="image"
                  />
                  <Text alignItems="flex-end" fontWeight="bold" fontSize={10}>
                    {data.xp_reward}
                  </Text>
                </HStack>
              </HStack>
            </VStack>
            {(type == class_activity_type_id ||
              type == panorama_activity_type_id) && (
              <VStack
                style={{
                  padding: 20,
                  paddingVertical: 10,
                  borderBottomWidth: 1,
                  borderColor: "#ECEDEF",
                }}>
                <HStack style={{ justifyContent: "space-between" }}>
                  <Text
                    style={{ fontSize: 10, color: "#757575" }}
                    alignItems="flex-start"
                    fontWeight="bold">
                    Location
                  </Text>
                  {/* <TouchableOpacity onPress={() => console.log("click here")}>
                  <HStack justifyItems="center" alignItems="center">
                    <Text
                      style={{ fontSize: 10, color: "#212121" }}
                      alignItems="flex-start"
                      fontWeight="bold"
                    >
                      See Map
                    </Text>
                    <Image
                      source={require("../../../assets/images/arrow-fwd.png")}
                      style={{ height: 12, width: 12, marginLeft: 3 }}
                      resizeMode="cover"
                      alt="image"
                    />
                  </HStack>
                </TouchableOpacity> */}
                </HStack>
                <Text
                  style={{ fontSize: 12, marginTop: 8 }}
                  alignItems="flex-start"
                  fontWeight="bold">
                  {_.get(activitySchedule, "[0].activity_court.court.name", "")}
                </Text>
                <Text
                  style={{ fontSize: 10, color: "#757575" }}
                  alignItems="flex-start"
                  fontWeight="bold">
                  {/* {dataTime[0].activity_court.court.name} */}
                </Text>
              </VStack>
            )}
            <VStack>
              <HStack
                style={{
                  padding: 20,
                  paddingVertical: 12,
                  borderBottomWidth: 1,
                  borderColor: "#ECEDEF",
                }}>
                <TouchableOpacity disabled onPress={onPressAction}>
                  <Image
                    source={require("../../../assets/images/ic-dates.png")}
                    style={{ height: 24, width: 24, marginRight: 16 }}
                    resizeMode="cover"
                    alt="image"
                  />
                </TouchableOpacity>
                <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                  {dateToGo.map(item => {
                    let isSelectDate =
                      moment(item.actualDate).format("DD MM yy") ==
                      moment(onSelectedDate).format("DD MM yy");
                    console.log("aksugdiau", isSelectDate);
                    return (
                      <VStack
                        style={{
                          marginRight: 16,
                          justifyContent: "center",
                          alignItems: "center",
                        }}>
                        <TouchableOpacity
                          onPress={() => changeDate(item.actualDate)}
                          style={{ display: "flex", flexDirection: "row" }}>
                          <Text
                            style={{
                              fontSize: 12,
                              color: isSelectDate ? "#000" : "#757575",
                              fontWeight: "bold",
                            }}>
                            {item.date}
                          </Text>
                          {moment(item.date).isBetween(
                            _.get(
                              activityById,
                              "non_member_furthest_booking_date",
                            ),
                            _.get(activityById, "member_furthest_booking_date"),
                          ) && (
                            <Image
                              style={{
                                width: 16,
                                height: 16,
                                marginLeft: 8,
                                marginTop: 4,
                              }}
                              source={require("../../../assets/images/crown.png")}
                              alt="image"
                              resizeMode="cover"
                            />
                          )}
                        </TouchableOpacity>
                        {isSelectDate && (
                          <Box
                            style={{
                              height: 2,
                              backgroundColor: "#212121",
                              position: "absolute",
                              bottom: 0,
                              width: 45,
                            }}
                          />
                        )}
                      </VStack>
                    );
                  })}
                </ScrollView>
              </HStack>
              <HStack style={{ padding: 20 }}>
                {type == rent_court_activity_type_id &&
                !_.isNull(dataAvailable) ? (
                  <VStack>
                    <HStack>
                      {dummyHours.map(item => (
                        <TouchableOpacity onPress={() => handlePressHour(item)}>
                          <Box
                            style={{
                              borderWidth: 1,
                              paddingHorizontal: 18,
                              paddingVertical: 8,
                              backgroundColor:
                                item.id == selectHours ? "#000" : "transparent",
                            }}>
                            <Text
                              style={{
                                fontFamily: "Montserrat-Regular",
                                fontWeight: "700",
                                fontSize: 14,
                                color: item.id == selectHours ? "#FFF" : "#000",
                              }}>
                              {item.title}
                            </Text>
                          </Box>
                        </TouchableOpacity>
                      ))}
                    </HStack>
                    <ScrollView
                      horizontal
                      style={{ marginTop: 20 }}
                      showsHorizontalScrollIndicator={false}>
                      {dataAvailable.map((item, index) => {
                        console.log("DATA TIME ITEM: ", item);
                        return (
                          <TouchableOpacity
                            onPress={() => handlePressListHour(item, index)}>
                            <Box
                              style={{
                                borderWidth: 1,
                                paddingHorizontal: 18,
                                paddingVertical: 17,
                                backgroundColor:
                                  index == selectListHours
                                    ? "#000"
                                    : "transparent",
                              }}>
                              <Text
                                style={{
                                  fontFamily: "Montserrat-Regular",
                                  fontWeight: "700",
                                  fontSize: 14,
                                  color:
                                    index == selectListHours ? "#FFF" : "#000",
                                }}>
                                {item}
                              </Text>
                            </Box>
                          </TouchableOpacity>
                        );
                      })}
                    </ScrollView>
                  </VStack>
                ) : (
                  <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                    {() => console.log("dataTime >>>>>>>>>>>>> :", dataTime)}
                    {dataTime.length !== 0 &&
                      _.orderBy(dataTime, [
                        c => moment(c.start_time).format("HHmm"),
                      ]).map((item, index) => {
                        let isAvailable = item.quota_left >= 0;
                        let isSelect = timeSelected === index;
                        let startTime = item.start_time;

                        console.log("date date date here", item);
                        console.log("MASSSOOKKKKKKKK", index);
                        return (
                          <TouchableOpacity
                            // style={{backgroundColor ? }}
                            onPress={() => changeTime(item.id, item, index)}
                            // disabled={isAvailable ? false : true}
                          >
                            <VStack
                              style={{
                                alignItems: "center",
                                borderColor:
                                  isSelect && item.quota_left <= 0
                                    ? "#E2BB4B"
                                    : isSelect
                                    ? "#000"
                                    : "#C4C4C4",
                                borderWidth: 1,
                                paddingHorizontal: 11,
                                paddingVertical: 8,
                                marginRight: 9,
                                backgroundColor:
                                  isSelect && item.quota_left <= 0
                                    ? "#E2BB4B"
                                    : isSelect
                                    ? "#000"
                                    : "transparent",
                              }}>
                              <Text
                                style={{
                                  fontSize: 14,
                                  color: isSelect
                                    ? "#FFF"
                                    : isAvailable
                                    ? "#000"
                                    : "#C4C4C4",
                                  fontWeight: "bold",
                                }}>
                                {/* {moment(startTime).format("HH:mm")} */}
                                {moment(startTime).add('hours', index - 1).format("HH:mm")}
                              </Text>
                              <Text
                                style={{
                                  fontSize: 10,
                                  color: isSelect
                                    ? "#FFF"
                                    : isAvailable
                                    ? "#000"
                                    : "#C4C4C4",
                                  fontWeight: "bold",
                                }}>
                                {item.quota_left <= 0
                                  ? "Full"
                                  : `1 Slot`}
                              </Text>
                            </VStack>
                          </TouchableOpacity>
                        );
                      })}
                  </ScrollView>
                )}
              </HStack>
            </VStack>
            <VStack style={{ padding: 20, paddingVertical: 10 }}>
              <Text
                onTextLayout={onTextLayout}
                numberOfLines={textShown ? undefined : 3}
                style={{ lineHeight: 21, color: "#445159", fontSize: 12 }}>
                {data.description}
              </Text>
              {lengthMore ? (
                <TouchableOpacity onPress={toggleNumberOfLines}>
                  <Box
                    style={{
                      backgroundColor: "#EBEEF4",
                      width: 96,
                      height: 24,
                      justifyContent: "center",
                      alignItems: "center",
                      marginTop: 16,
                      borderRadius: 4,
                    }}>
                    <Text
                      style={{ lineHeight: 21, fontSize: 12, color: "#000" }}>
                      {textShown ? "Show less" : "Show more"}
                    </Text>
                  </Box>
                </TouchableOpacity>
              ) : null}
            </VStack>
          </Box>
        </Box>
      </ScrollView>
      <Box
        style={{
          position: "absolute",
          bottom: 0,
          right: 0,
          width: "100%",
          padding: 20,
          paddingBottom: 40,
          borderTopWidth: 1,
          borderColor: "#ECEDEF",
          backgroundColor: "#FFF",
        }}
        onLayout={e => {
          console.log("e nya sportDetail: ", e.nativeEvent.layout.height);
          if (Platform.OS == "android") {
            setBottomHeight(e.nativeEvent.layout.height);
          }
        }}>
        <HStack style={{ justifyContent: "space-between" }}>
          <VStack>
            <Flex
              // direction="row"
              justify="flex-end"
              style={{ marginTop: 0, marginBottom: 16, flexDirection: "row" }}>
              {_.get(activityById, "members", []).length > 0 && (
                // family && family.user_family_details &&
                <Box>
                  {/* <Text>Booking for:</Text> */}
                  {/* <Select
                    selectedValue={selectedUser}
                    width={150}
                    accessibilityLabel="Choose User"
                    placeholder="Choose User"
                    _selectedItem={{
                      endIcon: <CheckIcon size="5" />,
                    }}
                    mt={1}
                    onValueChange={itemValue => callActivityById(itemValue)}>
                    {_.get(activityById, "members", []).length > 0 &&
                      _.get(activityById, "members", []).map(item => (
                        // family && family.user_family_details && family.user_family_details.map((family_member) =>
                        <Select.Item label={item.name} value={item.id} />
                      ))}
                  </Select> */}
                </Box>
              )}
            </Flex>
            <HStack>
              <Text
                style={{ fontSize: 10, color: "#606060" }}
                alignItems="flex-start"
                fontWeight="bold">
                {/* {moment(timeNow).format("DD MM yy") ===
                moment().format("DD MM yy")
                  ? "Today"
                  : moment(timeNow).add(1, "day").format("DD MM yy") ===
                    moment().format("DD MM yy")
                  ? "Tomorrow"
                  : moment(timeNow).format("dddd, DD-MM-yy")} */}
                {moment(moment(onSelectedDate).format("YYYY-MM-DD")).isSame(
                  moment().format("YYYY-MM-DD"),
                )
                  ? "Today"
                  : moment(moment().format("YYYY-MM-DD"))
                      .add(1, "days")
                      .isSame(moment(onSelectedDate).format("YYYY-MM-DD"))
                  ? "Tomorrow"
                  : moment(onSelectedDate).format("YYYY-MM-DD")}
              </Text>
              <Text
                style={{ fontSize: 10, color: "#606060" }}
                alignItems="flex-start"
                fontWeight="bold">
                ・
              </Text>
              <Text
                style={{ fontSize: 10, color: "#606060" }}
                alignItems="flex-start"
                fontWeight="bold">
                {type === rent_court_activity_type_id
                  ? rangeHours
                  : moment(timeNow).format("HH:mm")}
              </Text>
            </HStack>
            {type == rent_court_activity_type_id ? (
              <Text
                style={{ fontSize: 14, color: "#000" }}
                alignItems="flex-start"
                fontWeight="bold">
                {/* Rp {data.is_discounted 
                  ? Commons.convertToRupiah((data.discounted_price * selectHours)) 
                  : Commons.convertToRupiah((data.price * selectHours))
                } / hr */}
                {Commons.convertToRupiah(price)}
              </Text>
            ) : (
              <Text
                style={{ fontSize: 14, color: "#000" }}
                alignItems="flex-start"
                fontWeight="bold">
                {quota <= 0 ? "Full Slots" : `${quota} Slots Available`}
              </Text>
            )}
          </VStack>
          <Button
            w={130}
            // disabled={!selectedUser || (type == 1
            //   ? _.isNull(dataAvailable) || _.isNull(rangeHours)
            //     ? true
            //     : false
            //   : selectData.length !== 0
            //     ? false
            //     : true
            // )}
            bgColor={"#1035BB"}
            style={{ height: 50, bottom: 0, right: 0, position: "absolute" }}
            onPress={() => {
              if (_.get(activityById, "members", []).length > 0) {
                handleButtonNext();
              } else {
                Alert.alert("No users allowed to book this activity");
              }
            }}>
            {type == class_activity_type_id && selectData.quota_left <= 0
              ? "Join Waiting List"
              : "NEXT"}
          </Button>
        </HStack>
      </Box>

      <Actionsheet isOpen={isOpen} onClose={onClose} hideDragIndicator={true}>
        <Actionsheet.Content h={400}>
          <CalendarPicker
            weekdays={["S", "M", "T", "W", "T", "F", "S"]}
            dayLabelsWrapper={{ borderColor: "transparent" }}
            previousComponent={<ChevronLeftIcon />}
            nextComponent={<ChevronRightIcon />}
            dayShape={{ backgroundColor: "red" }}
            selectedDayStyle={{
              backgroundColor: "transparent",
              borderColor: "black",
              borderWidth: 1,
              borderRadius: 14,
              width: 40,
            }}
            onDateChange={changeDate}
          />
          <TouchableOpacity onPress={onSubmitDate}>
            <Box
              style={{
                backgroundColor: "#212121",
                padding: 10,
                width: 320,
                alignItems: "center",
              }}>
              <Text style={{ color: "#FFF" }}>{onSelectedDate}</Text>
            </Box>
          </TouchableOpacity>
        </Actionsheet.Content>
      </Actionsheet>
    </Box>
  );
};

SportDetail.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default SportDetail;
