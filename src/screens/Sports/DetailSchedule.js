import React, { useState, useEffect } from "react";
import { TouchableOpacity, Dimensions } from "react-native";
import {
  Box,
  Text,
  Image,
  HStack,
  VStack,
  ScrollView,
  Actionsheet,
  ChevronRightIcon,
  ChevronLeftIcon,
  FlatList,
  Select,
  CheckIcon,
} from "native-base";
import PropTypes from "prop-types";
import { useDispatch, useSelector } from "react-redux";
import Actions from "actions";
import CalendarPicker from "react-native-calendar-picker";
import Loading from "components/ui/Loading";
import moment from "moment";
import _ from "lodash";

import CardSchedule from "../../components/ui/CardSchedule";

const { width, height } = Dimensions.get("window");

const DetailSchedule = ({ navigation }) => {
  const dispatch = useDispatch();
  const [tabActive, setTabActive] = useState(1);
  // const [selectedUser, setSelectedUser] = useState("");

  // const dataUniqueFamily = useSelector(state => state.FAMILY.uniqueFamily);
  const profile = useSelector(state => state.AUTH.profile);
  // const dataFamily = _.get(dataUniqueFamily, "data", []);
  const data = useSelector(state => state.PRODUCT.activityScheduleAll);
  const dataUpcoming = useSelector(state => state.PRODUCT.upcomingSchedule);
  const isLoading = data.isLoading;

  const { class_activity_type_id } = useSelector(
    state => state.PRODUCT.activityTypeId.activityTypeId,
  );

  useEffect(() => {
    // dispatch(Actions.activityScheduleAll(profile.data.id));
    // setSelectedUser(profile.data.id);
    displayDay();
  }, []);

  const [isOpen, setIsOpen] = useState(false);
  const [dateToGo, setDate] = useState([]);
  const [onSelectedDate, setSelectedDate] = useState(
    moment().format("dddd, DD MMMM yyyy HH:mm"),
  );

  const onClose = () => {
    setIsOpen(false);
  };

  const onPressAction = () => {
    setIsOpen(true);
  };

  const handlePress = item => {
    let datas = item;
    let arryTemp = _.filter(dataUpcoming.upcomingSchedule, function (o) {
      return o.id === datas.item.id;
    });

    if (arryTemp.length === 0) {
      navigation.navigate("SportDetail", {
        data: { item: datas.item.activity_court.activity },
        type: datas.item.activity_court.activity.activity_type_id,
      });
    } else {
      navigation.navigate("DetailActivity", {
        title: "Detail Activity",
        data: datas,
        typeDetail: 1,
      });
    }
  };

  const displayDay = () => {
    let days = [];
    let n = 0;

    let fromThisDay = moment();

    while (n < 7) {
      console.log(
        "asjaasjiu",
        fromThisDay.format("DD MM yy"),
        moment().format("DD MM yy"),
      );

      if (fromThisDay.format("DD MM yy") === moment().format("DD MM yy")) {
        days.push({ date: "Today", actualDate: fromThisDay.clone() });
      } else if (
        fromThisDay.format("DD MM yy") ===
        moment().add(1, "days").format("DD MM yy")
      ) {
        days.push({ date: "Tomorrow", actualDate: fromThisDay.clone() });
      } else {
        days.push({
          date: fromThisDay.clone().format("ddd, MMM DD"),
          actualDate: fromThisDay.clone(),
        });
      }

      fromThisDay = fromThisDay.add(1, "day");
      n++;
    }

    console.log("days", days);
    setDate(days);
  };

  const onSubmitDate = () => {
    onClose();
    displayDay();
  };

  const changeDate = date => {
    let dateSelect = moment(date).format("dddd, DD MMMM yyyy");
    setSelectedDate(dateSelect);
  };

  const changeTab = idTab => {
    console.log("idTab >>>> ", idTab);
    setTabActive(idTab);
  };

  // const handleSelect = item => {
  //   setSelectedUser(item);
  //   dispatch(Actions.activityScheduleAll(item));
  //   dispatch(Actions.upcomingSchedule(item));
  //   displayDay();
  // };

  if (isLoading) {
    return <Loading />;
  }

  const dataFilter = _.filter(data.activityScheduleAll, function (o) {
    return (
      moment(o.start_time).format("DD MM YYYY") ===
        moment(onSelectedDate).format("DD MM YYYY") &&
      o.activity_court.activity.activity_type_id === class_activity_type_id
    );
  });

  const dataOrder = _.orderBy(dataFilter, ["start_time"], ["asc"]);
  console.log("sssss", dataOrder);
  return (
    <Box flex={1}>
      {/* <ScrollView style={{flex:1, backgroundColor: "white"}}> */}
      <Box style={{ flex: 1, backgroundColor: "white" }}>
        <HStack
          style={{
            padding: 20,
            alignItems: "center",
            justifyContent: "space-between",
          }}>
          <TouchableOpacity onPress={() => changeTab(1)}>
            <Box
              style={{
                padding: 12,
                paddingVertical: 4,
                borderRadius: 16,
                backgroundColor: tabActive === 1 ? "#000" : "transparent",
                marginRight: 16,
              }}>
              <Text
                style={{
                  fontSize: 12,
                  color: tabActive === 1 ? "#FFF" : "#616161",
                  fontWeight: "700",
                  fontFamily: "Montserrat-Regular",
                }}>
                All
              </Text>
            </Box>
          </TouchableOpacity>
          {/* <Select
            selectedValue={selectedUser}
            width={150}
            accessibilityLabel="Choose User"
            placeholder="Choose User"
            _selectedItem={{
              endIcon: <CheckIcon size="5" />,
            }}
            mt={1}
            onValueChange={itemValue => handleSelect(itemValue)}>
            {dataFamily.length !== 0 &&
              dataFamily.map(item => (
                <Select.Item label={`${item.name}`} value={item.id} />
              ))}
          </Select> */}
        </HStack>
        <Box w={"100%"}>
          <VStack>
            <HStack
              style={{
                padding: 20,
                paddingVertical: 12,
                borderWidth: 1,
                borderColor: "#ECEDEF",
              }}>
              <TouchableOpacity onPress={onPressAction}>
                <Image
                  source={require("../../../assets/images/ic-dates.png")}
                  style={{ height: 24, width: 24, marginRight: 16 }}
                  resizeMode="cover"
                  alt="image"
                />
              </TouchableOpacity>
              <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                {dateToGo.map(item => {
                  let isSelectDate =
                    moment(item.actualDate).format("DD MM yy") ===
                    moment(onSelectedDate).format("DD MM yy");
                  console.log("aksugdiau", isSelectDate);
                  return (
                    <VStack style={{ marginRight: 16, alignItems: "center" }}>
                      <TouchableOpacity
                        onPress={() => changeDate(item.actualDate)}>
                        <Text
                          style={{
                            fontSize: 12,
                            color: isSelectDate ? "#000" : "#757575",
                            fontWeight: "bold",
                          }}>
                          {item.date}
                        </Text>
                      </TouchableOpacity>
                      {isSelectDate && (
                        <Box
                          style={{
                            height: 2,
                            backgroundColor: "#212121",
                            position: "absolute",
                            bottom: 0,
                            width: 45,
                          }}
                        />
                      )}
                    </VStack>
                  );
                })}
              </ScrollView>
            </HStack>
            {dataOrder.length === 0 ? (
              <Box
                style={{
                  alignItems: "center",
                  justifyContent: "center",
                  height: height - 200,
                  width: width,
                }}>
                <Text
                  style={{
                    fontSize: 16,
                    color: "#000",
                    fontWeight: "bold",
                    fontFamily: "Montserrat-Regular",
                  }}>
                  No Activity
                </Text>
              </Box>
            ) : (
              <FlatList
                data={dataOrder}
                style={{ padding: 20 }}
                renderItem={item => {
                  console.log("item item", item);
                  if (
                    item.item.activity_court.activity.activity_type_id ===
                    class_activity_type_id
                  ) {
                    return (
                      <Box>
                        <CardSchedule
                          data={item}
                          dataDiff={dataUpcoming.upcomingSchedule}
                          onPress={() => handlePress(item)}
                          fromSchedule={true}
                        />
                      </Box>
                    );
                  }
                }}
              />
            )}
          </VStack>
        </Box>
      </Box>
      {/* </ScrollView> */}
      <Actionsheet isOpen={isOpen} onClose={onClose} hideDragIndicator={true}>
        <Actionsheet.Content h={450}>
          <CalendarPicker
            weekdays={["S", "M", "T", "W", "T", "F", "S"]}
            dayLabelsWrapper={{ borderColor: "transparent" }}
            previousComponent={<ChevronLeftIcon />}
            nextComponent={<ChevronRightIcon />}
            dayShape={{ backgroundColor: "red" }}
            selectedDayStyle={{
              backgroundColor: "transparent",
              borderColor: "black",
              borderWidth: 1,
              borderRadius: 14,
              width: 40,
            }}
            onDateChange={changeDate}
            minDate={moment()}
          />
          <TouchableOpacity
            onPress={onSubmitDate}
            style={{ position: "absolute", bottom: 40 }}>
            <Box
              style={{
                backgroundColor: "#212121",
                padding: 10,
                width: 320,
                alignItems: "center",
              }}>
              <Text style={{ color: "#FFF" }}>{onSelectedDate}</Text>
            </Box>
          </TouchableOpacity>
        </Actionsheet.Content>
      </Actionsheet>
    </Box>
  );
};

DetailSchedule.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default DetailSchedule;
