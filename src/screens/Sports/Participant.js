import React, { useCallback, useEffect, useState } from "react";
import {
  Box,
  Button,
  Text,
  ScrollView,
  Image,
  ChevronDownIcon,
  FlatList,
} from "native-base";
import { get } from 'lodash';
import { TouchableOpacity, ImageBackground, Dimensions, View, Alert } from "react-native";
import PropTypes from "prop-types";
import { useDispatch, useSelector } from "react-redux";
import { useIsFocused } from '@react-navigation/native'
import Selectors from "selectors";
import Actions from "actions";
import { moderateScale } from "react-native-size-matters";
import SearchBar from '../../components/ui/SearchBar';
import { image } from "images";
import { UserApi } from "utils/Api";
import FamilyCard from "../../components/ui/FamilyCard";
import ParticipantCard from "../../components/ui/ParticipantCard";

const user = UserApi.client;

const Participant = ({ navigation, route }) => {
  const activityParticipant = useSelector(
    state => state.FAMILY.user.activityParticipant,
  );

  const [searchText, setSearchText] = useState("")

  const activityBookingId = get(route.params, "activityBookingId", null)
  
  const dispatch = useDispatch();
  const isFocused = useIsFocused()
  // const family = dispatch(Actions.fetchAllFamilies({
  // }))

  const dataUniqueFamily = useSelector((state) => state.FAMILY.uniqueFamily.data)
  // const [dataUsers, setDataKids] = useState(dataUniqueFamily);
  const [dataUsers, setDataKids] = useState([]);
  const activityScheduleId = get(route.params, "activityScheduleId", 1)
  const userId = get(route.params, "userId", 1)
  const [participants, setParticipants] = useState(activityParticipant);

  const textOnChange = useCallback((text) => {
    console.log("AddFamily text: ", text);
    if (text === "") {
      refreshParticipant(userId);
      setDataKids(activityParticipant);
      // setParticipants(activityParticipant);
    }
    setSearchText(text)
  })

  const onSubmit = useCallback((e) => {
    searchPhoneNumber(searchText)
  })

  const searchPhoneNumber = async(text) => {
    // TODO: call the data from API
    if(text !== "") {
      // await dataUniqueFamily.filter(({ phone_number }) => 
      //   phone_number.toLowerCase().includes(text.toLowerCase())
      // ) 
      // console.log("filterData: ", filterData);

      setDataKids(await activityParticipant.filter(({ user }) => 
        user.phone_number.toLowerCase().includes(text.toLowerCase())
      ) )

      // await user.get(`/user?q=${encodeURIComponent(text)}`)
      //   .then(res => {
      //     console.log("the res: ", res);
      //     setDataKids(res.data.data)
      //   })
      //   .catch(err => {
      //     console.log("the err: ", err);
  
      //   })
    }
    // setDataKids(result)
  }

  const inviteParticipant = (id) => {

    setSearchText("")
    refreshParticipant(userId)
    Alert.alert("Invited!", " " [
      {
        text: "OK",
        onPress: () => {
          setSearchText("")
          refreshParticipant(userId)
          console.log("OK Pressed")
        },
        style: "default",
      }
    ])

    // navigation.pop()
  }

  const refreshParticipant = id => {
    // to retrieved the global schedule data & refresh the list
    setParticipants(activityParticipant);
  };

  const cancelInvite = (id) => {
   
  }

  useEffect(() => {
    // call API here
    // dispatch(Actions.fetchFamilyUnique())
    setDataKids(participants)
    refreshParticipant(userId)
    console.log("params nya: ", route.params);
    console.log("sss ", dataUniqueFamily);
  }, [isFocused])

  return (
    <Box
      alignItems="center"
      justifyContent="center"
      bg={"white"}
      style={{ flex: 1 }}>
      <Box
        style={{
          flex: 0.15,
          // height: moderateScale(8, 0.25),
          width: "100%",
          flexDirection: "row",
          alignItems: "center",
          paddingHorizontal: moderateScale(16, .25),
          justifyContent: "space-between",
        }}>
        <SearchBar
          placeholder="Search username or phone number"
          contentContainerStyle={{width: "90%"}}
          onChangeText={textOnChange}
          contentContainerStyle={{
            width: '75%'
          }}
          value={searchText}
          // onSubmit={onSubmit}
        />
        <TouchableOpacity
          onPress={() => onSubmit()}
        >
          <Box style={{
            backgroundColor: 'rgba(224, 224, 224, 1)',
            padding: moderateScale(14, .25),
            // paddingVertical: moderateScale(16, .25),
            // paddingHorizontal: moderateScale(14, .25),
            // width: moderateScale(100, .25),
            justifyContent: 'center',
            alignItems: "center"
          }}>
            <Text style={{
              color: '#000'
            }}>
              {`Search`}
            </Text>
          </Box>
        </TouchableOpacity>
        {/* </ImageBackground> */}
      </Box>
      <Box style={{ flex: 1, height: '100%'}}>
        {
          // dataUsers.length > 0 && (
            <FlatList 
              data={dataUsers}
              extraData={dataUsers}
              renderItem={({item, index}) => {
                console.log("test: ", item);
                return(
                  <ParticipantCard
                    participants={participants} 
                    data={item}
                    onPress={() => {
                      inviteParticipant(item.id)
                    }}
                    onCancelInvite={(id, name) => {
                      Alert.alert("Are you sure?", `You will cancel invitation to ${name}`, [
                        {
                          text: "Yes",
                          onPress: () => {
                            cancelInvite(id)
                          },
                          style: "destructive",
                        },
                        {
                          text: "No",
                          onPress: () => null,
                          style: "default",
                        },
                      ], {cancelable: false});
                    }}
                    onRemoveMember={() => {

                    }}
                  /> 
                )
              }}
              style={{
                width: Dimensions.get("screen").width
              }}
              keyExtractor={(item, index) => String(item + index)}
            />
          // )
        }
      </Box>
      <Button
        onPress={() => navigation.goBack()}
        isLoading={false}
        style={{
          backgroundColor: '#1035BB',
          marginVertical: moderateScale(16),
          width: '90%',
        }}
      >
        <Text
          style={{
            fontSize: moderateScale(14, 0.25),
            fontFamily: "Montserrat-Regular",
            fontWeight: "700",
            color: '#fff'
          }}
        > 
          DONE
        </Text>
      </Button>
    </Box>
  );
};

Participant.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default Participant;
