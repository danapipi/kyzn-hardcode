import React, { useEffect, useState } from 'react';
import {Box, Button,HStack , ChevronDownIcon, Flex, Select, CheckIcon, VStack} from 'native-base';
import PropTypes from 'prop-types';
import { TouchableOpacity, ScrollView,Image, Text, Dimensions, FlatList } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import Actions from "actions";
import Loading from "components/ui/Loading";
import _ from 'lodash';
import { UserApi } from "utils/Api";
import { useRoute } from '@react-navigation/core';
import Commons from '../../utils/common';
import moment from 'moment';
import { image } from "images";
import { moderateScale } from 'react-native-size-matters';

const userapi = UserApi.client;
const { width, height } = Dimensions.get('window');

const Additional = ({ navigation}) => {
  
  const route = useRoute()
  const params = route.params
  const {startTime, realPrice, multipler, type, data, activity, selectedUser,
    coupons, price, timeDuration, best_coupon_id } = params
  const dummySupply = [
    {
      "id": 1,
      "name": "Cock Brand1",
      "price": 15000
    },{
      "id": 2,
      "name": "Cock Brand2",
      "price": 12000
    },{
      "id": 3,
      "name": "Cock Brand3",
      "price": 13000
    },
  ]

  const dummyRent = [
    {
      "id": 1,
      "name": "Hand Towel",
      "price": 1000
    },{
      "id": 2,
      "name": "Towel",
      "price": 50000
    },{
      "id": 3,
      "name": "Cock Brand3",
      "price": 13000
    },
  ]

  const [selectAss, setSelectAss] = useState(null)
  const [selectSupp, setSelectSupp] = useState([])
  const [selectRent, setSelectRent] = useState([])

  console.log("Params additonal", params, startTime, realPrice, multipler);
  
  const handleSelectAss = (id) => {
    if(selectAss == id){
      setSelectAss(null)
    } else {
      setSelectAss(id)
    }
  }

  const clearAss = () => {
    setSelectAss(null)
  }

  const addItemSupply = (id, index) => {
    let filterData = _.filter(selectSupp, function(o) { return o.id == id});
    let count = _.get(filterData[0], 'count', 0)
    let dataTemp = {
      "id": id,
      "count": count + 1
    }

    if(filterData.length !== 0){
      let newArry = [...selectSupp]
      newArry[index] = dataTemp

      setSelectSupp(newArry)
    } else {
      setSelectSupp([...selectSupp, dataTemp])
    }

    console.log("dataTemp", dataTemp, count, filterData);
  }

  const reduceItemSupply = (id, index) => {
    let filterData = _.filter(selectSupp, function(o) { return o.id == id});
    let count = _.get(filterData[0], 'count', 0)
    let dataTemp = {
      "id": id,
      "count": count - 1
    }

    if(filterData.length !== 0){
      let newArry = [...selectSupp]
      newArry[index] = dataTemp

      setSelectSupp(newArry)
    } else {
      setSelectSupp([...selectSupp, dataTemp])
    }

    console.log("dataTemp", dataTemp, count, filterData);
  }

  const clearSupply = () => {
    setSelectSupp([])
  }

  const clearRent = () => {
    setSelectRent([])
  }

  const addItemRent = (id, index) => {
    let filterData = _.filter(selectRent, function(o) { return o.id == id});
    let count = _.get(filterData[0], 'count', 0)
    let dataTemp = {
      "id": id,
      "count": count + 1
    }

    if(filterData.length !== 0){
      let newArry = [...selectRent]
      newArry[index] = dataTemp

      setSelectRent(newArry)
    } else {
      setSelectRent([...selectRent, dataTemp])
    }

    console.log("dataTemp", dataTemp, count, filterData);
  }

  const reduceItemRent = (id, index) => {
    let filterData = _.filter(selectRent, function(o) { return o.id == id});
    let count = _.get(filterData[0], 'count', 0)
    let dataTemp = {
      "id": id,
      "count": count - 1
    }

    if(filterData.length !== 0){
      let newArry = [...selectRent]
      newArry[index] = dataTemp

      setSelectRent(newArry)
    } else {
      setSelectRent([...selectRent, dataTemp])
    }

    console.log("dataTemp", dataTemp, count, filterData);
  }

  return(
    <Box style={{flex:1, backgroundColor:"#FFF"}}>
      <ScrollView style={{paddingTop:32, marginBottom:100}}>
        <Box>
          <HStack style={{paddingHorizontal:16, justifyContent:"space-between"}}>
            <Text 
            style={{
              fontFamily: "Montserrat-Regular",
              fontWeight: "700",
              fontSize: 12,
            }}>
              Add Assistant
            </Text>
            <TouchableOpacity onPress={() => clearAss()}>
              <HStack justifyItems="center" alignItems="center">
                {/* <Image
                  source={image.}
                  style={{ height: 12, width: 12, marginLeft: 3 }}
                  resizeMode="cover"
                  alt="image"
                /> */}
                <Text
                style={{
                  fontFamily: "Montserrat-Regular",
                  fontWeight: "700",
                  fontSize: 12,
                }}>
                  Clear
                </Text>
              </HStack>
            </TouchableOpacity>
            
          </HStack>
          <HStack style={{padding:16, justifyContent:"space-between"}}>
          <TouchableOpacity onPress={() => handleSelectAss(1)}>
            <Box style={{borderColor: selectAss == 1 ? "#000" : "#EEE", borderWidth:1, width:(width/2)-24, height:120, alignItems:"center", justifyContent:"center"}}>
              <VStack style={{width:"80%", alignItems:"center"}}>
                <Image
                  resizeMode="contain"
                  source={image.coachImg}
                  alt="image"
                  style={{ height: 54, width:54 }}
                />
                {selectAss == 1 && 
                  <Image
                    resizeMode="contain"
                    source={image.checkmark}
                    alt="image"
                    style={{ height: 21, width:21, position:"absolute", right:0}}
                  />
                }
                <Box style={{marginTop:10, alignItems:"center"}}>
                  <Text 
                  style={{
                    fontFamily: "Montserrat-Regular",
                    fontWeight: "700",
                    fontSize: 12,
                  }}>
                    Add Coach
                  </Text>
                  <Text
                  style={{
                    fontFamily: "Montserrat-Regular",
                    fontWeight: "700",
                    fontSize: 10,
                  }}>
                      Rp 250.000
                  </Text>
                </Box>
              </VStack>
            </Box>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => handleSelectAss(2)}>
            <Box style={{borderColor:selectAss == 2 ? "#000" : "#EEE", borderWidth:1, width:(width/2)-24, height:120, alignItems:"center", justifyContent:"center"}}>
              <VStack style={{width:"80%", alignItems:"center"}}>
                <Image
                  resizeMode="contain"
                  source={image.ballboyImg}
                  alt="image"
                  style={{ height: 54, width:54 }}
                />
                {selectAss == 2 && 
                  <Image
                    resizeMode="contain"
                    source={image.checkmark}
                    alt="image"
                    style={{ height: 21, width:21, position:"absolute", right:0}}
                  />
                }
                <Box style={{marginTop:10, alignItems:"center"}}>
                  <Text 
                  style={{
                    fontFamily: "Montserrat-Regular",
                    fontWeight: "700",
                    fontSize: 12,
                  }}>
                    Add Ball Boys
                  </Text>
                  <Text
                  style={{
                    fontFamily: "Montserrat-Regular",
                    fontWeight: "700",
                    fontSize: 10,
                  }}>
                      Rp 250.000
                  </Text>
                </Box>
              </VStack>
            </Box>
            </TouchableOpacity>
          </HStack>
        </Box>
        <Box>
          <HStack style={{paddingHorizontal:16, justifyContent:"space-between"}}>
            <Text 
            style={{
              fontFamily: "Montserrat-Regular",
              fontWeight: "700",
              fontSize: 12,
            }}>
              Add Supply
            </Text>
            <TouchableOpacity onPress={() => clearSupply()}>
              <HStack justifyItems="center" alignItems="center">
                
                <Text
                style={{
                  fontFamily: "Montserrat-Regular",
                  fontWeight: "700",
                  fontSize: 12,
                }}>
                  Clear
                </Text>
              </HStack>
            </TouchableOpacity>
            
          </HStack>
          <HStack style={{padding:16, justifyContent:"space-between"}}>
            <FlatList
              data={dummySupply}
              horizontal
              renderItem={({item, index}) => {
                let filterSelect = _.filter(selectSupp, function(o) { return o.id == item.id });
                let count =  _.get(filterSelect[0], 'count', 0)
                console.log("select", filterSelect);
                return(
                  <VStack style={{width:moderateScale(124), borderColor:"#EEE", borderWidth:1, alignItems:"center", marginRight:12}}>
                    <Image
                      resizeMode="contain"
                      source={{uri: "https://via.placeholder.com/86x86"}}
                      alt="image"
                      style={{ height: 86, width:86 }}
                    />
                    <Box style={{marginTop:10, alignItems:"flex-start", width:"100%", paddingHorizontal:10,paddingBottom:10}}>
                      <Text 
                      style={{
                        fontFamily: "Montserrat-Regular",
                        fontWeight: "500",
                        fontSize: 8,
                        color:"#757575"
                      }}>
                        {item.name}
                      </Text>
                      <Text
                      style={{
                        fontFamily: "Montserrat-Regular",
                        fontWeight: "600",
                        fontSize: 10,
                      }}>
                      {Commons.convertToRupiah(item.price)}/item
                      </Text>
                    </Box>
                    {count == 0 
                      ? 
                        <TouchableOpacity onPress={() => addItemSupply(item.id, index)} style={{backgroundColor:"#000", width:"100%"}}>
                          <Box style={{padding:9, alignItems:"center"}}>
                            <Text 
                            style={{
                              fontFamily: "Montserrat-Regular",
                              fontWeight: "600",
                              fontSize: 12,
                              color:"#FFF"
                            }}>
                              + Add Item
                            </Text>
                          </Box>
                        </TouchableOpacity>
                      :
                      <HStack>
                        <TouchableOpacity onPress={() => reduceItemSupply(item.id, index)} style={{borderColor:"#000", borderWidth:1,width:"30%"}}>
                          <Box style={{padding:9, alignItems:"center"}}>
                            <Text 
                            style={{
                              fontFamily: "Montserrat-Regular",
                              fontWeight: "600",
                              fontSize: 12,
                              color:"#000"
                            }}>
                              - 
                            </Text>
                          </Box>
                        </TouchableOpacity>
                        <Box style={{padding:9, alignItems:"center", width:"40%", borderColor:"#000", borderBottomWidth:1, borderTopWidth:1}}>
                          <Text 
                          style={{
                            fontFamily: "Montserrat-Regular",
                            fontWeight: "600",
                            fontSize: 12,
                            color:"#000"
                          }}>
                            {count}
                          </Text>
                        </Box>
                        <TouchableOpacity onPress={() => addItemSupply(item.id, index)} style={{borderColor:"#000", borderWidth:1,backgroundColor:"#000", width:"30%"}}>
                          <Box style={{padding:9, alignItems:"center"}}>
                            <Text 
                            style={{
                              fontFamily: "Montserrat-Regular",
                              fontWeight: "600",
                              fontSize: 12,
                              color:"#FFF"
                            }}>
                              + 
                            </Text>
                          </Box>
                        </TouchableOpacity>
                      </HStack>
                    }
                  </VStack>
                )
              }}
            />
          </HStack>
        </Box>
        <Box style={{marginBottom:30}}>
          <HStack style={{paddingHorizontal:16, justifyContent:"space-between"}}>
            <Text 
            style={{
              fontFamily: "Montserrat-Regular",
              fontWeight: "700",
              fontSize: 12,
            }}>
              Rent
            </Text>
            <TouchableOpacity onPress={() => clearRent()}>
              <HStack justifyItems="center" alignItems="center">
                {/* <Image
                  source={image.}
                  style={{ height: 12, width: 12, marginLeft: 3 }}
                  resizeMode="cover"
                  alt="image"
                /> */}
                <Text
                style={{
                  fontFamily: "Montserrat-Regular",
                  fontWeight: "700",
                  fontSize: 12,
                }}>
                  Clear
                </Text>
              </HStack>
            </TouchableOpacity>
            
          </HStack>
          <HStack style={{padding:16, justifyContent:"space-between"}}>
            <FlatList
              data={dummyRent}
              horizontal
              renderItem={({item, index}) => {
                let filterSelect = _.filter(selectRent, function(o) { return o.id == item.id });
                let count =  _.get(filterSelect[0], 'count', 0)
                console.log("select", filterSelect);
                return(
                  <VStack style={{width:moderateScale(124), borderColor:"#EEE", borderWidth:1, alignItems:"center", marginRight:12}}>
                    <Image
                      resizeMode="contain"
                      source={{uri: "https://via.placeholder.com/86x86"}}
                      alt="image"
                      style={{ height: 86, width:86 }}
                    />
                    <Box style={{marginTop:10, alignItems:"flex-start", width:"100%", paddingHorizontal:10,paddingBottom:10}}>
                      <Text 
                      style={{
                        fontFamily: "Montserrat-Regular",
                        fontWeight: "500",
                        fontSize: 8,
                        color:"#757575"
                      }}>
                        {item.name}
                      </Text>
                      <Text
                      style={{
                        fontFamily: "Montserrat-Regular",
                        fontWeight: "600",
                        fontSize: 10,
                      }}>
                        {Commons.convertToRupiah(item.price)}/hr
                      </Text>
                    </Box>
                    {count == 0 
                      ? 
                        <TouchableOpacity onPress={() => addItemRent(item.id, index)} style={{backgroundColor:"#000", width:"100%"}}>
                          <Box style={{padding:9, alignItems:"center"}}>
                            <Text 
                            style={{
                              fontFamily: "Montserrat-Regular",
                              fontWeight: "600",
                              fontSize: 12,
                              color:"#FFF"
                            }}>
                              + Add Item
                            </Text>
                          </Box>
                        </TouchableOpacity>
                      :
                      <HStack>
                        <TouchableOpacity onPress={() => reduceItemRent(item.id, index)} style={{borderColor:"#000", borderWidth:1,width:"30%"}}>
                          <Box style={{padding:9, alignItems:"center"}}>
                            <Text 
                            style={{
                              fontFamily: "Montserrat-Regular",
                              fontWeight: "600",
                              fontSize: 12,
                              color:"#000"
                            }}>
                              - 
                            </Text>
                          </Box>
                        </TouchableOpacity>
                        <Box style={{padding:9, alignItems:"center", width:"40%", borderColor:"#000", borderBottomWidth:1, borderTopWidth:1}}>
                          <Text 
                          style={{
                            fontFamily: "Montserrat-Regular",
                            fontWeight: "600",
                            fontSize: 12,
                            color:"#000"
                          }}>
                            {count}
                          </Text>
                        </Box>
                        <TouchableOpacity onPress={() => addItemRent(item.id, index)} style={{borderColor:"#000", borderWidth:1,backgroundColor:"#000", width:"30%"}}>
                          <Box style={{padding:9, alignItems:"center"}}>
                            <Text 
                            style={{
                              fontFamily: "Montserrat-Regular",
                              fontWeight: "600",
                              fontSize: 12,
                              color:"#FFF"
                            }}>
                              + 
                            </Text>
                          </Box>
                        </TouchableOpacity>
                      </HStack>
                    }
                  </VStack>
                )
              }}
            />
          </HStack>
        </Box>
        <Box style={{height:8, backgroundColor:"#F8F9FC", marginBottom:32}}/>
        <Box style={{marginBottom:150}}>
          <HStack style={{paddingHorizontal:16, justifyContent:"space-between"}}>
            <Text 
            style={{
              fontFamily: "Montserrat-Regular",
              fontWeight: "700",
              fontSize: 12,
            }}>
              Participant
            </Text>
          </HStack>
          <HStack style={{padding:16}}>
            <VStack style={{width:60, alignItems:"center", justifyContent:"center", marginRight:10}}>
              <Image
                resizeMode="contain"
                source={{uri: "https://via.placeholder.com/50x50"}}
                alt="image"
                style={{ height: 50, width:50, borderRadius:100, marginBottom:5 }}
              />
              <Text 
              style={{
                fontFamily: "Montserrat-Regular",
                fontWeight: "600",
                fontSize: 10,
              }}>
                You
              </Text>
            </VStack>
            <VStack style={{width:60, alignItems:"center", justifyContent:"center", marginRight:10}}>
              <Image
                resizeMode="contain"
                source={{uri: "https://via.placeholder.com/50x50"}}
                alt="image"
                style={{ height: 50, width:50, borderRadius:100,marginBottom:5 }}
              />
              <Text 
              style={{
                fontFamily: "Montserrat-Regular",
                fontWeight: "600",
                fontSize: 10,
              }}>
                Saskia
              </Text>
            </VStack>
            <TouchableOpacity onPress={() => {
              // navigation.navigate("Participant", {activityBookingId: activity.id})
            }}>
            <VStack style={{width:60, alignItems:"center", justifyContent:"center", marginRight:10}}>
              <Box style={{height:50, width:50, borderRadius:100, backgroundColor:"#EEE", marginBottom:5, alignItems:"center", justifyContent:"center"}}>
                <Text 
                style={{
                  fontFamily: "Montserrat-Regular",
                  fontWeight: "600",
                  fontSize: 10,
                }}>
                  +
                </Text>
              </Box>
              <Text 
              style={{
                fontFamily: "Montserrat-Regular",
                fontWeight: "600",
                fontSize: 10,
              }}>
                Add More
              </Text>
            </VStack>
            </TouchableOpacity>
          </HStack>
        </Box>
      </ScrollView>
      <Box
        style={{
          position: "absolute",
          bottom: 0,
          right: 0,
          width: "100%",
          padding: 20,
          paddingBottom: 40,
          borderTopWidth: 1,
          borderColor: "#ECEDEF",
          backgroundColor: "#FFF",
        }}
      >
        <HStack style={{ justifyContent: "space-between" }}>
          <VStack>
            <HStack>
              <Text
                style={{
                  fontSize: 10,
                  color: "#606060",
                  marginBottom: 5,
                  fontFamily: "Montserrat-Regular",
                }}
              >
                {moment(startTime.onSelectedDate).format("DD MM yy") ==
                moment().format("DD MM yy")
                  ? "Today •"
                  : moment(startTime.onSelectedDate).add(1, "day").format("DD MM yy") ==
                    moment().format("DD MM yy")
                  ? "Tomorrow •"
                  : moment(startTime.onSelectedDate).format("dddd, DD-MM-yy")} {startTime.fullRangeHours}
              </Text>
            </HStack>
            <Text
              style={{
                fontSize: 14,
                color: "#000",
                fontWeight: "bold",
                fontFamily: "Montserrat-Regular",
              }}
              alignItems="flex-start"
            >
              {Commons.convertToRupiah(realPrice)} ({multipler}hr)
            </Text>
          </VStack>
          <Button
            w={130}
            bgColor={"#1035BB"}
            style={{height: 50, bottom: 0, right: 0, position: "absolute"}}
            onPress={
              // type == 1 
              // ? 
              () =>
                navigation.navigate("Payment", {
                  data: data,
                  activity: activity,
                  selectedUser: selectedUser,
                  type: type,
                  coupons: coupons,
                  price: price,
                  timeDuration: timeDuration,
                  startTime: startTime,
                  best_coupon_id: best_coupon_id,
                })
              // :
              // () =>
              //   navigation.navigate("Payment", {
              //     data: selectData,
              //     activity: data,
              //     selectedUser: selectedUser,
              //     coupons: dataActivityById.activityByID.coupons,
              //     type: type
              //   })
            }
          >
            NEXT
          </Button>
        </HStack>
      </Box>
    </Box>
  )
};

Additional.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default Additional;
