import React, { useEffect, useState } from "react";
import { Box, Button, Text, FlatList } from "native-base";
import dummyData from "./dummyData/dummySport";
import PropTypes from "prop-types";
import SportCard from "components/ui/SportCard";
import { useDispatch, useSelector } from "react-redux";
import Actions from "actions";
import { Select, CheckIcon, Flex } from "native-base";
import { UserApi } from "utils/Api";
import Loading from "components/ui/Loading";
import _ from "lodash";

const userapi = UserApi.client;

const SportList = ({ navigation, route }) => {
  const [numCol, setNumCol] = useState(2);
  const [isLoading, setIsLoading] = useState(false);
  const data = useSelector(state => state.PRODUCT.activity);
  console.log('SPORT LIST DATA BRO >>>>> : ', data);
  const [activityList, setActivityList] = useState([]);
  const family = useSelector(state => state.FAMILY.user.data);
  const dataUniqueFamily = useSelector(state => state.FAMILY.uniqueFamily);
  // const [selectedUser, setSelectedUser] = useState(dataUniqueFamily.data[0].id);
  const [selectedUser, setSelectedUser] = useState("all");
  const dispatch = useDispatch();

  const { activityId, categoryId, categoryChildId } = route.params.params;
  // let userId = _.get(profile.data, 'id', null)

  // console.log("ahsvdiuo", navigation, route, data);

  useEffect(() => {
    // dispatch(Actions.activity(activityId, categoryId, categoryChildId));
    // dispatch(Actions.fetchUserFamily());
    // setUser(selectedUser);
    // dispatch(Actions.fetchFamilyUnique());
  }, []);

  const setUser = async itemValue => {
    setSelectedUser(itemValue);
    let categorylink = `&categoryId=${categoryId}`;
    let categorychildlink = `&categoryChildId=${categoryChildId}`;
    let activitytypelink = `&activityTypeId=${activityId}`;
    let base = "activity?";
    if (categoryId) {
      base = base + categorylink;
    }
    if (categoryChildId) {
      base = base + categorychildlink;
    }
    if (activityId) {
      base = base + activitytypelink;
    }
    if (itemValue === "all") {
      console.log("qweasd", base);

      let test = await userapi.get(base);

      console.log("qweasd", test.data.data);
      setActivityList(test.data.data);
      setIsLoading(false);
    } else {
      let test = await userapi.get(base + `&userId=${itemValue}`);
      console.log("qweasd", base + `&userId=${itemValue}`);
      console.log("qweasd", test.data.data.length);
      setActivityList(test.data.data);
      setIsLoading(false);
    }
  };

  if (!isLoading) {
    return (
      <Box flex={1} style={{ backgroundColor: "#FFF" }}>
        {/* <Flex
          // direction="row"
          justify="flex-end"
          style={{ marginTop: 8, marginRight: 8, flexDirection: "row" }}>
          {dataUniqueFamily.data && (
            <Select
              selectedValue={selectedUser}
              width={150}
              accessibilityLabel="Choose User"
              placeholder="Choose User"
              _selectedItem={{
                endIcon: <CheckIcon size="5" />,
              }}
              mt={1}
              onValueChange={itemValue => setUser(itemValue)}>
              {/* <Select.Item label="All" value="all" /> */}
              {/* <Select.Item label="All" value="all" />
              {dataUniqueFamily.data &&
                dataUniqueFamily.data.map(family_member => (
                  <Select.Item
                    label={family_member.name}
                    value={family_member.id}
                  />
                ))}
            </Select> */}
          {/* )} */}
        {/* </Flex> */}
        <FlatList
          data={dummyData}
          showsVerticalScrollIndicator={false}
          style={{ paddingTop: 10 }}
          numColumns={numCol}
          renderItem={item => {
            console.log('SPORTLIST DATA: >>>> ', activityList)
            return (
              <SportCard
                data={item}
                numCol={numCol}
                type={activityId}
                onPress={() =>
                  navigation.navigate("SportDetail", {
                    data: item,
                    type: activityId,
                    userId: selectedUser,
                  })
                }
              />
            );
          }}
        />
      </Box>
    );
  } else {
    return <Loading />;
  }
};

SportList.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default SportList;
