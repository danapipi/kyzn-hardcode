import React, { useEffect, useState } from 'react';
import {Box, Button, Text, FlatList,HStack , ChevronDownIcon, Flex, Select, CheckIcon} from 'native-base';
import dummyData from './dummyData';
import PropTypes from 'prop-types';
import { TouchableOpacity } from 'react-native';
import SportCard from 'components/ui/SportCard';
import { useDispatch, useSelector } from 'react-redux';
import Actions from "actions";
import Loading from "components/ui/Loading";
import _ from 'lodash';
import { UserApi } from "utils/Api";

const userapi = UserApi.client;

const FacilityList = ({ navigation, route}) => {
  const [numCol, setNumCol] = useState(2);
  const [isLoading, setIsLoading] = useState(false);
  const dataActivity = useSelector((state) => state.PRODUCT.activity)
  const dataActivityById = useSelector((state) => state.PRODUCT.activityById)
  const family = useSelector((state) => state.FAMILY.user.data)
  const dataUniqueFamily = useSelector((state) => state.FAMILY.uniqueFamily)
  const [activityList, setActivityList] = useState([]);
  // const [selectedUser, setSelectedUser] = useState(dataUniqueFamily.data[0].id);
  const [selectedUser, setSelectedUser] = useState("all");
  const dispatch = useDispatch();
  
  const { activityId,  categoryId, categoryChildId, type} = route.params.params;


  const setUser = async (itemValue) => {
    setIsLoading(true)
    setSelectedUser(itemValue);
    let categorylink = `&categoryId=${categoryId}`
    let categorychildlink = `&categoryChildId=${categoryChildId}`
    let activitytypelink = `&activityTypeId=${activityId}`
    let base = `activity?`
    if (categoryId){
      base = base + categorylink
    }
    if (categoryChildId){
      base = base + categorychildlink
    }
    if (activityId){
      base = base + activitytypelink
    }
    if(itemValue == "all"){
      console.log("qweasd", `activity?`)
      let test = await userapi.get(
        base
      )

      console.log("qweasd", test.data.data)
      setActivityList(test.data.data)
      setIsLoading(false)
    } else {
    let test = await userapi.get(
      base+`&userId=${itemValue}`
    )
    console.log("qweasd", `activity?activityTypeId=${activityId}&categoryId=${categoryId}&categoryChildId=${categoryChildId}&userId=${itemValue}`)
    console.log("qweasd", test.data.data)
    setActivityList(test.data.data)
    setIsLoading(false)
    }
  }

  useEffect(() => {
    console.log("selectedUser: ", selectedUser);
    // setUser(selectedUser);
    // type == "category" 
    //   ? dispatch(Actions.activity(activityId, null, null))
    //   : dispatch(Actions.activity(activityId, categoryId, categoryChildId))
      // dispatch(Actions.fetchUserFamily())
      // dispatch(Actions.fetchFamilyUnique())
  }, []);

  if(!isLoading){

    console.log("data",activityList);
    return(
    <Box flex={1} style={{backgroundColor: "#FFF"}}>
      {/* <HStack style={{marginBottom:10}}>
        <TouchableOpacity>
          <Box style={{padding:16,  borderBottomWidth:1, borderBottomColor:"#000", paddingBottom:9}}>
            <Text 
              alignItems="flex-start" 
              fontWeight="bold"
              style={{ fontFamily: "Montserrat-Regular", fontWeight:"500", fontSize:12,  color:"#000"}}
            >
              Courts
            </Text>
          </Box>
        </TouchableOpacity>
        <TouchableOpacity>
          <Box style={{padding:16, paddingBottom:9}}>
            <Text 
              alignItems="flex-start" 
              fontWeight="bold"
              style={{ fontFamily: "Montserrat-Regular", fontWeight:"500", fontSize:12,  color:"#000"}}
            >
              Studio
            </Text>
          </Box>
        </TouchableOpacity>
      </HStack>
      <HStack style={{justifyContent:"space-between"}}>
        <Text 
          alignItems="flex-start" 
          fontWeight="bold"
          style={{ fontFamily: "Montserrat-Regular", fontWeight:"500", fontSize:12, padding:16, color:"#7C858B", paddingBottom:0 }}
        >
            Showing all {data.length} courts
        </Text>
        <TouchableOpacity>
          <Box style={{flexDirection:"row", alignItems:"center"}}>
            <Text 
              alignItems="flex-start" 
              fontWeight="bold"
              style={{ fontFamily: "Montserrat-Regular", fontWeight:"500", fontSize:12, padding:16, color:"#000", paddingBottom:0, marginRight:-10 }}
            >
                Sort by 
            </Text>
            <ChevronDownIcon style={{height:20, marginTop:16, marginRight:10}}/>
          </Box>
        </TouchableOpacity>
      </HStack> */}
      <FlatList
        // data = {activityList}
        data = {dataActivity.activity}
        showsVerticalScrollIndicator = {false}
        style = {{paddingTop:10}}
        numColumns= {numCol}
        renderItem={(item) => {
          console.log("price price data", item);
          return(
            <SportCard
              data={item}
              type={activityId}
              numCol={numCol}
              onPress={() => 
                navigation.navigate('SportDetail', {
                  data: item,
                  type: activityId,
                  userId: selectedUser
                })
              }
            />
          )
        }}
      />
    </Box>
    )
  } else {
    return <Loading/>
  }
};

FacilityList.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default FacilityList;
