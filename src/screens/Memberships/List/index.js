import { capitalize } from "lodash";
import {
  Box,
  Button,
  Column,
  FlatList,
  Image,
  Row,
  Spinner,
  Text,
  useTheme,
  VStack,
} from "native-base";
import React, { useEffect, useState } from "react";
import { dummyImages } from "images";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { useDispatch, useSelector } from "react-redux";
import DatePicker from "components/ui/DatePicker";
import Selectors from "selectors";
import { useIsFocused } from "@react-navigation/native";
import Actions from "actions";
import { image } from "images";

const MembershipList = ({ navigation }) => {
  const dispatch = useDispatch();
  const isFocused = useIsFocused();
  useEffect(() => {
    // dispatch(Actions.fetchMembershipCategories());
    // dispatch(Actions.fetchMembershipPackages());
    // }, [dispatch]);
  }, [isFocused]);

  const membershipCategories = useSelector(Selectors.getMembershipCategories);
  const membershipPackage = useSelector(Selectors.getMembershipPackages);

  const isLoading = useSelector(Selectors.getIsMembershipCategoriesLoading);

  const [membershipCategory, setMembershipCategory] = useState("personal");
  const { colors } = useTheme();

  const renderToggleButton = category => {
    const isSelected = category === membershipCategory;
    const color = isSelected ? "black" : "transparent";
    const textColor = isSelected ? "white" : "black";
    return (
      <Button
        borderRadius={25}
        marginRight={5}
        backgroundColor={color}
        _text={{
          color: textColor,
        }}
        _pressed={{ backgroundColor: "black:alpha.20" }}
        onPress={() => setMembershipCategory(category)}>
        {capitalize(category)}
      </Button>
    );
  };

  if (isLoading) {
    return (
      <Box
        flex={1}
        background={"#fff"}
        justifyContent={"center"}
        alignItems={"center"}>
        <Spinner accessibilityLabel="Loading memberships" size={"lg"} />
      </Box>
    );
  }

  const renderPersonal = () => {
    return (
      <FlatList
        margin={3}
        flex={1}
        data={membershipCategories}
        renderItem={({ item: { id, description, name, image_url } }) => (
          <Column
            marginBottom={8}
            justifyContent="center"
            style={{ backgroundColor: "#F8F9FC" }}>
            <Row marginBottom={5}>
              {image_url ? (
                <Image
                  source={{ uri: image_url }}
                  style={{ height: 85, width: "100%" }}
                  resizeMode={"cover"}
                />
              ) : (
                <Image
                  source={image.img_memberhip_lscape}
                  style={{ height: 85, width: "100%" }}
                  resizeMode={"cover"}
                />
              )}
              <VStack
                marginY={1}
                paddingX={3}
                style={{ position: "absolute", top: 25 }}>
                <Text
                  marginLeft={1}
                  flexWrap="wrap"
                  style={{
                    fontWeight: "700",
                    color: "#FFF",
                    fontFamily: "Montserrat-Regular",
                  }}
                  lineHeight="xs">
                  {name}
                </Text>
              </VStack>
            </Row>
            {description.split("\n").map(benefit => (
              <Row alignItems="center" marginY={1} paddingX={3}>
                <Icon name="check" color={colors.lightGreen} size={18} />
                <Text
                  marginLeft={1}
                  fontSize="sm"
                  flexWrap="wrap"
                  lineHeight="xs">
                  {benefit}
                </Text>
              </Row>
            ))}
            <Button
              marginTop={5}
              backgroundColor="black"
              borderRadius={0}
              marginX={3}
              marginBottom={3}
              _pressed={{ backgroundColor: "black:alpha.20" }}
              onPress={() =>
                navigation.navigate("MembershipPackagesList", {
                  id,
                  type: membershipCategory,
                })
              }>
              View Details
            </Button>
          </Column>
        )}
      />
    );
  };

  const renderGroup = () => {
    console.log("asss", membershipPackage);
    return (
      <FlatList
        data={membershipPackage}
        flex={1}
        margin={3}
        renderItem={({ item }) => {
          console.log("item item item", item);
          return (
            <Column
              marginBottom={8}
              justifyContent="center"
              style={{ backgroundColor: "#F8F9FC" }}>
              <Row marginBottom={5}>
                {item.image_url ? (
                  <Image
                    source={{ uri: item.image_url }}
                    style={{ height: 85, width: "100%" }}
                    resizeMode={"cover"}
                  />
                ) : (
                  <Image
                    source={image.img_memberhip_lscape}
                    style={{ height: 85, width: "100%" }}
                    resizeMode={"cover"}
                  />
                )}
                <VStack
                  marginY={1}
                  paddingX={3}
                  style={{ position: "absolute", top: 25 }}>
                  <Text
                    marginLeft={1}
                    flexWrap="wrap"
                    style={{
                      fontWeight: "700",
                      color: "#FFF",
                      fontFamily: "Montserrat-Regular",
                    }}
                    lineHeight="xs">
                    {item.name}
                  </Text>
                </VStack>
              </Row>
              {item.description.split("\n").map(benefit => (
                <Row alignItems="center" marginY={1} paddingX={3}>
                  <Icon name="check" color={colors.lightGreen} size={18} />
                  <Text
                    marginLeft={1}
                    fontSize="sm"
                    flexWrap="wrap"
                    lineHeight="xs">
                    {benefit}
                  </Text>
                </Row>
              ))}
              <Button
                marginTop={5}
                backgroundColor="black"
                borderRadius={0}
                marginX={3}
                marginBottom={3}
                _pressed={{ backgroundColor: "black:alpha.20" }}
                onPress={() =>
                  navigation.navigate("MembershipPackagesList", {
                    id: item.id,
                    type: membershipCategory,
                  })
                }>
                View Details
              </Button>
            </Column>
          );
        }}
      />
    );
  };

  return (
    <Box safeArea backgroundColor="white" flex={1}>
      <Row marginX={3}>
        {renderToggleButton("personal")}
        {/* {renderToggleButton("family")} */}
      </Row>
      {membershipCategory === "personal" ? renderPersonal() : renderGroup()}
    </Box>
  );
};

export default MembershipList;
