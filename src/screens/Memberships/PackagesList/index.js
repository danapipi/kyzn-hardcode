import { capitalize } from "lodash";
import {
  Box,
  Button,
  Column,
  FlatList,
  Image,
  Row,
  Spinner,
  Text,
  useTheme,
  VStack,
} from "native-base";
import React, { useEffect, useState } from "react";
import { dummyImages } from "images";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { useDispatch, useSelector } from "react-redux";
import Selectors from "selectors";
import Actions from "actions";
import { image } from "images";
import _ from "lodash";
import Commons from "../../../utils/common"

const MembershipPackagesList = ({ navigation, route: { params } }) => {
  const dispatch = useDispatch();
  useEffect(() => {
    // dispatch(Actions.fetchMembershipCategories());
  }, [dispatch]);

  const membershipCategories = useSelector(Selectors.getMembershipCategories);

  const membershipPackage = useSelector(Selectors.getMembershipPackages);
  const isLoading = useSelector(Selectors.getIsMembershipCategoriesLoading);

  const { colors } = useTheme();

  const { id, type } = params;

  const dataTemp = type == "family" ? membershipPackage : membershipCategories;
  const membership = dataTemp.find(({ id: categoryId }) => categoryId === id);

  useEffect(() => {
    if (membership?.name) {
      navigation.setOptions({
        title: `${capitalize(membership.name)} Membership`,
      });
    }
  }, [membership]);

  if (isLoading) {
    return (
      <Box
        flex={1}
        background={"#fff"}
        justifyContent={"center"}
        alignItems={"center"}>
        <Spinner accessibilityLabel="Loading memberships" size={"lg"} />
      </Box>
    );
  }

  let dataMembership =
    type == "family"
      ? membership?.membership_packages
      : membership?.memberships;

  const renderPersonal = () => {
    return (
      <FlatList
        margin={3}
        flex={1}
        data={dataMembership || []}
        renderItem={({
          item: {
            id: membershipId,
            perks_description = "",
            name,
            price,
            image_url,
          },
        }) => (
          <Column
            marginBottom={8}
            justifyContent="center"
            style={{ backgroundColor: "#F8F9FC" }}>
            <Row marginBottom={5}>
              {image_url ? (
                <Image
                  source={{ uri: image_url }}
                  style={{ height: 85, width: "100%" }}
                  resizeMode={"cover"}
                />
              ) : (
                <Image
                  source={image.img_memberhip_lscape}
                  style={{ height: 85 }}
                  resizeMode={"cover"}
                />
              )}
              <VStack
                marginY={1}
                paddingX={3}
                style={{ position: "absolute", top: 10 }}>
                <Text
                  style={{
                    fontWeight: "700",
                    color: "#FFF",
                    fontFamily: "Montserrat-Regular",
                    fontSize: 12,
                  }}>
                  {name}
                </Text>
                <Text
                  style={{
                    fontWeight: "bold",
                    color: "#FFF",
                    fontFamily: "Montserrat-Regular",
                    fontSize: 14,
                  }}>
                  {`${Commons.convertToRupiah(price)}`}
                </Text>
              </VStack>
            </Row>
            {perks_description.split("\n").map(benefit => (
              <Row alignItems="center" marginY={1} paddingX={3}>
                <Icon name="check" color={colors.lightGreen} size={18} />
                <Text
                  marginLeft={1}
                  fontSize="sm"
                  flexWrap="wrap"
                  lineHeight="xs">
                  {benefit}
                </Text>
              </Row>
            ))}
            <Button
              marginTop={5}
              backgroundColor="black"
              borderRadius={0}
              marginX={3}
              marginBottom={3}
              _pressed={{ backgroundColor: "black:alpha.20" }}
              onPress={() =>
                navigation.navigate("MembershipView", {
                  categoryId: id,
                  membershipId,
                })
              }>
              View Details
            </Button>
          </Column>
        )}
      />
    );
  };

  const renderGroup = () => {
    return (
      <FlatList
        margin={3}
        flex={1}
        data={dataMembership || []}
        renderItem={({ item }) => (
          <Column
            marginBottom={8}
            justifyContent="center"
            style={{ backgroundColor: "#F8F9FC" }}>
            <Row marginBottom={5}>
              {item.image_url ? (
                <Image
                  source={{ uri: item.image_url }}
                  style={{ height: 85, width: "100%" }}
                  resizeMode={"cover"}
                />
              ) : (
                <Image
                  source={image.img_memberhip_lscape}
                  style={{ height: 85 }}
                  resizeMode={"cover"}
                />
              )}
              <VStack
                marginY={1}
                paddingX={3}
                style={{ position: "absolute", top: 10 }}>
                <Text
                  style={{
                    fontWeight: "700",
                    color: "#FFF",
                    fontFamily: "Montserrat-Regular",
                    fontSize: 12,
                  }}>
                  {item.name} Package
                </Text>
                <Text
                  style={{
                    fontWeight: "bold",
                    color: "#FFF",
                    fontFamily: "Montserrat-Regular",
                    fontSize: 14,
                  }}>
                  {`${Commons.convertToRupiah(item.price)}`}
                </Text>
              </VStack>
            </Row>
            {!_.isUndefined(item.perks_description) &&
              item.perks_description.split("\n").map(benefit => (
                <Row alignItems="center" marginY={1} paddingX={3}>
                  <Icon name="check" color={colors.lightGreen} size={18} />
                  <Text
                    marginLeft={1}
                    fontSize="sm"
                    flexWrap="wrap"
                    lineHeight="xs">
                    {benefit}
                  </Text>
                </Row>
              ))}
            <Button
              marginTop={5}
              backgroundColor="black"
              borderRadius={0}
              marginX={3}
              marginBottom={3}
              _pressed={{ backgroundColor: "black:alpha.20" }}
              onPress={() =>
                navigation.navigate("MembershipViewGroup", {
                  data: item,
                })
              }>
              View Details
            </Button>
          </Column>
        )}
      />
    );
  };

  return (
    <Box safeArea backgroundColor="white" flex={1}>
      {type == "family" ? renderGroup() : renderPersonal()}
    </Box>
  );
};

export default MembershipPackagesList;
