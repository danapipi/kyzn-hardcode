import { capitalize, toUpper } from "lodash";
import {
  Box,
  Button,
  Column,
  FlatList,
  Image,
  Row,
  Actionsheet,
  Spinner,
  Text,
  useTheme,
  VStack
} from "native-base";
import {Platform} from 'react-native';
import React, { useEffect, useRef, useState } from "react";
import { image, dummyImages } from "images";
import { getScreenWidth } from "utils/size";
import { Alert, StyleSheet } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import Selectors from "selectors";
import Actions from "actions";
import DatePicker from "components/ui/DatePicker";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import moment from "moment";
import Commons from "utils/common";
import _ from 'lodash';

import { Select, CheckIcon, Flex } from 'native-base';
import { UserApi } from "../../../utils/Api";
const styles = StyleSheet.create({
  button: {
    marginBottom: 10,
  },
});

const user = UserApi.client

const GroupMemberView = ({ navigation, route: { params } }) => {
  const dispatch = useDispatch();

  const membershipData = useSelector((Selectors.getMembershipById));
  const profile = useSelector(Selectors.getProfile)
  const [insufficientBalanceOpen, setInsufficientBalanceOpen] = useState(false)
  const [differenceBalance, setDifferenceBalance] = useState(0)

  const isLoading = membershipData.isLoading
  const [selectedDate, setSelectedDate] = useState(moment().format("YYYY-MM-DD"));
  const [isOpen, setIsOpen] = useState(false);

  const balance = profile.balance
  const countRef = useRef(0);
  const { data } = params;
  const { colors } = useTheme();

  console.log("membershipppp data ", data, membershipData);

  const closeInsufficientBalanceAS = () => {
    setInsufficientBalanceOpen(false)
  }

  const goToTopUpDetails = () => {
    // console.log("goToTopUpDetails pressed");
    closeInsufficientBalanceAS()
    navigation.navigate("TopUpDetails", {userId: profile.id, amount: differenceBalance, isFromPayment: true})
  }


  useEffect(() => {
    setDifferenceBalance(data.price - balance)
    console.log("[membership] profile: ", profile);
    console.log("[membership] params: ", params);

    // console.log("[membership] price: ", membership.price);
  }, []);

  useEffect(() => {
    if (data?.name) {
      // let shortenName = _.truncate(`${data.name} Membership`, {
      //   'length': 28,
      //   'separator': ' '
      // });
      let shortenName = `Buy Group Membership`
      navigation.setOptions({
        title: `${shortenName}`,
        headerTitleStyle: {
          fontWeight: "bold",
          marginLeft: Platform?.OS === 'android' ? -15 : -120,
        },
      });
    }
    if(countRef.current <= 1){
      dispatch(Actions.fetchMembershipById(data.id));

      countRef.current++;
    }
    
  });

  const onClose = () => {
    setIsOpen(false);
  };

  // const [isShowingModal, setIsShowingModal] = useState(false);

  if (isLoading) {
    return <Spinner accessibilityLabel="Loading posts" />;
  }
  console.log("data data data", membershipData, data, profile);

  const renderCalendar = () => {
    return(
      <DatePicker
      value={moment(new Date(selectedDate))?.isValid() ? selectedDate : moment().format('YYYY-MM-DD')}
        label="Start datexx"
        bgColorLabel={"white"}
        onSelected={(val) => convertDOB(val)}
        maxDateProps={false}
        minDateProps={true}
      />
    )
  }

  const convertDOB = (date) => {
    let convertDate = moment(date).format("YYYY-MM-DD");
    setSelectedDate(convertDate);
  };

  const onDone = () => {
    navigation.navigate('BottomTabs', {refresh: true })
    setIsOpen(false);
  }

  const handlePurchase = () => {

    if(data.price > balance) { // insufficient balance
      setInsufficientBalanceOpen(true)
    } else {
      let payload = {
        "userId": profile.id,
        // "membershipPackageId": data.membership_package_category_id,
        "membershipPackageId": membershipData.membershipData.id,
        "paymentMethod": "kyzn_money",
        "paymentStatus": "success",
        "startDate": selectedDate
      }

      user.post(`/membership-package-payment`, payload)
      .then((res) => {
        console.log("response data", res);
        setIsOpen(true);
      })
      .catch(err => {
        console.log(" response data errr", err.response);
        let dataError = err.response
        let errorCode = _.get(dataError, "status", " ");
        let errorMassage = _.get(dataError, "data.response", " ");

        console.log("data data error", dataError.data.message);
        if(dataError.data.message == "Insufficient balance"){
          navigation.navigate("TopUp")
        } else {
          Alert.alert("Error", `${errorMassage}`, [
            { text: "OK", onPress: () => console.log("OK Pressed") },
          ]);
        }
    })
    }
  }

  const onViewDetail = () => {
    setIsOpen(false);
    navigation.navigate("UserMembership");
  }

  const renderPersonal = () => {
    const dataMembershipPackages = membershipData.membershipData.memberships
    return(
      <FlatList
        flex={1}
        data={dataMembershipPackages}
        renderItem={({
          item: {
            id: membershipId,
            perks_description = "",
            name,
            price,
            image_url,
          },
        }) => (
          <Column marginBottom={8} justifyContent="center" margin={3} style={{backgroundColor:"#F8F9FC"}} paddingBottom={3}>
            <Row marginBottom={5}>
              { image_url 
                ? <Image 
                    source={image_url} 
                    style={{height:85}}
                    resizeMode={"cover"}
                  /> 
                : <Image 
                    source={image.img_memberhip_lscape} 
                    style={{height:85}}
                    resizeMode={"cover"}
                  /> }
              <VStack marginY={1} paddingX={3} style={{position:"absolute", top:25}}>
                <Text
                  marginLeft={1}
                  flexWrap="wrap"
                  style={{ fontWeight: "700", color:"#FFF", 
                  fontFamily: "Montserrat-Regular", }}
                  lineHeight="xs">
                  {name}
                </Text>
                <Text
                  marginLeft={1}
                  flexWrap="wrap"
                  style={{ fontWeight: "bold" ,color:"#FFF", 
                  fontFamily: "Montserrat-Regular", }}
                  lineHeight="xs">
                  Worth {`${Commons.convertToRupiah(price)}`}
                </Text>
              </VStack>
            </Row>

            {/* <Row alignItems="center" marginY={1} paddingX={3}>
              <Text
                marginLeft={1}
                flexWrap="wrap"
                style={{ fontWeight: "bold" }}
                lineHeight="xs">
                {name}
              </Text>
            </Row>
            <Row alignItems="center" marginY={1} paddingX={3}>
              <Text
                marginLeft={1}
                flexWrap="wrap"
                style={{ fontWeight: "bold" }}
                lineHeight="xs">
                Worth {`${price.toLocaleString()}`} Credits
              </Text>
            </Row> */}
            {perks_description.split("\n").map(benefit => (
              <Row alignItems="center" marginY={1} paddingX={3}>
                <Icon name="check" color={colors.lightGreen} size={18} />
                <Text
                  marginLeft={1}
                  fontSize="sm"
                  flexWrap="wrap"
                  lineHeight="xs">
                  {benefit}
                </Text>
              </Row>
            ))}
            {/* <Button
              marginTop={5}
              backgroundColor="black"
              borderRadius={0}
              _pressed={{ backgroundColor: "black:alpha.20" }}
              disabled={true}
              onPress={() =>
                navigation.navigate("MembershipView", {
                  categoryId: id,
                  membershipId,
                })
              }>
              See More
            </Button> */}
          </Column>
        )}
      />
    )
  }

  return (
    <Box flex={1} safeArea backgroundColor="white">
      <Box style={{flex:2}}>
        {renderPersonal()}
      </Box>
      <Box style={{flex:1}}>
        {renderCalendar()}
      </Box>
      <Row
        position="absolute"
        bottom={0}
        width={getScreenWidth()}
        paddingBottom={10}
        paddingX={5}
        paddingTop={2}
        justifyContent="space-between"
        borderTopWidth={2}
        borderTopColor="lightGray:alpha.30">
        <Column>
          <Text fontSize="xs" fontWeight={300} color="gray">
            {toUpper(params.membershipType)}
          </Text>
          <Text fontWeight={300} fontSize="lg">
            {`${Commons.convertToRupiah(data.price)}`}
          </Text>
        </Column>
        <Button
          borderRadius={0}
          backgroundColor="button.blue"
          _pressed={{
            backgroundColor: "button.blue:alpha.20",
          }}
          _text={{
            paddingX: 5,
          }}
          onPress={() => handlePurchase()
          }>
          Purchase
        </Button>
      </Row>

      <Actionsheet isOpen={insufficientBalanceOpen} onClose={closeInsufficientBalanceAS}>
        <Actionsheet.Content h={365}>
          <Image
            source={image.bg_noPremium_actionsheet}
            style={{ width: 150, height: 150, resizeMode: "contain", position: "absolute", left: 0, top: -22, transform: [{rotate: "0deg"}] }}
            alt="image"
          />
          <Image
            source={image.not_premium_ic}
            style={{ width: 170, height:100 }}
            resizeMode={"contain"}
            alt="image"
          />
          <Box style={{width:270, alignItems:"center", marginTop:8}}>
            <Text
              style={{
                fontSize: 14,
                color: "#000",
                fontWeight: "bold",
                fontFamily: "Montserrat-Regular",
              }}
              alignItems="flex-start"
            >
              {`Sorry, You have insufficient balance`}
            </Text>
            <Text
              style={{
                fontSize: 12,
                color: "#000",
                fontWeight: "500",
                fontFamily: "Montserrat-Regular",
                textAlign:"center",
                marginTop:10
              }}
              alignItems="flex-start"
            >
              {`Please top up your balance. Your current balance is `}  
              <Text style={{
                fontWeight: "bold"
              }}>
                  {`${Commons.convertToRupiah(balance)}`}
              </Text>
            </Text>
          </Box>
            <Button
              w={"90%"}
              bgColor={"#212121"}
              onPress={goToTopUpDetails}
              style={{marginTop:50, borderRadius:0}}
            >
              <Text style={{
                color:"#fff",
                fontFamily: "Montserrat-Regular",
              }}>
                  Top Up now
              </Text>
            </Button>
            <Button
              w={"90%"}
              bgColor={"transparent"}
              onPress={closeInsufficientBalanceAS}
              color={"#000"}
              style={{marginTop:10, borderRadius:0}}
            >
              <Text style={{
                fontWeight: "bold",
                color:"#000",
                fontFamily: "Montserrat-Regular",
              }}>
                  Close
              </Text>
            </Button>
        </Actionsheet.Content>
      </Actionsheet>
      <Actionsheet isOpen={isOpen} onClose={onClose}>
        <Actionsheet.Content h={365}>
          <Image
            source={image.bg_actionsSheet}
            style={{ width: 300, height: 300, position: "absolute", right: 0 }}
            alt="image"
          />
          <Image
            source={image.succes_ic}
            style={{ width: 170, height:100 }}
            resizeMode={"contain"}
            alt="image"
          />
          <Box style={{width:300, alignItems:"center", marginTop:8}}>
            <Text
              style={{
                fontSize: 14,
                color: "#000",
                fontWeight: "bold",
                fontFamily: "Montserrat-Regular",
              }}
              alignItems="flex-start"
            >
              {data.name} was Purchased
            </Text>
            <Text
              style={{
                fontSize: 12,
                color: "#000",
                fontWeight: "500",
                fontFamily: "Montserrat-Regular",
                textAlign:"center",
                marginTop:10
              }}
              alignItems="flex-start"
            >
              Yay! your purchase was successful
            </Text>
          </Box>
            {/* <Button
              w={"90%"}
              bgColor={"#212121"}
              onPress={onViewDetail}
              style={{marginTop:50, borderRadius:0}}
            >
              <Text style={{
                color:"#fff",
                fontFamily: "Montserrat-Regular",
              }}>
                  View Details
              </Text>
            </Button> */}
            <Button
              w={"90%"}
              bgColor={"transparent"}
              onPress={onDone}
              color={"#000"}
              style={{marginTop:10, borderRadius:0}}
            >
              <Text style={{
                fontWeight: "bold",
                color:"#000",
                fontFamily: "Montserrat-Regular",
              }}>
                  Done
              </Text>
            </Button>
        </Actionsheet.Content>
      </Actionsheet>
    </Box>
  );
};

export default GroupMemberView;
