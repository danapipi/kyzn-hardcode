import { capitalize, toUpper } from "lodash";
import {
  Box,
  Button,
  Column,
  Image,
  Modal,
  Row,
  ScrollView,
  Spinner,
  Text,
  useTheme,
  VStack,
} from "native-base";
import React, { useEffect, useState } from "react";
import { image, dummyImages } from "images";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { getScreenWidth } from "utils/size";
import { StyleSheet, Platform, Alert } from "react-native";
import { useIsFocused } from "@react-navigation/native";
import { useDispatch, useSelector } from "react-redux";
import DatePicker from "components/ui/DatePicker";
import Selectors from "selectors";
import moment from "moment";
import Actions from "actions";
import Commons from "../../../utils/common";

import { Select, CheckIcon, Flex } from "native-base";
const styles = StyleSheet.create({
  button: {
    marginBottom: 10,
  },
});

const MembershipView = ({ navigation, route: { params } }) => {
  console.log("MEMBERSHIPVIEW PROPS: ", params);
  const dispatch = useDispatch();
  const isFocused = useIsFocused();
  useEffect(() => {
    // dispatch(Actions.fetchMembershipCategories());
  }, [dispatch]);

  useEffect(() => {
    // dispatch(Actions.fetchFamilyUnique());
  }, [isFocused]);

  const { categoryId, membershipId } = params;

  const membershipCategories = useSelector(Selectors.getMembershipCategories);
  const isLoading = useSelector(Selectors.getIsMembershipCategoriesLoading);
  const dataUniqueFamily = useSelector(state => state.FAMILY.uniqueFamily);
  const [selectedDate, setSelectedDate] = useState(
    moment().format("YYYY-MM-DD"),
  );

  const [selectedUser, setSelectedUser] = useState(dataUniqueFamily.data[0].id);
  const family = useSelector(state => state.FAMILY.user.data);

  const membershipCategory = membershipCategories.find(
    ({ id }) => id === categoryId,
  );

  const membership = membershipCategory.memberships.find(
    ({ id }) => id === membershipId,
  );
  console.log("membership wucauuuuuuu ", membership);

  useEffect(() => {
    if (membership?.name) {
      navigation.setOptions({
        // title: `${capitalize(membership.name)} Membership`,
        title: "Buy Membership",
        headerTitleStyle: {
          fontWeight: "bold",
          marginLeft: Platform?.OS === "android" ? -15 : -150,
        },
      });
    }
  }, [membership]);

  // const [isShowingModal, setIsShowingModal] = useState(false);

  const { colors } = useTheme();

  if (isLoading) {
    return (
      <Box
        flex={1}
        background={"#fff"}
        justifyContent={"center"}
        alignItems={"center"}>
        <Spinner accessibilityLabel="Loading posts" size={"lg"} />
      </Box>
    );
  }

  const convertDOB = date => {
    let convertDate = moment(date).format("YYYY-MM-DD");
    setSelectedDate(convertDate);
  };

  const renderCalendar = () => {
    return (
      <DatePicker
        value={
          moment(new Date(selectedDate))?.isValid()
            ? selectedDate
            : moment().format("YYYY-MM-DD")
        }
        label="Start date"
        bgColorLabel={"white"}
        onSelected={val => convertDOB(val)}
        maxDateProps={false}
        minDateProps={true}
      />
    );
  };
  const { description = "" } = membershipCategory;
  const {
    perks_description = "",
    price = 0,
    name = "",
    image_url = "",
  } = membership;
  console.log("IMG URLLLLL: ", image_url);

  return (
    <Box flex={1} safeArea backgroundColor="white">
      <ScrollView marginX={3} style={{ flex: 4 }}>
        <Column
          marginBottom={8}
          justifyContent="center"
          margin={3}
          style={{ backgroundColor: "#F8F9FC" }}
          paddingBottom={3}>
          <Row marginBottom={5}>
            {image_url ? (
              <Image
                source={{ uri: image_url }}
                style={{ height: 85, width: "100%" }}
                resizeMode={"cover"}
              />
            ) : (
              <Image
                source={image.img_memberhip_lscape}
                style={{ height: 85, width: "100%" }}
                resizeMode={"cover"}
              />
            )}
            <VStack
              marginY={1}
              paddingX={3}
              style={{ position: "absolute", top: 25 }}>
              <Text
                marginLeft={1}
                flexWrap="wrap"
                style={{
                  fontWeight: "700",
                  color: "#FFF",
                  fontFamily: "Montserrat-Regular",
                }}
                lineHeight="xs">
                {name} Package
              </Text>
              <Text
                marginLeft={1}
                flexWrap="wrap"
                style={{
                  fontWeight: "bold",
                  color: "#FFF",
                  fontFamily: "Montserrat-Regular",
                }}
                lineHeight="xs">
                Worth {`${Commons.convertToRupiah(price)}`}
              </Text>
            </VStack>
          </Row>

          {/* <Row alignItems="center" marginY={1} paddingX={2}>
            <Text
              marginLeft={1}
              flexWrap="wrap"
              style={{ fontWeight: "bold" }}
              lineHeight="xs">
              {name}
            </Text>
          </Row>
          <Row alignItems="center" marginY={1} paddingX={2}>
            <Text
              marginLeft={1}
              flexWrap="wrap"
              style={{ fontWeight: "bold" }}
              lineHeight="xs">
              {`${price.toLocaleString()}`} Credits
            </Text>
          </Row> */}
          {description.split("\n").map(benefit => (
            <Row alignItems="center" marginY={4} paddingX={3}>
              <Icon name="check" color={colors.lightGreen} size={18} />
              <Text
                marginLeft={1}
                fontSize="sm"
                flexWrap="wrap"
                lineHeight="xs">
                {benefit}
              </Text>
            </Row>
          ))}
        </Column>
        <Column>
          <Text
            fontWeight="bold"
            marginLeft={1}
            paddingX={2}
            style={{
              color: "#000",
              fontFamily: "Montserrat-Regular",
              fontWeight: "700",
              fontSize: 12,
            }}>
            Additional Benefits
          </Text>
          {perks_description.split("\n").map(benefit => (
            <Row alignItems="center" marginY={1} paddingX={3}>
              <Icon name="check" color={colors.lightGreen} size={18} />
              <Text
                marginLeft={1}
                fontSize="sm"
                flexWrap="wrap"
                lineHeight="xs">
                {benefit}
              </Text>
            </Row>
          ))}
        </Column>
      </ScrollView>

      <Box style={{ flex: 1 }}>{renderCalendar()}</Box>
      <Row
        position="absolute"
        bottom={0}
        width={getScreenWidth()}
        paddingBottom={10}
        paddingX={5}
        paddingTop={2}
        justifyContent="space-between"
        borderTopWidth={2}
        borderTopColor="lightGray:alpha.30">
        <Column>
          <Flex
            // direction="row"
            justify="flex-end"
            style={{ marginTop: 0, marginBottom: 0, flexDirection: "row" }}>
            {membership && membership.members && (
              // {dataUniqueFamily.data &&
              <Box>
                {/* <Text>Buying for:</Text>
                <Select
                  selectedValue={selectedUser}
                  width={150}
                  accessibilityLabel="Choose User"
                  placeholder="Choose User"
                  _selectedItem={{
                    endIcon: <CheckIcon size="5" />,
                  }}
                  mt={1}
                  onValueChange={itemValue => setSelectedUser(itemValue)}> */}
                  {/* {dataUniqueFamily.data && dataUniqueFamily.data.map((family_member) =>  */}
                  {/* {membership.members &&
                    membership.members.map(family_member => (
                      <Select.Item
                        label={family_member.name}
                        value={family_member.id}
                      />
                    ))} */}
                {/* </Select> */}
              </Box>
            )}
          </Flex>
          <Text fontSize="xs" fontWeight={300} color="gray">
            {toUpper(params.membershipType)}
          </Text>
          <Text fontWeight={300} fontSize="lg">
            {`${Commons.convertToRupiah(price)}`}
          </Text>
        </Column>

        <Button
          disabled={!selectedDate || !selectedUser}
          borderRadius={0}
          backgroundColor="button.blue"
          _pressed={{
            backgroundColor: "button.blue:alpha.20",
          }}
          _text={{
            paddingX: 5,
          }}
          onPress={() => {
            if (membership.members.length > 0) {
              navigation.navigate("MembershipPayment", {
                membershipId,
                categoryId,
                selectedUser,
                selectedDate,
              });
            } else {
              Alert.alert("No users allowed to buy this membership");
            }
          }}>
          Purchase
        </Button>
      </Row>
    </Box>
  );
};

export default MembershipView;
