import React, { useState, useEffect } from "react";
import { Dimensions, ActivityIndicator, Text, TouchableOpacity, Alert } from "react-native";
import {
  Box,
  Button,
  Column,
  FlatList,
  ScrollView,
  HStack,
  Image,
  ZStack,
  Modal,
  VStack,
  Actionsheet,
  WarningIcon
} from "native-base";
import _, {isString, toInteger} from 'lodash';
import { useIsFocused } from '@react-navigation/native';
import PropTypes from "prop-types";
import { capitalize, toUpper, isEmpty } from "lodash";
import { image, dummyImages } from "images";

import { StyleSheet } from "react-native";
import { backgroundColor } from "styled-system";
import { useDispatch, useSelector } from "react-redux";
import Selectors from "selectors";
import Actions from "actions";
import Commons from "utils/common";
import { moderateScale } from "react-native-size-matters";
import moment from 'moment';


const styles = StyleSheet.create({
  button: {
    marginBottom: 10,
  },
});

const MembershipPayment = ({ navigation, route: { params } }) => {
  const [membership_status, setStatusMember] = useState(1);
  const [differenceBalance, setDifferenceBalance] = useState(0)
  const [insufficientBalanceOpen, setInsufficientBalanceOpen] = useState(false)

  const [couponApplied, setCouponApplied] = useState("")
  const [isCouponValid, setIsCouponValid] = useState(null)
  const [discountFromCoupon, setDiscountFromCoupon] = useState(0)
  const [discountType, setDiscountType] = useState("amount")
  const [couponId, setCouponId] = useState(null) // only for synchronizing on clearing coupon
  const [newPrice, setNewPrice] = useState(0)

  const isFocused = useIsFocused()

  const profile = useSelector(Selectors.getProfile);
  let coupons = _.get(params, "coupons", [])
  const balance = profile.balance
  const isLoadingProfile = useSelector(Selectors.getIsLoadingProfile);
  const dispatch = useDispatch();


  let nameProfile = _.get(profile, "name", "-");
  let email = _.get(profile, "email", "-");
  let phoneProfile = _.get(profile, "phone_number", "-");
  let emergencyPhone = _.get(profile, "emergency_number", "-");
  let gender = _.get(profile, "gender", "-");
  let birthdate = _.get(profile, "birthdate", "-");
  let birthdateFormat =
    birthdate !== "-" || birthdate !== null
      ? moment(birthdate).format("YYYY-MM-DD")
      : "-";
  let nationality = _.get(profile, "nationality", "-");
  let isParent = _.get(profile, "phone_number", "");
  let postal_code = _.get(profile, "postal_code", "-");
  let province = _.get(profile, "province", "-");
  let district = _.get(profile, "district", "-");
  let city = _.get(profile, "city", "-");
  let address = _.get(profile, "address", "-");
  let username = _.get(profile, "username", "-");

  const [nameValue, setNameValue] = useState(nameProfile);
  const [emailValue, setEmailValue] = useState(email);
  const [phoneValue, setPhoneValue] = useState(phoneProfile);
  const [emergencyPhoneValue, setEmergencyPhoneValue] =
    useState(emergencyPhone);
  const [genderValue, setGenderValue] = useState(gender);
  const [birthdateValue, setBirthdateValue] = useState(birthdateFormat);
  const [nationalityValue, setNationalityValue] = useState(nationality);
  const [photoValue, setPhotoValue] = useState(null);
  const [postalCodeValue, setPostalCodeValue] = useState(postal_code);
  const [provinceValue, setProvinceValue] = useState(province);
  const [districtValue, setDistrictValue] = useState(district);
  const [cityValue, setCityValue] = useState(city);
  const [addressValue, setAddressValue] = useState(address);
  const [usernameValue, setUsernameValue] = useState(username);

  console.log(isEmpty(profile));
  useEffect(() => {
    // dispatch(Actions.fetchMembershipCategories());
    if (isEmpty(profile) && !isLoadingProfile) {
      // dispatch(Actions.profile());
    }
  }, [dispatch]);

  // useEffect(() => {
  //   dispatch(Actions.topupHistory());
  //   dispatch(Actions.profile());
  // }, [isFocused]);

  const { id: userId } = profile;
  const { categoryId, membershipId, selectedUser, selectedDate } = params;

  const membershipCategories = useSelector(Selectors.getMembershipCategories);
  const isLoading =
    useSelector(Selectors.getIsMembershipCategoriesLoading) && isLoadingProfile;
  const [isShowingModal, setIsShowingModal] = useState(false);

  const closeInsufficientBalanceAS = () => {
    setInsufficientBalanceOpen(false)
  }

  const goToTopUpDetails = () => {
    // console.log("goToTopUpDetails pressed");
    closeInsufficientBalanceAS()
    navigation.navigate("TopUpDetails", {userId: userId, amount: differenceBalance, isFromPayment: true})
  }

  const membershipCategory = membershipCategories.find(
    ({ id }) => id === categoryId,
  );

  const membership = membershipCategory.memberships.find(
    ({ id }) => id === membershipId,
  );

  const { description = "" } = membershipCategory;
  const {
    perks_description = "",
    price = 0,
    name = "",
    image_url = "",
  } = membership;

  // this one can automatically refresh and mutate whatever data on this screen
  const onRefresh = (data) => {
    console.log("onRefresh: ", data);
    setCouponApplied(data.couponApplied)
    setIsCouponValid(data.isCouponValid)
    setDiscountFromCoupon(data.discountFromCoupon)
    setDiscountType(data.discountType)
    setCouponId(data.couponId)

    if(data.isCouponValid == true) {
      if(isString(price) == true) {
        price = toInteger(price) * 1000
        console.log("price new: ", price);
      }
      // setNewPrice(data.discountFromCoupon)

      if(data.discountType === "amount") {
        setNewPrice((price) - data.discountFromCoupon)
      } else if(data.discountType === "percentage") {
        setNewPrice((price) - ((price) * (data.discountFromCoupon / 100)))
      }
    }
  }

  useEffect(() => {
    setDifferenceBalance(price - balance)
    console.log("[membership] profile: ", profile);
    console.log("[membership] params: ", params);

    // console.log("[membership] price: ", membership.price);
  }, []);

  const buyMembership = () => {
    if(price > balance) { // insufficient balance
      setInsufficientBalanceOpen(true)
    } else {

      let profile = {
        id: 2,
        name: nameProfile,
        username: null,
        phone_number: phoneProfile,
        xp: 0,
        coin: 0,
        balance: 0,
        status: "Registered",
        birthdate: "2001-11-30T16:00:00.000Z",
        gender: genderValue,
        address: addressValue,
        photo: "https://api.kyzn.life/api/uploads/default-avatar.png",
        nationality: nationalityValue,
        postal_code: postalCodeValue,
        province: provinceValue,
        district: districtValue,
        city: cityValue,
        referral_code: "Grand Opening",
        id_card_no: null,
        instagram_username: null,
        company_name: null,
        physical_limitation: null,
        emergency_contact_name: null,
        emergency_contact_relationship: null,
        emergency_phone_number: null,
        emergency_home_number: null,
        email: null,
        rfid_card_no: null,
        created_at: "2021-09-25T01:00:00.000Z",
        updated_at: "2021-11-22T18:25:52.000Z",
        user_memberships: [
          {
            id: 24,
            user_id: 2,
            membership_id: 1,
            user_membership_package_id: null,
            is_package: false,
            status: "active",
            started_at: "2021-10-20T07:41:20.000Z",
            expired_at: "2021-10-20T07:41:20.000Z",
            is_recurring: false,
            deposit_amount: null,
            recurrence_progress: null,
            recurring_status: null,
            revoke_reason: null,
            created_at: "2021-10-20T07:41:20.000Z",
            updated_at: "2021-10-20T07:41:20.000Z",
            deleted_at: null,
            UserMembershipPackageId: null,
            membership: {
              id: 1,
              venue_id: 1,
              membership_type_id: 1,
              membership_category_id: 7,
              name: "Non-Member",
              image_url: null,
              portrait_image_url: null,
              perks_description: "Non Member",
              duration_unit: "month",
              duration: 120,
              price: 0,
              age_unit: "years",
              min_age: 0,
              max_age: 99,
              available_quota: 10000,
              quota_left: 10000,
              publish_date: "2021-11-21T17:00:00.000Z",
              available_until: "2021-11-21T17:00:00.000Z",
              is_limited: true,
              is_selling: false,
              is_preset: true,
              is_recurring_allowed: false,
              deposit_amount: null,
              recurring_amount: null,
              interval_unit: null,
              interval: null,
              min_recurrence_progress: null,
              created_at: "2021-11-21T17:00:00.000Z",
              updated_at: null,
              deleted_at: null,
            },
          },
        ],
        user_membership_packages: [
          {
            id: 2,
            user_id: 2,
            membership_package_id: 5,
            status: "active",
            started_at: "2021-12-01T00:00:00.000Z",
            expired_at: "2021-12-31T16:00:00.000Z",
            is_recurring: false,
            deposit_amount: null,
            recurrence_progress: null,
            recurring_status: null,
            revoke_reason: null,
            created_at: "2021-11-03T04:49:51.000Z",
            updated_at: "2021-11-03T04:49:51.000Z",
            deleted_at: null,
            membership_package: {
              id: 5,
              venue_id: 1,
              membership_package_category_id: 4,
              name: name,
              image_url:
                "https://api.kyzn.life/api/uploads/imageUrl-1639386628951.jpg",
              portrait_image_url:
                "https://api.kyzn.life/api/uploads/portraitImageUrl-1639709476279.jpg",
              price: 6615000,
              is_selling: true,
              is_recurring_allowed: false,
              deposit_amount: null,
              recurring_amount: null,
              interval_unit: null,
              interval: null,
              min_recurrence_progress: null,
              duration_unit: "month",
              duration: 3,
              created_at: "2021-11-26T10:06:21.000Z",
              updated_at: "2021-12-17T02:51:16.000Z",
              deleted_at: null,
            },
          },
        ],
        user_banks: [],
        current_level: 3,
        user_experiences: [
          {
            id: 1,
            level: 1,
            exp_needed: 0,
            created_at: "2021-11-11T09:51:36.000Z",
            updated_at: null,
            deleted_at: null,
          },
          {
            id: 2,
            level: 2,
            exp_needed: 100,
            created_at: "2021-11-11T09:51:36.000Z",
            updated_at: null,
            deleted_at: null,
          },
          {
            id: 3,
            level: 3,
            exp_needed: 200,
            created_at: "2021-11-11T09:51:36.000Z",
            updated_at: null,
            deleted_at: null,
          },
          {
            id: 4,
            level: 4,
            exp_needed: 300,
            created_at: "2021-11-11T09:51:36.000Z",
            updated_at: null,
            deleted_at: null,
          },
        ],
      }

      dispatch(Actions.profileSuccess(profile));
      // dispatch(
      //   Actions.makeMembershipPayment({
      //     data: {
      //       userId: selectedUser,
      //       membershipId,
      //       paymentMethod: "kyzn_money",
      //       paymentStatus: "success",
      //       startDate: selectedDate
      //     },
      //     successCallback: () => {
            // dispatch(Actions.profile()); 
            setIsShowingModal(true);
        //   failureCallback: () => {},
        // }),
      // );
    }
  };

  // const error = useSelector((state) => state.MEMBERSHIP.makeMembershipPayment.error);

  // useEffect(() => {
  //   if (error) {
  //     Alert.alert("Error", error, [
  //       {
  //         text: "OK",
  //         onPress: () => dispatch(Actions.membershipPaymentReset()),
  //         style: "cancel",
  //       },
  //     ]);
  //   }
  // }, [error]);


  
  return (
    <Box flex={1}>
      <ScrollView style={{ backgroundColor: "white", flex: 1 }}>
        {membership_status == 1 ? (
          <HStack
            style={{ alignItems: "center", padding: 20, paddingVertical: 27 }}>
            {/* <Image
              style={{ width: 32, height: 32 }}
              source={require("../../../../assets/images/crown.png")}
              alt="image"
              resizeMode="cover"
            /> */}
            <VStack style={{ justifyContent: "center", marginLeft: 8 }}>
              <Text
                style={{
                  fontWeight: "bold",
                  fontSize: 12,
                  letterSpacing: 0.2,
                  fontFamily: "Montserrat-Regular",
                }}>
                PAY WITH MY CREDIT
              </Text>
              {/* <Text
                style={{
                  fontWeight: "bold",
                  fontSize: 12,
                  color: "#757575",
                  letterSpacing: 0.2,
                  fontFamily: "Montserrat-Regular",
                }}
              >
                PERSONAL PLATINUM PASS
              </Text> */}
            </VStack>
          </HStack>
        ) : (
          <HStack
            style={{ alignItems: "center", padding: 20, paddingVertical: 32 }}>
            <Text
              style={{
                fontWeight: "bold",
                fontSize: 12,
                letterSpacing: 0.2,
                fontFamily: "Montserrat-Regular",
              }}>
              PAY WITH MY CREDIT
            </Text>
          </HStack>
        )}
        <Box style={{ height: 8, backgroundColor: "#F8F9FC" }} />
        {/* COUPONS DISPLAY */}
        
        {/* <TouchableOpacity onPress={() => {
          navigation.navigate("Coupons", {
            // filter: activity.name,
            onRefresh: onRefresh, // part of onRefresh 
            // activityId: activity.id, // TODO: on membership, there is no activityId set yet
            isFromPayment: true,
            couponApplied: couponApplied,
            couponId: couponId,
            couponAvailable: coupons
          })
        }}>
          <HStack style={{
            alignItems: "center",
            paddingVertical: moderateScale(12, .25),
            paddingHorizontal: moderateScale(16, .25),
            marginHorizontal: moderateScale(20, .25),
            // justifyContent: "space-between",
            backgroundColor: "#FFFEEA",
            borderColor: '#FFCA52',
            borderWidth: 1
          }}>
            {
              isCouponValid == false && (
                <WarningIcon 
                  size={4}
                  color={"#ef4444"}
                  marginRight={moderateScale(12, .25)}
                />
              )
            }
            <Text style={{
              flex: 1,
              fontSize: moderateScale(12, .25),
              fontWeight: "600",
              color: "#212121",
              fontFamily: "Montserrat-Regular",
            }}>
              {
                couponApplied === ""
                  ? `${coupons.length} Available Coupon(s)`
                  : couponApplied  
              }
            </Text>
            <Image 
              source={image.ic_coupon}
              style={{
                width: moderateScale(16, .25),
                height: moderateScale(12, .25),
              }}
              alt={"imageAlt"}
            />
          </HStack>
        </TouchableOpacity> */}
        <Box style={{ padding: 20, paddingBottom: 32 }}>
          <HStack
            style={{
              alignItems: "center",
              paddingVertical: 9,
              justifyContent: "space-between",
              backgroundColor: "#F8F9FC",
            }}>
            <VStack style={{ justifyContent: "center", marginLeft: 10 }}>
              <Text
                style={{
                  fontSize: 12,
                  letterSpacing: 0.2,
                  fontFamily: "Montserrat-Regular",
                }}>
                {name}
              </Text>
              {/* <Text
                style={{
                  fontSize: 12,
                  color: "#757575",
                  letterSpacing: 0.2,
                  fontFamily: "Montserrat-Regular",
                }}
              >
                Kids
              </Text> */}
            </VStack>
            <Text
              style={{
                fontWeight: "bold",
                fontSize: 12,
                letterSpacing: 0.2,
                marginRight: 10,
                fontFamily: "Montserrat-Regular",
              }}>
              {`${Commons.convertToRupiah(price)}`}
            </Text>
          </HStack>
        </Box>
        {/* <Box style={{ padding: 20 }}>
          <VStack
            style={{
              borderColor: "#DFE3EC",
              borderWidth: 1,
              borderBottomWidth: 0,
              padding: 16,
            }}
          >
            <Text
              style={{
                fontSize: 12,
                color: "#757575",
                letterSpacing: 0.2,
                marginBottom: 10,
                fontFamily: "Montserrat-Regular",
              }}
            >
              Booking for
            </Text>
            <Text
              style={{
                fontWeight: "bold",
                fontSize: 12,
                letterSpacing: 0.2,
                fontFamily: "Montserrat-Regular",
              }}
            >
              PAY WITH MY CREDIT
            </Text>
            <Text
              style={{
                fontSize: 12,
                color: "#757575",
                letterSpacing: 0.2,
                fontFamily: "Montserrat-Regular",
              }}
            >
              Tomorrow, 23 Sep 2020 | 18:00
            </Text>
          </VStack>
          <VStack
            style={{ borderColor: "#DFE3EC", borderWidth: 1, padding: 16 }}
          >
            <Text
              style={{
                fontSize: 12,
                color: "#757575",
                letterSpacing: 0.2,
                marginBottom: 10,
                fontFamily: "Montserrat-Regular",
              }}
            >
              Excercise at
            </Text>
            <Text
              style={{
                fontWeight: "bold",
                fontSize: 12,
                letterSpacing: 0.2,
                fontFamily: "Montserrat-Regular",
              }}
            >
              Kyzn Studio - 3th Floor
            </Text>
            <Text
              style={{
                fontSize: 12,
                color: "#757575",
                letterSpacing: 0.2,
                fontFamily: "Montserrat-Regular",
              }}
              numberOfLines={1}
            >
              Menara Kuningan - Jln Rasuna Sahid Level lorem ipsuasjdiwonowinini
            </Text>
          </VStack>
        </Box> */}
      </ScrollView>
      <Box
        style={{
          position: "absolute",
          bottom: 0,
          right: 0,
          width: "100%",
          padding: 20,
          paddingBottom: 40,
          borderTopWidth: 1,
          borderColor: "#ECEDEF",
          backgroundColor: "#FFF",
        }}>
        <HStack style={{ justifyContent: "space-between" }}>
          <VStack>
            <HStack>
              <Text
                style={{
                  fontSize: 10,
                  color: "#606060",
                  marginBottom: 5,
                  fontFamily: "Montserrat-Regular",
                }}>
                Credit Used
              </Text>
            </HStack>
            {
              isCouponValid == true
                ? (
                  <HStack>
                    <Text
                      style={{
                        fontSize: 14,
                        color: "#000",
                        fontWeight: "bold",
                        fontFamily: "Montserrat-Regular",
                        textDecorationLine: "line-through",
                        textDecorationStyle: "solid"
                      }}
                      alignItems="flex-start"
                    >
                      {`${Commons.convertToRupiah(price)}`}
                    </Text>
                    <Text
                      style={{
                        fontSize: 14,
                        color: "#000",
                        fontWeight: "bold",
                        fontFamily: "Montserrat-Regular",
                        marginLeft: moderateScale(6, .25)
                        // textDecorationLine: "line-through",
                        // textDecorationStyle: "solid"
                      }}
                      alignItems="flex-start"
                    >
                      { 
                        Commons.convertToRupiah(newPrice)
                      }
                    </Text>
                    <Text
                      style={{
                        fontSize: 14,
                        color: "#000",
                        fontWeight: "bold",
                        fontFamily: "Montserrat-Regular",
                        marginLeft: moderateScale(6, .25)
                        // textDecorationLine: "line-through",
                        // textDecorationStyle: "solid"
                      }}
                      alignItems="flex-start"
                    >
                      Credits
                    </Text>
                  </HStack>
                ) : (
                  <Text
                    style={{
                      fontSize: 14,
                      color: "#000",
                      fontWeight: "bold",
                      fontFamily: "Montserrat-Regular",
                    }}
                    alignItems="flex-start">
                    {`${Commons.convertToRupiah(price)}`}
                  </Text>
                )
            }
          </VStack>
          <Button w={130} bgColor={"#1035BB"} onPress={() => buyMembership()}>
            BUY NOW
          </Button>
        </HStack>
      </Box>

      <Modal isOpen={isShowingModal}>
        <Modal.Content width="90%" height="60%">
          <Image
            source={image.successCorner}
            position="absolute"
            top={0}
            right={0}
            zIndex={-1}
          />
          <Box flex={1} padding={5} justifyContent="center" alignItems="center">
            <Image source={image.successLogo} />
            <Column
              marginTop={3}
              marginBottom={6}
              justifyContent="center"
              alignItems="center">
              <Text
                fontWeight={300}
                fontSize="md"
                textAlign="center"
                style={{ fontWeight: "bold" }}
                marginBottom={3}>
                {`${name} was Purchased`}
              </Text>
              <Text style={{ textAlign: "center" }}>
                Yay! Your purchase was successful!
              </Text>
            </Column>
            {/* <Button
              borderRadius={0}
              width="100%"
              backgroundColor="button.black"
              _pressed={{
                backgroundColor: "button.black:alpha.20",
              }}
              onPress={() => {
                setIsShowingModal(false);
                navigation.navigate("UserMembership");
              }}
              style={styles.button}>
              View Details
            </Button> */}
            <Button
              borderRadius={0}
              width="100%"
              style={styles.button}
              backgroundColor="transparent"
              _pressed={{
                backgroundColor: "button.black:alpha.20",
              }}
              _text={{
                color: "black",
              }}
              onPress={() => {
                setIsShowingModal(false);
                navigation.navigate("Home");
              }}>
              Done
            </Button>
          </Box>
        </Modal.Content>
      </Modal>
      <Actionsheet isOpen={insufficientBalanceOpen} onClose={closeInsufficientBalanceAS}>
        <Actionsheet.Content h={365}>
          <Image
            source={image.bg_noPremium_actionsheet}
            style={{ width: 150, height: 150, resizeMode: "contain", position: "absolute", left: 0, top: -22, transform: [{rotate: "0deg"}] }}
            alt="image"
          />
          <Image
            source={image.not_premium_ic}
            style={{ width: 170, height:100 }}
            resizeMode={"contain"}
            alt="image"
          />
          <Box style={{width:270, alignItems:"center", marginTop:8}}>
            <Text
              style={{
                fontSize: 14,
                color: "#000",
                fontWeight: "bold",
                fontFamily: "Montserrat-Regular",
              }}
              alignItems="flex-start"
            >
              {`Sorry, You have insufficient balance`}
            </Text>
            <Text
              style={{
                fontSize: 12,
                color: "#000",
                fontWeight: "500",
                fontFamily: "Montserrat-Regular",
                textAlign:"center",
                marginTop:10
              }}
              alignItems="flex-start"
            >
              {`Please top up your balance. Your current balance is `}  
              <Text style={{
                fontWeight: "bold"
              }}>
                  {`${Commons.convertToRupiah(balance)}`}
              </Text>
            </Text>
          </Box>
            <Button
              w={"90%"}
              bgColor={"#212121"}
              onPress={goToTopUpDetails}
              style={{marginTop:50, borderRadius:0}}
            >
              <Text style={{
                color:"#fff",
                fontFamily: "Montserrat-Regular",
              }}>
                  Top Up now
              </Text>
            </Button>
            <Button
              w={"90%"}
              bgColor={"transparent"}
              onPress={closeInsufficientBalanceAS}
              color={"#000"}
              style={{marginTop:10, borderRadius:0}}
            >
              <Text style={{
                fontWeight: "bold",
                color:"#000",
                fontFamily: "Montserrat-Regular",
              }}>
                  Close
              </Text>
            </Button>
        </Actionsheet.Content>
      </Actionsheet>
    </Box>
  );
};

MembershipPayment.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default MembershipPayment;
