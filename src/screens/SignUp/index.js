import React, { useState } from "react";
import {
  Box,
  Button,
  ChevronLeftIcon,
  IconButton,
  Input,
  Text,
} from "native-base";
import { ScrollView } from "react-native";
import PropTypes from "prop-types";
import DividerHorizontal from "components/ui/DividerHorizontal";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { SafeAreaProvider } from "react-native-safe-area-context";
import { moderateScale } from "react-native-size-matters";
import ButtonCustom from "components/ui/ButtonCustom";
import InputField from "components/ui/InputField";
import { useDispatch } from "react-redux";
import Actions from "actions";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

const Register = ({ navigation, route }) => {
  console.log("Route Register: ", route);
  const dispatch = useDispatch();

  const [referralCode, setReferalCode] = useState(
    route?.params?.code ? route?.params?.code : "",
  );
  const [pass, setPass] = useState("");
  const [showPassword, setShowPassword] = useState(false);
  const [passwordError, setPasswordError] = useState(false);
  const [passwordErrorValue, setPasswordErrorValue] = useState("");
  const [phone, setPhone] = useState("");
  const [countryCode, setCountryCode] = useState("+62");
  const [phoneError, setPhoneError] = useState(false);
  const [phoneErrorValue, setPhoneErrorValue] = useState("");
  const [confirmPass, setConfirmPass] = useState("");
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);
  const [confirmPasswordError, setConfirmPasswordError] = useState(false);
  const [confirmPasswordErrorValue, setConfirmPasswordErrorValue] =
    useState("");
  const [name, setName] = useState("");
  const [nameError, setNameError] = useState(false);
  const [nameErrorValue, setNameErrorValue] = useState("");

  const validation =
    pass === confirmPass &&
    !passwordError &&
    !phoneError &&
    !confirmPasswordError &&
    pass !== "" &&
    confirmPass !== "" &&
    phone !== "" &&
    name !== ""
      ? true
      : false;

  // console.log("validation", validation, pass, confirmPass, passwordError, phoneError, confirmPasswordError,name, "phone :", phone);

  const changePasswordHide = val => {
    if (val === "pass") {
      setShowPassword(!showPassword);
    } else {
      setShowConfirmPassword(!showConfirmPassword);
    }
  };

  const checkPassword = (key, value) => {
    if (value.length < 6) {
      setPasswordError(true);
      setPasswordErrorValue("Password must be minimal 6 characters");
    } else {
      setPasswordError(false);
      setPasswordErrorValue("");
    }
    if (confirmPass !== "") {
      if (value === confirmPass) {
        setConfirmPasswordError(false);
        setConfirmPasswordErrorValue("");
      } else {
        setConfirmPasswordError(true);
        setConfirmPasswordErrorValue("Confirm passwords do not match");
      }
    }
  };

  const confirmPassword = (key, value) => {
    if (value.length < 6) {
      setConfirmPasswordError(true);
      setConfirmPasswordErrorValue(
        "Confirm Password must be minimal 6 characters",
      );
    } else {
      setConfirmPasswordError(false);
      setConfirmPasswordErrorValue("");
    }
    if (pass !== value) {
      setConfirmPasswordError(true);
      setConfirmPasswordErrorValue("Confirm passwords do not match");
    } else {
      setConfirmPasswordError(false);
      setConfirmPasswordErrorValue("");
    }
  };

  const checkPhone = (key, value) => {
    // if (
    //   !value.startsWith("8") ||
    //   value.match(/8[0-9 ]{6,16}$/) === null
    // ) {
    //   setPhoneError(true);
    //   setPhoneErrorValue("Invalid phone number format");
    // } else {
    //   setPhoneError(false);
    //   setPhoneErrorValue("");
    // }
  };

  const goToRegisterDetail = () => {
    let adjustPhoneNumber = "";
    // if (phone.startsWith("8")) {
    adjustPhoneNumber = `${countryCode}${phone}`;
    // }
    navigation.navigate("SignUpDetail", {
      title: "Personal Information",
      pass,
      adjustPhoneNumber,
      confirmPass,
      name,
      referralCode,
    });
  };

  return (
    <SafeAreaProvider
      style={{
        flex: 1,
        backgroundColor: "white",
      }}
      edges={["right", "bottom", "left"]}>
      <KeyboardAwareScrollView
        enableOnAndroid={true}
        showsVerticalScrollIndicator={false}>
        <Box flex={1} style={{ marginTop: moderateScale(40, 0.25) }}>
          <Box
            flex={1}
            style={
              {
                // marginHorizontal: moderateScale(24, 0.25),
                // marginTop: moderateScale(40, 0.25),
              }
            }>
            <Box
              flex={1}
              // alignItems="center"
              // justifyContent="center"
              marginY="5"
              // style={{ backgroundColor: "red" }}
            >
              {/* <Text
              style={{
                fontSize: moderateScale(24, 0.25),
                fontWeight: "600",
                lineHeight: moderateScale(36, 0.25),
                fontFamily: "Montserrat-Regular",
              }}
              textAlign="center"
            >
              Sign Up
            </Text> */}
              {/* <Box flex={1} /> */}
              <Box
                style={{
                  // marginTop: moderateScale(113, 0.25),
                  // marginHorizontal: moderateScale(24, 0.25),
                  paddingHorizontal: moderateScale(24, 0.25),
                }}>
                <InputField
                  placeholder={"Full Name"}
                  showIcon={true}
                  iconValue={"name"}
                  onChangeText={val => {
                    setName(val);
                  }}
                  value={name}
                  // containerBgcolor={"blue"}
                  error={nameError}
                  errorMessage={nameErrorValue}
                />
                <InputField
                  placeholder={"Phone"}
                  showIcon={false}
                  iconValue={"phone"}
                  changeValue={val => {
                    setPhone(val);
                    checkPhone("phone", val);
                  }}
                  changeCountry={val => {
                    setCountryCode(`+${val.callingCode[0]}`);
                  }}
                  value={phone}
                  // containerBgcolor={"blue"}
                  error={phoneError}
                  errorMessage={phoneErrorValue}
                />
                <InputField
                  placeholder={"Password"}
                  isPassword={true}
                  onChangeText={val => {
                    setPass(val);
                    checkPassword("password", val);
                  }}
                  value={pass}
                  changeShowPassword={() => changePasswordHide("pass")}
                  showPassword={showPassword}
                  secureTextEntry={!showPassword}
                  error={passwordError}
                  errorMessage={passwordErrorValue}
                  showIcon={true}
                  iconValue={"password"}
                  // containerBgcolor={"blue"}
                  containerMarginBottom={0}
                />
                <InputField
                  placeholder={"Confirm Password"}
                  isPassword={true}
                  onChangeText={val => {
                    setConfirmPass(val);
                    confirmPassword("confirmPassword", val);
                  }}
                  value={confirmPass}
                  changeShowPassword={() => changePasswordHide("confirmPass")}
                  showPassword={showConfirmPassword}
                  secureTextEntry={!showConfirmPassword}
                  error={confirmPasswordError}
                  errorMessage={confirmPasswordErrorValue}
                  showIcon={true}
                  iconValue={"password"}
                  // containerBgcolor={"blue"}
                />
                <InputField
                  placeholder={"Referral Code"}
                  showIcon={false}
                  iconValue={"referral"}
                  onChangeText={val => {
                    setReferalCode(val);
                  }}
                  value={referralCode}
                  // containerBgcolor={"blue"}
                  error={""}
                  errorMessage={""}
                />
                {validation ? (
                  <ButtonCustom
                    label={"Register"}
                    onPressButton={() => goToRegisterDetail()}
                    // buttonMarginHorizontal={24}
                    buttonMarginBottom={-10}
                    buttonMarginTop={14}
                    // isLoading={loading}
                  />
                ) : (
                  <ButtonCustom
                    label={"Register"}
                    disableButton={true}
                    buttonColor={"#838586"}
                    // buttonMarginHorizontal={24}
                    buttonMarginBottom={-10}
                    buttonMarginTop={14}
                    // isLoading={loading}
                  />
                )}

                <DividerHorizontal />
                <Box flexDirection="row" style={{ justifyContent: "center" }}>
                  <Button
                    onPress={() => navigation.goBack()}
                    variant="ghost"
                    _text={{
                      fontSize: "sm",
                      color: "black",
                      fontFamily: "Montserrat-Regular",
                    }}
                    _pressed={{
                      _text: {
                        color: "black:alpha.20",
                      },
                      backgroundColor: "transparent",
                    }}>
                    Already have an account? Sign in here
                  </Button>
                </Box>
              </Box>
              <Box flex={1} />
            </Box>
            {/* <IconButton
            position="absolute"
            top="1"
            // left="1"
            icon={<ChevronLeftIcon size={12} />}
            onPress={() => navigation.goBack()}
            style={{
              marginTop: moderateScale(5, 0.25),
              // backgroundColor: "blue",
              alignItems: "flex-start",
            }}
          /> */}
          </Box>
        </Box>
      </KeyboardAwareScrollView>
    </SafeAreaProvider>
  );
};

Register.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default Register;
