import React, { useState } from "react";
import {
  Box,
  Button,
  ChevronLeftIcon,
  IconButton,
  Input,
  Text,
  ScrollView,
  View,
} from "native-base";
import PropTypes from "prop-types";
import DividerHorizontal from "components/ui/DividerHorizontal";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { SafeAreaProvider } from "react-native-safe-area-context";
import { moderateScale } from "react-native-size-matters";
import ButtonCustom from "components/ui/ButtonCustom";
import InputField from "components/ui/InputField";
import { useDispatch } from "react-redux";
import Actions from "actions";
import FloatingInput from "components/ui/FloatingInput";
import GenderPicker from "components/ui/GenderPicker";
import NationPicker from "components/ui/NationPicker";
import DatePicker from "components/ui/DatePicker";
import moment from "moment";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { Alert, Dimensions } from "react-native";

const RegisterDetail = ({ navigation, route }) => {
  const dispatch = useDispatch();
  const { title, pass, adjustPhoneNumber, confirmPass, name, referralCode } =
    route.params;

  const [userName, setUsername] = useState("");
  const [gender, setGender] = useState("");
  const [nationality, setNationality] = useState("Indonesia");
  const [birthdate, setBirthdate] = useState("");
  const [postalCode, setPostalCode] = useState("");
  const [address, setAddress] = useState("");
  const [province, setProvince] = useState("");
  const [district, setDistrict] = useState("");
  const [city, setCity] = useState("");

  const validation =
    userName !== "" && gender !== ""
      ? // gender !== "" &&
        // postalCode !== "" &&
        // address !== "" &&
        // city !== "" &&
        // province !== "" &&
        // district !== ""
        true
      : false;

  console.log(
    "cek isi val",
    title,
    pass,
    adjustPhoneNumber,
    confirmPass,
    name,
    gender,
    nationality,
    birthdate,
  );

  const register = () => {
    // dispatch(
    //   Actions.register(
    //     adjustPhoneNumber,
    //     pass,
    //     confirmPass,
    //     name,
    //     gender,
    //     nationality,
    //     birthdate,
    //     postalCode,
    //     address,
    //     province,
    //     district,
    //     city
    //   )
    // );
    if (birthdate === "") {
      Alert.alert("You need to fill in your birthdate in order to continue");
    } else {
      navigation.navigate("SignUpConfirmation", {
        pass,
        adjustPhoneNumber,
        confirmPass,
        name,
        gender,
        nationality,
        birthdate,
        postalCode,
        address,
        province,
        district,
        city,
        userName,
        referralCode,
      });
    }
  };

  const convertDOB = date => {
    let convertDate = moment(date).format("YYYY-MM-DD");
    setBirthdate(convertDate);
  };
  return (
    <SafeAreaProvider
      style={{
        flex: 1,
        backgroundColor: "white",
      }}
      edges={["right", "bottom", "left"]}>
      <Box flex={1} style={{ marginTop: moderateScale(18, 0.25) }}>
        <KeyboardAwareScrollView
          enableOnAndroid={true}
          style={{ marginBottom: moderateScale(40, 0.25) }}
          showsVerticalScrollIndicator={false}>
          <Box
            flex={1}
            style={
              {
                // marginHorizontal: moderateScale(24, 0.25),
                // marginTop: moderateScale(40, 0.25),
              }
            }>
            <Box
              flex={1}
              // alignItems="center"
              // justifyContent="center"
              marginY="5"
              // style={{ backgroundColor: "red" }}
            >
              {/* <Box flex={1} /> */}
              <Text
                style={{
                  fontSize: moderateScale(16, 0.25),
                  fontWeight: "600",
                  lineHeight: moderateScale(24, 0.25),
                  marginBottom: moderateScale(12, 0.25),
                  fontFamily: "Montserrat-Regular",
                  color: "black",
                  marginHorizontal: moderateScale(40, 0.25),
                }}>
                Please Complete the Details
              </Text>
              <Box
                style={{
                  // marginTop: moderateScale(113, 0.25),
                  // marginHorizontal: moderateScale(24, 0.25),
                  paddingHorizontal: moderateScale(24, 0.25),
                }}>
                <FloatingInput
                  label="Username *"
                  value={userName}
                  onChangeText={val => setUsername(val)}
                  placeholder={"Enter Username"}
                  bgColorLabel={"white"}></FloatingInput>
                <DatePicker
                  label="Birthdate *"
                  bgColorLabel={"white"}
                  onSelected={val => convertDOB(val)}
                />
                <GenderPicker
                  label="Gender *"
                  bgColorLabel={"white"}
                  onSelected={val => setGender(val)}></GenderPicker>
                <NationPicker
                  label="Nationality *"
                  bgColorLabel={"white"}
                  onSelected={val => setNationality(val)}
                />
                <View
                  style={{
                    alignSelf: "center",
                    height: 1,
                    backgroundColor: "gray",
                    width: Dimensions.get("screen").width / 1.4,
                    marginVertical: 12,
                  }}
                />
                <FloatingInput
                  label="Postal Code (Optional)"
                  value={postalCode}
                  onChangeText={val => setPostalCode(val)}
                  placeholder={"Enter postal code"}
                  bgColorLabel={"white"}
                  keyboardType={"number-pad"}></FloatingInput>
                <FloatingInput
                  label="Address (Optional)"
                  value={address}
                  onChangeText={val => setAddress(val)}
                  placeholder={"Enter address"}
                  bgColorLabel={"white"}></FloatingInput>
                <FloatingInput
                  label="City (Optional)"
                  value={city}
                  onChangeText={val => setCity(val)}
                  placeholder={"Enter city"}
                  bgColorLabel={"white"}></FloatingInput>
                <FloatingInput
                  label="District (Optional)"
                  value={district}
                  onChangeText={val => setDistrict(val)}
                  placeholder={"Enter district"}
                  bgColorLabel={"white"}></FloatingInput>
                <FloatingInput
                  label="Province (Optional)"
                  value={province}
                  onChangeText={val => setProvince(val)}
                  placeholder={"Enter province"}
                  bgColorLabel={"white"}></FloatingInput>
                {validation ? (
                  <ButtonCustom
                    label={"Next"}
                    onPressButton={() => register()}
                    // buttonMarginHorizontal={24}
                    buttonMarginBottom={-10}
                    buttonMarginTop={14}
                    // isLoading={loading}
                  ></ButtonCustom>
                ) : (
                  <ButtonCustom
                    label={"Next"}
                    disableButton={true}
                    buttonColor={"#838586"}
                    // buttonMarginHorizontal={24}
                    buttonMarginBottom={-10}
                    buttonMarginTop={14}
                    // isLoading={loading}
                  ></ButtonCustom>
                )}
              </Box>
              <Box flex={1} />
            </Box>
          </Box>
        </KeyboardAwareScrollView>
      </Box>
    </SafeAreaProvider>
  );
};

RegisterDetail.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default RegisterDetail;
