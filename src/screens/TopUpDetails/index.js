import React, { useState, useEffect, useReducer } from "react";
import { Dimensions, ActivityIndicator, TextInput, View, Alert, TouchableOpacity, StyleSheet, Platform } from "react-native";
import {
  Box,
  Text,
  Button,
  FlatList,
  ScrollView,
  HStack,
  Image,
  ZStack,
  VStack,
  Actionsheet,
} from "native-base";
import PropTypes from "prop-types";
import _get from 'lodash/get'
import { image } from "../../../assets/images";
import { moderateScale } from "react-native-size-matters";
import { connect, useDispatch, useSelector } from "react-redux";
import Actions from "actions";
import PaymentMethodList from "components/ui/PaymentMethodList";
import Commons from "utils/common";
import ButtonCustom from "components/ui/ButtonCustom";
import { Select, CheckIcon, Flex } from 'native-base';
import Selectors from 'selectors';
import { xenditService } from "../../services";
import { toInteger } from "lodash";
import { UserApi } from "utils/Api";

const userapi = UserApi.client;

const TopUpDetails = ({ navigation, route }) => {
  const dispatch = useDispatch();
  const dataUniqueFamily = useSelector((state) => state.FAMILY.uniqueFamily);
  const profile = useSelector(state => state.AUTH.profile);

  const [selectedUser, setSelectedUser] = useState(dataUniqueFamily.data[0].id);
  const family = useSelector((state) => state.FAMILY.user.data)
  const userId = _get(route.params, "userId", 1)
  const minimumAmount = _get(route.params, "amount", 0)
  const minimumTopup = _get(route.params, "minTopup", 100000)
  const isFromPayment = _get(route.params, "isFromPayment", false)

  const [topUpPrice, setTopUpPrice] = useState(minimumAmount == 0 ? "" : String(minimumAmount));
  const [showMinAmount, setShowMinAmount] = useState(false)
  const [convertTopUpPrice, setConvertTopUpPrice] = useState("");
  const [selectedAmount, setSelectedAmount] = useState(null)
  const [topupLoading, setTopupLoading] = useState(false)
  const [dataDummy, setDataDummy] = useState([
    {
      id: 1,
      name: "Gopay",
      image: image.gopay_ic,
    },
  ]);
  const [dummyAmount, setDummyAmount] = useState([
    {
      id: 1,
      amount: 250000,
      xp_reward: 100,
      // coin_reward: 100 // OR use this
    },
    {
      id: 2,
      amount: 500000,
      xp_reward: 150,
      // coin_reward: 100 
    },
    {
      id: 2,
      amount: 1000000,
      xp_reward: 200,
      // coin_reward: 100 
    },
  ])

  //   useEffect(() => {
  //     let convertPrice = Commons.convertToRupiah(topUpPrice);
  //     setConvertTopUpPrice(convertPrice);

  //   },[topUpPrice])

  

  useEffect(() => {
    console.log("minimumAmount: ", minimumAmount); // .id
    console.log("params: ", route.params); 
    console.log("profile ", profile); 
    setSelectedUser(userId)
    // dispatch(Actions.fetchFamilyUnique())
  }, [])

  const openActionSheet = () => {
    setShowMinAmount(true)
  }

  const closeActionSheet = () => {
    setShowMinAmount(false)
  }

  const selectAmount = (index, amount) => {
    setSelectedAmount(index)
    setTopUpPrice(String(amount))
  }

  const unselectAmount = () => {
    setSelectedAmount(null)
  }

  const submitTopUp = () => {
    setTopupLoading(true)
    const userId = selectedUser
    console.log("selectedUserTopUp", userId, toInteger(topUpPrice));
    // dispatch(Actions.topup(topUpPrice, selectedUser));
    // dispatch(Actions.topup(topUpPrice, userId));
    // navigation.navigate("TopUp");

    if(toInteger(topUpPrice) >= minimumAmount ) {
      if(toInteger(topUpPrice) < minimumTopup) {
        setTopupLoading(false)
        Alert.alert("Warning", `Minimum Topup amount is ${Commons.convertToRupiah(minimumTopup)}\nPlease Topup more`)
      } else {
        profile.data.balance = profile.data.balance + toInteger(topUpPrice);
        setTopupLoading(false);
        navigation.goBack();
      }
    } else {
      setTopupLoading(false)
      openActionSheet()
    }

  }

  return (
    <Box style={{ backgroundColor: "white", flex: 1 }}>
      <Text style={{
        marginTop: moderateScale(16, 0.25),
        marginHorizontal: moderateScale(20, 0.25),
        fontSize: moderateScale(16, 0.25),
        fontWeight: 'bold',
        color: "#000",
        fontFamily: "Montserrat-Regular",
      }}>
        {`Amount to topup:`}
      </Text>
      <Text style={{
        marginTop: moderateScale(16, 0.25),
        marginHorizontal: moderateScale(20, 0.25),
        fontSize: moderateScale(16, 0.25),
        fontWeight: 'bold',
        color: "#000",
        fontFamily: "Montserrat-Regular",
      }}>
        {`Rp. 1 is equal to $ 1`}
      </Text>
      <HStack
        style={{
          height: moderateScale(47, 0.25),
          borderWidth: 1,
          marginHorizontal: moderateScale(20, 0.25),
          justifyContent: "center",
          alignItems: "center",
          marginTop: moderateScale(16, 0.25),
          marginBottom: moderateScale(31, 0.25),
        }}
      >
        {
          Platform?.OS === 'ios' ? 
          <Text style={{
            fontSize: moderateScale(14, 0.25),
            fontWeight: '600',
            color: "#000",
            fontFamily: "Montserrat-Regular",
          }}>
            {`RP.`}
          </Text>:
          <Text style={{
            left: 20,
            fontSize: moderateScale(14, 0.25),
            fontWeight: '600',
            color: "#000",
            fontFamily: "Montserrat-Regular",
          }}>
            {`RP.`}
          </Text>
        }
        <TextInput
          onChangeText={(value) => {
            setTopUpPrice(value);
          }}
          value={topUpPrice}
          placeholder={"Enter Amount"}
          style={{
            fontSize: moderateScale(16, 0.25),
            color: "#000",
            fontFamily: "Montserrat-Regular",
            width: Platform.OS == 'ios' ? "35%" : '100%',
            textAlign: "center",
          }}
          keyboardType={"number-pad"}
        ></TextInput>
      </HStack>
      <View style={{
        // flex: 1,
        marginHorizontal: moderateScale(20)
      }}>
        <FlatList 
          data={dummyAmount}
          horizontal
          showsHorizontalScrollIndicator={false}
          renderItem={({item, index}) => (
            <TouchableOpacity onPress={() => {
              console.log("item selected: ", item);
              if(index == selectedAmount) {
                unselectAmount()
              } else {
                selectAmount(index, item.amount)
              }
            }}>
              <View style={index == selectedAmount ? styles.buttonActive : styles.buttonInactive}>
                <Text style={{
                  fontSize: moderateScale(16, 0.25),
                  color: "#000",
                  fontWeight: "600",
                  fontFamily: "Montserrat-Regular",
                  marginBottom: moderateScale(2, 0.25),
                }}>
                  {`${Commons.convertToRupiah(item.amount)}`}
                </Text>
                <Text style={{
                  fontSize: moderateScale(12, 0.25),
                  color: "#000",
                  fontFamily: "Montserrat-Regular",
                }}>
                  {/* {`${item.xp_reward} Credits`} */}
                  {/* {`${item.coin_reward} Credits`} */}
                  Credits
                </Text>
              </View>
            </TouchableOpacity>
          )}
          keyExtractor={(item, index) => String(item + index)}
        />
      </View>
      <View
        style={{
          backgroundColor: "#F8F9FC",
          height: moderateScale(8, 0.25),
          width: "100%",
        }}
      />
      {/* <Text
        style={{
          fontSize: moderateScale(12, 0.25),
          color: "#212121",
          fontWeight: "600",
          marginTop: moderateScale(21, 0.25),
          marginBottom: moderateScale(12, 0.25),
          marginHorizontal: moderateScale(20, 0.25),
          fontFamily : "Montserrat-Regular"
        }}
      >
        Payment Method
      </Text>
      <FlatList
        data={dataDummy}
        renderItem={(_item) => {
          return <PaymentMethodList navigation={navigation} data={_item} />;
        }}
      />
      <View
        style={{
          backgroundColor: "#EFEFEF",
          height: moderateScale(2, 0.25),
          width: "100%",
        }}
      /> */}
      <ButtonCustom
        label={"Top up"}
        onPressButton={() => submitTopUp()}
        buttonMarginBottom={40}
        buttonMarginTop={40}
        buttonHeight={40}
        isLoading={topupLoading}
      ></ButtonCustom>
      <Actionsheet isOpen={showMinAmount} onClose={closeActionSheet}>
        <Actionsheet.Content h={220}>
          <Image
            source={image.bg_actionsSheet}
            style={{ width: 300, height: 300, position: "absolute", right: 0 }}
            alt="image"
          />

          <View style={{ width: 270, marginTop: 8, alignItems: "center", justifyContent: 'center' }}>
            <Text
              style={{
                fontSize: moderateScale(16, 0.25),
                color: "#000",
                fontWeight: "bold",
                fontFamily: "Montserrat-Regular",
                marginBottom: moderateScale(12, 0.25),
              }}
              //   alignItems="flex-start"
            >
              {`Warning`}
            </Text>
            
            <Text
              style={{
                fontSize: 14,
                color: "#000",
                fontWeight: "500",
                fontFamily: "Montserrat-Regular",
                textAlign:"center",
                marginTop:10
              }}
              alignItems="flex-start"
            >
              {`Minimum top up to complete this booking is `}  
              <Text style={{
                fontWeight: "bold"
              }}>
                  {`${Commons.convertToRupiah(minimumAmount)}`}
              </Text>
            </Text>

            <TouchableOpacity onPress={() => closeActionSheet()}>
              <Text style={{
                fontSize: moderateScale(18, 0.25),
                fontFamily: "Montserrat-Regular",
                fontWeight: "700",
                color: '#000',
                marginTop: moderateScale(30)
              }}> 
                {`Close`}
              </Text>
            </TouchableOpacity>
          </View>
        </Actionsheet.Content>
      </Actionsheet>
    </Box>
  );
};

const styles = StyleSheet.create({
  buttonInactive: {
    borderWidth: 1,
    borderColor: "#000",
    backgroundColor: "transparent",
    paddingVertical: moderateScale(10),
    paddingHorizontal: moderateScale(16),
    marginRight: moderateScale(10),
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonActive: {
    borderWidth: 1,
    borderColor: "rgba(16, 53, 187, 1)",
    backgroundColor: "#E8E7FF",
    paddingVertical: moderateScale(10),
    paddingHorizontal: moderateScale(16),
    marginRight: moderateScale(10),
    alignItems: 'center',
    justifyContent: 'center',
    
  }
})

TopUpDetails.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default TopUpDetails;
