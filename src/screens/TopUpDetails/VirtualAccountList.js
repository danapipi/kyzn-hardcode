import React, { useState, useEffect, useReducer } from "react";
import {
  Dimensions,
  ActivityIndicator,
  TextInput,
  View,
  Alert,
  TouchableOpacity,
  StyleSheet,
  ToastAndroid,
  Platform,
} from "react-native";
import {
  Box,
  Text,
  Button,
  FlatList,
  ScrollView,
  HStack,
  Image,
  ZStack,
  VStack,
  Actionsheet,
} from "native-base";
import PropTypes from "prop-types";
import _get from "lodash/get";
import { image } from "../../../assets/images";
import { moderateScale } from "react-native-size-matters";
import { connect, useDispatch, useSelector } from "react-redux";
import Actions from "actions";
import PaymentMethodList from "components/ui/PaymentMethodList";
import Commons from "utils/common";
import ButtonCustom from "components/ui/ButtonCustom";
import { Select, CheckIcon, Flex } from "native-base";
import Selectors from "selectors";
import BankCard from "components/ui/BankCard";
import { xenditService } from "../../services";
import { toInteger } from "lodash";
import { UserApi } from "utils/Api";
import _ from "lodash";
import Clipboard from "@react-native-clipboard/clipboard";
import Icons from "react-native-vector-icons/Feather";

const userapi = UserApi.client;

const VirtualAccountsList = ({ navigation, route }) => {
  const dispatch = useDispatch();

  const [selectedBank, setSelectedBank] = useState({});
  const family = useSelector(state => state.FAMILY.user.data);
  const profile = useSelector(state => state.AUTH.profile);
  const minimumAmount = _get(route.params, "amount", 0);
  const [showMinAmount, setShowMinAmount] = useState(false);
  const [selectedIndex, setSelectedIndex] = useState(null);

  const openActionSheet = () => {
    setShowMinAmount(true);
  };

  const closeActionSheet = () => {
    setShowMinAmount(false);
  };

  const copyClipboard = () => {
    try {
      const accNumber =
        profile?.data?.user_banks[selectedIndex]?.account_number;
      console.log("accNumber: ", accNumber);
      Clipboard.setString(accNumber);
      if(Platform?.OS === 'android') {
        ToastAndroid.show("Account Number Copied", 1000);
      } else {
        Alert.alert("Account Number Copied")
      }
    } catch (error) {
      console.log("Error: ", error);
    }
  };

  return (
    <Box style={{ backgroundColor: "white", flex: 1 }}>
      <FlatList
        data={profile.data.user_banks}
        showsVerticalScrollIndicator={false}
        style={{ paddingTop: 10 }}
        numColumns={2}
        renderItem={item => {
          console.log("price price data", item);
          return (
            <BankCard
              data={item}
              numCol={2}
              onPress={() => {
                setSelectedIndex(item?.index);
                setSelectedBank(item);
                openActionSheet();
              }}
            />
          );
        }}
      />
      {/* <Text>{JSON.stringify(profile.data.user_banks)}</Text> */}
      <Actionsheet isOpen={showMinAmount} onClose={closeActionSheet}>
        {selectedBank && (
          <Actionsheet.Content h={600}>
            <Image
              source={image.bg_actionsSheet}
              style={{
                width: 300,
                height: 300,
                position: "absolute",
                right: 0,
              }}
              alt="image"
            />

            <View
              style={{
                width: 270,
                marginTop: 8,
                alignItems: "center",
                justifyContent: "center",
              }}>
              <Text
                style={{
                  fontSize: moderateScale(16, 0.25),
                  color: "#000",
                  fontWeight: "bold",
                  fontFamily: "Montserrat-Regular",
                  marginBottom: moderateScale(12, 0.25),
                  textAlign: "center",
                }}
                //   alignItems="flex-start"
              >
                {`Instructions for\n`}
                {_.get(selectedBank.item, "bank.name", "")}
              </Text>

              <TouchableOpacity
                onPress={() => copyClipboard()}
                style={{ flexDirection: "row" }}>
                <Text
                  style={{
                    fontSize: 14,
                    color: "#000",
                    fontWeight: "500",
                    fontFamily: "Montserrat-Regular",
                    textAlign: "center",
                    marginTop: 10,
                  }}
                  alignItems="flex-start">
                  {`Transfer money to\n`}
                  <Text
                    style={{
                      fontWeight: "bold",
                    }}>
                    {`Account number: \n`}
                    {_.get(
                      selectedBank.item,
                      "bank.fixed_base_account_number",
                      "",
                    )}
                    {_.get(selectedBank.item, "account_number", "")}
                  </Text>
                </Text>
                <Icons name="copy" color="black" size={15} />
              </TouchableOpacity>

              <TouchableOpacity onPress={() => closeActionSheet()}>
                <Text
                  style={{
                    fontSize: moderateScale(18, 0.25),
                    fontFamily: "Montserrat-Regular",
                    fontWeight: "700",
                    color: "#000",
                    marginTop: moderateScale(350),
                  }}>
                  {`Done`}
                </Text>
              </TouchableOpacity>
            </View>
          </Actionsheet.Content>
        )}
      </Actionsheet>
    </Box>
  );
};

const styles = StyleSheet.create({
  buttonInactive: {
    borderWidth: 1,
    borderColor: "#000",
    backgroundColor: "transparent",
    paddingVertical: moderateScale(10),
    paddingHorizontal: moderateScale(16),
    marginRight: moderateScale(10),
    alignItems: "center",
    justifyContent: "center",
  },
  buttonActive: {
    borderWidth: 1,
    borderColor: "rgba(16, 53, 187, 1)",
    backgroundColor: "#E8E7FF",
    paddingVertical: moderateScale(10),
    paddingHorizontal: moderateScale(16),
    marginRight: moderateScale(10),
    alignItems: "center",
    justifyContent: "center",
  },
});

VirtualAccountsList.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default VirtualAccountsList;
