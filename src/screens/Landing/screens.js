import {Text} from 'native-base';
import React from 'react';
import {image} from '../../../assets/images';

export const Header = ({children, ...props}) => (
  <Text style={{marginHorizontal: 4}} fontSize="xl" marginY="1.5" {...props} fontWeight={300}>
    {children}
  </Text>
);

export const Subheader = ({children, ...props}) => (
  <Text  style={{marginHorizontal: 4}} {...props}>{children}</Text>
);

const screenList = [
  {
    header: <Header>Train everything!</Header>,
    subheader: (
      <Subheader>
        Family account that could track your kids training progress.
      </Subheader>
    ),
    image: image.carouselBackground_1,
  },
  {
    header: <Header>Kids training Program</Header>,
    subheader: (
      <Subheader>
        Family account that could track your kids training progress.
      </Subheader>
    ),
    image: image.carouselBackground_1,
  },
  {
    header: <Header>Food & Beverages</Header>,
    subheader: (
      <Subheader>
        Family account that could track your kids training progress.
      </Subheader>
    ),
    image: image.carouselBackground_1,
  },
];

export default screenList;
