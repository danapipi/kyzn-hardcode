import React, { useRef, useState } from "react";
import { StyleSheet, Image, Platform } from "react-native";
import { getScreenWidth } from "utils/size";
import Carousel, { Pagination } from "react-native-snap-carousel";
import screens from "./screens";
import {
  Box,
  IconButton,
  Button,
  useTheme,
  ChevronRightIcon,
} from "native-base";
import { image } from "../../../assets/images";
import { getScreenHeight } from "utils/size";
import PropTypes from "prop-types";
import LinearGradient from "react-native-linear-gradient";

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },
  imageContainer: {
    width: "100%",
    height: "90%",
    alignItems: "center",
    marginTop: 50,
  },
  footerImageContainer: {
    width: "100%",
    alignItems: "flex-end",
    position: "absolute",
    zIndex: 2,
    bottom: 0,
  },
  fadeContainer: {
    width: "100%",
    height: getScreenHeight() * 0.5,
    position: "absolute",
    zIndex: 2,
    bottom: 0,
  },
  contentContainer: {
    width: "100%",
    position: "absolute",
    alignItems: "center",
    bottom: 0,
  },
  content: {
    flex: 1,
    marginBottom: 80,
    zIndex: 3,
  },
  textContainer: {
    marginHorizontal: 30,
    width: "100%",
  },
  paginationContainer: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    paddingLeft: Platform?.OS === "android" ? 10 : 1,
    paddingRight: Platform?.OS === "android" ? 10 : 1,
  },
  pagination: {
    flex: 1,
  },
  paginationDot: {
    width: 10,
    height: 10,
    borderRadius: 5,
    borderWidth: 1,
  },
  paginationButtons: {
    marginHorizontal: 15,
  },
});

const Landing = ({ navigation }) => {
  const [currentIndex, setCurrentIndex] = useState(0);
  const { colors } = useTheme();
  const hasSlides = screens.length !== 0;
  const carouselRef = useRef(null);

  return (
    <Box style={styles.flex} bgColor="white">
      <Box>
        {hasSlides && (
          <Box>
            <Carousel
              layout="default"
              ref={carouselRef}
              data={screens}
              renderItem={({ item: { image: img } }) => (
                <Box style={styles.imageContainer}>
                  <Image resizeMode="contain" source={img} alt="image" />
                </Box>
              )}
              sliderWidth={getScreenWidth()}
              onBeforeSnapToItem={index => setCurrentIndex(index)}
              itemWidth={getScreenWidth()}
              snapToAlignment="center"
              enableSnap
              loop
            />
          </Box>
        )}
        <LinearGradient
          pointerEvents="none"
          colors={["rgba(255, 255, 255, 0)", "#FFFFFF"]}
          locations={[0.0326, 0.3622]}
          style={styles.fadeContainer}
        />
      </Box>
      {hasSlides && (
        <Box style={styles.contentContainer}>
          <Box style={styles.content}>
            <Box style={styles.textContainer}>
              {screens[currentIndex].header}
              {screens[currentIndex].subheader}
            </Box>
            <Box style={styles.paginationContainer}>
              <Button
                style={styles.paginationButtons}
                variant="ghost"
                _text={{
                  fontSize: "md",
                  color: "black",
                  fontWeight: "bold",
                }}
                _pressed={{
                  _text: {
                    color: "black:alpha.20",
                  },
                  backgroundColor: "transparent",
                }}
                onPress={() => {
                  navigation.navigate("Login");
                }}>
                Skip
              </Button>
              <Pagination
                containerStyle={styles.pagination}
                dotsLength={screens.length}
                activeDotIndex={currentIndex}
                dotStyle={styles.paginationDot}
                dotColor={colors.black}
                inactiveDotColor={colors.white}
                inactiveDotStyle={styles.paginationDot}
                inactiveDotOpacity={1}
                inactiveDotScale={1}
              />
              <IconButton
                onPress={() => {
                  setTimeout(() => {
                    if (carouselRef?.current) {
                      if (currentIndex == screens.length - 1) {
                        navigation.navigate("Login");
                      } else {
                        carouselRef.current.snapToNext();
                      }
                    }
                  }, 250);
                }}
                icon={<ChevronRightIcon />}
                borderRadius="full"
                style={styles.paginationButtons}
                backgroundColor="button.orange"
                _pressed={{
                  backgroundColor: "button.orange:alpha.20",
                }}
                padding="0.5"
              />
            </Box>
          </Box>
          <Box style={styles.footerImageContainer}>
            <Image
              resizeMode="contain"
              source={image.landingFooter}
              alt="image"
            />
          </Box>
        </Box>
      )}
    </Box>
  );
};

Landing.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default Landing;
