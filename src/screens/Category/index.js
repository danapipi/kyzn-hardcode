import React, { useState, useEffect, useCallback } from "react";
import {
  Box,
  Text,
  Button,
  Actionsheet,
  useDisclose,
  FlatList,
  HStack,
} from "native-base";
import { ImageBackground , Image} from "react-native";
import PropTypes from "prop-types";
import dataDummyWellnes from "./dummyDataWellness";
import dataDummyFitness from "./dummyDataFitness";
import dataDummySport from "./dummyDataSport";
import CategoryChildCard from "components/ui/CategoryChildCard";
import { moderateScale } from "react-native-size-matters";
import { image } from "../../../assets/images";
import _ from "lodash";
import { useDispatch, useSelector } from "react-redux";
import Actions from "actions";

const Category = ({ navigation, route }) => {
  const dispatch = useDispatch();
  const data = useSelector((state) => state.PRODUCT.categoryChild);
  console.log("data category child aja", useSelector((state) => state.PRODUCT.categoryChild.categoryChild[0].wellness), data.categoryChild[0].wellness)
  const { categoryID } = route.params;
  let dataCategoryChild = categoryID === 8 ? data.categoryChild[0].wellness : categoryID === 9 ? data.categoryChild[1].sport : data.categoryChild[2].fitness;
  useEffect(() => {
    console.log("cek categoryID", categoryID);
    // dispatch(Actions.categoryChild(categoryID));
  }, []);

  const { onOpen } = useDisclose();
  const [onSelectData, setSelectedData] = useState({});
  const [isOpen, setIsOpen] = useState(false);
  const [data1, setData] = useState({});
  const [uriImage, setUriImage] = useState("");

  let dataDummy =
    categoryID === 1
      ? dataDummyWellnes
      : categoryID === 2
      ? dataDummySport
      : dataDummyFitness;

  const onClose = () => {
    setIsOpen(false);
  };

  const onPressAction = e => {
    const dataSelectedTemp = _.get(e, "item.activity_types", []);
    if(dataSelectedTemp.length != 0){
      setIsOpen(true);
    }
    setSelectedData(e);
  };

  const { 
    class_activity_type_id, 
    panorama_activity_type_id, 
    pt_activity_type_id, 
    rent_court_activity_type_id,
    wellness_category_id,
    fitness_category_id,
    sport_category_id,
  } = useSelector((state) => state.PRODUCT.activityTypeId.activityTypeId);
  

  const dataSelected = _.get(onSelectData, "item.activity_types", []);
  console.log("ajshdiqyiuiwgiqwy", data, dataSelected, uriImage);
  return (
    <Box flex={1} bg="white">
      <Box>
        <FlatList
          // data={data.categoryChild}
          data={dataCategoryChild}
          style={{ paddingTop: moderateScale(32, 0.25) }}
          renderItem={(_item) => {
            return (
              <CategoryChildCard
                navigation={navigation}
                data={_item}
                openSheet={() => onPressAction(_item)}
              />
            );
          }}
        />
      </Box>
      <Actionsheet isOpen={isOpen} onClose={onClose}>
        <Actionsheet.Content h={365}>
          <Image
            source={image.bg_actionsSheet}
            style={{ width: 300, height: 300, position: "absolute", right: 0 }}
            alt="image"
          />
          <Box style={{ alignItems: "flex-start", width: "100%", padding: 20 }}>
            <Text
              style={{
                fontSize: 16,
                color: "#000",
                fontFamily: "Montserrat-Regular",
              }}
              fontWeight="bold"
            >
              Select Services
            </Text>
          </Box>
          {dataSelected.length !== 0 ? (
            dataSelected.map((item) => {
              let arrayTemp = [];
              let imageTemp = uriImage == "" ? { uri: item.image_url } : uriImage
              console.log("asjdbiqqw image", imageTemp);
              if (item.id == rent_court_activity_type_id) {
                arrayTemp.push(
                  <Actionsheet.Item
                    onPress={() => {
                      onClose();
                      navigation.navigate("FacilityList", {
                        screen: "Home",
                        title: "Facilities",
                        params: {
                          title: item.name,
                          activityId: item.id,
                          categoryId: onSelectData.item.category_id,
                          categoryChildId: onSelectData.item.id,
                        },
                      });
                    }}
                  >
                    <HStack justifyItems="center" alignItems="center">
                      <Image
                        source={imageTemp}
                        onError={(e) => setUriImage(image.placeholder)}
                        style={{ height: 40, width: 40, marginRight: 8 }}
                        resizeMode="cover"
                        alt="image"
                      />
                      <Text
                        style={{
                          fontSize: 14,
                          color: "#000",
                          fontFamily: "Montserrat-Regular",
                        }}
                        alignItems="flex-start"
                        fontWeight="bold"
                      >
                        {item.name}
                      </Text>
                    </HStack>
                  </Actionsheet.Item>
                );
              }
              if (item.id == class_activity_type_id) {
                arrayTemp.push(
                  <Actionsheet.Item
                    onPress={() => {
                      onClose();
                      navigation.navigate("SportList", {
                        screen: "Home",
                        title: item.name,
                        params: {
                          title: item.name,
                          activityId: item.id,
                          categoryId: onSelectData.item.category_id,
                          categoryChildId: onSelectData.item.id,
                        },
                      });
                    }}
                  >
                    <HStack justifyItems="center" alignItems="center">
                      <Image
                        source={imageTemp}
                        onError={(e) => setUriImage(image.placeholder)}
                        style={{ height: 40, width: 40, marginRight: 8 }}
                        resizeMode="cover"
                        alt="image"
                      />
                      <Text
                        style={{
                          fontSize: 14,
                          color: "#000",
                          fontFamily: "Montserrat-Regular",
                        }}
                        alignItems="flex-start"
                        fontWeight="bold"
                      >
                        {item.name}
                      </Text>
                    </HStack>
                  </Actionsheet.Item>
                );
              }

              if (item.id == pt_activity_type_id) {
                arrayTemp.push(
                  <Actionsheet.Item
                    onPress={() => {
                      onClose();
                      navigation.navigate("PersonalTrainers", {
                        screen: "PersonalTrainersList",
                        params: {
                          categoryId: onSelectData.item.category_id,
                          categoryChildId: onSelectData.item.id,
                        }
                      })
                    }}
                  >
                    <HStack justifyItems="center" alignItems="center">
                      <Image
                        source={imageTemp}
                        onError={(e) => setUriImage(image.placeholder)}
                        style={{ height: 40, width: 40, marginRight: 8 }}
                        resizeMode="cover"
                        alt="image"
                      />
                      <Text
                        style={{
                          fontSize: 14,
                          color: "#000",
                          fontFamily: "Montserrat-Regular",
                        }}
                        alignItems="flex-start"
                        fontWeight="bold"
                      >
                        {item.name}
                      </Text>
                    </HStack>
                  </Actionsheet.Item>
                );
              }
              return arrayTemp;
            })
          ) : (
            <Box />
          )}
        </Actionsheet.Content>
      </Actionsheet>
    </Box>
  );
};

Category.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default Category;
