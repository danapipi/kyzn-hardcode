import React, { useState, useEffect } from "react";
import {
  Box,
  Button,
  ChevronLeftIcon,
  IconButton,
  Image,
  Input,
  Text,
} from "native-base";
import { View, TouchableOpacity, Alert } from "react-native";

import PropTypes from "prop-types";
import Actions from "actions";
import { useDispatch, useSelector } from "react-redux";
import { image } from "../../../assets/images";
import { getScreenHeight } from "utils/size";
import DividerHorizontal from "components/ui/DividerHorizontal";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { SafeAreaProvider } from "react-native-safe-area-context";
import { moderateScale } from "react-native-size-matters";
import ButtonCustom from "components/ui/ButtonCustom";
import InputField from "components/ui/InputField";
import RNStorage from "../../utils/storage";
import { UserApi } from "../../utils/Api";
import _ from "lodash";


const user = UserApi.client;

const LoginPhoneTab = ({}) => {
  const dispatch = useDispatch();
  const [pass, setPass] = useState("");
  const [showPassword, setShowPassword] = useState(false);
  const [passwordError, setPasswordError] = useState(false);
  const [passwordErrorValue, setPasswordErrorValue] = useState("");
  const [phone, setPhone] = useState("");
  const [countryCode, setCountryCode] = useState("+62");
  const [phoneError, setPhoneError] = useState(false);
  const [phoneErrorValue, setPhoneErrorValue] = useState("");
  const [loading, setLoading] = useState(false);

  const validation =
    !passwordError && !phoneError && pass !== "" && phone !== "" ? true : false;

  const changePasswordHide = () => {
    setShowPassword(!showPassword);
  };

  const checkPassword = (key, value) => {
    if (value.length < 6) {
      setPasswordError(true);
      setPasswordErrorValue("Password must be minimal 6 characters");
    } else {
      setPasswordError(false);
      setPasswordErrorValue("");
    }
  };

  console.log("phone number :", phone, countryCode)

  const checkPhone = (key, value) => {
    // if (
    //   !value.startsWith("8") ||
    //   value.match(/8[0-9 ]{6,16}$/) === null
    // ) {
    //   setPhoneError(true);
    //   setPhoneErrorValue("Invalid phone number format");
    // } else {
    //   setPhoneError(false);
    //   setPhoneErrorValue("");
    // }
  };

  const login = () => {
    // if (phone.startsWith("8")) {
      let adjustPhoneNumber = `${countryCode}${phone}`;
      const requestBody = {
        phoneNumber: adjustPhoneNumber,
        password: pass,
      };
      // closeSheet()
      setLoading(true);
      if((adjustPhoneNumber === "0851616543211" || adjustPhoneNumber === "+62851616543211") && pass === "user123"){
            RNStorage.setFirstTime("true");
          dispatch(Actions.firstTime(true));
          let profile = {
            id: 2,
            name: "user",
            username: null,
            phone_number: "+62851616543211",
            xp: 200,
            coin: 500,
            balance: 84508500,
            status: "Registered",
            birthdate: "2001-11-30T16:00:00.000Z",
            gender: "male",
            address: "Jl. Asd",
            photo: "https://api.kyzn.life/api/uploads/default-avatar.png",
            nationality: "Indonesia",
            postal_code: "213321",
            province: "Bali",
            district: "Nusa dua",
            city: "Bali",
            referral_code: "Grand Opening",
            id_card_no: null,
            instagram_username: null,
            company_name: null,
            physical_limitation: null,
            emergency_contact_name: null,
            emergency_contact_relationship: null,
            emergency_phone_number: null,
            emergency_home_number: null,
            email: null,
            rfid_card_no: null,
            created_at: "2021-09-25T01:00:00.000Z",
            updated_at: "2021-11-22T18:25:52.000Z",
            user_memberships: [
              {
                id: 24,
                user_id: 2,
                membership_id: 1,
                user_membership_package_id: null,
                is_package: false,
                status: "active",
                started_at: "2021-10-20T07:41:20.000Z",
                expired_at: "2021-10-20T07:41:20.000Z",
                is_recurring: false,
                deposit_amount: null,
                recurrence_progress: null,
                recurring_status: null,
                revoke_reason: null,
                created_at: "2021-10-20T07:41:20.000Z",
                updated_at: "2021-10-20T07:41:20.000Z",
                deleted_at: null,
                UserMembershipPackageId: null,
                membership: {
                  id: 1,
                  venue_id: 1,
                  membership_type_id: 1,
                  membership_category_id: 7,
                  name: "Non-Member",
                  image_url: null,
                  portrait_image_url: null,
                  perks_description: "Non Member",
                  duration_unit: "month",
                  duration: 120,
                  price: 0,
                  age_unit: "years",
                  min_age: 0,
                  max_age: 99,
                  available_quota: 10000,
                  quota_left: 10000,
                  publish_date: "2021-11-21T17:00:00.000Z",
                  available_until: "2021-11-21T17:00:00.000Z",
                  is_limited: true,
                  is_selling: false,
                  is_preset: true,
                  is_recurring_allowed: false,
                  deposit_amount: null,
                  recurring_amount: null,
                  interval_unit: null,
                  interval: null,
                  min_recurrence_progress: null,
                  created_at: "2021-11-21T17:00:00.000Z",
                  updated_at: null,
                  deleted_at: null,
                },
              },
            ],
            user_membership_packages: [
              {
                id: 2,
                user_id: 2,
                membership_package_id: 5,
                status: "active",
                started_at: "2021-12-01T00:00:00.000Z",
                expired_at: "2021-12-31T16:00:00.000Z",
                is_recurring: false,
                deposit_amount: null,
                recurrence_progress: null,
                recurring_status: null,
                revoke_reason: null,
                created_at: "2021-11-03T04:49:51.000Z",
                updated_at: "2021-11-03T04:49:51.000Z",
                deleted_at: null,
                membership_package: {
                  id: 5,
                  venue_id: 1,
                  membership_package_category_id: 4,
                  name: "Family Club Pass - 3 Months (2 Adults + 1 Kid)",
                  image_url:
                    "https://api.kyzn.life/api/uploads/imageUrl-1639386628951.jpg",
                  portrait_image_url:
                    "https://api.kyzn.life/api/uploads/portraitImageUrl-1639709476279.jpg",
                  price: 6615000,
                  is_selling: true,
                  is_recurring_allowed: false,
                  deposit_amount: null,
                  recurring_amount: null,
                  interval_unit: null,
                  interval: null,
                  min_recurrence_progress: null,
                  duration_unit: "month",
                  duration: 3,
                  created_at: "2021-11-26T10:06:21.000Z",
                  updated_at: "2021-12-17T02:51:16.000Z",
                  deleted_at: null,
                },
              },
            ],
            user_banks: [],
            current_level: 3,
            user_experiences: [
              {
                id: 1,
                level: 1,
                exp_needed: 0,
                created_at: "2021-11-11T09:51:36.000Z",
                updated_at: null,
                deleted_at: null,
              },
              {
                id: 2,
                level: 2,
                exp_needed: 100,
                created_at: "2021-11-11T09:51:36.000Z",
                updated_at: null,
                deleted_at: null,
              },
              {
                id: 3,
                level: 3,
                exp_needed: 200,
                created_at: "2021-11-11T09:51:36.000Z",
                updated_at: null,
                deleted_at: null,
              },
              {
                id: 4,
                level: 4,
                exp_needed: 300,
                created_at: "2021-11-11T09:51:36.000Z",
                updated_at: null,
                deleted_at: null,
              },
            ],
          }
  
          setLoading(false);
          dispatch(Actions.profileSuccess(profile));

          UserApi.setHeaderToken("VYkVCcr0Pkw1hW36Hze01YZW0jHLb4FSkTAlcK7Yf8d6WJoFSp53a0ManpdLzcaSP8IrCi7D9UewFdvr8hLL3WLxrWqrDop9UWAh");
          RNStorage.setToken("VYkVCcr0Pkw1hW36Hze01YZW0jHLb4FSkTAlcK7Yf8d6WJoFSp53a0ManpdLzcaSP8IrCi7D9UewFdvr8hLL3WLxrWqrDop9UWAh");
          dispatch(Actions.loginSuccess("VYkVCcr0Pkw1hW36Hze01YZW0jHLb4FSkTAlcK7Yf8d6WJoFSp53a0ManpdLzcaSP8IrCi7D9UewFdvr8hLL3WLxrWqrDop9UWAh"));
      } else {
        setLoading(false);
          let errorMessage = "Username or password is incorrect";
  
          Alert.alert("Error", errorMessage, [
            {
              text: "OK",
              onPress: () => console.log("OK Pressed"),
              style: "cancel",
            },
          ]);
      }
      // user
      //   .post(`/auth/login`, requestBody)
      //   .then((res) => {
      //     RNStorage.setFirstTime("true");
      //     dispatch(Actions.firstTime(true));
  
      //     setLoading(false);
      //     UserApi.setHeaderToken(res.data.token);
      //     RNStorage.setToken(res.data.token);
      //     dispatch(Actions.loginSuccess(res.data.token));
      //     console.log("login phone res: ", res);
      //   })
      //   .catch((err) => {
      //     setLoading(false);
      //     // console.log("register err: ", err.response.data.message);
      //     let errorMessage = _.get(err, "response.data.message", " ");
  
      //     Alert.alert("Error", errorMessage, [
      //       {
      //         text: "OK",
      //         onPress: () => console.log("OK Pressed"),
      //         style: "cancel",
      //       },
      //     ]);
      //   });
      // RNStorage.setFirstTime("true");
      // dispatch(Actions.firstTime(true))
      // dispatch(
      //   Actions.login({
      //     phoneNumber: adjustPhoneNumber,
      //     password: pass,
      //   })
      // );
      // dispatch(Actions.firstTime(true))
    // }
  };

  // const loading = useSelector((state) => state.AUTH.login.isLoading);
  // const error = useSelector((state) => state.AUTH.login.error);

  // useEffect(() => {
  //   if (error) {
  //     Alert.alert("Error", error, [
  //       {
  //         text: "OK",
  //         onPress: () => dispatch(Actions.reset()),
  //         style: "cancel",
  //       },
  //     ]);
  //   }
  // }, [error]);

  // for login test purpose
  // useEffect(() => {
  //   dispatch(
  //     Actions.login({
  //       phoneNumber: "085161654321",
  //       password: "user123",
  //     })
  //   );
  // },[])

  return (
    <View>
      <InputField
        placeholder={"Phone"}
        showIcon={false}
        iconValue={"phone"}
        changeCountry={(val) => {
          setCountryCode(`+${val.callingCode[0]}`);
        }}
        changeValue={(val) => {
          setPhone(val);
          checkPhone("phone", val);
        }}
        value={phone}
        error={phoneError}
        errorMessage={phoneErrorValue}
      ></InputField>
      <InputField
        placeholder={"Password"}
        isPassword={true}
        onChangeText={(val) => {
          setPass(val);
          checkPassword("password", val);
        }}
        value={pass}
        changeShowPassword={() => changePasswordHide()}
        showPassword={showPassword}
        secureTextEntry={!showPassword}
        error={passwordError}
        errorMessage={passwordErrorValue}
        showIcon={true}
        iconValue={"password"}
      ></InputField>
      {validation ? (
        <ButtonCustom
          label={"Login"}
          onPressButton={() => login()}
          // buttonMarginHorizontal={24}
          buttonMarginBottom={-10}
          buttonMarginTop={14}
          isLoading={loading}
        ></ButtonCustom>
      ) : (
        <ButtonCustom
          label={"Login"}
          disableButton={true}
          buttonColor={"#838586"}
          // buttonMarginHorizontal={24}
          buttonMarginBottom={-10}
          buttonMarginTop={14}
          // isLoading={loading}
        ></ButtonCustom>
      )}
    </View>
  );
};

export default LoginPhoneTab;
