import React, { useState, useEffect } from "react";
import {
  Box,
  Button,
  ChevronLeftIcon,
  IconButton,
  Image,
  Input,
  Text,
} from "native-base";
import { View, TouchableOpacity, Alert } from "react-native";
import PropTypes from "prop-types";
import Actions from "actions";
import { useDispatch, useSelector } from "react-redux";
import { image } from "../../../assets/images";
import { getScreenHeight } from "utils/size";
import DividerHorizontal from "components/ui/DividerHorizontal";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { SafeAreaProvider } from "react-native-safe-area-context";
import { moderateScale } from "react-native-size-matters";
import ButtonCustom from "components/ui/ButtonCustom";
import InputField from "components/ui/InputField";
import RNStorage from "../../utils/storage";
import { UserApi } from "../../utils/Api";
import _ from "lodash";



const user = UserApi.client;

const LoginUsernameTab = ({}) => {
  const dispatch = useDispatch();
  const [pass, setPass] = useState("");
  const [showPassword, setShowPassword] = useState(false);
  const [passwordError, setPasswordError] = useState(false);
  const [passwordErrorValue, setPasswordErrorValue] = useState("");
  const [username, setUsername] = useState("");
  const [usernameError, setUsernameError] = useState(false);
  const [usernameErrorValue, setUsernameErrorValue] = useState("");
  const [loading, setLoading] = useState(false);


  const validation =
    !passwordError && pass !== "" && username !== "" ? true : false;

  const changePasswordHide = () => {
    setShowPassword(!showPassword);
  };

  const checkPassword = (key, value) => {
    if (value.length < 6) {
      setPasswordError(true);
      setPasswordErrorValue("Password must be minimal 6 characters");
    } else {
      setPasswordError(false);
      setPasswordErrorValue("");
    }
  };

  const login = () => {
    // dispatch(Actions.loginUserName(username, pass));
    const requestBody = {
      username,
      password: pass,
    };
    // closeSheet()
    setLoading(true);
    user
      .post(`/auth/login-username`, requestBody)
      .then((res) => {
        RNStorage.setFirstTime("true");
        dispatch(Actions.firstTime(true));
        
        setLoading(false);
        UserApi.setHeaderToken(res.data.token);
        RNStorage.setToken(res.data.token);
        dispatch(Actions.loginSuccess(res.data.token));

        console.log("login username res: ", res);
      })
      .catch((err) => {
        setLoading(false);
        // console.log("register err: ", err.response.data.message);
        let errorMessage = _.get(err, "response.data.message", " ");

        Alert.alert("Error", errorMessage, [
          {
            text: "OK",
            onPress: () => console.log("OK Pressed"),
            style: "cancel",
          },
        ]);
      });
  };

  // const loading = useSelector((state) => state.AUTH.loginUserName.isLoading);
  // const error = useSelector((state) => state.AUTH.loginUserName.error);

  // useEffect(() => {
  //   if (error) {
  //     Alert.alert("Error", error, [
  //       {
  //         text: "OK",
  //         onPress: () => dispatch(Actions.loginUserNameReset()),
  //         style: "cancel",
  //       },
  //     ]);
  //   }
  // }, [error]);

  return (
    <View>
      <InputField
        placeholder={"Username"}
        showIcon={true}
        iconValue={"name"}
        onChangeText={(val) => {
          setUsername(val);
        }}
        value={username}
        error={usernameError}
        errorMessage={usernameErrorValue}
      ></InputField>
      <InputField
        placeholder={"Password"}
        isPassword={true}
        onChangeText={(val) => {
          setPass(val);
          checkPassword("password", val);
        }}
        value={pass}
        changeShowPassword={() => changePasswordHide()}
        showPassword={showPassword}
        secureTextEntry={!showPassword}
        error={passwordError}
        errorMessage={passwordErrorValue}
        showIcon={true}
        iconValue={"password"}
      ></InputField>
      {validation ? (
        <ButtonCustom
          label={"Login"}
          onPressButton={() => login()}
          // buttonMarginHorizontal={24}
          buttonMarginBottom={-10}
          buttonMarginTop={14}
          isLoading={loading}
        ></ButtonCustom>
      ) : (
        <ButtonCustom
          label={"Login"}
          disableButton={true}
          buttonColor={"#838586"}
          // buttonMarginHorizontal={24}
          buttonMarginBottom={-10}
          buttonMarginTop={14}
          // isLoading={loading}
        ></ButtonCustom>
      )}
    </View>
  );
};

export default LoginUsernameTab;
