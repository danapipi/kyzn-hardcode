import React, { useState } from "react";
import {
  Box,
  Button,
  ChevronLeftIcon,
  IconButton,
  Image,
  Input,
  Text,
} from "native-base";
import { View, TouchableOpacity, StyleSheet, ScrollView } from "react-native";

import PropTypes from "prop-types";
import Actions from "actions";
import { useDispatch } from "react-redux";
import { image } from "../../../assets/images";
import { getScreenHeight } from "utils/size";
import DividerHorizontal from "components/ui/DividerHorizontal";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { SafeAreaProvider } from "react-native-safe-area-context";
import { moderateScale } from "react-native-size-matters";
import ButtonCustom from "components/ui/ButtonCustom";
import InputField from "components/ui/InputField";
import LoginPhoneTab from "./LoginPhoneTab";
import LoginUsernameTab from "./LoginUsernameTab";

const Login = (props) => {
  const { navigation } = props;
  console.log("Login Props: ", props);
  const [data, useData] = useState([
    { id: 1, name: "Phone", screenName: "LoginPhoneTab" },
    { id: 2, name: "Username", screenName: "LoginUsernameTab" },
  ]);
  const [selectedIndex, setSelectedIndex] = useState(0);

  const renderTabs = () => {
    return data.map((item, index) => {
      let isSelected = selectedIndex === index;
      return (
        <TouchableOpacity
          key={`Tab${index}`}
          style={
            ([styles.tabButton],
            {
              // marginHorizontal: moderateScale(3, 0.25),
              alignItems: "center",
              paddingBottom: 10,
              width: moderateScale(150, 0.25),
            })
          }
          onPress={() => onChangeTab(item, index)}
        >
          <Text
            style={[
              styles.tabText,
              {
                color: "black",
                fontWeight: isSelected ? "700" : "600",
                fontFamily: "Montserrat-Regular",
              },
            ]}
          >
            {item.name}
          </Text>
          <View
            style={{
              position: "absolute",
              bottom: -1,
              left: 0,
              height: 2,
              width: "100%",
              alignItems: "center",
            }}
          >
            <View
              style={{
                width: moderateScale(150, 0.25),
                borderBottomWidth: isSelected ? 2 : 0,
                borderBottomColor: isSelected ? "black" : "rgba(255,255,255,0)",
              }}
            />
          </View>
        </TouchableOpacity>
      );
    });
  };

  const onChangeTab = (item, index) => {
    setSelectedIndex(index);
  };

  const renderContent = () => {
    if (selectedIndex === 0) {
      return renderLoginPhoneTab();
    } else {
      return renderLoginUsernameTab();
    }
  };

  const renderLoginPhoneTab = () => {
    return <LoginPhoneTab />;
  };

  const renderLoginUsernameTab = () => {
    return <LoginUsernameTab />;
  };

  return (
    <SafeAreaProvider
      style={{
        flex: 1,
        backgroundColor: "white",
      }}
      edges={["right", "bottom", "left"]}
    >
      <ScrollView showsVerticalScrollIndicator={false}>
        <Box style={{ marginTop: moderateScale(40, 0.25), flex: 1 }}>
          <Box
            flex={1}
            style={{
              marginHorizontal: moderateScale(24, 0.25),
              marginTop: moderateScale(75, 0.25),
              marginBottom: moderateScale(40, 0.25),
            }}
          >
            <Box
              alignItems="center"
              // style={{ marginTop: moderateScale(64, 0.25) }}
              marginBottom="5"
            >
              <Image source={image.loginLogo} alt="image" />
              <Text
                // textAlign="center"
                style={{
                  fontSize: moderateScale(16, 0.25),
                  fontWeight: "600",
                  lineHeight: moderateScale(24, 0.25),
                  marginTop: moderateScale(50, 0.25),
                  fontFamily: "Montserrat-Regular",
                  color: "black",
                  marginHorizontal: moderateScale(18),
                }}
              >
                Improve quality of life with a healthy lifestyle, starts here
              </Text>
            </Box>
            <View
              style={{
                flexDirection: "row",
                marginBottom: moderateScale(24, 0.25),
                // backgroundColor: "red",
                paddingHorizontal: moderateScale(14, 0.25),
              }}
            >
              {renderTabs()}
            </View>
            {renderContent()}
            <DividerHorizontal />
            <Box
              flexDirection="row"
              style={{ justifyContent: "center", alignItems: "center" }}
            >
              <Button
                onPress={() =>
                  navigation.navigate("SignUp", { title: "Sign Up" })
                }
                variant="ghost"
                _text={{
                  fontSize: "sm",
                  color: "black",
                  fontFamily: "Montserrat-Regular",
                }}
                _pressed={{
                  _text: {
                    color: "black:alpha.20",
                  },
                  backgroundColor: "transparent",
                }}
              >
                Create Account
              </Button>
              {/* <Button
              variant="ghost"
              _text={{
                fontSize: "sm",
                color: "black",
                fontFamily: "Montserrat-Regular",
              }}
              _pressed={{
                _text: {
                  color: "black:alpha.20",
                },
                backgroundColor: "transparent",
              }}>
              Forgot Password
            </Button> */}
            </Box>
          </Box>
          <IconButton
            position="absolute"
            top="1"
            left="1"
            icon={<ChevronLeftIcon size={12} />}
            onPress={() => navigation.goBack()}
          />
        </Box>
      </ScrollView>
    </SafeAreaProvider>
  );
};

Login.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default Login;

const styles = StyleSheet.create({
  tabButton: {
    marginHorizontal: 5,
    padding: 10,
  },
  tabText: {
    fontSize: moderateScale(12, 0.25),
    // fontWeight: "400",
    padding: 2,
    // fontFamily:'OpenSans-Light'
    // marginRight: -5,
  },
});
