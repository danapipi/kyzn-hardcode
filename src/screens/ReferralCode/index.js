import React from "react";
import {
  Share,
  View,
  Text,
  TouchableOpacity,
  Image,
  Platform,
} from "react-native";
import { image } from "images";
import { moderateScale } from "react-native-size-matters";
import { useDispatch, useSelector } from "react-redux";
import Selectors from "selectors";
import axios from "axios";

const dynamicLinkGenerator =
  "https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=AIzaSyCV5WJi94zL0b6dhdnbaGjFcEuCepQho04";

const ReferralCode = () => {
  const profile = useSelector(Selectors.getProfile);

  // just generate to test
  const buildDynamicLinkDummy = () => {
    return new Promise(async (resolve, reject) => {
      try {
        const params = {
          dynamicLinkInfo: {
            domainUriPrefix: "https://kyzn.page.link",
            link: `https://kyzn.page.link/app/?link=kyzn://referal?code=${profile.referral_code}`,
            androidInfo: {
              androidPackageName: "com.kyzn.kyznapps",
            },
            iosInfo: {
              iosBundleId: "com.kyzn.kyznapps",
              iosAppStoreId: "1596314646",
            },
          },
        };

        const config = {
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
          },
        };

        const generator = await axios?.post(
          dynamicLinkGenerator,
          params,
          config,
        );
        console.log("generator: ", generator);
        if (generator?.data?.shortLink) {
          resolve(generator?.data?.shortLink);
        } else {
          resolve(null);
        }
      } catch (error) {
        console.log("Error Create Dynamic Link: ", error);
        reject(error);
      }
    });
  };

  const onShare = async () => {
    try {
      const generateDynamicLink = await buildDynamicLinkDummy();
      console.log("generateDynamicLink: ", generateDynamicLink);

      if (generateDynamicLink) {
        const iosOption = {
          title: "Code",
          message: `Get Rewards For Every New User You Refer \n\nShare your referral code stay tuned for your rewards *${profile?.referral_code}*`,
          url: generateDynamicLink,
        };
        const androidOption = {
          title: "Code",
          message: `Get Rewards For Every New User You Refer \n\nShare your referral code stay tuned for your rewards ${profile?.referral_code} \n\n${generateDynamicLink}`,
        };
        const options = (Platform.OS = "android" ? androidOption : iosOption);
        const result = await Share.share(options);
        console.log("result: ", result);

        if (result.action === Share.sharedAction) {
          if (result.activityType) {
            // shared with activity type of result.activityType
          } else {
            // shared
          }
        } else if (result.action === Share.dismissedAction) {
          // dismissed
        }
      } else {
        throw new Error("Failed generate dynamic link referal");
      }
    } catch (error) {
      console.log("Error Share: ", error);
    }
  };

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: "white",
        paddingHorizontal: moderateScale(16),
        alignItems: "center",
      }}>
      <Image
        source={image.referral_img}
        resizeMode="cover"
        style={{
          width: moderateScale(250, 0.25),
          height: moderateScale(180, 0.25),
          marginTop: moderateScale(24),
          marginBottom: moderateScale(12),
        }}
      />
      <Text
        style={{
          color: "black",
          fontSize: moderateScale(16, 0.25),
          fontFamily: "Montserrat-Regular",
          fontWeight: "700",
          textAlign: "center",
          marginBottom: moderateScale(10),
        }}>
        {"Get Rewards \nFor Every New User You Refer"}
      </Text>
      <Text
        style={{
          color: "black",
          fontSize: moderateScale(12, 0.25),
          fontFamily: "Montserrat-Regular",
          fontWeight: "500",
          textAlign: "center",
          lineHeight: moderateScale(18, 0.25),
          marginBottom: moderateScale(32),
        }}>
        {"Share your referral code and\nstay tuned for your rewards!"}
      </Text>
      <View
        style={{
          flexDirection: "row",
          backgroundColor: "#E8E7FF",
          width: moderateScale(296),
          height: moderateScale(60),
          alignItems: "center",
          borderStyle: "dashed",
          borderWidth: 1,
          borderColor: "#6E75FF",
          justifyContent: "space-around",
        }}>
        <View style={{ flexDirection: "row", alignItems: "center" }}>
          <Image
            source={image.ic_copy}
            resizeMode="cover"
            style={{
              width: moderateScale(24, 0.25),
              height: moderateScale(24, 0.25),
            }}
          />
          <Text
            style={{
              color: "black",
              fontSize: moderateScale(16, 0.25),
              fontFamily: "Montserrat-Regular",
              fontWeight: "700",
              textAlign: "center",
              lineHeight: moderateScale(18, 0.25),
              marginLeft: moderateScale(10),
            }}>
            {profile.referral_code}
          </Text>
        </View>
        <TouchableOpacity
          onPress={onShare}
          style={{
            backgroundColor: "#1035BB",
            width: moderateScale(71),
            height: moderateScale(24),
            justifyContent: "center",
          }}>
          <Text
            style={{
              color: "white",
              fontSize: moderateScale(14, 0.25),
              fontFamily: "Montserrat-Regular",
              fontWeight: "600",
              textAlign: "center",
              lineHeight: moderateScale(18, 0.25),
            }}>
            Share
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default ReferralCode;
