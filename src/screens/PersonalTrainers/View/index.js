import {
  Box,
  Button,
  Column,
  FlatList,
  HStack,
  Modal,
  Row,
  ScrollView,
  Text,
  VStack,
  Flex,
  Select,
  CheckIcon,
} from "native-base";
import _get from "lodash/get";
import _isEmpty from "lodash/isEmpty";
import _isNull from "lodash/isNull";
import _filter from "lodash/filter";
import _orderBy from "lodash/orderBy";
import _ from "lodash";
import React, { useState, useCallback, useEffect, useRef } from "react";
import { moderateScale } from "react-native-size-matters";
import { image } from "images";
import { UserApi } from "../../../utils/Api";
import {
  Dimensions,
  StyleSheet,
  TouchableOpacity,
  Image,
  DatePickerIOSBase,
  Platform,
  Alert,
} from "react-native";
import PersonalTrainerCard from "components/ui/PersonalTrainerCard";
import Commons from "utils/common";
import Actions from "actions";
import { useDispatch, useSelector } from "react-redux";
import moment from "moment";

import dummyHours from "../../Sports/dummyData/dummyHours.json";

const userapi = UserApi.client;

const PersonalTrainerView = ({ navigation, route: { params } }) => {
  const dispatch = useDispatch();
  // const data = params.data
  const [data, setData] = useState(params.data);
  console.log("data pt", data);
  const dataUniqueFamily = useSelector(state => state.FAMILY.uniqueFamily);
  const { id, name, rating, activity_workers } = data;
  const [isTrainingModalOpen, setIsTrainingModalOpen] = useState(false);
  // const [selectedTraining, setSelectedTraining] = useState(filterTrainingItem(activity_workers)[0]);
  const [selectedTraining, setSelectedTraining] = useState(activity_workers[0]);
  const [totalPrice, setTotalPrice] = useState(350000);
  const member = _get(selectedTraining, "members", dataUniqueFamily.data);
  const [showAfterBookMessage, setShowAfterBookMessage] = useState(false);
  const [textShown, setTextShown] = useState(false); //To show ur remaining Text
  const [lengthMore, setLengthMore] = useState(false);
  const [profilePic, setProfilePic] = useState("");

  const [rangeHours, setRangeHour] = useState(moment().format("HH:mm"));
  const [selectHours, setSelectHours] = useState(1);
  const [selectListHours, setSelectListHours] = useState(null);
  const [selectedUser, setSelectedUser] = useState(_.get(params, "userId", "")); // TODO: IMPORTANT! PASS userId PARAMS FROM PREV SCREEN TO GET THE PT PRICE
  const [onSelectedDate, setSelectedDate] = useState(
    moment().format("dddd, DD MMMM yyyy"),
  );
  const [dataTime, setDataTime] = useState([]);
  const [dateToGo, setDate] = useState([]);

  const [bottomHeight, setBottomHeight] = useState(0);

  const [availableTimes, setAvailableTimes] = useState([]);

  const count = useRef(0);

  const profile = useSelector(state => state.AUTH.profile);

  const {
    class_activity_type_id,
    panorama_activity_type_id,
    pt_activity_type_id,
    rent_court_activity_type_id,
  } = useSelector(state => state.PRODUCT.activityTypeId.activityTypeId);

  // const userId = profile.data.id
  const userId = _.get(params, "userId", profile.data.id);

  const coachId = _get(params, "id", 7); // TODO: use THIS coachId to get the data
  // const coachId = 7 // dummy coachId

  console.log("params: ", params);
  console.log("dataUniqueFamily: ", dataUniqueFamily);

  const activityType = _get(params, "type", 6); // 6 is PT

  const dataActivityById = useSelector(state => state.PRODUCT.activityById);

  // let availableTimes = _get(dataActivityById.activityByID, `actvitiy_courts[0].available_times`, null)
  // let availableTimes = availableTimes

  const onTextLayout = useCallback(e => {
    setLengthMore(e.nativeEvent.lines.length >= 3);
  }, []);

  const toggleNumberOfLines = () => {
    //To toggle the show text or hide it
    setTextShown(!textShown);
  };

  const closetrainingModal = useCallback(() => {
    setIsTrainingModalOpen(false);
  }, []);

  const hourPress = item => {
    setSelectHours(item.id);
    // dispatch(Actions.activityById(activityType, activityType, moment(onSelectedDate).format('YYYY-MM-DD'), item.id))
  };

  const handlePressListHour = (item, index) => {
    console.log("hours: ", item, index);
    setSelectListHours(index);
    setRangeHour(item);
  };

  const populateTrainerTypes = worker_activity => {
    let activity_data = [];

    worker_activity.map(item => {
      const activity = item.activity;
      console.log("activity: ", activity);

      activity_data.push(activity.name);
    });

    const firstActivity = activity_data[0];
    const otherActivity = activity_data.slice(0, 0);

    console.log("activity_data: ", activity_data);
    console.log("firstActivity: ", firstActivity);
    console.log("otherActivity: ", otherActivity);

    if (otherActivity.length > 0) {
      return `${firstActivity} & ${otherActivity.length} Others`;
    } else {
      return `${firstActivity}`;
    }
  };

  const determineAvailableTimes = range_times => {
    const one_hour = [
      "07:00 - 07:55",
      "08:00 - 08:55",
      "09:00 - 09:55",
      "10:00 - 10:55",
      "11:00 - 11:55",
      "12:00 - 12:55",
      "13:00 - 13:55",
      "14:00 - 14:55",
      "15:00 - 15:55",
      "16:00 - 16:55",
      "17:00 - 17:55",
      "18:00 - 18:55",
      "19:00 - 19:55",
    ];
    const two_hour = [
      "07:00 - 08:55",
      "09:00 - 10:55",
      "11:00 - 12:55",
      "13:00 - 14:55",
      "15:00 - 16:55",
      "17:00 - 18:55",
      "19:00 - 20:55",
    ];
    const three_hour = [
      "07:00 - 09:55",
      "10:00 - 12:55",
      "13:00 - 15:55",
      "16:00 - 18:55",
    ];
    const four_hour = ["07:00 - 10:55", "11:00 - 14:55", "15:00 - 18:55"];

    switch (range_times) {
      case 1:
        return one_hour;
      case 2:
        return two_hour;
      case 3:
        return three_hour;
      case 4:
        return four_hour;

      default:
        break;
    }
  };

  async function determineDiffDuration(hour, callback) {
    const settingDestinationTime = moment().set({
      hour: hour,
      minute: 0,
    });

    const timeDiff = moment
      .duration(moment(moment(), "HH:mm").diff(settingDestinationTime, "HH:mm"))
      .asHours();

    await callback(timeDiff);
  }

  async function populateAvailableTimesCompareToNow() {
    const hoursAvailable = determineAvailableTimes(selectHours);

    if (
      moment(onSelectedDate).format("dddd, DD MMMM yyyy") ===
      moment().format("dddd, DD MMMM yyyy")
    ) {
      // this way can run the asynchronous function very smoothly. see here https://stackoverflow.com/a/49499491
      await hoursAvailable.reduce(async (promise, item) => {
        await promise;

        const hour = item.split("-")[0].split(":")[0];

        await determineDiffDuration(hour, timeDiff => {
          if (timeDiff > 0) {
            hoursAvailable.splice(0, 1);
          }
        });
      }, Promise.resolve());

      setAvailableTimes(hoursAvailable);
    } else {
      setAvailableTimes(hoursAvailable);
    }
  }

  const populateCoachData = () => {
    console.log("gaaaaaaaaa", moment(onSelectedDate).format("YYYY-MM-DD"));
    console.log("gaaaaaaaaa", selectHours);

    populateAvailableTimesCompareToNow();
  };

  const changeDate = date => {
    let dateSelect = moment(date).format("dddd, DD MMMM yyyy");
    setSelectedDate(dateSelect);
    console.log(
      "askdjasdlas",
      `/worker/${params.id}?date=${moment(date).format(
        "YYYY-MM-DD",
      )}&stepMultiplier=${selectHours}`,
    );

    populateAvailableTimesCompareToNow();
  };

  const displayDayFacility = () => {
    let days = [];
    let m = 0;
    console.log("gahgehgah", selectedTraining.furthest_booking_date);
    let end_date = moment().add(7, "days");
    console.log("END_DATE", end_date);
    let diff = 7;
    console.log("END_DATE DIFF", diff);
    console.log(
      "dataActivityById.activityByID: ",
      selectedTraining.furthest_booking_date,
    );

    while (m < diff) {
      let date = moment().add(m, "days");
      console.log("date nya: ", date);
      if (date !== null) {
        if (moment(date).format("DD MM yy") == moment().format("DD MM yy")) {
          console.log("asjaasjiu date 1", date);
          days.push({ date: "Today", actualDate: date });
        } else if (
          moment(date).format("DD MM yy") ==
          moment().add(1, "days").format("DD MM yy")
        ) {
          // console.log("asjaasjiu date 2", date);
          days.push({ date: "Tomorrow", actualDate: date });
        } else {
          // console.log("asjaasjiu date 3", date);
          days.push({
            date: moment(date).format("ddd, MMM DD"),
            actualDate: date,
          });
        }
      }
      m++;
    }

    console.log("daysdaysdaysdays", days);

    let daysTemp = _orderBy(days, ["actualDate"], ["asc"]);
    setDate(daysTemp);
  };

  const onSelectTraining = training => {
    console.log("onSelectTraining: ", training);
    setSelectedTraining(training);
    setIsTrainingModalOpen(false);
    // dispatch(Actions.activityById(selectedTraining.activity_id, selectedTraining.activity_id, moment(onSelectedDate).format('YYYY-MM-DD'), selectHours))
    dispatch(
      Actions.activityById(
        _get(training, "activity_courts[0].activity_id", training.activity_id),
        _get(training, "activity_courts[0].activity_id", training.activity_id),
        moment(onSelectedDate).format("YYYY-MM-DD"),
        selectHours,
      ),
    );
  };

  const bookPT = () => {
    const activityID =
      selectedTraining.activity.category_child_activities[0].activity_id;
    const userID = selectedUser;
    const workerId = data.id;
    const duration = selectHours * 60;
    const startTime = `${onSelectedDate} ${rangeHours.split("-")[0]}`;
    const status = "active";

    const requestBody = {
      userId: userID,
      activityId: activityID,
      workerId: workerId,
      startTime: startTime,
      duration: duration,
      status: status,
    };

    console.log("requestBody: ", requestBody);

    navigation.navigate("Payment", {
      data: params.data,
      activity: selectedTraining.activity,
      selectedUser: selectedUser,
      coupons: selectedTraining.coupons,
      timeDuration: duration,
      multipler: selectHours,
      activityWorkerId: selectedTraining.id,
      // price: (selectHours) * totalPrice,
      price: totalPrice,
      startTime: startTime,
      hours: rangeHours,
      type: 6, // is a PT type
      best_coupon_id: selectedTraining.best_coupon_id,
    });

    // userapi.post(`/activity-booking/coach`, requestBody)
    //   .then(res => {
    //     console.log("/activity-booking/coach res: ", res);
    //     // TODO: add a modal telling user that the booking is done
    //     navigation.navigate("Home")
    //   })
    //   .catch(err => {
    //     console.log("/activity-booking/coach err: ", err);

    //   })
  };

  const filterTrainingItem = activity_workers => {
    activity_workers.filter(item => {
      return item.activity.activity_type_id === 6;
    });
  };

  useEffect(() => {
    displayDayFacility();
  }, [selectedTraining]);

  useEffect(() => {
    console.log("data passed: ", data);
    console.log("selectedTraining: ", selectedTraining);
    populateAvailableTimesCompareToNow();
    populateCoachData();
    dispatch(
      Actions.activityById(
        selectedTraining.activity_id,
        selectedTraining.activity_id,
        moment(onSelectedDate).format("YYYY-MM-DD"),
        selectHours,
      ),
    );
    // setSelectedUser(member[0].id)
  }, []);

  useEffect(() => {
    populateAvailableTimesCompareToNow();
  }, [onSelectedDate, selectHours, selectedUser]);

  let imageTemp = profilePic == "" ? { uri: params.data.photo } : profilePic;

  return (
    <Box safeArea backgroundColor="white" flex={1}>
      <ScrollView
        style={{
          backgroundColor: "white",
          marginBottom:
            Platform.OS === "ios" ? moderateScale(120, 0.25) : bottomHeight,
        }}
        // paddingBottom={50}
      >
        <Box flex={1} alignItems="center">
          <Image
            // source={{ uri: "https://via.placeholder.com/103x104" }}
            source={imageTemp}
            onError={err => {
              setProfilePic(image.img_product_placeholder);
            }}
            style={{
              width: moderateScale(90, 0.5),
              height: moderateScale(90, 0.5),
            }}
            borderRadius={10}
          />
          <Row>
            <Text fontWeight={300}>{name}</Text>
            <Row alignItems="center" marginLeft={2}>
              <Image
                source={image.star}
                style={{
                  width: moderateScale(10, 0.5),
                  height: moderateScale(10, 0.5),
                  marginRight: moderateScale(2, 0.5),
                }}
              />
              <Text
                fontWeight={300}
                style={{
                  color: "black",
                  fontSize: moderateScale(10, 0.5),
                }}>
                {rating}
              </Text>
            </Row>
          </Row>
          <Text fontWeight={200} color="gray">
            {populateTrainerTypes(activity_workers)}
          </Text>
        </Box>

        {
          // showAfterBookMessage == true && (
          //   <Box style={{
          //     width: Dimensions.get("screen").width * .9,
          //     paddingVertical: moderateScale(9, .25),
          //     justifyContent: 'center',
          //     alignItems: "center",
          //     alignSelf: "center",
          //     borderRadius: 8,
          //     backgroundColor: "#FFFEEA",
          //     marginBottom: moderateScale(16, .25),
          //     borderWidth: 1,
          //     borderColor: "#FDECB4"
          //   }}>
          //     <Text style={{
          //       fontSize: moderateScale(10, .25),
          //       color: "#4754FE",
          //       fontWeight: "600",
          //       fontFamily: "Montserrat-Regular",
          //       textAlign:"center",
          //     }}>
          //       {`Personal Trainer will send you zoom link via whatsapp`}
          //     </Text>
          //   </Box>
          // )
        }

        {/* <Box style={{
          width: '100%',
          paddingVertical: moderateScale(9, .25),
          justifyContent: 'center',
          alignItems: "center",
          backgroundColor: "#E8E7FF"
        }}>
          <Text style={{
            fontSize: moderateScale(10, .25),
            color: "#4754FE",
            fontWeight: "600",
            fontFamily: "Montserrat-Regular",
            textAlign:"center",
          }}>
            {`Active membership is applied`}
          </Text>
        </Box> */}
        {activityType === pt_activity_type_id && (
          <Box flex={1}>
            <HStack
              justifyContent="flex-start"
              alignItems="center"
              marginX={moderateScale(16)}>
              <TouchableOpacity>
                <Box
                  justifyContent={"flex-end"}
                  alignItems={"center"}
                  paddingY={moderateScale(8, 0.25)}>
                  <Image
                    source={require("../../../../assets/images/ic-dates.png")}
                    style={{ height: 24, width: 24, marginRight: 16 }}
                    resizeMode="cover"
                    alt="image"
                  />
                </Box>
              </TouchableOpacity>
              <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                {dateToGo.map(item => {
                  let isSelectDate =
                    moment(item.actualDate).format("DD MM yy") ==
                    moment(onSelectedDate).format("DD MM yy");
                  console.log("aksugdiau", isSelectDate);
                  return (
                    <VStack
                      style={{
                        marginRight: 16,
                        justifyContent: "center",
                        alignItems: "center",
                      }}>
                      <TouchableOpacity
                        onPress={() => changeDate(item.actualDate)}
                        style={{ display: "flex", flexDirection: "row" }}>
                        <Text
                          style={{
                            fontSize: 12,
                            color: isSelectDate ? "#000" : "#757575",
                            fontWeight: "bold",
                          }}>
                          {item.date}
                        </Text>
                        {moment(item.date).isBetween(
                          _.get(
                            selectedTraining,
                            "non_member_furthest_booking_date",
                          ),
                          _.get(
                            selectedTraining,
                            "member_furthest_booking_date",
                          ),
                        ) && (
                          <Image
                            style={{
                              width: 16,
                              height: 16,
                              marginLeft: 8,
                              marginTop: 4,
                            }}
                            source={require("../../../../assets/images/crown.png")}
                            alt="image"
                            resizeMode="cover"
                          />
                        )}
                      </TouchableOpacity>
                      {isSelectDate && (
                        <Box
                          style={{
                            height: 2,
                            backgroundColor: "#212121",
                            position: "absolute",
                            bottom: 0,
                            width: 45,
                          }}
                        />
                      )}
                    </VStack>
                  );
                })}
              </ScrollView>
            </HStack>
            <Box
              style={{
                width: Dimensions.get("screen").width,
                borderColor: "#ECEDEF",
                borderWidth: 1,
              }}
            />
          </Box>
        )}
        {activityType == pt_activity_type_id &&
          _isNull(availableTimes) == false && (
            <Box
              flex={1}
              marginX={moderateScale(16, 0.25)}
              marginTop={moderateScale(24, 0.25)}>
              <FlatList
                data={dummyHours}
                horizontal
                showsHorizontalScrollIndicator={false}
                renderItem={({ item }) => (
                  <TouchableOpacity
                    onPress={() => {
                      hourPress(item);
                    }}>
                    <Box
                      style={{
                        borderWidth: 1,
                        paddingHorizontal: 18,
                        paddingVertical: 8,
                        backgroundColor:
                          item.id == selectHours ? "#000" : "transparent",
                      }}>
                      <Text
                        style={{
                          fontFamily: "Montserrat-Regular",
                          fontWeight: selectHours == item.id ? "600" : "700",
                          fontSize: 14,
                          color: item.id == selectHours ? "#FFF" : "#000",
                        }}>
                        {item.title}
                      </Text>
                    </Box>
                  </TouchableOpacity>
                )}
              />

              <FlatList
                data={availableTimes}
                horizontal
                showsHorizontalScrollIndicator={false}
                contentContainerStyle={{ marginTop: moderateScale(20, 0.25) }}
                renderItem={({ item, index }) => (
                  <TouchableOpacity
                    onPress={() => {
                      handlePressListHour(item, index);
                    }}>
                    <Box
                      style={{
                        borderWidth: 1,
                        paddingHorizontal: 18,
                        paddingVertical: 17,
                        backgroundColor:
                          index == selectListHours ? "#000" : "transparent",
                      }}>
                      <Text
                        style={{
                          fontFamily: "Montserrat-Regular",
                          fontWeight: selectListHours == index ? "600" : "700",
                          fontSize: 14,
                          color: index == selectListHours ? "#FFF" : "#000",
                        }}>
                        {item}
                      </Text>
                    </Box>
                  </TouchableOpacity>
                )}
              />
            </Box>
          )}

        <VStack style={{ padding: 20, paddingVertical: 10 }}>
          <Text
            onTextLayout={onTextLayout}
            numberOfLines={textShown ? undefined : 3}
            style={{ lineHeight: 21, color: "#445159", fontSize: 12 }}>
            {selectedTraining.activity.description}
          </Text>
          {lengthMore ? (
            <TouchableOpacity onPress={toggleNumberOfLines}>
              <Box
                style={{
                  backgroundColor: "#EBEEF4",
                  width: 96,
                  height: 24,
                  justifyContent: "center",
                  alignItems: "center",
                  marginTop: 16,
                  borderRadius: 4,
                }}>
                <Text style={{ lineHeight: 21, fontSize: 12, color: "#000" }}>
                  {textShown ? "Show less" : "Show more"}
                </Text>
              </Box>
            </TouchableOpacity>
          ) : null}
        </VStack>

        {/* <Box marginX={5}>
          <PersonalTrainerCard label={"Personal Trainer you might like"} />
        </Box> */}
      </ScrollView>
      <HStack
        width="100%"
        padding={5}
        position="absolute"
        borderTopWidth={2}
        borderColor="gray:alpha.25"
        backgroundColor="white"
        bottom={0}
        onLayout={e => {
          console.log("e nya PT: ", e.nativeEvent.layout.height);
          if (Platform.OS == "android") {
            setBottomHeight(e.nativeEvent.layout.height);
          }
        }}>
        <VStack>
          <Column>
            <Text color="gray" fontSize="xs">
              {/* {`Today • ${rangeHours.split("-")[0]}`} */}
              {`${
                moment(onSelectedDate).format("DD MM yy") ==
                moment().add(1, "days").format("DD MM yy")
                  ? `Tomorrow`
                  : moment(onSelectedDate).format("DD MM yy") ==
                    moment().format("DD MM yy")
                  ? `Today`
                  : moment(onSelectedDate).format("ddd, MMM DD")
              } • ${rangeHours.split("-")[0]}`}
            </Text>
            <Text fontWeight={300}>{`${Commons.convertToRupiah(
              // (selectHours) * selectedTraining.price,
              totalPrice,
            )}`}</Text>
          </Column>
        </VStack>
        <Column flex={1} />
        <Button
          onPress={() => {
            if (member.length === 0) {
              Alert.alert("No users allowed to book this activity");
            } else {
              bookPT();
              setShowAfterBookMessage(true);
            }
          }}
          borderRadius={0}
          backgroundColor="button.blue"
          _text={{ paddingX: 6 }}
          _pressed={{
            backgroundColor: "button.blue:alpha.20",
          }}>
          BOOK
        </Button>
      </HStack>

      <Modal
        isOpen={isTrainingModalOpen}
        onClose={closetrainingModal}
        margin={0}>
        <Modal.Content>
          <Modal.Header>
            <Text fontWeight={300}>Select Training</Text>
          </Modal.Header>
          <Modal.Body>
            {activity_workers.map(training => (
              <TouchableOpacity
                onPress={() => {
                  onSelectTraining(training);
                }}>
                <Box
                  borderColor="gray:alpha.25"
                  borderWidth={2}
                  borderRadius={10}
                  padding={2}
                  marginY={1}>
                  <Text fontWeight={300} color="gray">
                    {training.activity.name}
                  </Text>
                  <Text fontWeight={300}>{`IDR ${Commons.convertToRupiah(
                    training.price,
                  )}`}</Text>
                  <Text marginTop={3}>
                    {`Max ${_get(
                      training,
                      "activity.max_participants",
                      5,
                    )} persons`}
                  </Text>
                </Box>
              </TouchableOpacity>
            ))}
          </Modal.Body>
        </Modal.Content>
      </Modal>
    </Box>
  );
};

const styles = StyleSheet.create({
  daySelected: {
    height: "100%",
    paddingHorizontal: moderateScale(4),
    justifyContent: "center",
    borderBottomColor: "#212121",
    // backgroundColor: 'blue',
    borderBottomWidth: 3,
  },
  dayNotSelected: {
    height: "100%",
    paddingHorizontal: moderateScale(4),
    justifyContent: "center",
    borderBottomColor: "transparent",
    // backgroundColor: 'blue',
    borderBottomWidth: 3,
  },
});

export default PersonalTrainerView;
