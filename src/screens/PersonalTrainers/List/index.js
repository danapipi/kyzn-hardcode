import {
  Box,
  Button,
  Column,
  FlatList,
  HamburgerIcon,
  IconButton,
  Image,
  Input,
  Modal,
  Row,
  SearchIcon,
  Text,
  Actionsheet,
  Select,
  CheckIcon,
} from "native-base";
import _findIndex from "lodash/findIndex";
import _get from "lodash/get";
import _minBy from "lodash/minBy";
import React, { useState, useEffect, useCallback, useRef } from "react";
import Commons from "utils/common";
import { UserApi } from "../../../utils/Api";
import ButtonCustom from "../../../components/ui/ButtonCustom";
import { moderateScale } from "react-native-size-matters";
import { image } from "images";
import DividerHorizontal from "components/ui/DividerHorizontal";
import {
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  ScrollView,
  ActivityIndicator,
  Image as ImgReact,
} from "react-native";
import SearchBar from "../../../components/ui/SearchBar";
import { isNull, stubFalse } from "lodash";
import { useDispatch, useSelector } from "react-redux";
import Actions from "actions";

const userapi = UserApi.client;

const TOP_FILTER = [
  {
    id: 1,
    name: "All",
  },
  {
    id: 8,
    name: "Wellness",
  },
  {
    id: 9,
    name: "Sports",
  },
  {
    id: 10,
    name: "Fitness",
  },
];

const DUMMY_FILTER = [
  {
    id: 1,
    name: "Level",
    filter_type: [
      { id: 10, name: "Medium" },
      { id: 20, name: "Expert" },
    ],
  },
  {
    id: 2,
    name: "Experience",
    filter_type: [
      { id: 10, name: "1-3 Years" },
      { id: 20, name: "5 Years" },
      { id: 30, name: "8 Years" },
    ],
  },
  {
    id: 3,
    name: "Capability",
    filter_type: [
      { id: 10, name: "Pound Fit 1on1" },
      { id: 20, name: "Cardio" },
      { id: 30, name: "Pilates" },
      { id: 40, name: "Functional Training" },
      { id: 50, name: "Fat Loss" },
      { id: 60, name: "Virtual Trainer" },
    ],
  },
];

const PersonalTrainersList = ({ navigation, route }) => {
  const dispatch = useDispatch();
  const dataUniqueFamily = useSelector(state => state.FAMILY.uniqueFamily);
  const dataWorker = useSelector(state => state.WORKER.featuredWorker.worker);

  const [isFilterModalOpen, setIsFilterModalOpen] = useState(false);
  const [searchText, setSearchText] = useState("");
  const [filterItems, setFilterItems] = useState(DUMMY_FILTER);
  const [dataCoach, setDataCoach] = useState(dataWorker);
  const [selectedFilterId, setSelectedFilterId] = useState(0);
  const [selectedIdForTopFilter, setSelectedIdForTopFilter] = useState(1);
  const [additionalFilterData, setAdditionalFilterData] = useState([]);
  const [idOfAdditionalFilter, setIdOfAdditionalFilter] = useState(null);
  const [subCategoryLoading, setSubCategoryLoading] = useState(false);
  // const [selectedUser, setSelectedUser] = useState(dataUniqueFamily.data[0].id)
  const [selectedUser, setSelectedUser] = useState("all");

  const categoryChildIdFilter = useRef(null);
  const categoryIdFilter = useRef(null);

  const filterId = useRef(0);

  const {
    class_activity_type_id,
    panorama_activity_type_id,
    pt_activity_type_id,
    rent_court_activity_type_id,
    wellness_category_id,
    fitness_category_id,
    sport_category_id,
  } = useSelector(state => state.PRODUCT.activityTypeId.activityTypeId);

  const filteredData = dataCoach.filter(
    (
      { name }, // for the coach
    ) => name.toLowerCase().includes(searchText.toLowerCase()),
  );

  const [showFilterAS, setShowFilterAS] = useState(false);

  const isFromHome =
    _get(route.params, "categoryId", null) == null &&
    _get(route.params, "categoryChildId", null) == null;

  // const categoryId = _get(route.params, "categoryId", null)
  // const categoryChildId = _get(route.params, "categoryChildId", null)

  const [categoryId, setCategoryId] = useState(
    _get(route.params, "categoryId", null),
  );
  const [categoryChildId, setCategoryChildId] = useState(
    _get(route.params, "categoryChildId", null),
  );
  const [uriImage, setUriImage] = useState("");

  const categoryData = useSelector(state => state.PRODUCT.categoryChild);

  const populateTrainerTypes = worker => {
    let activity_data = [];

    worker.map(item => {
      const activity = item.activity;
      console.log("activity: ", activity);

      activity_data.push(activity.name);
    });

    const firstActivity = activity_data[0];
    const otherActivity = activity_data.slice(0, 0);

    console.log("activity_data: ", activity_data);
    console.log("firstActivity: ", firstActivity);
    console.log("otherActivity: ", otherActivity);
    if (otherActivity.length > 0) {
      return `${firstActivity} & ${otherActivity.length} Others`;
    } else {
      return `${firstActivity}`;
    }
  };

  const changeUser = async id => {
    // user id
    setSelectedUser(id);
    if (isFromHome == true) {
      // setCategoryChildId(null)
      // setCategoryId(null)
    }

    let url = `/worker/`;

    if (isFromHome == true) {
      if (id == "all") {
        if (selectedIdForTopFilter == 1) {
          // top filter == all
          url = `/worker/?userId=${id}`;
        } else {
          // if(isNull(categoryChildId) == false) {
          //   url = `/worker/?categoryId=${selectedIdForTopFilter}&categoryChildId=${categoryChildId}`
          // } else {
          // url = `/worker/?categoryId=${selectedIdForTopFilter}`
          url = `/worker/?categoryId=${categoryId}&userId=${id}`;
          // }
        }
      } else {
        if (selectedIdForTopFilter == 1) {
          url = url + `?userId=${id}`;
        } else {
          url = url + `?categoryId=${selectedIdForTopFilter}&userId=${id}`;
        }

        // if(isNull(categoryChildId) == true) {
        //   if(isNull(categoryId) == true) {
        //     if(selectedIdForTopFilter == 1) {
        //       url = url + `?userId=${id}`
        //     } else {
        //       url = url + `?workerClassificationId=${selectedIdForTopFilter}&userId=${id}`
        //     }
        //   } else {
        //     if(categoryId == 1 || selectedIdForTopFilter == 1) { // the top filter is "all"
        //       // url = url + `?categoryId=${selectedIdForTopFilter}&userId=${id}`
        //       url = url + `?userId=${id}`
        //     } else {
        //       url = url + `?categoryId=${selectedIdForTopFilter}&userId=${id}`
        //     }
        //   }
        // } else {
        //   if(selectedIdForTopFilter == 1) {
        //     url = url + `?categoryId=${selectedIdForTopFilter}&categoryChildId=${categoryChildId}&userId=${id}`
        //   } else {
        //     url = url + `?workerClassificationId=${selectedIdForTopFilter}&categoryId=${selectedIdForTopFilter}&categoryChildId=${categoryChildId}&userId=${id}`
        //   }
        // }
      }
      // url = url + `?workerClassificationId=${selectedIdForTopFilter}&userId=${id}`
      // if(isNull(categoryId) == true && isNull(categoryChildId) == true) {
      //   url = url + `?workerClassificationId=${selectedIdForTopFilter}&userId=${id}`
      // } else {
      //   url = url + `?workerClassificationId=${selectedIdForTopFilter}&categoryId=${categoryId}&categoryChildId=${categoryChildId}&userId=${id}`
      // }
    } else {
      if (id == "all") {
        url = `/worker/?categoryId=${categoryId}&categoryChildId=${categoryChildId}`;
      } else {
        url = `/worker/?categoryId=${categoryId}&categoryChildId=${categoryChildId}&userId=${id}`;
        // if(isNull(categoryId) == true) {
        //   url = url + `?categoryChildId=${categoryChildId}&userId=${id}`
        // } else if(isNull(categoryChildId) == true) {
        //   url = url + `?categoryId=${categoryId}&userId=${id}`
        // } else {
        //   url = url + `?categoryId=${categoryId}&categoryChildId=${categoryChildId}&userId=${id}`
        // }
      }
    }

    console.log("user selected: ", id);
  };

  const filteringItems = () => {
    // THE COACH LEVEL
  };

  const setCoachDataByFilter = filterId => {
    // worker classification id
    let urlExt = ``;
    setSelectedFilterId(filterId);
    // setSelectedIdForTopFilter(1)
    // setSelectedIdForTopFilter(filterId)

    if (selectedUser != "all") {
      urlExt = `&userId=${selectedUser}`;
    }

    if (isFromHome == false) {
    } else {
      if (selectedUser == "all") {
        if (selectedIdForTopFilter == 1) {
        } else {
        }
      } else {
        if (selectedIdForTopFilter != 1) {
        } else {
        }
      }
    }
  };

  const setCoachDataByFilterAndSubFilter = (catChildId, catId, filterId) => {
    // category child id
    // setSelectedFilterId(filterId)
    setCategoryId(catId);
    setCategoryChildId(catChildId);

    let urlExtension = ``;

    if (selectedUser != "all") {
      urlExtension = `&userId=${selectedUser}`;
    }

    if (filterId == 0) {
    } else {
    }
  };

  const getFilterDataForTopFilter = async id => {
    // category id
    setSubCategoryLoading(true);
    setCategoryId(id);

    let uri = ``;

    if (selectedUser != 0) {
      uri = `&userId=${selectedUser}`;
    }

    if (id == 1) {
      if (selectedUser == "all") {
      } else {
      }
    } else {
      if (selectedUser == "all") {
      } else {
      }

      console.log("categoryData: ", categoryData);
      setTimeout(() => {
        setSubCategoryLoading(false);
      }, 500);
    }
  };

  useEffect(() => {
    // setFilterItems(DUMMY_FILTER)
    let extendedUrl = ``;

    if (selectedUser != "all") {
      extendedUrl = `&userId=${selectedUser}`;
    }

    if (isFromHome == false) {
      if (selectedUser == "all") {
      } else {
      }
    } else {
      if (selectedUser == "all") {
      } else {
      }
    }
  }, []);

  useEffect(() => {
    // do other things here
    // dispatch(Actions.fetchFamilyUnique());
    filteringItems();

    console.log("isFromHome: ", isFromHome);

    return () => {
      categoryChildIdFilter.current = null;
      categoryIdFilter.current = null;
      filterId.current = 0;

      console.log(
        "resetting ref: ",
        categoryChildIdFilter.current,
        categoryIdFilter.current,
        filterId.current,
      );
    };
  }, []);

  const closeFilterAS = () => {
    setShowFilterAS(false);
  };

  const FilterComponent = ({
    openFilter = false,
    onCloseFilter,
    filterData,
    onFilterSelected,
    onSubFilterSelected,
    selectedFilterId = 0,
    selectedSubFilter = 0,
  }) => {
    const [selectedFilter, setSelectedFilter] = useState(selectedFilterId);
    const [selectedSubCatFilter, setSelectedSubCatFilter] =
      useState(selectedSubFilter);

    const determineCallFilter = () => {
      console.log(
        "categoryChildIdFilter, categoryIdFilter: ",
        categoryChildIdFilter.current,
        categoryIdFilter.current,
        selectedSubCatFilter,
      );
      if (categoryIdFilter.current == null) {
        applyFilter();
      } else {
        applySubFilter();
      }
    };

    const applyFilter = useCallback(() => {
      console.log("applyFilter selected Filter: ", filterId.current);
      if (filterId.current != 0) {
        onFilterSelected(filterId.current);
        closeFilterAS();
      } else {
        onFilterSelected(1);
      }
    }, [onFilterSelected]);

    const applySubFilter = useCallback(() => {
      const data = {
        filterId: filterId.current,
        categoryId: categoryIdFilter.current,
        categoryChildId: categoryChildIdFilter.current,
      };

      console.log("applySubFilter selected Filter: ", data);
      onSubFilterSelected(data);
      closeFilterAS();
    }, [onSubFilterSelected]);

    const closeFilterAS = useCallback(() => {
      onCloseFilter();
    }, [onCloseFilter]);

    useEffect(() => {}, []);

    return (
      <Actionsheet
        isOpen={openFilter}
        onClose={closeFilterAS}
        hideDragIndicator={false}>
        <Actionsheet.Content style={{ paddingVertical: moderateScale(100) }}>
          <Image
            source={image.bg_actionsSheet}
            style={{ width: 300, height: 300, position: "absolute", right: 0 }}
            alt="image"
          />

          {/* HEADER */}
          <Row
            style={{
              width: "100%",
              marginBottom: moderateScale(35),
              paddingHorizontal: moderateScale(10, 0.25),
              // backgroundColor: 'green',
            }}>
            <Text
              style={{
                flex: 1,
                fontSize: moderateScale(16, 0.25),
                color: "#000",
                fontWeight: "700",
                fontFamily: "Montserrat-Regular",
                // marginBottom: moderateScale(35, 0.25),
              }}
              // alignItems="flex-start"
            >
              {`Filters`}
            </Text>
            <TouchableOpacity
              onPress={() => {
                if (isFromHome == true) {
                  setCategoryId(null);
                  setCategoryChildId(null);
                }
                setSelectedFilter(0);
                setSelectedIdForTopFilter(1);
                categoryChildIdFilter.current = null;
                categoryIdFilter.current = null;
                filterId.current = 0;
              }}>
              <Text
                style={{
                  fontSize: moderateScale(12, 0.25),
                  color: "#000",
                  fontWeight: "600",
                  fontFamily: "Montserrat-Regular",
                }}
                //   alignItems="flex-start"
              >
                {`RESET`}
              </Text>
            </TouchableOpacity>
          </Row>

          {/* BODY */}
          <ScrollView
            style={{
              width: "100%",
              paddingHorizontal: moderateScale(10, 0.25),
            }}
            showsVerticalScrollIndicator={false}>
            {selectedIdForTopFilter != 1 && additionalFilterData.length > 0 && (
              <Box>
                <Text style={styles.textFilterTitle}>{`Sub-Category`}</Text>
                {subCategoryLoading == true ? (
                  <ActivityIndicator size={"small"} />
                ) : (
                  <FlatList
                    data={additionalFilterData}
                    contentContainerStyle={{
                      flexWrap: "wrap",
                      marginBottom: moderateScale(16, 0.25),
                      flexShrink: 1, // this is a required style in order to enable horizontal flatlist flexWrap
                    }}
                    horizontal
                    renderItem={({ item, index }) => {
                      let isItemMatched =
                        categoryChildIdFilter.current === item.id;
                      // console.log("selectedFilter: ", selectedSubCatFilter);
                      // console.log("item.id: ", item.id);
                      // console.log("selectedFilter.includes(item): ", isItemMatched);
                      console.log("item of sub category: ", item);
                      return (
                        <TouchableOpacity
                          onPress={() => {
                            if (isItemMatched == true) {
                              categoryChildIdFilter.current = null;
                              categoryIdFilter.current = null;
                              setSelectedSubCatFilter(0);
                            } else {
                              categoryChildIdFilter.current = item.id;
                              categoryIdFilter.current = item.category_id;
                              console.log(
                                "categoryChildIdFilter: ",
                                categoryChildIdFilter.current,
                              );
                              console.log("item.id: ", item.id);
                              // setCategoryChildIdFilter(item.id)
                              // setCategoryIdFilter(item.category_id)
                              // categoryChildIdFilter = item.id
                              // categoryIdFilter = item.category_id
                              setSelectedSubCatFilter(item.id);
                            }
                          }}>
                          <Box
                            style={
                              isItemMatched == true
                                ? styles.buttonActive
                                : styles.buttonInactive
                            }>
                            <Text style={styles.textInactive}>{item.name}</Text>
                          </Box>
                        </TouchableOpacity>
                      );
                    }}
                  />
                )}
              </Box>
            )}
            <Text style={styles.textFilterTitle}>{`Level`}</Text>

            <FlatList
              data={filterData}
              contentContainerStyle={{
                // flexDirection: "row",
                flexWrap: "wrap",
                marginBottom: moderateScale(16, 0.25),
                flexShrink: 1, // this is a required style in order to enable horizontal flatlist flexWrap
              }}
              horizontal
              renderItem={({ item, index }) => {
                let isItemMatched = filterId.current === item.id;
                console.log("selectedFilter: ", selectedFilter);
                console.log("item.id: ", item.id);
                console.log("selectedFilter.includes(item): ", isItemMatched);
                return (
                  <TouchableOpacity
                    onPress={() => {
                      if (isItemMatched == true) {
                        filterId.current = 0;
                        setSelectedFilter(0);
                      } else {
                        filterId.current = item.id;
                        setSelectedFilter(item.id);
                      }
                      console.log("selectedFilter: ", selectedFilter);
                    }}>
                    <Box
                      style={
                        isItemMatched == true
                          ? styles.buttonActive
                          : styles.buttonInactive
                      }>
                      <Text style={styles.textInactive}>{item.name}</Text>
                    </Box>
                  </TouchableOpacity>
                );
              }}
            />
          </ScrollView>

          <TouchableOpacity
            onPress={() => {
              // applyFilter()
              determineCallFilter();
            }}>
            <Box
              style={{
                backgroundColor: "#1035BB",
                justifyContent: "center",
                alignItems: "center",
                width: Dimensions.get("screen").width * 0.9,
                paddingVertical: moderateScale(12, 0.25),
              }}>
              <Text
                style={{
                  fontSize: moderateScale(14, 0.25),
                  color: "#fff",
                  fontWeight: "600",
                  fontFamily: "Montserrat-Regular",
                }}>
                {`Apply filter`}
              </Text>
            </Box>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => closeFilterAS()}>
            <Text
              style={{
                marginVertical: moderateScale(14),
                fontSize: moderateScale(14, 0.25),
                color: "#212121",
                fontWeight: "600",
                fontFamily: "Montserrat-Regular",
              }}>
              {`Cancel`}
            </Text>
          </TouchableOpacity>
        </Actionsheet.Content>
      </Actionsheet>
    );
  };

  return (
    <Box backgroundColor="white" flex={1}>
      <Row>
        <Input
          InputLeftElement={<SearchIcon size={5} marginX={3} />}
          marginLeft={3}
          marginRight={isFromHome == true ? 3 : 0}
          flex={1}
          onChangeText={text => setSearchText(text)}
          placeholder={"Search Coach"}
        />
        {
          // isFromHome == false && (
          //   <TouchableOpacity
          //     onPress={() => setShowFilterAS(true)}
          //     style={{
          //       justifyContent: 'center',
          //       alignItems: "center",
          //       padding: moderateScale(10, .25)
          //     }}
          //   >
          //     <Image
          //       source={image.funnel_ic}
          //       width={5}
          //       height={5}
          //     />
          //   </TouchableOpacity>
          // )
        }
        {/* <IconButton
          icon={<HamburgerIcon />}
          onPress={() => setShowFilterAS(true)}
        /> */}
      </Row>
      {/* <Modal isOpen={isFilterModalOpen}>
        <Modal.Content>
          <Modal.Header>
            <Text fontWeight={300}>Filters</Text>
          </Modal.Header>
          <Modal.Body>
            <Text>Filters go here</Text>
          </Modal.Body>
          <Modal.Footer>
            <Button onPress={() => setIsFilterModalOpen(false)}>Save</Button>
          </Modal.Footer>
        </Modal.Content>
      </Modal> */}
      {isFromHome == true && (
        <Box width={"100%"} justifyContent={"center"} flexDirection={"row"}>
          {/* <FlatList 
              data={TOP_FILTER}
              horizontal
              showsHorizontalScrollIndicator={false}
              contentContainerStyle={{
                paddingHorizontal: moderateScale(12, .25),
                paddingVertical: moderateScale(10, .25)
              }}
              renderItem={({item: {id, name}}) => (
                <TouchableOpacity onPress={() => {
                  console.log("id pressed: ", id);
                  if(selectedIdForTopFilter == id) {
                    setSelectedIdForTopFilter(1)
                    getFilterDataForTopFilter(1)
                  } else {
                    setSelectedIdForTopFilter(id)
                    getFilterDataForTopFilter(id)
                  }
                }}>
                  <Box style={selectedIdForTopFilter == id ? styles.buttonActive : styles.buttonInactive}> 
                    <Text style={selectedIdForTopFilter == id ? styles.textActive :styles.textInactive}>
                      {name}
                    </Text>
                  </Box>
                </TouchableOpacity>
              )}
            /> */}
          {
            // isFromHome == true && (
            //   <TouchableOpacity
            //     onPress={() => setShowFilterAS(true)}
            //     style={{
            //       justifyContent: 'center',
            //       alignItems: "center",
            //       padding: moderateScale(12, .25),
            //       paddingBottom: moderateScale(16, .25),
            //     }}
            //   >
            //     <ImgReact
            //       source={image.funnel_ic}
            //       style={{height: 20, width:20}}
            //     />
            //   </TouchableOpacity>
            // )
          }
          {/* <DividerHorizontal style={{marginTop: moderateScale(6, .25), marginBottom: moderateScale(16, .25)}}/> */}
        </Box>
      )}
      {
        // dataUniqueFamily.data && (
        //   <Select
        //   selectedValue={selectedUser}
        //   width={Dimensions.get('screen').width * .95}
        //   marginTop={isFromHome == true ? moderateScale(-10, .25) : 0}
        //   marginLeft={moderateScale(10, .25)}
        //   accessibilityLabel="Choose User"
        //   placeholder="Choose User"
        //   _selectedItem={{
        //     endIcon: <CheckIcon size="5" />
        //   }}
        //   mt={1}
        //   onValueChange={(itemValue) => changeUser(itemValue)}
        //   >
        //     <Select.Item label="All" value="all" />
        //     {dataUniqueFamily.data && dataUniqueFamily.data.map((family_member) =>
        //       <Select.Item label={family_member.name} value={family_member.id} />
        //       )}
        //   </Select>
        // )
      }
      <DividerHorizontal
        style={{
          marginTop: moderateScale(6, 0.25),
          marginBottom: moderateScale(16, 0.25),
        }}
      />
      <FlatList
        data={filteredData}
        // renderItem={({ item: { id, title, type, price, rating }, index }) => {
        renderItem={({ item, index }) => {
          console.log(
            "populateTrainerTypes: ",
            populateTrainerTypes(item.activity_workers),
          );
          console.log("asjdbiquwgoeoquwg", item);

          let imageTemp = { uri: "https://kyzn-api-staging.tbtft.com/api/uploads/photo-1640778749290.jpg" }

          return (
            <Column>
              {/* {index === 0 && (
                <DividerHorizontal style={{ marginVertical: 25 }} />
              )} */}
              <TouchableOpacity
                onPress={() =>
                  navigation.navigate("PersonalTrainerView", {
                    id: item.id,
                    type: pt_activity_type_id,
                    data: item,
                    userId: selectedUser,
                  })
                }>
                <Row paddingX={3}>
                  <Column marginRight={3}>
                    <ImgReact
                      // source={{ uri: "https://via.placeholder.com/103x104" }}
                      onError={({ nativeEvent: { error } }) => {
                        setUriImage(image.img_product_placeholder);
                      }}
                      style={{ width: 50, height: 50 }}
                      source={imageTemp}
                      // alt="image"
                    />
                  </Column>
                  <Column flex={1}>
                    <Text fontWeight={300}>{item.name}</Text>
                    <Text color="gray">
                      {populateTrainerTypes(item.activity_workers)}
                    </Text>
                    <Text marginTop={5} fontWeight={300} color="gray">
                      Starts from{" "}
                      <Text fontWeight={300}>{`${Commons.convertToRupiah(
                        _get(item, "lowest_price"),
                      )}`}</Text>
                    </Text>
                  </Column>
                  <Column justifyContent="flex-end">
                    <Row>
                      <ImgReact
                        source={image.star}
                        style={{
                          marginRight: moderateScale(4, 0.25),
                          height: 20,
                          width: 20,
                        }}
                        resizeMode="contain"
                        alt="image"
                      />
                      <Text fontWeight={300}>{item.rating}</Text>
                    </Row>
                  </Column>
                </Row>
              </TouchableOpacity>
              <DividerHorizontal style={{ marginTop: 15, marginBottom: 25 }} />
            </Column>
          );
        }}
      />
      <FilterComponent
        filterData={filterItems}
        openFilter={showFilterAS}
        onCloseFilter={closeFilterAS}
        selectedFilterId={selectedFilterId}
        selectedSubFilter={idOfAdditionalFilter}
        onFilterSelected={id => {
          setCoachDataByFilter(id);
        }}
        onSubFilterSelected={data => {
          console.log("data received: ", data);
          setCoachDataByFilterAndSubFilter(
            data.categoryChildId,
            data.categoryId,
            data.filterId,
          );
        }}
      />
    </Box>
  );
};

const styles = StyleSheet.create({
  textInactive: {
    fontSize: moderateScale(12, 0.25),
    color: "#000",
    fontWeight: "500",
    fontFamily: "Montserrat-Regular",
  },
  textActive: {
    fontSize: moderateScale(12, 0.25),
    color: "#1035BB",
    fontWeight: "600",
    fontFamily: "Montserrat-Regular",
  },
  buttonInactive: {
    borderWidth: 1,
    borderColor: "#9E9E9E",
    backgroundColor: "transparent",
    paddingVertical: moderateScale(10),
    paddingHorizontal: moderateScale(16),
    marginRight: moderateScale(10),
    alignItems: "center",
    justifyContent: "center",
    marginBottom: moderateScale(12, 0.25),
  },
  buttonActive: {
    borderWidth: 1,
    borderColor: "rgba(16, 53, 187, 1)",
    backgroundColor: "#E8E7FF",
    paddingVertical: moderateScale(10),
    paddingHorizontal: moderateScale(16),
    marginRight: moderateScale(10),
    alignItems: "center",
    justifyContent: "center",
    marginBottom: moderateScale(12, 0.25),
  },
  textFilterTitle: {
    fontWeight: "600",
    fontSize: moderateScale(14, 0.25),
    marginBottom: moderateScale(10, 0.25),
    fontFamily: "Montserrat-Regular",
    color: "#9E9E9E",
  },
});

export default PersonalTrainersList;
