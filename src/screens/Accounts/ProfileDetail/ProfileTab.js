import React from "react";
import { View, Text, ScrollView } from "react-native";
import FloatingInput from "components/ui/FloatingInput";
import { moderateScale } from "react-native-size-matters";
import { useDispatch, useSelector } from "react-redux";
import Selectors from "selectors";
import moment from "moment";
import _ from "lodash";

export default function ProfileTab({ user }) {
  const profile = user || useSelector(Selectors.getProfile);
  let name = _.get(profile, "name", "-");
  let email = _.get(profile, "email", "-");
  let phone = _.get(profile, "phone_number", "-");
  let emergencyPhone = _.get(profile, "emergency_number", "-");
  let gender = _.get(profile, "gender", "-");
  let birthdate = _.get(profile, "birthdate", "-");
  let birthdateFormat = birthdate !== "-" || birthdate !== null ? moment(birthdate).format("DD/MM/YYYY") : "-"
  let nationality = _.get(profile, "nationality", "-");
  let postal_code = _.get(profile, "postal_code", "-");
  let province = _.get(profile, "province", "-");
  let district = _.get(profile, "district", "-");
  let city = _.get(profile, "city", "-");
  let address = _.get(profile, "address", "-");
  let username = _.get(profile, "username", "-");

  return (
    <ScrollView style={{ flex: 1, backgroundColor: "#F8F9FC" }} showsVerticalScrollIndicator={false}>
      <View style={{ paddingBottom: moderateScale(60, 0.25) }}>
        <FloatingInput
          label="Name"
          value={name}
          editable={false}
        ></FloatingInput>
        <FloatingInput
          label="Username"
          value={username}
          editable={false}
        ></FloatingInput>
        {/* <FloatingInput
          label="Email"
          value={email}
          editable={false}
        ></FloatingInput> */}
        <FloatingInput
          label="Phone Number"
          value={phone}
          editable={false}
        ></FloatingInput>
        {/* <FloatingInput
          label="Emergency Number"
          value={emergencyPhone}
          editable={false}
        ></FloatingInput> */}
        <FloatingInput
          label="Gender"
          value={gender}
          editable={false}
        ></FloatingInput>
        <FloatingInput
          label="Birthday"
          value={birthdateFormat}
          editable={false}
        ></FloatingInput>
        <FloatingInput
          label="Nation"
          value={nationality}
          editable={false}
        ></FloatingInput>
        <FloatingInput
          label="Postal Code"
          value={postal_code}
          editable={false}
        ></FloatingInput>
        <FloatingInput
          label="Addresss"
          value={address}
          editable={false}
        ></FloatingInput>
        <FloatingInput
          label="City"
          value={city}
          editable={false}
        ></FloatingInput>
        <FloatingInput
          label="District"
          value={district}
          editable={false}
        ></FloatingInput>
        <FloatingInput
          label="Province"
          value={province}
          editable={false}
        ></FloatingInput>
      </View>
    </ScrollView>
  );
}
