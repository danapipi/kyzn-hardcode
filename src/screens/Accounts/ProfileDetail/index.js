import React, { useState } from "react";
import { View, Text, Image, TouchableOpacity, StyleSheet } from "react-native";
import { image } from "images";
import { moderateScale } from "react-native-size-matters";
import ProfileTab from "./ProfileTab";
import PanoramaTab from "./PanoramaTab";
import { useDispatch, useSelector } from "react-redux";
import Selectors from "selectors";
import moment from "moment";
import _ from "lodash";

const ProfileDetail = ({ route, navigation }) => {
  const [data, useData] = useState([
    { id: 1, name: "Profile", screenName: "ProfileTab" },
    // { id: 2, name: "Panorama", screenName: "PanoramaTab" },
  ]);
  const [selectedIndex, setSelectedIndex] = useState(0);
  const self = useSelector(Selectors.getProfile);
  const profile = route.params.user || useSelector(Selectors.getProfile);
  const photo = _.get(profile, "photo", "");
  let name = _.get(profile, "name", "-");

  console.log("profileeeee detail", profile);

  const renderTabs = () => {
    return data.map((item, index) => {
      let isSelected = selectedIndex === index;
      return (
        <TouchableOpacity
          key={`Tab${index}`}
          style={
            ([styles.tabButton],
            {
              //   marginHorizontal: 16,
              alignItems: "center",
              paddingBottom: 10,
              width: moderateScale(80, 0.25),
            })
          }
          onPress={() => onChangeTab(item, index)}
        >
          <Text
            style={[
              styles.tabText,
              {
                color: "black",
                fontWeight: isSelected ? "700" : "600",
                fontFamily: "Montserrat-Regular",
              },
            ]}
          >
            {item.name}
          </Text>
          <View
            style={{
              position: "absolute",
              bottom: -1,
              left: 0,
              height: 2,
              width: "100%",
              alignItems: "center",
            }}
          >
            <View
              style={{
                width: moderateScale(80, 0.25),
                borderBottomWidth: isSelected ? 2 : 0,
                borderBottomColor: isSelected ? "black" : "rgba(255,255,255,0)",
              }}
            />
          </View>
        </TouchableOpacity>
      );
    });
  };

  const onChangeTab = (item, index) => {
    setSelectedIndex(index);
  };

  const renderContent = () => {
    if (selectedIndex === 0) {
      return renderProfileTab();
    } else {
      return renderPanoramaTab();
    }
  };

  const renderProfileTab = () => {
    if(route.params.user) {
      route.params.user.is_parent = false // identify that is child
    }
    return <ProfileTab user={route.params.user} />;
  };

  const renderPanoramaTab = () => {
    return <PanoramaTab />;
  };

  const goToEditProfile = () => {
    navigation.navigate("EditProfile", { title: "Edit Profile", user: route.params.user });
  };

  return (
    <View style={{ backgroundColor: "white", flex: 1 }}>
      <View
        style={{
          flexDirection: "row",
          marginHorizontal: moderateScale(20, 0.25),
          marginBottom: moderateScale(34, 0.25),
          marginTop: moderateScale(16, 0.25)
          //   flex: 1
        }}
      >
        <Image
          source={photo ? {uri: photo} : image.ic_profile_placeholder}
          resizeMode="cover"
          alt="image"
          // size="xl"
          style={{
            width: moderateScale(50, 0.25),
            height: moderateScale(50, 0.25),
            // marginLeft: moderateScale(16, 0.25),
            marginRight: moderateScale(16, 0.25),
          }}
        />
        <View style={{ flexDirection: "column", flex: 2 }}>
          <Text
            style={{
              fontSize: moderateScale(14, 0.25),
              fontWeight: "600",
              color: "#212121",
              fontFamily: "Montserrat-Regular",
              marginBottom: moderateScale(4, 0.25)
            }}
          >
            {name}
          </Text>
          <Text
            style={{
              color: "#757575",
              fontSize: moderateScale(10, 0.25),
              fontWeight: "600",
              fontFamily: "Montserrat-Regular",
            }}
          >
            {/* as Parents */}
          </Text>
        </View>
        {(profile.id == self.id || !profile.phone_number) &&
        <TouchableOpacity
          style={{ flex: 1 }}
          onPress={() => goToEditProfile(navigation)}
        >
          <View style={{alignItems: "flex-end"}}>
            <Image
              source={image.edit_ic}
              resizeMode="contain"
              alt="image"
              // size="xl"
              style={{
                width: moderateScale(24, 0.25),
                height: moderateScale(24, 0.25),
              }}
            />
          </View>
        </TouchableOpacity>
        }
      </View>
      {/* <View style={{ flexDirection: "row" }}>{renderTabs()}</View> */}
      {renderContent()}
    </View>
  );
};

export default ProfileDetail;

const styles = StyleSheet.create({
  tabButton: {
    marginHorizontal: 5,
    padding: 10,
  },
  tabText: {
    fontSize: moderateScale(12, 0.25),
    // fontWeight: "400",
    padding: 2,
    // fontFamily:'OpenSans-Light'
    // marginRight: -5,
  },
});
