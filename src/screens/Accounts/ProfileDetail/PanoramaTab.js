import React from "react";
import { View, Text, Image } from "react-native";
import { image } from "images";
import _ from "lodash";
import { moderateScale } from "react-native-size-matters";

export default function PanoramaTab() {
  return (
    <View style={{ flex: 1, backgroundColor: "#F8F9FC",  alignItems: "center" }}>
      <Image
        source={image.in_progress}
        style={{
          width: moderateScale(126),
          height: moderateScale(115),
          resizeMode: "cover",
          marginBottom: moderateScale(32),
          marginTop: moderateScale(62)
        }}
      />
      <Text
        style={{
          color: "black",
          fontSize: moderateScale(16, 0.25),
          fontFamily: "Montserrat-Regular",
          fontWeight: "700",
          marginBottom: moderateScale(10)
        }}
      >
        Panorama In the Making
      </Text>
      <Text
        style={{
          color: "black",
          fontSize: moderateScale(12, 0.25),
          fontFamily: "Montserrat-Regular",
          fontWeight: "500",
          marginHorizontal: moderateScale(24),
          lineHeight: moderateScale(18, 0.25),
          textAlign: "center"
        }}
      >
        We are cooking something exciting to support your healthy performance.
        Coming Soon!
      </Text>
    </View>
  );
}
