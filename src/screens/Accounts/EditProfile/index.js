import React, { useState } from "react";
import {
  View,
  Text,
  ScrollView,
  Image,
  ImageBackground,
  TouchableOpacity,
  Pressable,
  Platform,
  Alert,
} from "react-native";
import { Modal, useScreenReaderEnabled } from "native-base";
import { launchCamera, launchImageLibrary } from "react-native-image-picker";
import GenderPicker from "components/ui/GenderPicker";
import NationPicker from "components/ui/NationPicker";
import DatePicker from "components/ui/DatePicker";
import FloatingInput from "components/ui/FloatingInput";
import { moderateScale } from "react-native-size-matters";
import { useDispatch, useSelector } from "react-redux";
import Selectors from "selectors";
import moment from "moment";
import _ from "lodash";
import { image } from "images";
import ButtonCustom from "components/ui/ButtonCustom";
import Actions from "actions";
import Commons from "../../../utils/common";
import axios from "axios";
import { BASE_URL } from "../../../../config";
import RNStorage from "../../../utils/storage";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

const EditProfile = ({ navigation ,route }) => {
  const dispatch = useDispatch();
  const profile = route.params.user || useSelector(Selectors.getProfile);

  let id = _.get(profile, "id", null);
  let name = _.get(profile, "name", "-");
  let email = _.get(profile, "email", "-");
  let phone = _.get(profile, "phone_number", "-");
  let emergencyPhone = _.get(profile, "emergency_number", "-");
  let gender = _.get(profile, "gender", "-");
  let birthdate = _.get(profile, "birthdate", "-");
  let birthdateFormat =
    birthdate !== "-" || birthdate !== null
      ? moment(birthdate).format("YYYY-MM-DD")
      : "-";
  let nationality = _.get(profile, "nationality", "-");
  let isParent = _.get(profile, "phone_number", "");
  let postal_code = _.get(profile, "postal_code", "-");
  let province = _.get(profile, "province", "-");
  let district = _.get(profile, "district", "-");
  let city = _.get(profile, "city", "-");
  let address = _.get(profile, "address", "-");
  let username = _.get(profile, "username", "-");



  const [nameValue, setNameValue] = useState(name);
  const [emailValue, setEmailValue] = useState(email);
  const [phoneValue, setPhoneValue] = useState(phone);
  const [emergencyPhoneValue, setEmergencyPhoneValue] =
    useState(emergencyPhone);
  const [genderValue, setGenderValue] = useState(gender);
  const [birthdateValue, setBirthdateValue] = useState(birthdateFormat);
  const [nationalityValue, setNationalityValue] = useState(nationality);
  const [photoValue, setPhotoValue] = useState(null);
  const [isShowingModal, setIsShowingModal] = useState(false);
  const [isLoading, setLoading] = useState(false);
  const [postalCodeValue, setPostalCodeValue] = useState(postal_code);
  const [provinceValue, setProvinceValue] = useState(province);
  const [districtValue, setDistrictValue] = useState(district);
  const [cityValue, setCityValue] = useState(city);
  const [addressValue, setAddressValue] = useState(address);
  const [usernameValue, setUsernameValue] = useState(username);

  const sendUpdatedData = () => {
    let formData = new FormData();
    // formData.append("name", nameValue);
    formData.append("birthdate", birthdateValue);
    formData.append("gender", genderValue);
    formData.append('address', addressValue);
    formData.append("nationality", nationalityValue);
    formData.append('postalCode', postalCodeValue);
    formData.append('province', provinceValue);
    formData.append('district', districtValue);
    formData.append('city', cityValue);
    if (photoValue) {
      formData.append("photo", {
        uri:
          Platform.OS === "android"
            ? photoValue.uri
            // : photoValue.uri,
            // : "file://" + photoValue.uri,
            : photoValue.uri.replace("file://", ""),
        name: photoValue.fileName,
        type: "image/jpeg",
      });
    }
    return formData;
  };

  const updateProfileDirectly = async () => {
    setLoading(true);
    let config;
    let token = await RNStorage.getToken();
    console.log("cek update data", sendUpdatedData());

    if (isParent) {
      config = {
        method: "put",
        url: `${BASE_URL}/api/user/auth`,
        data: sendUpdatedData(),
        headers: {
          "Content-Type": "multipart/form-data",
          Authorization: `${token}`,
        },
      };
    } else {
      config = {
        method: "put",
        url: `${BASE_URL}/api/user/family/edit-child/${id}`,
        data: sendUpdatedData(),
        headers: {
          "Content-Type": "multipart/form-data",
          Authorization: `${token}`,
        },
      };
    }

    let profile = {
      "id": 2,
      "name": name,
      "username": null,
      "phone_number": phone,
      "xp": 0,
      "coin": 0,
      "balance": 0,
      "status": "Registered",
      "birthdate": "2001-11-30T16:00:00.000Z",
      "gender": genderValue,
      "address": addressValue,
      "photo": "https://api.kyzn.life/api/uploads/default-avatar.png",
      "nationality": nationalityValue,
      "postal_code": postalCodeValue,
      "province": provinceValue,
      "district": districtValue,
      "city": cityValue,
      "referral_code": "Grand Opening",
      "id_card_no": null,
      "instagram_username": null,
      "company_name": null,
      "physical_limitation": null,
      "emergency_contact_name": null,
      "emergency_contact_relationship": null,
      "emergency_phone_number": null,
      "emergency_home_number": null,
      "email": null,
      "rfid_card_no": null,
      "created_at": "2021-09-25T01:00:00.000Z",
      "updated_at": "2021-11-22T18:25:52.000Z",
      "user_memberships": [],
      "user_membership_packages": [],
      "user_banks": [],
      "current_level": 3,
      "user_experiences": [
        {
          "id": 1,
          "level": 1,
          "exp_needed": 0,
          "created_at": "2021-11-11T09:51:36.000Z",
          "updated_at": null,
          "deleted_at": null,
        },
        {
          "id": 2,
          "level": 2,
          "exp_needed": 100,
          "created_at": "2021-11-11T09:51:36.000Z",
          "updated_at": null,
          "deleted_at": null,
        },
        {
          "id": 3,
          "level": 3,
          "exp_needed": 200,
          "created_at": "2021-11-11T09:51:36.000Z",
          "updated_at": null,
          "deleted_at": null,
        },
        {
          "id": 4,
          "level": 4,
          "exp_needed": 300,
          "created_at": "2021-11-11T09:51:36.000Z",
          "updated_at": null,
          "deleted_at": null,
        },
      ],
    };
    
    dispatch(Actions.profileSuccess(profile));

    navigation.goBack();
    // axios(config)
    //   .then((response) => {
    //     // dispatch(Actions.profile());
    //     setLoading(false);
    //     Alert.alert("Success", "", [
    //       {
    //         text: "OK",
    //         onPress: () => navigation.goBack(),
    //         style: "cancel",
    //       },
    //     ]);
    //   })
    //   .catch((error) => {
    //     setLoading(false);
    //     Alert.alert("Error", error.response.data.message, [
    //       {
    //         text: "OK",
    //         onPress: () => console.log("OK Pressed"),
    //         style: "cancel",
    //       },
    //     ]);
    //   });
  };

  const updateProfile = () => {
    dispatch(
      Actions.updateProfile({
        name: nameValue,
        // email: emailValue,
        birthdate: birthdateValue,
        gender: genderValue,
        nationality: nationalityValue,
        // "address",
        postalCode: postalCodeValue,
        province: provinceValue,
        district: districtValue,
        photo: photoValue,
      })
    );
  };

  const updateProfileChild = () => {
    dispatch(
      Actions.updateProfileChild({
        id: id,
        name: nameValue,
        // email: emailValue,
        birthdate: birthdateValue,
        gender: genderValue,
        nationality: nationalityValue,
        // "address",
        postalCode: postalCodeValue,
        province: provinceValue,
        district: districtValue,
        photo: photoValue,
      })
    );
  };

  const getPhoto = (fromLibrary) => {
    setIsShowingModal(false);
    let options = {
      maxWidth: 200,
      maxHeight: 200,
      quality: 0.3,
    }

    if (fromLibrary) {
      launchImageLibrary(options, (photo) => {
        if (photo.assets) {
          setPhotoValue(photo.assets[0]);
        }
      });
    } else {
      launchCamera(options, (photo) => {
        if (photo.assets) {
          setPhotoValue(photo.assets[0]);
        }
      });
    }
  };

  const convertDOB = (date) => {
    let convertDate = moment(date).format("YYYY-MM-DD");
    setBirthdateValue(convertDate);
  };

  return (
    <KeyboardAwareScrollView
      enableOnAndroid={true}
      // style={{ flex: 1, backgroundColor: "white" }}
      contentContainerStyle={{ backgroundColor: "white" }}
      showsVerticalScrollIndicator={false}
    >
      <View
        style={{
          justifyContent: "center",
          alignItems: "center",
          marginTop: moderateScale(16, 0.25),
          marginBottom: moderateScale(40, 0.25),
        }}
      >
        <TouchableOpacity onPress={() => setIsShowingModal(true)}>
          <ImageBackground
            source={photoValue ? { uri: photoValue.uri } : image.add_photo}
            resizeMode="contain"
            alt="image"
            // size="xl"
            style={{
              width: moderateScale(80, 0.25),
              height: moderateScale(80, 0.25),
              justifyContent: "center",
              // marginLeft: moderateScale(16, 0.25),
              //   marginRight: moderateScale(16, 0.25),
            }}
          >
            <Image
              source={image.camera_ic}
              resizeMode="contain"
              alt="image"
              // size="xl"
              style={{
                width: moderateScale(40, 0.25),
                height: moderateScale(40, 0.25),
                alignSelf: "center",
                // marginLeft: moderateScale(16, 0.25),
                //   marginRight: moderateScale(16, 0.25),
              }}
            ></Image>
          </ImageBackground>
        </TouchableOpacity>
      </View>
      <View style={{ paddingBottom: moderateScale(60, 0.25) }}>
        <FloatingInput
          label="Name"
          value={nameValue}
          onChangeText={(val) => setNameValue(val)}
        ></FloatingInput>
        <FloatingInput
          label="Username"
          value={usernameValue}
          editable={false}
        ></FloatingInput>
        {/* <FloatingInput
          label="Email"
          value={emailValue}
          onChangeText={(val) => setEmailValue(val)}
        ></FloatingInput> */}
        <FloatingInput
          label="Phone Number"
          value={phoneValue}
          editable={false}
        ></FloatingInput>
        {/* <FloatingInput
          label="Emergency Number"
          value={emergencyPhoneValue}
          onChangeText={(val) => setEmergencyPhoneValue(val)}
        ></FloatingInput> */}
        <GenderPicker
          label="Gender"
          value={Commons.capitalized(genderValue)}
          bgColorLabel={"white"}
          onSelected={(val) => setGenderValue(val)}
        ></GenderPicker>
        <DatePicker
          label="Birthdate"
          value={birthdate}
          disabled
          bgColorLabel={"white"}
          onSelected={(val) => convertDOB(val)}
        />
        <NationPicker
          label="Nationality"
          value={nationalityValue}
          bgColorLabel={"white"}
          onSelected={(val) => setNationalityValue(val)}
        />
        <FloatingInput
          label="Postal Code (Optional)"
          value={postalCodeValue}
          bgColorLabel={"white"}
          onChangeText={(val) => setPostalCodeValue(val)}
        ></FloatingInput>
        <FloatingInput
          label="Addresss (Optional)"
          value={addressValue}
          bgColorLabel={"white"}
          onChangeText={(val) => setAddressValue(val)}
        ></FloatingInput>
        <FloatingInput
          label="City (Optional)"
          value={cityValue}
          bgColorLabel={"white"}
          onChangeText={(val) => setCityValue(val)}
        ></FloatingInput>
        <FloatingInput
          label="District (Optional)"
          value={districtValue}
          bgColorLabel={"white"}
          onChangeText={(val) => setDistrictValue(val)}
        ></FloatingInput>
        <FloatingInput
          label="Province (Optional)"
          value={provinceValue}
          bgColorLabel={"white"}
          onChangeText={(val => setProvinceValue(val))}
        ></FloatingInput>
      </View>
      <ButtonCustom
        isLoading={isLoading}
        label={"Save"}
        onPressButton={() => {
          updateProfileDirectly();
        }}
        // buttonMarginHorizontal={24}
        buttonMarginTop={10}
        buttonMarginBottom={30}
      ></ButtonCustom>
      <Modal isOpen={isShowingModal} onClose={() => setIsShowingModal(false)}>
        <Modal.Content maxWidth="400px">
          <Modal.CloseButton />
          <Modal.Header>
            <Text
              alignItems="flex-start"
              fontWeight="bold"
              fontSize={12}
              style={{ fontFamily: "Montserrat-Regular" }}
            >
              Select Photo
            </Text>
          </Modal.Header>
          <Modal.Body>
            <Pressable
              style={{ paddingVertical: 8 }}
              onPress={() => getPhoto(false)}
            >
              <Text
                alignItems="flex-start"
                fontWeight="bold"
                fontSize={12}
                style={{ fontFamily: "Montserrat-Regular" }}
              >
                Take Photo
              </Text>
            </Pressable>
            <Pressable
              style={{ paddingVertical: 8 }}
              onPress={() => getPhoto(true)}
            >
              <Text
                alignItems="flex-start"
                fontWeight="bold"
                fontSize={12}
                style={{ fontFamily: "Montserrat-Regular" }}
              >
                Choose from library
              </Text>
            </Pressable>
          </Modal.Body>
        </Modal.Content>
      </Modal>
    </KeyboardAwareScrollView>
  );
};

export default EditProfile;
