import React, { useEffect, useState, useReducer, useCallback } from "react";
import { Box, Button, Text, ScrollView, ChevronDownIcon } from "native-base";
import { TouchableOpacity, ImageBackground, Image } from "react-native";
import PropTypes from "prop-types";
import { useDispatch, useSelector } from "react-redux";
import Selectors from "selectors";
import Actions from "actions";
import AccountsMenu from "components/ui/AccountsMenu";
import AboutMenu from "components/ui/About";
import AddKidsCard from "components/ui/AddKidsCard";
import PremiumCard from "components/ui/PremiumCard";
import { getScreenWidth } from "utils/size";
import DividerHorizontal from "components/ui/DividerHorizontal";
import Loading from "components/ui/Loading";
import { moderateScale } from "react-native-size-matters";
import { image } from "images";
import { useIsFocused } from "@react-navigation/native";
import _ from "lodash";
import moment from "moment";

const Accounts = ({ navigation }) => {
  const dispatch = useDispatch();
  const isFocused = useIsFocused();
  const profile = useSelector(Selectors.getProfile);
  console.log("profile jancuk: ", profile);
  const isLoading = useSelector(Selectors.getIsLoadingProfile);

  const [uriImage, setUriImage] = useState("");
  const [_refresh, forceUpdate] = useReducer(x => x + 1, 0);
  const [_refresh1, updateState] = useState();
  const isMember =
    profile.user_memberships &&
    [...profile.user_membership_packages, ...profile.user_memberships].filter(
      item => _.get(item, "membership.name") !== "Non-Member",
    ).length > 0;

  const photo = _.get(profile, "photo", "");
  const [dataKids, setDataKids] = useState([
    { id: 1, name: "Indrayana", sports_count: 4 },
    { id: 2, name: "Agnesia", sports_count: 3 },
  ]);

  useEffect(() => {
    // dispatch(Actions.profile());
    console.log("profileeee", profile);

    const willFocusSubscription = navigation.addListener("focus", () => {
      // dispatch(Actions.profile());
    });

    return willFocusSubscription;
  });

  let imageTemp = uriImage == "" ? { uri: photo } : uriImage;
  if (isLoading) {
    <Loading />;
  }
  const [forceRefresher, setForceRefresher] = useState(false);

  const forceReload = useCallback(() => {
    updateState({});
  }, []);

  const refreshFromCard = data => {
    if (_.get(data, "forceRefresh") == true) {
      console.log("refreshFromCard data: ", data);
      setForceRefresher(true);
      // dispatch(Actions.profile());
      // dispatch(Actions.fetchAllFamilies());
      // setTimeout(() => {
      // forceUpdate()
      // }, 2000);
    } else {
      setForceRefresher(false);
    }
  };

  useEffect(() => {
    console.log("[Profile] isFocused", isFocused);
    // if(isFocused == true) {
    // dispatch(Actions.profile());
    // dispatch(Actions.fetchAllFamilies());
    setTimeout(() => {
      forceUpdate();
      // forceReload()
    }, 700);

    // }
  }, [isFocused, forceRefresher]);

  // useEffect(() => {
  //   console.log("[Profile] forceRefresher", forceRefresher);
  //   if(forceRefresher == true) {
  //     dispatch(Actions.profile());
  //     dispatch(Actions.fetchAllFamilies());
  //     setTimeout(() => {
  //       forceUpdate()
  //     }, 700);
  //     setForceRefresher(false)
  //   }
  // }, [forceRefresher])

  return (
    <Box
      alignItems="center"
      justifyContent="center"
      bg={"white"}
      style={{ flex: 1 }}>
      <Box
        style={{
          marginBottom: moderateScale(24, 0.25),
          flex: 0.2,
          height: moderateScale(8, 0.25),
          width: "100%",
          flexDirection: "row",
          alignItems: "flex-end",
          justifyContent: "center",
        }}>
        {/* <ImageBackground
          source={image.payment_background}
          resizeMode="contain"
          alt="image"
          // size="xl"
          style={{
            width: moderateScale(80, 0.25),
            height: moderateScale(80, 0.25),
            justifyContent: "center",
            flexDirection: "row",

            // marginLeft: moderateScale(16, 0.25),
            //   marginRight: moderateScale(16, 0.25),
          }}
        > */}
        <Image
          source={photo ? imageTemp : image.ic_profile_placeholder}
          onError={e => setUriImage(image.ic_profile_placeholder)}
          resizeMode="cover"
          alt="image"
          // size="xl"
          style={{
            width: moderateScale(50, 0.25),
            height: moderateScale(50, 0.25),
            marginLeft: moderateScale(24, 0.25),
            marginRight: moderateScale(16, 0.25),
          }}
        />
        <Box style={{ flexDirection: "column", flex: 2 }}>
          <TouchableOpacity>
            <Box style={{ flexDirection: "row" }}>
              <Text
                style={{
                  color: "#212121",
                  fontSize: moderateScale(14, 0.25),
                  fontFamily: "Montserrat-Regular",
                  fontWeight: "700",
                  marginRight: moderateScale(4, 0.25),
                }}>
                {profile.name}
              </Text>
              {/* <ChevronDownIcon size={6} /> */}
            </Box>
            <Text
              style={{
                color: "#212121",
                fontSize: moderateScale(12, 0.25),
                fontFamily: "Montserrat-Regular",
                fontWeight: "600",
                marginRight: moderateScale(4, 0.25),
              }}>
              {/* {profile.phone_number ? "Adult" : "Child"} */}
              {`${moment().diff(profile.birthdate, "years")} Years Old`}
            </Text>
          </TouchableOpacity>
        </Box>
        {isMember && (
          <Box
            style={{
              flex: 1,
              alignItems: "flex-end",
              marginRight: moderateScale(20, 0.25),
              height: moderateScale(44, 0.25),
              justifyContent: "center",
            }}>
            <TouchableOpacity
              onPress={() => navigation.navigate("UserMembership")}>
              <Image
                style={{ width: 24, height: 24, alignSelf: "center" }}
                source={image.premium_logo}
                alt="image"
                resizeMode="contain"
              />
            </TouchableOpacity>
          </Box>
        )}
        {/* </ImageBackground> */}
      </Box>
      <ScrollView
        style={{ flex: 1 }}
        contentContainerStyle={{ paddingBottom: 5 }}
        showsVerticalScrollIndicator={false}>
        <TouchableOpacity
          onPress={
            isMember
              ? () => navigation.navigate("UserMembership")
              : () => navigation.navigate("Memberships")
          }>
          <PremiumCard isMember={isMember} data={profile} />
        </TouchableOpacity>
        {/* {profile.phone_number && (
          <AddKidsCard
            navigation={navigation}
            route={{ onRefresh: refreshFromCard }}
          />
        )} */}
        <AddKidsCard
          navigation={navigation}
          route={{ onRefresh: refreshFromCard }}
        />
        <AccountsMenu navigation={navigation} />
        <DividerHorizontal
          style={{
            marginVertical: moderateScale(24, 0.25),
            backgroundColor: "#F8F9FC",
            height: moderateScale(8, 0.25),
          }}
        />
        <AboutMenu navigation={navigation} />

        {/* <Button onPress={() => dispatch(Actions.logout())}>Logout</Button> */}
      </ScrollView>
    </Box>
  );
};

Accounts.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default Accounts;
