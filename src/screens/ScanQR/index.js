import React, { useRef } from "react";
import { View, Text } from "react-native";
import { CloseIcon, IconButton, ChevronLeftIcon } from "native-base";
import { RNCamera } from "react-native-camera";

const ScanQR = ({ navigation }) => {
  const camera = useRef();

  const scanQR = (QRCode) => {
    console.log("ScanQR", QRCode);
  };

  return (
    <View style={{ flex: 1 }}>
      <IconButton
        icon={<ChevronLeftIcon size={16} />}
        position="absolute"
        top={12}
        left={3}
        onPress={() => navigation.goBack()}
        zIndex={20}
      />
      <RNCamera
        ref={camera}
        style={{
          height: "100%",
          width: "100%",
          alignSelf: "center",
          marginBottom: 51,
        }}
        type={RNCamera.Constants.Type.back}
        androidCameraPermissionOptions={{
          title: "Permission to use camera",
          message: "We need your permission to use your camera",
          buttonPositive: "Ok",
          buttonNegative: "Cancel",
        }}
        onBarCodeRead={(QRCode) => scanQR(QRCode)}
        barCodeTypes={[RNCamera.Constants.BarCodeType.qr]}
        captureAudio={false}
      />
    </View>
  );
};

export default ScanQR;
