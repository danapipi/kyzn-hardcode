import { ScrollView, Checkbox } from "native-base";
import React, { useState, useEffect } from "react";
import { View, Text, Alert } from "react-native";
import RenderHtml from "react-native-render-html";
import { moderateScale } from "react-native-size-matters";
import ButtonCustom from "components/ui/ButtonCustom";
import { useDispatch, useSelector } from "react-redux";
import Actions from "actions";
import RNStorage from "../../utils/storage";
import { UserApi } from "../../utils/Api";
import _ from "lodash";
import { takeLatest, all, fork, put, delay, call } from "redux-saga/effects";


const user = UserApi.client;

const signUpConfirmation = ({ navigation, route }) => {
  const registerData = useSelector(state => state.AUTH.register);
  const familyOne = useSelector(state => state.FAMILY.all.dataFamilyOne);
  console.log("ajsdjqouwgpoq", registerData);
  console.warn("familyOne: ", familyOne);

  const dummyTermsAndCondition = {
    html: `

    <h2>Term And Condition of Membership</h2>

            <h4>1. The Clubhouse</h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sit amet posuere diam, vel vestibulum libero. Sed auctor viverra metus id rutrum. Suspendisse odio lectus, sollicitudin et egestas nec, rhoncus congue purus. Donec quis dapibus quam. Mauris eget vehicula velit, et ullamcorper turpis. Proin vitae urna a neque auctor efficitur. Suspendisse in odio ac augue dapibus feugiat ut non sem. Aenean non urna vestibulum, iaculis lectus at, porttitor dui. Cras porta mollis lectus, accumsan gravida justo iaculis at. Mauris id faucibus justo, vitae volutpat risus. Pellentesque vel aliquam neque. Vestibulum pulvinar, urna eu auctor viverra, mauris eros mattis nisl, sed bibendum nulla tortor congue diam. Praesent ultricies felis sem, quis malesuada dui sodales quis.</p>
        
            <h4>2. Force Majure</h4>
            <p>Praesent sit amet condimentum sem. Proin ultricies dui vel metus semper, in fringilla massa ornare. Nunc facilisis orci vel dignissim ullamcorper. Vestibulum imperdiet finibus tellus, malesuada lobortis nisi posuere at. Aliquam nec semper lorem, vel scelerisque felis. Proin eleifend sit amet libero eu tincidunt. In at lorem eleifend, posuere tortor eget, dictum odio. In accumsan ante lacus, vitae ornare turpis laoreet id. Mauris eget semper leo. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
        

  `,
  };
  const dispatch = useDispatch();

  const [checked, setChecked] = useState(false);
  const {
    pass,
    adjustPhoneNumber,
    confirmPass,
    name,
    gender,
    nationality,
    birthdate,
    postalCode,
    address,
    province,
    district,
    city,
    userName,
    referralCode,
  } = route.params;

  console.log("checked", checked);
  const validation = checked;
  // const loading = useSelector(state => state.AUTH.register.isLoading);
  // const error = useSelector((state) => state.AUTH.register.error);
  const [loading, setLoading] = useState(false);

  const register = () => {
    // dispatch(
    //   Actions.register(
    //     adjustPhoneNumber,
    //     pass,
    //     confirmPass,
    //     name,
    //     gender,
    //     nationality,
    //     birthdate,
    //     postalCode,
    //     address,
    //     province,
    //     district,
    //     city,
    //     userName
    //   )
    // );
    const requestBody = {
      phoneNumber: adjustPhoneNumber,
      password: pass,
      confPassword: confirmPass,
      name,
      gender,
      nationality,
      birthdate,
      postalCode,
      address,
      province,
      district,
      city,
      userName,
      referralCode,
    };

    let profile = {
      "id": 2,
      "name": userName,
      "username": null,
      "phone_number": adjustPhoneNumber,
      "xp": 0,
      "coin": 0,
      "balance": 0,
      "status": "Registered",
      "birthdate": "2001-11-30T16:00:00.000Z",
      "gender": gender,
      "address": address,
      "photo": "https://api.kyzn.life/api/uploads/default-avatar.png",
      "nationality": nationality,
      "postal_code": postalCode,
      "province": province,
      "district": district,
      "city": city,
      "referral_code": "Grand Opening",
      "id_card_no": null,
      "instagram_username": null,
      "company_name": null,
      "physical_limitation": null,
      "emergency_contact_name": null,
      "emergency_contact_relationship": null,
      "emergency_phone_number": null,
      "emergency_home_number": null,
      "email": null,
      "rfid_card_no": null,
      "created_at": "2021-09-25T01:00:00.000Z",
      "updated_at": "2021-11-22T18:25:52.000Z",
      "user_memberships": [],
      "user_membership_packages": [],
      "user_banks": [],
      "current_level": 3,
      "qr_string": "U2FsdGVkX1/JwImmnMw2T4NSMsa0RJISRjVdX8P/1P5H0xRny+SvaRfIK6HH/N23pG+5Zkp15rjokjEQ6JhRwRGk/zc/q4QUevc0ecAuasGPCV6sqIcWYmpm9+fPgFodK8DP9LWx0zkUAoFEN2fi3QMj92HCkLSbFkguAZceYKw=",
      "user_experiences": [
        {
          "id": 1,
          "level": 1,
          "exp_needed": 0,
          "created_at": "2021-11-11T09:51:36.000Z",
          "updated_at": null,
          "deleted_at": null,
        },
        {
          "id": 2,
          "level": 2,
          "exp_needed": 100,
          "created_at": "2021-11-11T09:51:36.000Z",
          "updated_at": null,
          "deleted_at": null,
        },
        {
          "id": 3,
          "level": 3,
          "exp_needed": 200,
          "created_at": "2021-11-11T09:51:36.000Z",
          "updated_at": null,
          "deleted_at": null,
        },
        {
          "id": 4,
          "level": 4,
          "exp_needed": 300,
          "created_at": "2021-11-11T09:51:36.000Z",
          "updated_at": null,
          "deleted_at": null,
        },
      ],
    };
    // closeSheet()
    setLoading(true);
    UserApi.setHeaderToken(
      "VYkVCcr0Pkw1hW36Hze01YZW0jHLb4FSkTAlcK7Yf8d6WJoFSp53a0ManpdLzcaSP8IrCi7D9UewFdvr8hLL3WLxrWqrDop9UWAh",
    );
    RNStorage.setToken(
      "VYkVCcr0Pkw1hW36Hze01YZW0jHLb4FSkTAlcK7Yf8d6WJoFSp53a0ManpdLzcaSP8IrCi7D9UewFdvr8hLL3WLxrWqrDop9UWAh",
    );
    dispatch(
      Actions.loginSuccess(
        "VYkVCcr0Pkw1hW36Hze01YZW0jHLb4FSkTAlcK7Yf8d6WJoFSp53a0ManpdLzcaSP8IrCi7D9UewFdvr8hLL3WLxrWqrDop9UWAh",
      ),
    );
    familyOne.user_family_details.unshift(profile);

    dispatch(Actions.profileSuccess(profile));
    // user
    //   .post(`/auth/register`, requestBody)
    //   .then((res) => {
    //     setLoading(false);
    //     UserApi.setHeaderToken(res.data.token);
    //     RNStorage.setToken(res.data.token);
    //     dispatch(Actions.loginSuccess(res.data.token));

    //     console.log("register res: ", res);
    //   })
    //   .catch((err) => {
    //     setLoading(false);
    //     let errorResponse = err.response.data.message;
    //     let errorMessage;

    //     if (errorResponse.hasOwnProperty("password")) {
    //       errorMessage = errorResponse.password;
    //     } else if (errorResponse.hasOwnProperty("confPassword")) {
    //       errorMessage = errorResponse.confPassword;
    //     } else if (errorResponse.hasOwnProperty("name")) {
    //       errorMessage = errorResponse.name;
    //     } else {
    //       errorMessage = errorResponse;
    //     }

    //     Alert.alert("Error", errorMessage, [
    //       {
    //         text: "OK",
    //         onPress: () => console.log("OK Pressed"),
    //         style: "cancel",
    //       },
    //     ]);
    //   });

    dispatch(Actions.firstTime(false));
    RNStorage.setFirstTime("false");
  };

  // useEffect(() => {
  //   if (error) {
  //     Alert.alert("Error", error, [
  //       {
  //         text: "OK",
  //         onPress: () => dispatch(Actions.signUpReset()),
  //         style: "cancel",
  //       },
  //     ]);
  //   }
  // }, [error]);

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: "white",
        paddingHorizontal: moderateScale(24, 0.25),
        paddingBottom: moderateScale(32, 0.25),
      }}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <RenderHtml source={dummyTermsAndCondition} />
        <View
          style={{
            flexDirection: "row",
            height: moderateScale(40),
            alignItems: "center",
          }}>
          <Checkbox
            value="agree"
            accessibilityLabel="This is a agreement checkbox"
            onChange={() => setChecked(!checked)}
            colorScheme="info"
          />
          <Text
            style={{
              marginLeft: moderateScale(6, 0.25),
              textAlign: "left",
              lineHeight: moderateScale(16),
            }}>
            {`I have read and agree with \nTerms and Conditions and Privacy Policy.`}
          </Text>
        </View>
        {validation ? (
          <ButtonCustom
            label={"Submit"}
            onPressButton={() => {
              register();
              // navigation.navigate("SetPreferences", { title: " " })
            }}
            buttonMarginHorizontal={-10}
            buttonMarginBottom={40}
            buttonMarginTop={14}
            isLoading={loading}></ButtonCustom>
        ) : (
          <ButtonCustom
            label={"Submit"}
            disableButton={true}
            buttonColor={"#838586"}
            buttonMarginHorizontal={-10}
            buttonMarginBottom={40}
            buttonMarginTop={14}
            // isLoading={loading}
          ></ButtonCustom>
        )}
      </ScrollView>
    </View>
  );
};

export default signUpConfirmation;
