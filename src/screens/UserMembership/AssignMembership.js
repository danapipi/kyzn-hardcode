import React, { useCallback, useEffect, useState, useRef } from "react";
import {
  Box,
  Button,
  Text,
  ScrollView,
  Image,
  ChevronDownIcon,
  FlatList,
  Select,
  CheckIcon,
  Actionsheet,
  KeyboardAvoidingView,
} from "native-base";
import {
  TouchableOpacity,
  ImageBackground,
  Dimensions,
  View,
  TouchableWithoutFeedback,
  Alert,
} from "react-native";
import PropTypes from "prop-types";
import { useDispatch, useSelector } from "react-redux";
import Selectors from "selectors";
import Actions from "actions";
import AccountsMenu from "components/ui/AccountsMenu";
import AboutMenu from "components/ui/About";
import AddKidsCard from "components/ui/AddKidsCard";
import PremiumCard from "components/ui/PremiumCard";
import { getScreenWidth } from "utils/size";
import DividerHorizontal from "components/ui/DividerHorizontal";
import { moderateScale } from "react-native-size-matters";
import SearchBar from "components/ui/SearchBar";
import { image } from "images";
import FamilyCard from "components/ui/FamilyCard";
import { UserApi } from "utils/Api";
import _ from "lodash";
import { useRoute } from "@react-navigation/core";

const user = UserApi.client;

const AssignMembership = ({ navigation }) => {
  const dispatch = useDispatch();
  let route = useRoute();
  let { id, data } = route.params;
  console.log("id params", id, data);
  const fams = useSelector((state) => state.FAMILY.uniqueFamily);
  const [families, setFamilies] = useState(fams.data.length > 0 ? fams.data : []);
  const [userIds, setUserIds] = useState([]);
  const [membershipPackage, setMembershipPackage] = useState({});
  const [isOpen, setIsOpen] = useState(false);
  const [userList, setUserList] = useState({});
  const [sendList, setSendList] = useState({});
  const [userAlreadyHave, setUserAlreadyHave] = useState([]);
  const [selectedUserMemberships, setSelectedUserMemberships] = useState([]);
  const [selectedUser, setSelectedUser] = useState([]);
  console.log("families", useSelector(Selectors.getAllFamilies), families, fams);

  const [selectedIndex, setSelectedIndex] = useState(0);

  const onSelected = (item) => {
    console.log("index selectedIndex", item);
    // setSelectedIndex(item);
  };

  console.log("asjdiqviweuqiwuvroqwu", families)

  const addToList = (membershipId) => {
    let copy = Object.assign({}, membershipPackage);
    let themember = _.findIndex(copy.memberships, { id: membershipId });
    copy.memberships[themember].quota_remaining =
      copy.memberships[themember].quota_remaining - 1;
    setMembershipPackage(copy);
    console.log(membershipPackage.memberships[themember]);

    let copy2 = Object.assign({}, sendList);
    copy2[membershipId].userIds = [
      ...copy2[membershipId].userIds,
      selectedUser,
    ];
    setSendList(copy2);
    setIsOpen(false);
  };

  const getAllUsersAssigned = (list) => {
    let result = [];
    _.values(list).forEach((element) => {
      result = [...result, ...element.userIds];
    });
    return result;
  };
  const onClose = () => {
    setIsOpen(false);
  };

  const openAssignSheet = (id, membership) => {
    // setSelectedUserMemberships(membership);
    setSelectedUser(id);
    setIsOpen(true);
  };

  const saveAssign = async () => {
    console.log("gagageh", `user-membership-package/${id}/assign`);
    navigation.navigate("UserMembership");

    // let payload = { memberships: _.values(sendList) };
    // console.log(payload);
    // let test = await user
    //   .post(`user-membership-package/${id}/assign`, payload)
    //   .then((res) => {
    //     if (res.data.success == true) {
    //       // dispatch(Actions.profile());
    //       navigation.navigate("UserMembership");
    //     }
    //   })
    //   .catch((err) => {
    //     Alert.alert("Error", err.response.data.message, [
    //       {
    //         text: "OK",
    //         onPress: () => console.log("OK Pressed"),
    //         style: "cancel",
    //       },
    //     ]);
    //   });
  };

  const onButtonPress = (id) => {
    console.log("onButtonPress");
    setUserIds((state) => [...state, id]);
  };

  const getPackage = async () => {
    let test = await user.get(`user-membership-package/${id}`);
    console.log("bahasd", `membership-package/${id}`);

    let userlist = {};
    let sendlist_temp = {};
    let already = [];
    let mplist = test.data.data;
    mplist.memberships.forEach((membership) => {
      let member_temp = Object.assign({}, membership);
      member_temp["members"] = null;
      member_temp["quota_remaining"] = member_temp.quota - member_temp.users.length
      membership["quota_remaining"] = member_temp.quota - member_temp.users.length
      sendlist_temp[membership.id] = { id: membership.id, userIds: [] };
      membership.users.forEach((element) => {
        already.push(element.id);
        // sendlist_temp[membership.id].userIds.push(element.id)
      });
      membership.members.forEach((member) => {
        if (!userlist[member.id]) {
          userlist[member.id] = member;
          userlist[member.id]["membership"] = {};
        }
        userlist[member.id]["membership"][membership.id] = member_temp;
      });
    });
    userlist = _.values(userlist);
    userlist.forEach((user) => {
      user["membership"] = _.values(user["membership"]);
    });
    setMembershipPackage(mplist);
    setSendList(sendlist_temp);
    setUserAlreadyHave(already);
    setUserList(userlist);
  };
  useEffect(() => {
    // getPackage();
  }, []);
  console.log("userIds", userIds);

  const renderListFams = ({ item }) => {
    // const [imageUrl, setImage] = useState({ uri: item.photo });
    console.log("item fams", item);
    return (
      <View
        style={{
          flex: 1,
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-between",
          // backgroundColor: 'green',
          paddingHorizontal: moderateScale(16),
          paddingVertical: moderateScale(10),
          borderColor: "#DFE3EC",
          borderTopWidth: 0.5,
          width: moderateScale(350),
        }}
      >
        <View style={{ flex: 1, flexDirection: "row", alignItems: "center" }}>
          <View style={{ width: moderateScale(35), height: moderateScale(35) }}>
            <Image
              source={{ uri: item.photo }}
              style={{
                width: moderateScale(35),
                height: moderateScale(35),
                marginRight: moderateScale(10),
              }}
              // onError={(err) => setImage(image.ic_profile_placeholder)}
            />
          </View>
          <Text
            style={{
              fontSize: moderateScale(14, 0.25),
              fontFamily: "Montserrat-Regular",
              fontWeight: "700",
              marginBottom: moderateScale(4),
              marginLeft: moderateScale(20),
            }}
          >
            {item.name}
            {/* {JSON.stringify(item.membership)} */}
          </Text>
        </View>
        {/* <Text>{JSON.stringify(item)}</Text> */}

        {/* <View style={{ flex: 1 }}> */}
        {/* </View> */}
        {/* TODO: determine the button color and the text */}
        <View
          style={{
            backgroundColor:
              item.role == "adult" ? "#1035BB" : "rgba(224, 224, 224, 1)",
            padding: moderateScale(4),
          }}
        >
          {getAllUsersAssigned(sendList).includes(item.id) ||
          userAlreadyHave.includes(item.id) ? (
            <TouchableWithoutFeedback>
              <Text
                style={{
                  color: item.role == "adult" ? "#fff" : "#000",
                  fontSize: moderateScale(12, 0.25),
                  fontFamily: "Montserrat-Regular",
                  fontWeight: "700",
                }}
              >
                {/* {`+  ${role}`} */}
                {`Added`}
              </Text>
            </TouchableWithoutFeedback>
          ) : (
            <TouchableWithoutFeedback
              onPress={() => openAssignSheet(item.id, item.membership)}
            >
              <Text
                style={{
                  color: item.role == "adult" ? "#fff" : "#000",
                  fontSize: moderateScale(12, 0.25),
                  fontFamily: "Montserrat-Regular",
                  fontWeight: "700",
                }}
              >
                {/* {`+  ${role}`} */}
                {`+  Add`}
              </Text>
            </TouchableWithoutFeedback>
          )}
        </View>
      </View>
    );
  };

  return (
    <Box
      alignItems="center"
      //   justifyContent="center"
      bg={"white"}
      style={{ flex: 1 }}
    >
      <Box
        style={{
          //   flex: 0.15,
          height: moderateScale(50, 0.25),
          width: "100%",
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "center",
          marginHorizontal: moderateScale(24),
          //   backgroundColor:"red"
        }}
      >
        {/* <Select
          selectedValue={families[selectedIndex].name}
          width={"100%"}
          accessibilityLabel="Choose Family"
          placeholder="Choose Family"
          _selectedItem={{
            endIcon: <CheckIcon size="5" />,
          }}
          mt={1}
          onValueChange={(item, index) => onSelected(item, index)}
        >
          {families.map((item, index) => (
            <Select.Item label={item.name} value={(item.id, index)} />
          ))}
        </Select> */}
        {/* </ImageBackground> */}
      </Box>
      <ScrollView
        style={{ flex: 1, height: "100%" }}
        showsVerticalScrollIndicator={false}
      >
        {families.length > 0 &&
          families.map((item, index) => renderListFams({ item, index }))}
      </ScrollView>
      {/* <Text>{JSON.stringify(_.values(sendList))}</Text>*/}
      {/* <Text>{JSON.stringify(_.values(sendList))}</Text>  */}
      <Button
        onPress={() => saveAssign()}
        isLoading={false}
        style={{
          backgroundColor: "#1035BB",
          marginVertical: moderateScale(16),
          width: "90%",
        }}
      >
        <Text
          style={{
            fontSize: moderateScale(18, 0.25),
            fontFamily: "Montserrat-Regular",
            fontWeight: "700",
            color: "#fff",
          }}
        >
          SAVE
        </Text>
      </Button>

      <Actionsheet isOpen={isOpen} onClose={onClose}>
        <Actionsheet.Content h={500}>
          <Image
            source={image.bg_actionsSheet}
            style={{ width: 300, height: 300, position: "absolute", right: 0 }}
            alt="image"
          />

          <KeyboardAvoidingView
            style={{
              width: 270,
              marginTop: 8,
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            {selectedUserMemberships.length > 0 &&
              selectedUserMemberships.map((item, index) => {
                let themember = _.findIndex(membershipPackage.memberships, {
                  id: item.id,
                });
                if (membershipPackage.memberships[themember].quota_remaining > 0) {
                  return (
                    <TouchableOpacity
                      onPress={() => addToList(item.id)}
                      style={{ height: moderateScale(50) }}
                    >
                      <Text>
                        {membershipPackage.memberships[themember].name} (
                        {membershipPackage.memberships[themember].quota_remaining}{" "}
                        remaining)
                      </Text>
                    </TouchableOpacity>
                  );
                }
              })}
            <TouchableOpacity onPress={() => onClose()}>
              <Text
                style={{
                  fontSize: moderateScale(18, 0.25),
                  fontFamily: "Montserrat-Regular",
                  fontWeight: "700",
                  color: "#000",
                }}
              >
                {`Cancel`}
              </Text>
            </TouchableOpacity>
          </KeyboardAvoidingView>
        </Actionsheet.Content>
      </Actionsheet>
    </Box>
  );
};

export default AssignMembership;
