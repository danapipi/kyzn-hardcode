import { capitalize, isArray, toUpper } from "lodash";
import {
  Box,
  Button,
  ChevronLeftIcon,
  Column,
  IconButton,
  Image,
  Modal,
  Progress,
  Row,
  ScrollView,
  Spinner,
  StatusBar,
  Text,
  useTheme,
} from "native-base";
import React, { useEffect, useState } from "react";
import Carousel, { Pagination } from "react-native-snap-carousel";
import { image, dummyImages } from "images";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { getScreenHeight, getScreenWidth } from "utils/size";
import PremiumCard from "components/ui/PremiumCard";
import { ImageBackground, StyleSheet } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import Actions from "actions";
import { moderateScale } from "react-native-size-matters";

import Selectors from "selectors";
import moment from "moment";
import _ from "lodash";

import { UserApi } from "utils/Api";
const userapi = UserApi.client;
const styles = StyleSheet.create({
  button: {
    marginBottom: 10,
  },
  pagination: {
    marginTop: -80,
  },
});

const UserMembership = ({ navigation }) => {
  const dispatch = useDispatch();
  const profile = useSelector(Selectors.getProfile);
  // const profile = useSelector((state) => state.AUTH.profile);

  const {
    user_memberships: memberships,
    user_membership_packages: membership_packages,
  } = profile;

  const combined_memberships_temp = [...memberships, ...membership_packages];
  const combined_memberships = combined_memberships_temp.filter(item => {
    if (item.membership) {
      return item.membership_id !== 1;
    }
    return true;
  });
  console.log('combined_memberships >>>>>> ', combined_memberships)

  useEffect(async () => {
    if (!profile) {
      console.log("profile is null", profile);
      // dispatch(Actions.profile());
    } else {
      console.log("profile is not null", profile);
      if (combined_memberships[currentMembership].membership_package) {
        // const result = await userapi.get(
        //   `/user-membership-package/${combined_memberships[currentMembership].id}`,
        // );
        // setCurrentMembershipPackageData(result.data.data);
      }
    }
  }, []);

  const isLoading = useSelector(Selectors.getIsLoadingProfile);
  console.log("profile is null 2", isLoading, profile, combined_memberships);

  const { colors } = useTheme();
  const [currentMembership, setCurrentMembership] = useState(0);
  const [currentMembershipPackageData, setCurrentMembershipPackageData] =
    useState({});

  console.log("profile is null 3", currentMembershipPackageData);

  const changeCurrentMembership = async index => {
    setCurrentMembership(index);
    if (combined_memberships[index].membership_package) {
      // const result = await userapi.get(
      //   `/user-membership-package/${combined_memberships[index].id}`,
      // );
      // setCurrentMembershipPackageData(result.data.data);
    }
  };
  if (isLoading) {
    return <Spinner />;
  }

  if (!combined_memberships) {
    return null;
  }

  console.log("currentMembershipPackageData", currentMembershipPackageData);

  const renderProgress = (startDate, endDate, now, diff) => {
    let diffEnd = endDate.diff(startDate, "days");
    let diffStart = now.diff(startDate, "days");
    console.log("check on", diffStart, diffEnd);

    if (diff <= 0) {
      return 100;
    } else if (diff > 0 && now > startDate && diffStart >= 0) {
      let progresTemp = (diffStart / diffEnd) * 100;
      return progresTemp;
    } else {
      return 0;
    }
  };

  const {
    started_at,
    expired_at,
    membership = {},
  } = combined_memberships[currentMembership];
  const { perks_description = "", price, name } = membership;

  const periodDays =
    started_at && expired_at
      ? moment(expired_at).diff(moment(started_at), "day")
      : 0;

  const remainingDays = expired_at ? moment().diff(moment(expired_at)) : 0;

  const membershipInfoRows = [
    {
      title: "Period",
      value: `${periodDays} days`,
    },
    {
      title: "Start Date",
      value: started_at ? moment(started_at).format("DD MMM YYYY") : "-",
    },
    {
      title: "Expired Date",
      value: expired_at ? moment(expired_at).format("DD MMM YYYY") : "-",
    },
  ];

  const recurringInfoRows = [
    {
      title: "Status",
      value: "Active",
      textProps: {
        color: "green",
      },
    },
    {
      title: "Amount",
      value: price ? `${price.toLocaleString()}` : "-",
    },
    {
      title: "Last Recurring",
      value: "-",
    },
    {
      title: "Next Recurring",
      value: "-",
    },
    {
      title: "Payment Using",
      value: "-",
    },
    {
      title: "Bank",
      value: "-",
    },
  ];

  const renderRow = ({ title, value, textProps, component }, index) => (
    <Row
      backgroundColor={index % 2 === 0 ? "gray:alpha.10" : "transparent"}
      flex={1}
      justifyContent="space-between"
      paddingY={3}
      paddingX={2}>
      <Text>{title}</Text>
      {value ? (
        <Text fontWeight={300} {...textProps}>
          {value}
        </Text>
      ) : (
        component
      )}
    </Row>
  );

  const renderGroupPackageRow = (item, index) => {
    return (
      <Box marginBottom={10}>
        <Text
          marginTop={2}
          marginBottom={3}
          fontSize="xs"
          fontWeight={300}
          color="gray">
          MEMBERSHIP INFO
        </Text>
        <Row
          backgroundColor={"gray:alpha.10"}
          flex={1}
          justifyContent="space-between"
          paddingY={3}
          paddingX={2}>
          <Text>Package Name</Text>
          <Text>{item.name}</Text>
          {/* {item.perks_description && (
            <Text fontWeight={300}>{item.perks_description}</Text>
          )} */}
        </Row>
        <Row
          // backgroundColor={index % 2 === 0 ? "gray:alpha.10" : "transparent"}
          flex={1}
          justifyContent="space-between"
          paddingY={3}
          paddingX={2}>
          <Text>Package Description</Text>
          {item.perks_description && (
            <Text fontWeight={300}>{item.perks_description}</Text>
          )}
        </Row>
        <Row
          backgroundColor={"gray:alpha.10"}
          flex={1}
          justifyContent="space-between"
          paddingY={3}
          paddingX={2}>
          <Text>Quota</Text>
          <Text>{item.quota}</Text>
        </Row>
        {/* <Row
          flex={1}
          justifyContent="space-between"
          paddingY={3}
          paddingX={2}
        >
          <Text>Assigned to</Text>
          <Text>-</Text>
        </Row> */}
        <Row
          // backgroundColor={index % 2 === 0 ? "gray:alpha.10" : "transparent"}
          flex={1}
          // backgroundColor={"gray:alpha.10"}
          justifyContent="space-between"
          paddingY={3}
          paddingX={2}>
          <Text>Duration</Text>
          <Text>
            {item.duration} {item.duration_unit}
          </Text>
        </Row>
        <Row
          backgroundColor={"gray:alpha.10"}
          flex={1}
          justifyContent="space-between"
          paddingY={3}
          paddingX={2}>
          <Text>Quota Remaining</Text>
          <Text>{item.quota - item.users.length}</Text>
        </Row>
      </Box>
    );
  };

  return (
    <Box flex={1} safeArea backgroundColor="white">
      <IconButton
        zIndex={5}
        position="absolute"
        top="12"
        left="1"
        icon={<ChevronLeftIcon size={10} />}
        onPress={() => navigation.goBack()}
      />
      <StatusBar
        backgroundColor={colors.platinumGreen}
        barStyle="dark-content"
      />
      <Box style={{ flex: 1 }}>
        <Carousel
          layout="default"
          data={combined_memberships}
          height={300}
          renderItem={({ item }) => {
            let startDate = moment(item.started_at);
            let endDate = moment(item.expired_at);
            let now = moment();

            let diff = endDate.diff(now, "days");

            let progress = renderProgress(startDate, endDate, now, diff);
            console.log("abscasdawda", item);
            return (
              <Box height={getScreenHeight() / 3}>
                {/* <Text>{JSON.stringify(item.membership)}</Text> */}
                <ImageBackground
                  source={
                    item?.membership?.image_url == null
                      ? image.img_memberhip_lscape
                      : { uri: item?.membership?.image_url }
                  }
                  // source={image.img_memberhip_lscape}
                  // resizeMode="contain"
                  alt="image"
                  // size="xl"
                  style={{
                    width: "100%",
                    height: moderateScale(120, 0.25),
                    marginTop: moderateScale(100, 0.25),
                    //   marginLeft: moderateScale(24, 0.25),
                    //   marginRight: moderateScale(16, 0.25),
                  }}>
                  {/* <Box backgroundColor="platinumGreen" flex={1} /> */}

                  {/* <Image
              style={{ width: 24, height: 24, alignSelf: "center" }}
              source={image.premium_logo}
              alt="image"
              resizeMode="contain"
            /> */}
                  <Box position="absolute" bottom={0} padding={5}>
                    <Text
                      fontSize="md"
                      fontWeight={300}
                      color="white"
                      marginBottom={5}
                      numberOfLines={1}>
                      {item.membership && item.membership.name}
                      {item.membership_package && item.membership_package.name}
                    </Text>
                    <Text fontSize="xs" color="white" marginBottom={2}>
                      {`START ${moment(item.started_at).format(
                        "DD MMM YYYY",
                      )} - END ${moment(item.expired_at).format(
                        "DD MMM YYYY",
                      )}`}
                    </Text>
                    <Progress
                      value={progress}
                      width={getScreenWidth() * 0.75}
                      bg="white:alpha.50"
                      _filledTrack={{ backgroundColor: "white" }}
                    />
                  </Box>
                </ImageBackground>
              </Box>
            );
          }}
          sliderWidth={getScreenWidth()}
          onSnapToItem={index => changeCurrentMembership(index)}
          itemWidth={320}
          snapToAlignment="center"
          enableSnap
        />
      </Box>

      <Box style={{ flex: 1 }}>
        <Pagination
          containerStyle={styles.pagination}
          dotsLength={combined_memberships.length}
          activeDotIndex={currentMembership}
          dotStyle={styles.paginationDot}
          dotColor={colors.black}
          inactiveDotColor={colors.gray}
          inactiveDotStyle={styles.paginationDot}
          inactiveDotOpacity={1}
          inactiveDotScale={1}
        />
        {combined_memberships.length > 0 &&
          combined_memberships[currentMembership].membership_package && (
            <ScrollView>
              <Box marginX={5}>
                <Box>
                  {/* {membershipInfoRows.map((row, index) => renderRow(row, index))} */}

                  <Button
                    borderRadius={0}
                    width="100%"
                    backgroundColor="button.black"
                    _pressed={{
                      backgroundColor: "button.black:alpha.20",
                    }}
                    style={[
                      styles.button,
                      { marginHorizontal: 0, marginTop: 14 },
                    ]}
                    onPress={() =>
                      navigation.navigate("AssignMembership", {
                        title: "Assign Membership",
                        id: combined_memberships[currentMembership].id,
                        data: combined_memberships[currentMembership]
                          .membership_package,
                      })
                    }>
                    Assign Family Members
                  </Button>
                  {!_.isEmpty(currentMembershipPackageData) &&
                    currentMembershipPackageData.memberships.map(
                      (item, index) => renderGroupPackageRow(item, index),
                    )}
                </Box>
              </Box>
              {/* <Text>{JSON.stringify(currentMembershipPackageData)}</Text> */}
            </ScrollView>
          )}
        {combined_memberships.length > 0 &&
          combined_memberships[currentMembership].membership && (
            <ScrollView>
              <Box marginX={5}>
                <Text
                  marginTop={2}
                  marginBottom={3}
                  fontSize="xs"
                  fontWeight={300}
                  color="gray">
                  MEMBERSHIP INFO
                </Text>
                <Box>
                  {membershipInfoRows.map((row, index) =>
                    renderRow(row, index),
                  )}
                </Box>
                <Text
                  marginTop={6}
                  marginBottom={3}
                  fontSize="xs"
                  fontWeight={300}
                  color="gray">
                  RECURRING INFO
                </Text>
                <Box>
                  {recurringInfoRows.map((row, index) => renderRow(row, index))}
                </Box>
                {/* <Box marginY={5}>
            <Button
              borderRadius={0}
              width="100%"
              backgroundColor="button.black"
              _pressed={{
                backgroundColor: "button.black:alpha.20",
              }}
              style={styles.button}>
              Upgrade
            </Button>
            <Button
              borderRadius={0}
              width="100%"
              style={styles.button}
              backgroundColor="transparent"
              _pressed={{
                backgroundColor: "button.black:alpha.20",
              }}
              _text={{
                color: "black",
              }}
              onPress={() => {}}>
              Stop Recurring
            </Button>
          </Box> */}
                <Box
                  borderWidth={2}
                  marginTop={5}
                  marginBottom={10}
                  padding={3}
                  borderColor="gray:alpha.10">
                  <Text
                    marginBottom={3}
                    fontSize="sm"
                    fontWeight={300}
                    color="gray">
                    What you get:
                  </Text>
                  {perks_description.split("\n").map(benefit => (
                    <Row alignItems="center" marginY={1}>
                      <Icon name="check" color={colors.lightGreen} size={18} />
                      <Text
                        marginLeft={1}
                        fontSize="sm"
                        flexWrap="wrap"
                        lineHeight="xs">
                        {benefit}
                      </Text>
                    </Row>
                  ))}
                </Box>
              </Box>
            </ScrollView>
          )}
      </Box>
    </Box>
  );
};

export default UserMembership;
