import React, { useState } from "react";
import { View, Text, TouchableOpacity, Image } from "react-native";
import { moderateScale } from "react-native-size-matters";
import Commons from "../../utils/common";
import { image } from "images";
import _ from 'lodash';
import moment from "moment";

const HomeActivityCard1 = ({ label, data, navigation, onPressViewAll, dataDiff}) => {

  let dataFiltered = _.filter(data.activityScheduleAll, function(o) { 
    return moment(o.start_time) >= moment() && o.activity_court.activity.activity_type_id !== 1
  });
  let dataOrder = _.orderBy(dataFiltered, ['start_time'], ['asc']);

  const handlePress = (item) => {
    let arryTemp = _.filter(dataDiff.upcomingSchedule, function(o) { 
      return o.id == item.id
    });

    if(arryTemp.length == 0){
      navigation.navigate("SportDetail", {
        data: { item: item.activity_court.activity },
        type: item.activity_court.activity.activity_type_id,
      })
    } else {
      navigation.navigate(
        "DetailActivity", 
        { 
          "title": "Detail Activity",
          "data" : {item: item},
          "typeDetail": 1 
        })
    }
  } 


  return (
    <View
      style={{
        flex: 1,
        marginTop: moderateScale(32, 0.25),
        marginRight: moderateScale(16, 0.25),
        marginLeft:16
      }}
    >
        <View>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              marginBottom: moderateScale(16, 0.25),
              alignItems: "center",
              
            }}
          >
            <Text
              style={{
                color: "black",
                fontSize: moderateScale(14, 0.25),
                fontWeight: "700",
                fontFamily: "Rubik-Regular",
              }}
            >
              {label}
            </Text>
            <TouchableOpacity onPress={onPressViewAll}>
              <Text
                style={{
                  color: "black",
                  fontSize: moderateScale(10, 0.25),
                  fontWeight: "400",
                  fontFamily: "Rubik-Regular",
                }}
              >
                View All
              </Text>
            </TouchableOpacity>
          </View>
          <View style={{ flexDirection: "row" }}>
            {dataOrder.map((item, index) => {
              // console.log("item doang", item)
              if(index < 3) {
                console.log("item kan", item.activity_court.activity);
                return (
                  <View
                    key={item.id}
                    style={{
                      height: moderateScale(173, 0.25),
                      width: moderateScale(108, 0.25),
                      borderWidth: 1,
                      borderColor: "#EEEEEE",
                      marginRight:10
                      // marginHorizontal:
                      //   item.id % 2 == 0 ? moderateScale(10, 0.25) : 0,
                      // flex: 1,
                    }}
                  >
                    <TouchableOpacity
                      onPress={() => handlePress(item)}
                    >
                      <Image
                        source={{ uri: item.activity_court.activity.image_thumbnail }}
                        // source={item.image_thumbnail}
                        style={{
                          width: moderateScale(108, 0.25),
                          height: moderateScale(104, 0.25),
                        }}
                      ></Image>
                      <View
                        style={{
                          flexDirection: "row",
                          marginHorizontal: moderateScale(8, 0.25),
                          // justifyContent: "center",
                          alignItems: "center",
                          marginTop: moderateScale(6, 0.25),
                        }}
                      >
                        <Text
                          style={{
                            color: "#424242",
                            fontSize: moderateScale(6, 0.25),
                            fontWeight: "500",
                            fontFamily: "Rubik-Regular",
                          }}
                        >
                          Earn
                        </Text>
                        <View
                          style={{
                            flexDirection: "row",
                            backgroundColor: "#6778D0",
                            borderRadius: 10,
                            marginHorizontal: moderateScale(4, 0.25),
                            width: moderateScale(28, 0.25),
                            height: moderateScale(10, 0.25),
                            justifyContent: "center",
                            alignItems: "center",
                          }}
                        >
                          <Text
                            style={{
                              color: "white",
                              fontSize: moderateScale(5, 0.25),
                              fontWeight: "500",
                              fontFamily: "Rubik-Regular",
                              marginRight: moderateScale(1, 0.25),
                            }}
                          >
                            XP
                          </Text>
                          <Text
                            style={{
                              color: "white",
                              fontSize: moderateScale(6, 0.25),
                              fontWeight: "500",
                              fontFamily: "Rubik-Regular",
                            }}
                          >
                            {item.activity_court.activity.xp_reward}
                          </Text>
                        </View>
                        <View
                          style={{
                            flexDirection: "row",
                            backgroundColor: "#FFA800",
                            borderRadius: 10,
                            width: moderateScale(32, 0.25),
                            height: moderateScale(10, 0.25),
                            justifyContent: "center",
                            alignItems: "center",
                          }}
                        >
                          <Text
                            style={{
                              color: "white",
                              fontSize: moderateScale(5, 0.25),
                              fontWeight: "500",
                              fontFamily: "Rubik-Regular",
                              marginRight: moderateScale(1, 0.25),
                            }}
                          >
                            ZENI
                          </Text>
                          <Text
                            style={{
                              color: "white",
                              fontSize: moderateScale(6, 0.25),
                              fontWeight: "500",
                              fontFamily: "Rubik-Regular",
                            }}
                          >
                            {item.activity_court.activity.coin_reward}
                          </Text>
                        </View>
                      </View>
                      <View
                        style={{
                          marginTop: moderateScale(11, 0.25),
                          marginHorizontal: moderateScale(8, 0.25),
                        }}
                      >
                        <Text
                          style={{
                            color: "black",
                            fontSize: moderateScale(8, 0.25),
                            fontWeight: "500",
                            fontFamily: "Rubik-Regular",
                            marginBottom: moderateScale(12, 0.25),
                          }}
                        >
                          {item.activity_court.activity.name}
                        </Text>
                        <View
                          style={{
                            flexDirection: "row",
                            justifyContent: "space-between",
                          }}
                        >
                          <Text
                            style={{
                              color: "black",
                              fontSize: moderateScale(8, 0.25),
                              fontWeight: "700",
                              fontFamily: "Rubik-Regular",
                            }}
                          >
                          {Commons.convertToRupiah(item.activity_court.activity.step_duration_price)}
                          </Text>
                          {label === "Personal Trainer" && (
                            <View style={{ flexDirection: "row" }}>
                              <Image
                                source={image.star}
                                style={{
                                  width: moderateScale(8, 0.25),
                                  height: moderateScale(8, 0.25),
                                  marginRight: moderateScale(2, 0.25),
                                }}
                              ></Image>
                              <Text
                                style={{
                                  color: "black",
                                  fontSize: moderateScale(8, 0.25),
                                  fontWeight: "500",
                                  fontFamily: "Rubik-Regular",
                                }}
                              >
                                {item.activity_court.activity.rating}
                              </Text>
                            </View>
                          )}
                        </View>
                      </View>
                    </TouchableOpacity>
                  </View>
                );
              }
            })}
          </View>
        </View>
    </View>
  );
};

export default HomeActivityCard1;
