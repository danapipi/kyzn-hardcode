import React from "react";
import _get from "lodash/get";
import { View, Text, Image, ImageBackground, Dimensions } from "react-native";
import { image } from "images";
import { moderateScale } from "react-native-size-matters";
import moment from "moment";
import { HStack } from "native-base";
import _ from "lodash";

const PremiumCard = ({ data, isMember }) => {
  const membershipData =
    data.user_memberships &&
    [...data.user_membership_packages, ...data.user_memberships].filter(
      item => _.get(item, "membership.name") !== "Non-Member",
    );
  console.log('Wucau membershipData: ', membershipData)
  let imageUrl;
  let start_at;
  let expire_at;
  let memberStatus;

  if (membershipData.length > 0) {
    imageUrl = _get(
      membershipData[membershipData.length - 1],
      "membership.image_url",
      null,
    );
    start_at = _get(
      membershipData[membershipData.length - 1],
      "started_at",
      moment().format("DD MMM yyyy"),
    );
    expire_at = _get(
      membershipData[membershipData.length - 1],
      "expired_at",
      moment().format("DD MMM yyyy"),
    );
    memberStatus =
      _get(membershipData[0], "membership.name", " ") !== " "
        ? _get(membershipData[0], "membership.name", " ")
        : _get(membershipData[0], "membership_package.name", " ");
  } else {
    imageUrl = _get(membershipData[0], "membership.image_url", null);
    start_at = _get(
      membershipData[0],
      "started_at",
      moment().format("DD MMM yyyy"),
    );
    expire_at = _get(
      membershipData[0],
      "expired_at",
      moment().format("DD MMM yyyy"),
    );
    memberStatus =
      _get(membershipData[0], "membership.name", " ") !== " "
        ? _get(membershipData[0], "membership.name", " ")
        : _get(membershipData[0], "membership_package.name", " ");
  }

  const StaticCard = () => {
    if (imageUrl) {
      return (
        <Image
          source={{ uri: imageUrl }}
          // resizeMode="contain"
          alt="image"
          // size="xl"
          style={{
            width: "100%",
            height: moderateScale(85, 0.25),
            //   marginLeft: moderateScale(24, 0.25),
            //   marginRight: moderateScale(16, 0.25),
          }}
        />
      );
    } else {
      return (
        <ImageBackground
          source={image.img_memberhip_lscape}
          // resizeMode="contain"
          alt="image"
          // size="xl"
          style={{
            // width: "100%",
            width: Dimensions.get("window").width * 0.922,
            height: moderateScale(85, 0.25),
            //   marginLeft: moderateScale(24, 0.25),
            //   marginRight: moderateScale(16, 0.25),
            padding: moderateScale(16, 0.25),
          }}>
          <Text
            style={{
              fontWeight: "700",
              fontSize: moderateScale(14, 0.25),
              letterSpacing: 0.2,
              marginBottom: moderateScale(17, 0.25),
              fontFamily: "Montserrat-Regular",
              color: "#fff",
            }}>
            {memberStatus}
          </Text>
          {memberStatus != "Non-Member" && (
            <Text
              style={{
                fontWeight: "600",
                fontSize: moderateScale(10, 0.25),
                letterSpacing: 0.2,
                // marginBottom: moderateScale(8, .25),
                fontFamily: "Montserrat-Regular",
                color: "#fff",
              }}>
              {`START ${moment(start_at).format("DD MMM yyyy")} - END ${moment(
                expire_at,
              ).format("DD MMM yyyy")}`}
            </Text>
          )}
        </ImageBackground>
      );
    }
  };

  const EmptyData = () => {
    return (
      <Image
        source={image.no_member_img}
        // resizeMode="contain"
        alt="image"
        // size="xl"
        style={{
          width: "100%",
          height: moderateScale(85, 0.25),
          //   marginLeft: moderateScale(24, 0.25),
          //   marginRight: moderateScale(16, 0.25),
        }}
      />
    );
  };

  const DynamicCard = () => {};
  return (
    <View
      style={{
        marginBottom: moderateScale(15, 0.25),
        marginHorizontal: moderateScale(16, 0.25),
      }}>
      {membershipData.length > 0 ? <StaticCard /> : <EmptyData />}
    </View>
  );
};

export default PremiumCard;
