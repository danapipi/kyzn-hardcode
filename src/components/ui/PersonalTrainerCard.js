import React, { useEffect, useState } from "react";
import _ from 'lodash'
import { View, Text, TouchableOpacity, Image, Touchable } from "react-native";
import { moderateScale } from "react-native-size-matters";
import Commons from "../../utils/common";
import { image } from "images";
import { UserApi } from '../../utils/Api'
import { useDispatch, useSelector } from "react-redux";


const userapi = UserApi.client

const PersonalTrainerCard = ({ label, data, navigation, onPressViewAll }) => {
  const [dataDummy, setDataDummy] = useState([
    {
      id: 1,
      xp: 800,
      zeni: 500,
      title: "Shameer Jainab",
      price: 300000,
      rating: 4.7,
    },
    {
      id: 3,
      xp: 800,
      zeni: 500,
      title: "Kevin Jacob",
      price: 300000,
      rating: 4.7,
    },
    {
      id: 3,
      xp: 800,
      zeni: 500,
      title: "Shameer Jainab",
      price: 300000,
      rating: 4.7,
    },
  ]);

  const [uriImage, setUriImage] = useState("")
  const [coachList, setCoachList] = useState([])
  const profile = useSelector((state) => state.AUTH.profile);
  const dataWorker = useSelector(state => state.WORKER.featuredWorker.worker);


  const { 
    class_activity_type_id, 
    panorama_activity_type_id, 
    pt_activity_type_id, 
    rent_court_activity_type_id
  } = useSelector((state) => state.PRODUCT.activityTypeId.activityTypeId);



  useEffect(() => {
    // userapi.get(`/worker/`)
    //   .then(res => {
    //     console.log("res worker: ", res.data.data);
    //     setCoachList(res.data.data)
    //   })
    //   .catch(err => {
    //     console.log("err: ", err);
    //   })
  }, [])

  const viewCoachDetail = (item,index) => {
    const userId = _.get(profile,"data.id")
    navigation.navigate("PersonalTrainers", {
      screen: "PersonalTrainerView",
      params: {
        id: item.id, 
        type: pt_activity_type_id, 
        data: dataWorker[index]
      } 
    })

    // userapi.get(`/worker/${item.id}?userId=${userId}`)
    //   .then(res => {
    //     console.log("res worker one: ", res.data.data);
    //     const item = res.data.data
    //     navigation.navigate("PersonalTrainers", {
    //       screen: "PersonalTrainerView",
    //       params: {
    //         id: item.id, 
    //         type: pt_activity_type_id, 
    //         data: item
    //       } 
    //     })
    //   })
    //   .catch(err => {
    //     console.log("err: ", err);
    //   })

  }


  
  return (
    <View
      style={{
        flex: 1,
        marginTop: moderateScale(32, 0.25),
        marginRight: moderateScale(16, 0.25),
      }}>
      <View>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            marginBottom: moderateScale(16, 0.25),
            alignItems: "center",
          }}>
          <Text
            style={{
              color: "black",
              fontSize: moderateScale(14, 0.25),
              fontWeight: "700",
              fontFamily: "Rubik-Regular",
            }}>
            {label}
          </Text>
          {onPressViewAll && (
            <TouchableOpacity onPress={onPressViewAll}>
              <Text
                style={{
                  color: "black",
                  fontSize: moderateScale(10, 0.25),
                  fontWeight: "400",
                  fontFamily: "Rubik-Regular",
                }}>
                View All
              </Text>
            </TouchableOpacity>
          )}
        </View>
        <View style={{ flexDirection: "row" }}>
          {data.map((item, index) => {
            let imageTemp = uriImage == "" ? { uri: item.worker.photo } : uriImage
            return (
              <TouchableOpacity onPress={() => viewCoachDetail(item,index)}>
                <View
                  key={item.id}
                  style={{
                    height: moderateScale(173, 0.25),
                    width: moderateScale(108, 0.25),
                    borderWidth: 1,
                    borderColor: "#EEEEEE",
                    marginHorizontal:
                      index % 2 !== 0 ? moderateScale(10, 0.25) : 0,
                  }}>
                  {/* <TouchableOpacity
                      onPress={() =>
                        navigation.navigate("SportDetail", {
                          data: {item : item},
                          type: item.activity_type_id,
                        })
                      }
                    > */}
                  <Image
                    // source={{ uri: "https://via.placeholder.com/103x104" }}
                    // source={item.image_thumbnail}
                    source={imageTemp}
                    onError={({ nativeEvent: {error} }) => {
                      setUriImage(image.img_product_placeholder)
                    }}
                    style={{
                      width: moderateScale(108, 0.25),
                      height: moderateScale(104, 0.25),
                    }}></Image>
                  {/* <View
                    style={{
                      flexDirection: "row",
                      marginHorizontal: moderateScale(8, 0.25),
                      // justifyContent: "center",
                      alignItems: "center",
                      marginTop: moderateScale(6, 0.25),
                    }}>
                    <Text
                      style={{
                        color: "#424242",
                        fontSize: moderateScale(6, 0.25),
                        fontWeight: "500",
                        fontFamily: "Rubik-Regular",
                      }}>
                      Earn
                    </Text>
                    <View
                      style={{
                        flexDirection: "row",
                        backgroundColor: "#6778D0",
                        borderRadius: 10,
                        marginHorizontal: moderateScale(4, 0.25),
                        width: moderateScale(28, 0.25),
                        height: moderateScale(10, 0.25),
                        justifyContent: "center",
                        alignItems: "center",
                      }}>
                      <Text
                        style={{
                          color: "white",
                          fontSize: moderateScale(5, 0.25),
                          fontWeight: "500",
                          fontFamily: "Rubik-Regular",
                          marginRight: moderateScale(1, 0.25),
                        }}>
                        XP
                      </Text>
                      <Text
                        style={{
                          color: "white",
                          fontSize: moderateScale(6, 0.25),
                          fontWeight: "500",
                          fontFamily: "Rubik-Regular",
                        }}>
                        {item.status}
                      </Text>
                    </View>
                    <View
                      style={{
                        flexDirection: "row",
                        backgroundColor: "#FFA800",
                        borderRadius: 10,
                        width: moderateScale(32, 0.25),
                        height: moderateScale(10, 0.25),
                        justifyContent: "center",
                        alignItems: "center",
                      }}>
                      <Text
                        style={{
                          color: "white",
                          fontSize: moderateScale(5, 0.25),
                          fontWeight: "500",
                          fontFamily: "Rubik-Regular",
                          marginRight: moderateScale(1, 0.25),
                        }}>
                        ZENI
                      </Text>
                      <Text
                        style={{
                          color: "white",
                          fontSize: moderateScale(6, 0.25),
                          fontWeight: "500",
                          fontFamily: "Rubik-Regular",
                        }}>
                        {Commons.convertToRupiah(item.lowest_price)}
                      </Text>
                    </View>
                  </View> */}
                  <View
                    style={{
                      height: "30%",
                      marginTop: moderateScale(11, 0.25),
                      marginHorizontal: moderateScale(8, 0.25),
                    }}>
                    <Text
                      style={{
                        color: "black",
                        fontSize: moderateScale(10, 0.25),
                        fontWeight: "500",
                        fontFamily: "Rubik-Regular",
                        marginBottom: moderateScale(12, 0.25),
                      }}>
                      {item.worker.name}
                      {/* {item.name} */}
                    </Text>
                    <View
                      style={{
                        flexDirection: "row",
                        position: "absolute",
                        bottom: 0
                      }}>
                      <Text
                        style={{
                          color: "black",
                          fontSize: moderateScale(8, 0.25),
                          fontWeight: "700",
                          fontFamily: "Rubik-Regular",
                          flex: 1,
                        }}>
                        {/* Rp {Commons.convertToRupiah(item.lowest_price)} */}
                        {/* {Commons.convertToRupiah(_.minBy(item.activity_workers, "price").step_price)} */}
                      </Text>
                      {label === "Personal Trainer" && (
                        <View style={{ flexDirection: "row" }}>
                          <Image
                            source={image.star}
                            style={{
                              width: moderateScale(8, 0.25),
                              height: moderateScale(8, 0.25),
                              marginRight: moderateScale(2, 0.25),
                            }}></Image>
                          <Text
                            style={{
                              color: "black",
                              fontSize: moderateScale(8, 0.25),
                              fontWeight: "500",
                              fontFamily: "Rubik-Regular",
                            }}>
                            {item.worker.rating}
                            {/* {item.rating} */}
                          </Text>
                        </View>
                      )}
                    </View>
                  </View>
                  {/* </TouchableOpacity> */}
                </View>
              </TouchableOpacity>
            );
          })}
        </View>
      </View>
    </View>
  );
};

export default PersonalTrainerCard;
