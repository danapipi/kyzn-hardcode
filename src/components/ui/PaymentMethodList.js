import React from "react";
import { View, Text, Image } from "react-native";
import { moderateScale } from "react-native-size-matters";
import DividerHorizontal from "components/ui/DividerHorizontal";
import {
  Box,
  IconButton,
  Button,
  useTheme,
  ChevronRightIcon,
} from "native-base";

export default function PaymentMethodList({ data }) {
  return (
    <View
      style={{
        paddingHorizontal: moderateScale(21, 0.25),
        flexDirection: "row",
        alignItems: "center",
        flex: 1,
        height: moderateScale(51, 0.25),
        borderWidth: 1,
        borderColor: "#EEEEEE"
      }}
    >
      <View style={{flex: 2}}>
        <Image
          source={data.item.image}
          style={{
            height: moderateScale(20, 0.25),
            width: moderateScale(73, 0.25),
          }}
        />
      </View>
      <IconButton
        onPress={() => {
          console.log("topup");
        }}
        icon={<ChevronRightIcon />}
        style={{
        //   width: moderateScale(7, 0.25),
        //   height: moderateScale(20, 0.25),
        }}
      />
    </View>
  );
}
