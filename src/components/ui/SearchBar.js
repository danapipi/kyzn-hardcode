import React, { useCallback } from 'react';
import propTypes from 'prop-types';
import { Text, View, TextInput, KeyboardAvoidingView, Platform } from 'react-native';
import { moderateScale } from 'react-native-size-matters';

const SearchBar = ({
  placeholder,  
  onChangeText,
  onSubmit,
  value="",
  contentContainerStyle
}) => {
  const onTextChange = useCallback((text) => {
    onChangeText(text)
  }) 

  const onSubmitEdit = useCallback(({nativeEvent}) => {
    onSubmit(nativeEvent)
  }) 

  return(
    <View style={[{
      paddingVertical: moderateScale(Platform.OS == 'ios' ? 15 : -2, .25),
      paddingLeft: moderateScale(Platform.OS == 'ios' ? 23 : 16, .25),
      paddingRight: moderateScale(16, .25),
      alignItems: 'flex-start',
      justifyContent: 'center',
      borderColor: '#C5CDDB',
      borderWidth: 1
    }, contentContainerStyle]}>
      {/* <KeyboardAvoidingView> */}
        <TextInput 
          placeholder={placeholder}
          placeholderTextColor={"rgba(162, 168, 172, 1)"}
          onChangeText={onTextChange}
          onSubmitEditing={onSubmitEdit}
          style={{
            width: '100%',
            fontSize: moderateScale(12, .25)
          }}
          value={value == "" ? null : value}
        />
      {/* </KeyboardAvoidingView> */}
    </View>
  )
};

SearchBar.defaultProps = {
  placeholder: "placeholder here",
  onChangeText: () => null,
  onSubmit: () => null,
  contentContainerStyle: {}
}

SearchBar.propTypes = {
  placeholder: propTypes.string,
  onChangeText: propTypes.func,
  onSubmit: propTypes.func,
  contentContainerStyle: propTypes.shape({})
}

export default SearchBar;
