import React from "react";
import {
  Box,
  Text,
  Button,
  FlatList,
  ScrollView,
  HStack,
  Image,
  ZStack,
  VStack,
} from "native-base";
import _isNull from 'lodash/isNull';
import { TouchableOpacity } from "react-native";
import { moderateScale } from "react-native-size-matters";
import LinearGradient from "react-native-linear-gradient";
import { image } from "../../../assets/images";

export default function TopUpCard({ creditValue, navigation, cardStyle, nameOfUser = "My", userId = null }) {
  console.log("credit", creditValue);
  const ComponentCard1 = () => {
    return (
      <TouchableOpacity onPress={() => {
        if(_isNull(userId) == true) navigation.navigate("TopUpDetails")
        else navigation.navigate("TopUpDetails", {userId: userId})
        // navigation.navigate("TopUpDetails")
      }}>
        <Box
          style={{
            height: moderateScale(63, 0.25),
            marginHorizontal: moderateScale(16, 0.25),
            marginBottom: moderateScale(24, 0.25),
            borderWidth: 1,
            borderColor: "#EBEEF4",
            marginTop: moderateScale(11, 0.25),
          }}
          shadow={1}>
          <ZStack
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
            <Image
              source={image.topup_bg}
              // size="xl"
              alt="topup_bg"
              style={{
                width: moderateScale(64, 0.25),
                height: moderateScale(63, 0.25),
                justifyContent: "flex-end",
                alignSelf: "flex-end",
              }}
            />
            <Box
              style={{ flexDirection: "row", alignItems: "center", flex: 1 }}>
              <Box
                style={{
                  flexDirection: "column",
                  flex: 2,
                  paddingLeft: moderateScale(20, 0.25),
                }}>
                <Text
                  style={{
                    fontSize: moderateScale(12, 0.25),
                    color: "#57626A",
                    fontWeight: "700",
                    fontFamily: "Montserrat-Regular",
                  }}>
                  {nameOfUser} Credits
                </Text>
                <Text
                  style={{
                    fontSize: moderateScale(14, 0.25),
                    color: "black",
                    fontWeight: "700",
                    fontFamily: "Montserrat-Regular",
                  }}>
                  {creditValue}
                </Text>
              </Box>
              <Box
                style={{
                  alignItems: "flex-end",
                  borderColor: "#1035BB",
                  borderWidth: 1,
                  paddingHorizontal: moderateScale(15, 0.25),
                  marginHorizontal: moderateScale(24, 0.25),
                }}>
                <Text
                  style={{
                    fontSize: moderateScale(12, 0.25),
                    color: "#1035BB",
                    fontWeight: "700",
                    fontFamily: "Montserrat-Regular",
                  }}>
                  TOPUP
                </Text>
              </Box>
            </Box>
          </ZStack>
        </Box>
      </TouchableOpacity>
    );
  };
  const ComponentCard2 = () => {
    return (
      <TouchableOpacity
        onPress={() => navigation.navigate("TopUp")}>
        <LinearGradient
          colors={["#1035BB", "#8BBA96"]}
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 0 }}
          style={{
            height: moderateScale(44, 0.25),
            marginHorizontal: moderateScale(16, 0.25),
            marginBottom: moderateScale(32, 0.25),
            //   backgroundColor: "red",
          }}>
          <ZStack
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
            <Image
              source={image.topup_bg}
              // size="xl"
              alt="topup_bg"
              style={{
                width: moderateScale(42, 0.25),
                height: moderateScale(47, 0.25),
                justifyContent: "flex-end",
                alignSelf: "flex-end",
              }}
            />
            <Box
              style={{ flexDirection: "row", alignItems: "center", flex: 1 }}>
              <Text
                style={{
                  fontSize: moderateScale(12, 0.25),
                  color: "#ffffff",
                  fontWeight: "700",
                  marginLeft: moderateScale(20, 0.25),
                  flex: 2,
                  fontFamily: "Montserrat-Regular",
                }}>
                {nameOfUser} Credits {creditValue}
              </Text>
              <Box
                style={{
                  alignItems: "flex-end",
                  borderColor: "#ffffff",
                  borderWidth: 1,
                  paddingHorizontal: moderateScale(15, 0.25),
                  marginHorizontal: moderateScale(13, 0.25),
                }}>
                <Text
                  style={{
                    fontSize: moderateScale(12, 0.25),
                    color: "#ffffff",
                    fontWeight: "700",
                    fontFamily: "Montserrat-Regular",
                  }}>
                  TOPUP
                </Text>
              </Box>
            </Box>
          </ZStack>
        </LinearGradient>
      </TouchableOpacity>
    );
  };
  return <Box>{cardStyle === 1 ? <ComponentCard1 /> : <ComponentCard2 />}</Box>;
}
