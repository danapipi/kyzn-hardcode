import React, { useRef, useEffect, useState } from "react";
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  Image,
} from "react-native";
import { moderateScale } from "react-native-size-matters";
import Icon from "react-native-vector-icons/FontAwesome";
import { image } from "../../../assets/images";
import CountryFlag from "react-native-phone-number-input";

// import { CachedImage } from "react-native-cached-image";

const InputField = ({
  label,
  labelColor,
  labelMarginBottom,
  placeHolder,
  labelFontSize,
  labelFontFamily,
  showPassword,
  boxColor,
  isPassword,
  changeShowPassword,
  changeValue,
  changeCountry,
  valueInput,
  error,
  errorMessage,
  errorLogo = true,
  message,
  textColor,
  widthBox,
  containerMarginHorizontal,
  containerMarginBottom,
  containerBgcolor,
  refInputName,
  appiumLabel,
  appiumLabelPassword,
  disableEdit,
  textInfo,
  textInfoColor,
  showIcon,
  iconValue,
  ...props
}) => {
  let refInput = useRef(() => refInputName);
  const [isFocused, setFocus] = useState(false);
  const phoneInput = useRef(() => refInputName);
  const EyeIcon = <Icon name="rocket" size={30} color="#900" />;

  const onFocus = () => {
    // if(error){
    setFocus(true);
    // }
    console.log(
      appiumLabel,
      "onFocus :",
      "\n isFocused =",
      isFocused,
      "\n prevFocus =",
      prevFocus,
      "\n error =",
      error
    );
  };
  const onBlur = () => {
    setFocus(false);
    console.log(
      appiumLabel,
      "onBlur  :",
      "\n isFocused =",
      isFocused,
      "\n prevFocus =",
      prevFocus,
      "\n error =",
      error
    );
  };

  const prevFocusRef = useRef();
  useEffect(() => {
    // console.log(appiumLabel, "useEffect =");
    prevFocusRef.current = isFocused;
  }, []);
  const prevFocus = prevFocusRef.current;

  // message = "coba2 dulu"

  // console.log(
  //   appiumLabel,
  //   "refInput",
  //   refInput,
  //   "\n isFocused",
  //   isFocused,
  //   "\n prevFocus",
  //   prevFocus,
  //   "\n errorMessage",
  //   errorMessage,
  //   "\n message",
  //   message
  // );
  return (
    <View
      style={{
        marginHorizontal:
          moderateScale(containerMarginHorizontal) || moderateScale(16),
        marginBottom:
          moderateScale(containerMarginBottom) || moderateScale(12, 0.25),
        backgroundColor: containerBgcolor,
      }}
    >
      {label ? (
        <Text
          style={{
            marginBottom:
              moderateScale(labelMarginBottom, 0.25) || moderateScale(0, 0.25),
            fontSize: moderateScale(labelFontSize) || moderateScale(12, 0.25),
            fontFamily: labelFontFamily || "Montserrat-Regular",
            color: disableEdit ? "#454849" : labelColor || "#838586",
          }}
        >
          {label}
        </Text>
      ) : (
        <View></View>
      )}

      <View
        style={{
          backgroundColor: disableEdit ? "#212325FC" : boxColor || "#FFFFFF",
          height: moderateScale(40, 0.25),
          width: widthBox || "100%",
          justifyContent: "center",
          flexDirection: "row",
          borderRadius: 2,
          borderColor:
            error && !isFocused ? "#FF5252" : isFocused ? "#1035BB" : "#7C858B",
          borderWidth: 1,
          marginBottom: moderateScale(4),
        }}
      >
        {showIcon ? (
          <View
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center",
              marginLeft: moderateScale(12, 0.25),
            }}
          >
            {iconValue === "phone" ? (
              <Icon size={24} color="#A2A8AC" name="phone" />
            ) : iconValue === "name" ? (
              <Icon size={24} color="#A2A8AC" name="user" />
            ) : (
              <Image
                resizeMode="contain"
                source={
                  iconValue === "password" ? image.pass_ic : image.pass_ic
                }
                style={{ width: 24, height: 24 }}
              />
            )}
          </View>
        ) : (
          <View></View>
        )}
        {disableEdit ? (
          <Text
            style={{
              marginHorizontal: moderateScale(16),
              flex:
                isPassword && showIcon ? 7 : !isPassword && showIcon ? 8 : 1,
              fontSize: valueInput || moderateScale(14),
              fontFamily: labelFontFamily || "Montserrat-Regular",
              color: textColor || "#000000",
              alignSelf: "center",
            }}
            placeholder={placeHolder || "Enter your ...."}
            placeholderTextColor={"#838586"}
            // secureTextEntry={!showPassword || false}
            // onChangeText={changeValue}
            // ref={refInput}
            // onSubmitEditing={() => refInput.current.focus()}
            // selectionColor={"#1F93FF"}
            accessibilityLabel={appiumLabel}
            testID={appiumLabel}
            {...props}
          >
            {props.value}
          </Text>
        ) : iconValue === "phone" ? (
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              flex: 7,
              // backgroundColor: "red",
            }}
          >
            {/* <Text
              style={{
                fontSize: moderateScale(14, 0.25),
                color: "#838586",
                fontFamily: "Montserrat-Regular",
              }}
            >
              +62
            </Text> */}
            <CountryFlag
              ref={phoneInput}
              defaultCode="ID"
              layout="first"
              containerStyle={{
                backgroundColor: "rgba(52, 52, 52, 0)",
                overflow: "hidden",
                marginRight: moderateScale(32),
              }}
              
              codeTextStyle={{
                height: moderateScale(35),
                justifyContent: "center",
                alignItems: "center",
                alignSelf: "center",
                marginLeft: moderateScale(-10),
                paddingVertical: moderateScale(7.5),
                fontFamily: "Montserrat-Regular",
                fontWeight: "normal",
              }}
              countryPickerButtonStyle={{ height: moderateScale(35) }}
              textInputStyle={{
                height: moderateScale(35),
                fontFamily: "Montserrat-Regular",
                color: "#000000",
              }}
              flagButtonStyle={{
                height: moderateScale(35),
                width: moderateScale(60),
                paddingLeft: moderateScale(8),
              }}
              textContainerStyle={{
                flexGrow: 1,
                height: moderateScale(35),
                backgroundColor: "rgba(52, 52, 52, 0)",
              }}
              onChangeText={changeValue}
              onChangeCountry={changeCountry}
            />
            <TextInput
              style={{
                marginHorizontal: moderateScale(6),
                flex:
                  isPassword && showIcon ? 7 : !isPassword && showIcon ? 7 : 1,
                fontSize: valueInput || moderateScale(14),
                fontFamily: labelFontFamily || "Montserrat-Regular",
                color: textColor || "#000000",
                backgroundColor: "red"
              }}
              placeholder={placeHolder || "Enter your ...."}
              placeholderTextColor={"#838586"}
              // secureTextEntry={!showPassword || false}
              onChangeText={changeValue}
              ref={refInput}
              onSubmitEditing={() => refInput.current.focus()}
              selectionColor={"#1035BB"}
              accessibilityLabel={appiumLabel}
              testID={appiumLabel}
              onFocus={onFocus}
              onBlur={onBlur}
              {...props}
              // selectionColor={'#1F93FF'}
            />
          </View>
        ) : (
          <TextInput
            style={{
              marginHorizontal: moderateScale(6),
              flex:
                isPassword && showIcon ? 7 : !isPassword && showIcon ? 8 : 1,
              fontSize: valueInput || moderateScale(14),
              fontFamily: labelFontFamily || "Montserrat-Regular",
              color: textColor || "#000000",
            }}
            placeholder={placeHolder || "Enter your ...."}
            placeholderTextColor={"#838586"}
            // secureTextEntry={!showPassword || false}
            onChangeText={changeValue}
            ref={refInput}
            onSubmitEditing={() => refInput.current.focus()}
            selectionColor={"#1035BB"}
            accessibilityLabel={appiumLabel}
            testID={appiumLabel}
            onFocus={onFocus}
            onBlur={onBlur}
            {...props}
            // selectionColor={'#1F93FF'}
          />
        )}

        {isPassword && (
          <TouchableOpacity
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center",
            }}
            onPress={changeShowPassword}
            accessibilityLabel={appiumLabelPassword}
            testID={appiumLabelPassword}
          >
            {showPassword ? (
              <Icon size={16} color="#1035BB" name="eye" />
            ) : (
              <Icon size={16} color="#1035BB" name="eye-slash" />
            )}
          </TouchableOpacity>
        )}
      </View>
      {appiumLabel === "profileEditName" && disableEdit ? (
        <View style={{ flexDirection: "row", alignItems: "center" }}>
          <Text
            style={{
              color: `${textInfoColor}`,
              fontSize: moderateScale(10),
              fontFamily: "Montserrat-Regular",
              // marginLeft: moderateScale(4),
            }}
          >
            {textInfo}
          </Text>
        </View>
      ) : message !== "" && errorMessage === "" ? (
        <View style={{ flexDirection: "row", alignItems: "center" }}>
          <Text
            style={{
              color: "#838586",
              fontSize: moderateScale(10),
              fontFamily: "Montserrat-Regular",
              marginLeft: moderateScale(4),
            }}
          >
            {message}
          </Text>
        </View>
      ) : (
        error && (
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <Text
              style={{
                color: "#FF5252",
                fontSize: moderateScale(10),
                fontFamily: "Montserrat-Regular",
                // marginLeft: moderateScale(4),
              }}
            >
              {errorMessage}
            </Text>
          </View>
        )
      )}
    </View>
  );
};

export default InputField;
