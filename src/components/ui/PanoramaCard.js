import React from "react";
import {
  Box,
  Text,
  Button,
  FlatList,
  ScrollView,
  HStack,
  ZStack,
  VStack,
} from "native-base";
import { TouchableOpacity, Image } from "react-native";
import { moderateScale } from "react-native-size-matters";
import LinearGradient from "react-native-linear-gradient";
import { image } from "../../../assets/images";
import { UserApi } from "../../utils/Api";
import { useSelector } from "react-redux";

const user = UserApi.client;

export default function TopUpCard({ data, navigation, cardStyle }) {
  console.log("credit", data);
  const {
    class_activity_type_id,
    panorama_activity_type_id,
    pt_activity_type_id,
    rent_court_activity_type_id,
  } = useSelector(state => state.PRODUCT.activityTypeId.activityTypeId);

  const navigatePanorama = async () => {
    // let detailData = await user.get(`/activity/35`)
    // console.log("gagabah", detailData.data.data)

    navigation.navigate("FacilityList", {
      screen: "Home",
      title: "Panorama",
      params: {
        type: "category",
        activityId: panorama_activity_type_id,
      },
    });
    // navigation.navigate("FacilityList", {
    //   params:{ activityId: 7 },
    // })
  };

  const navigateSchedule = async () => {
    navigation.navigate("DetailSchedule");
  };

  const handlePress = item => {
    if (item.id == 1) {
      navigateSchedule();
    } else if (item.id == 2) {
      console.log("do nothing");
    } else if (item.id == 3) {
      console.log("here nothing");
      navigatePanorama();
    }
  };

  const ComponentCard2 = data => {
    console.log("check panoram data", data);
    return (
      <TouchableOpacity
        onPress={() => handlePress(data)}
        disabled={data.id === 2 ? true : false}>
        <Box
          style={{
            height: moderateScale(70, 0.25),
            marginHorizontal: moderateScale(16, 0.25),
            marginBottom: moderateScale(16, 0.25),
            //   backgroundColor: "red",
          }}>
          <ZStack
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
            <Image
              source={data.image}
              // size="xl"
              style={{
                width: moderateScale(328, 0.25),
                height: moderateScale(70, 0.25),
              }}
              resizeMode={"cover"}
            />
            {data.id === 2 && (
              <Box
                style={{
                  backgroundColor: "rgba(0, 0, 0, .5)",
                  height: moderateScale(70, 0.25),
                  alignItems: "center",
                  justifyContent: "center",
                  width: moderateScale(328, 0.25),
                }}>
                <Text
                  style={{
                    color: "#FFF",
                    fontSize: moderateScale(20, 0.25),
                    fontWeight: "500",
                    fontFamily: "Rubik-Regular",
                  }}>
                  Coming Soon
                </Text>
              </Box>
            )}
          </ZStack>
        </Box>
      </TouchableOpacity>
    );
  };

  return (
    <Box>
      <Box style={{ marginHorizontal: moderateScale(16, 0.25) }}>
        <Box
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            marginBottom: moderateScale(16, 0.25),
            alignItems: "center",
          }}>
          <Text
            style={{
              color: "black",
              fontSize: moderateScale(14, 0.25),
              fontWeight: "700",
              fontFamily: "Rubik-Regular",
            }}>
            Info
          </Text>
        </Box>
      </Box>
      {data.map(item => ComponentCard2(item))}
    </Box>
  );
}
