import React, { useCallback, useEffect, useState } from "react";
import { isNull } from "lodash";
import _get from "lodash/get";
import { Text, View, Image, TouchableOpacity, Alert } from "react-native";
import propTypes from "prop-types";
import { Button } from "native-base";
import { moderateScale } from "react-native-size-matters";
import { image } from "images";

const ParticipantCard = ({
  participants = [], // only for comparing the user that has been registered on the family
  data,
  onPress,
  onRemoveMember,
  onCancelInvite,
  selfUserId = 0,
}) => {
  const dataTemp = data.user;
  const [imageUrl, setImage] = useState({
    uri: _get(dataTemp, "photo", data.photo),
  });
  // const [imageUrl, setImage] = useState(null)

  const userId = _get(dataTemp, "id", data.id);
  const name = _get(dataTemp, "name", data.name);
  const phone_number = _get(dataTemp, "phone_number", data.phone_number);
  const username = _get(dataTemp, "username", data.username);
  const role = isNull(phone_number) == true ? "children" : "adult";
  const status = _get(dataTemp, "status", data.status); // TODO: from BE will be arranged
  let userOnFamily = participants.filter(
    fam => _get(fam.user, "id", fam.id) === userId,
  );
  let isUserOnFamily = userOnFamily.length > 0;
  const isUserPending =
    userOnFamily.length > 0
      ? userOnFamily[0].invite_status == "pending"
        ? true
        : false
      : false;

  console.log("user id: ", data.id);
  // console.log("participants: ", participants);
  // console.log("isUserOnFamily: ", isUserOnFamily);
  console.log("data: ", data);
  console.log("selfUserId: ", selfUserId);

  const onButtonPress = useCallback(() => {
    if (isUserOnFamily == true) {
      if (isUserPending == true) onCancelInvite(userId, name);
      else onRemoveMember(userId);
    } else {
      onPress(userId);
    }
    console.log("button pressed: ", isUserOnFamily);
  }, [onPress, onRemoveMember]);

  // useEffect(() => {
  //   setIsUserOnFamily(participants.filter((fam) => fam.id == userId).length > 0)
  // }, [onPress])

  return (
    <View
      style={{
        flex: 1,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "flex-start",
        // backgroundColor: 'green',
        paddingHorizontal: moderateScale(16),
        paddingVertical: moderateScale(10),
        borderColor: "#DFE3EC",
        borderTopWidth: 0.5,
      }}>
      <Image
        source={imageUrl}
        style={{
          width: moderateScale(35),
          height: moderateScale(35),
          marginRight: moderateScale(10),
        }}
        onError={err => setImage(image.ic_profile_placeholder)}
      />
      <View style={{ flex: 1 }}>
        <Text
          style={{
            fontSize: moderateScale(14, 0.25),
            fontFamily: "Montserrat-Regular",
            fontWeight: "700",
            marginBottom: moderateScale(4),
          }}>
          {name}
        </Text>
        {phone_number !== "" && <Text>{phone_number}</Text>}
      </View>
      {/* TODO: determine the button color and the text */}
      {selfUserId != userId && (
        <View
          style={{
            backgroundColor:
              isUserOnFamily == false
                ? "#1035BB"
                : isUserPending
                ? "rgba(224, 224, 224, 1)"
                : "#1035BB",
            padding: moderateScale(4),
          }}>
          <TouchableOpacity onPress={onButtonPress}>
            <Text
              style={{
                color:
                  isUserOnFamily == false
                    ? "#fff"
                    : isUserPending
                    ? "#000"
                    : "#fff",
                fontSize: moderateScale(12, 0.25),
                fontFamily: "Montserrat-Regular",
                fontWeight: "700",
              }}>
              {/* {isUserOnFamily == false ? `+  Add` : (isUserPending ? 'Pending' : `-  Remove`)} */}
              {isUserOnFamily == false
                ? "+  Invite"
                : isUserPending
                ? "-  Pending"
                : "Invited"}
            </Text>
          </TouchableOpacity>
        </View>
      )}
    </View>
  );
};

ParticipantCard.defaultProps = {
  data: {},
};

ParticipantCard.propTypes = {
  data: propTypes.shape({}),
};

export default ParticipantCard;
