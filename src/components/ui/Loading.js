import React from "react";
import { ActivityIndicator } from "react-native";
import { Box } from "native-base";

export default function Loading() {
  return (
    <Box style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
      <ActivityIndicator color={"#1035BB"} size={"large"} />
    </Box>
  );
}
