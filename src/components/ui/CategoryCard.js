import React, { useState } from "react";
import {
  Box,
  Text,
  Image,
  AspectRatio,
  HStack,
  VStack,
  Center,
  ZStack,
  Spacer,
  // Button,
} from 'native-base';
import {TouchableOpacity} from 'react-native';
import {moderateScale} from 'react-native-size-matters';
import {image} from '../../../assets/images';
import { useSelector } from "react-redux";


export default function CategoryCard({ navigation, data, numCol }) {
  console.log("data category card", data);
  const [dataBackgroundImage, setDataDummyCategory] = useState([
    {
      background_image: image.wellnessBG,
    },
    {
      background_image: image.sportBG,
    },
    {
      background_image: image.fitnessBG,
    },
    {
      background_image: image.bookingBG,
    },
    {
      background_image: image.nutritionBG,
    },
    {
      background_image: image.educationBG,
    },
  ]);

  const { 
    class_activity_type_id, 
    panorama_activity_type_id, 
    pt_activity_type_id, 
    rent_court_activity_type_id
  } = useSelector((state) => state.PRODUCT.activityTypeId.activityTypeId);
  

  return (
    <TouchableOpacity
      disabled={data?.index >= 4 ? true : false}
      onPress={data.item.id == 4
          ? () => {
            navigation.navigate("FacilityList", {
              screen: "Home",
              title: "Facilities",
              params: {
                type: "category",
                activityId: rent_court_activity_type_id
              },
            })}
          : data?.item?.id === 6 ? () => {navigation.navigate("Education", {
            categoryID: data.item.id,
            title: data.item.name,
            // for testing purpose
            // categoryID : 8,
          })} : () => {
            navigation.navigate("Category", {
              categoryID: data.item.id,
              title: data.item.name,
              // for testing purpose
              // categoryID : 8,
            });}
      }>
      <Box
        // flex={1 / numCol}
        // alignItems="center"
        style={{
          marginLeft: (data.index + 1) % 2 == 0 ? 12 : 18,
          marginRight: (data.index + 1) % 2 == 0 ? 16 : 12,
          height: moderateScale(42, 0.25),
          width: moderateScale(158, 0.25),
          marginBottom: moderateScale(19),
          backgroundColor: '#ffffff',
          borderColor: '#EEEEEE',
          borderWidth: moderateScale(1, 0.25),
        }}
        shadow={2}>
        <ZStack>
          <Image
            source={data.item.background_image}
            // size="xl"
            alt="Alternate Text"
            style={{
              width: moderateScale(44, 0.25),
              height: moderateScale(42, 0.25),
              justifyContent: 'flex-end',
              alignSelf: 'flex-end',
            }}
          />
          <HStack
            align="center"
            justify="center"
            style={{
              // backgroundColor: "blue",
              justifyContent: "center",
              alignItems: "center",
              height: moderateScale(42, 0.25),
            }}
          >
            <Image
              source={data.item.image}
              resizeMode="contain"
              alt="image"
              // size="xl"
              style={{
                width: moderateScale(30, 0.25),
                height: moderateScale(25, 0.25),
                marginLeft: moderateScale(16, 0.25),
                marginRight: moderateScale(10, 0.25),
              }}
            />
            <Text
              style={{
                color: '#616161',
                fontSize: moderateScale(10, 0.25),
                fontWeight: '500',
                fontFamily : "Rubik-Regular"
              }}>
              {data.item.name.toUpperCase()}
            </Text>
          </HStack>
          {data.index >= 4 && 
          <Box style={{backgroundColor:"rgba(0, 0, 0, .5)", width:"100%", height:moderateScale(42, 0.25), alignItems:"center", justifyContent:"center"}}>
            <Text
              style={{
                color: '#FFF',
                fontSize: moderateScale(20, 0.25),
                fontWeight: '500',
                fontFamily : "Rubik-Regular"
              }}>
              Coming Soon
            </Text>
          </Box>
          }
        </ZStack>
      </Box>
    </TouchableOpacity>
  );
}
