
import React from 'react'
import { Text, ImageBackground, TouchableOpacity } from 'react-native'
import {
  Box,
  HStack,
  VStack,
} from "native-base";
import { image } from "../../../assets/images";
import { moderateScale } from "react-native-size-matters";

export default function CardOrder({ data, index, onPress }) {
  return(
  <TouchableOpacity onPress={onPress}>
    <VStack
      style={{ 
        borderColor: "#EBEEF4", 
        borderBottomWidth:0,
        borderWidth: 1, 
        padding: 16,
        width: "100%",
        height: moderateScale(85, .25), 
      }}
    >
      <HStack>
        <Box style={{flex:1}}>
          <Text
            style={{
              fontWeight: "bold",
              fontSize: 12,
              letterSpacing: 0.2,
              fontFamily: "Montserrat-Bold",
            }}
          >
            Yoga for Beginner
          </Text>
          <Text
            style={{
              fontSize: 10,
              color: "#757575",
              letterSpacing: 0.2,
              marginBottom:11,
              fontFamily: "Montserrat-Regular",
            }}
          >
            BCA Virtual Account
          </Text>
          <Text
            style={{
              fontSize: 8,
              color: "#009174",
              letterSpacing: 0.2,
              fontFamily: "Montserrat-Bold",
            }}
            numberOfLines={1}
          >
            2 Feb 2021
          </Text>
        </Box>
        <Box style={{flex:1, alignItems:"flex-end"}}>
          <Text
            style={{
              fontSize: 12,
              color: "#000",
              fontFamily: "Montserrat-Bold",
            }}
          >
            RP 499.000
          </Text>
          <Text
            style={{
              fontSize: 8,
              color: "#009174",
              fontFamily: "Montserrat-Bold",
            }}
          >
            Completed
          </Text>
        </Box>
      </HStack>
    </VStack>
  </TouchableOpacity>
  )
}