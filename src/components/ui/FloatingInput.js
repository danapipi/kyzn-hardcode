import React, { useState } from "react";
import { View, Text, TextInput, StyleSheet } from "react-native";
import { moderateScale } from "react-native-size-matters";

const FloatingInput = ({
  label,
  bgColorLabel = "#F8F9FC",
  isPassword = false,
  ...props
}) => {
  const [isFocused, setIsFocused] = useState(true);

  const handleFocus = () => {
    setIsFocused(true);
  };
  const handleBlur = () => {
    //for hiding the text if it is empty
    if (props.hideLabel) {
      setIsFocused(true);
    } else {
      setIsFocused(false);
    }
  };
  const labelStyle = {
    position: "absolute",
    left: 0,
    top: !isFocused ? moderateScale(35) : moderateScale(10),
    fontSize: !isFocused ? moderateScale(14) : moderateScale(10, 0.25),
    color: !isFocused ? "#aaa" : "#757575",
    marginLeft: moderateScale(18),
    marginBottom: !isFocused ? moderateScale(20) : moderateScale(0),
    zIndex: !isFocused ? moderateScale(0) : moderateScale(9),
    paddingLeft: !isFocused ? moderateScale(0) : moderateScale(5),
    paddingRight: !isFocused ? moderateScale(0) : moderateScale(5),
    backgroundColor: !isFocused ? "transparent" : bgColorLabel,
  };

  const inputContainer = {
    marginTop: 20,
    //   marginLeft: 10,
    marginRight: 10,
    borderLeftWidth: 2,
    borderRightWidth: 2,
    borderBottomWidth: 2,
    borderRadius: 4,
    height: 50,
    width: "100%",
    borderColor: !isFocused ? "#aaa" : "#9E9E9E",
    borderTopWidth: !isFocused ? 2 : 2,
    zIndex: 1,
    overflow: "visible",
  };
  const parent = {
    marginHorizontal: moderateScale(16),
    marginBottom: moderateScale(12, 0.25),
  };
  return (
    <View style={parent}>
      <Text style={labelStyle}>{label}</Text>
      <View style={inputContainer}>
          <TextInput
            {...props}
            style={{
              height: 46,
              fontSize: moderateScale(12, 0.25),
              color: "#000",
              marginLeft: 20,
              marginBottom: 10,
            }}
            secureTextEntry={isPassword}
            onFocus={handleFocus}
            //   onBlur={handleBlur}
            underlineColorAndroid={"transparent"}
          />
      </View>
    </View>
  );
};

export default FloatingInput;

const styles = StyleSheet.create({
  bottom_style: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-start",
    marginTop: 400,
  },
  hrstyle: {
    textAlign: "left",
  },
});
