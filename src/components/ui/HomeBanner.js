import React, { useState } from "react";
import { View, Text, TouchableOpacity, Image , FlatList} from "react-native";
import { moderateScale } from "react-native-size-matters";
import Commons from "../../utils/common";
import { image } from "images";

const HomeBanner = ({ label, data, onPressViewAll }) => {
  const [dataDummy, setDataDummy] = useState([
    {
      id: 1,
      imageUrl: image.banner_dummy
    },
    {
      id: 2,
      imageUrl: image.banner_dummy
    },
  ]);
  return (
    <View
      style={{
        flex: 1,
        marginTop: moderateScale(32, 0.25),
      }}
    >
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          marginBottom: moderateScale(16, 0.25),
          alignItems: "center",
          paddingLeft: moderateScale(16, .25),
          paddingRight: moderateScale(16, 0.25),
        }}
      >
        <Text
          style={{
            color: "black",
            fontSize: moderateScale(14, 0.25),
            fontWeight: "700",
            fontFamily: "Rubik-Regular",
          }}
        >
          {label}
        </Text>
        <TouchableOpacity onPress={onPressViewAll}>
          <Text
            style={{
              color: "black",
              fontSize: moderateScale(10, 0.25),
              fontWeight: "400",
              fontFamily: "Rubik-Regular",
            }}
          >
            View All
          </Text>
        </TouchableOpacity>
      </View>
      <View style={{ flexDirection: "row" }}>
        <FlatList
          data={dataDummy}
          horizontal
          showsHorizontalScrollIndicator={false}
          renderItem={({item, index}) => {
            console.log("iabsidaosidh", item);
            let data = item
            return (
              <View
                key={data.id}
                style={{
                  width: moderateScale(315, 0.25),
                  height: moderateScale(100, 0.25),
                  marginRight: moderateScale(12, 0.25),
                  paddingLeft: index === 0 ? moderateScale(16, 0.25) : 0
                }}
              >
                <TouchableOpacity>
                  <Image
                    source={data.imageUrl}
                    style={{
                      width: "100%",
                      height: "100%",
                    }}
                    resizeMode={"cover"}
                  />
                </TouchableOpacity>
              </View>
            );
          }}
        />
      </View>
    </View>
  );
};

export default HomeBanner;
