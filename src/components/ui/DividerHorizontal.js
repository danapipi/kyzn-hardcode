import React from "react";
import { StyleSheet, View } from "react-native";
import PropTypes from "prop-types";

const styles = StyleSheet.create({
  main: {
    borderWidth: StyleSheet.hairlineWidth,
    width: "100%",
    borderColor: "#C7CBCD",
    marginVertical: 50,
  },
});

const DividerHorizontal = ({ style }) => <View style={[styles.main, style]} />;

DividerHorizontal.propTypes = {
  style: PropTypes.object,
};

DividerHorizontal.defaultProps = {
  style: {},
};

export default DividerHorizontal;
