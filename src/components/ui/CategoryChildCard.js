import React, {useState} from 'react';
import {
  Box,
  Text,
  AspectRatio,
  HStack,
  VStack,
  Center,
  ZStack,
  Spacer,
  // Button,
} from 'native-base';
import {TouchableOpacity, Image} from 'react-native';
import {moderateScale} from 'react-native-size-matters';
import {image} from '../../../assets/images';

export default function CategoryChildCard({ navigation, data, openSheet }) {
  console.log(
    "data category child card",
    data,
    "image" + "." + data.item.image
  );

  const [uriImage, setUriImage] = useState({uri: data.item.image});

  return (
    <TouchableOpacity onPress={openSheet}>
      <Box
        style={{
          marginHorizontal: moderateScale(22, 0.25),
          //   height: moderateScale(42, 0.25),
          //   width: moderateScale(158, 0.25),
          marginBottom: moderateScale(19),
        }}
      >
        <HStack
          style={{
            // backgroundColor: "blue",
            // justifyContent: "center",
            alignItems: "flex-start",
          }}
        >
          <Image
            source={uriImage}
            onError={(e) => setUriImage(image.placeholder)}
            // resizeMode="cover"
            alt="image"
            // size="xl"
            style={{
              width: moderateScale(27, 0.25),
              height: moderateScale(27, 0.25),
              // marginLeft: moderateScale(24, 0.25),
              marginRight: moderateScale(14, 0.25),
            }}
          />
          <VStack style={{ marginRight: moderateScale(24, 0.25) }}>
            <Text
              style={{
                color: "black",
                fontWeight: "700",
                fontSize: moderateScale(17, 0.25),
                fontFamily: "Montserrat-Regular",
              }}
            >
              {data.item.name}
            </Text>
            {data.item.category_id === 10 && (
              <Text
                style={{
                  color: "black",
                  fontWeight: "500",
                  fontSize: moderateScale(17, 0.25),
                  fontFamily: "Montserrat-Regular",
                }}
              >
                {data.item.description}
              </Text>
            )}
          </VStack>
        </HStack>
      </Box>
    </TouchableOpacity>
  );
}
