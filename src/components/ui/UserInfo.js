import React, { useState } from "react";
import {
  Box,
  Text,
  Button,
  FlatList,
  ScrollView,
  HStack,
  Image,
} from "native-base";
import PropTypes from "prop-types";
import { image } from "../../../assets/images";
import { moderateScale } from "react-native-size-matters";

export default function UserInfo({ name, exp, coin }) {
  return (
    <Box
      style={{
        marginTop: moderateScale(20, 0.25),
        marginBottom: moderateScale(30, 0.25),
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <Text
        style={{
          fontSize: moderateScale(14, 0.25),
          fontWeight: "700",
          color: "#0E0E0E",
          lineHeight: moderateScale(18, 0.25),
          marginBottom: moderateScale(9, 0.25),
          fontFamily : "Rubik-Regular"

        }}
      >
        {name}
      </Text>
      <HStack
        style={{
          // backgroundColor: "blue",
          justifyContent: "center",
          alignItems: "center",
          // height: moderateScale(42, 0.25)
        }}
      >
        <Image
          source={image.exp}
          resizeMode="contain"
          alt="imageExp"
          // size="xl"
          style={{
            width: moderateScale(19, 0.25),
            height: moderateScale(19, 0.25),
            marginRight: moderateScale(6, 0.25),
          }}
        />
        <Text
          style={{
            color: "#212121",
            fontSize: moderateScale(12, 0.25),
            fontWeight: "500",
            lineHeight: moderateScale(14, 0.25),
            fontFamily : "Rubik-Regular"

          }}
        >
          {exp} XP
        </Text>
        <Image
          source={image.coin}
          resizeMode="contain"
          alt="imageCoin"
          // size="xl"
          style={{
            width: moderateScale(19, 0.25),
            height: moderateScale(19, 0.25),
            marginRight: moderateScale(6, 0.25),
            marginLeft: moderateScale(11, 0.25),
          }}
        />
        <Text
          style={{
            color: "#212121",
            fontSize: moderateScale(12, 0.25),
            fontWeight: "500",
            lineHeight: moderateScale(14, 0.25),
            fontFamily : "Rubik-Regular"

          }}
        >
          {coin} Coin
        </Text>
      </HStack>
    </Box>
  );
}
