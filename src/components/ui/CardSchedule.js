
import React from 'react'
import { Text, ImageBackground, TouchableOpacity } from 'react-native'
import {
  Box,
  HStack,
  VStack,
} from "native-base";
import { image } from "../../../assets/images";
import { moderateScale } from "react-native-size-matters";
import moment from 'moment';
import _ from 'lodash';
import { useDispatch, useSelector } from "react-redux";


export default function CardSchedule({ data, dataDiff, onPress, fromSchedule }) {
  console.log("activi data card", data);
  const dataActivity = data.item
  const startTime = dataActivity.start_time
  const endTime = dataActivity.end_time
  const isToday = moment(startTime).format("dddd, DD MMMM YYYY") == moment().format("dddd, DD MMMM YYYY")
  const isTommorow = moment(startTime).format("dddd, DD MMMM YYYY") == moment().add(1, "day").format("dddd, DD MMMM YYYY")
  const title = _.get(dataActivity, 'activity_court.activity.name', '')
  const arryTemp = _.filter(dataDiff, function(o) { 
    return o.id == data.item.id
  });
  const status = arryTemp.length !== 0 ? "Booked" : " ";

  const { 
    class_activity_type_id, 
    panorama_activity_type_id, 
    pt_activity_type_id, 
    rent_court_activity_type_id,
    sport_category_id,
    fitness_category_id,
    wellness_category_id
  } = useSelector((state) => state.PRODUCT.activityTypeId.activityTypeId);
  

  return(
  <TouchableOpacity onPress={onPress}>
    <VStack
      style={{ 
        borderColor: "#DFE3EC", 
        borderWidth: 1, 
        padding: 16,
        marginBottom: 16,
        width: moderateScale(328, .25),
        height: moderateScale(85, .25), 
      }}
    >
      <ImageBackground
        source={image.bgCard}
        resizeMode="contain"
        alt="imageProfile"
        style={{
          width: moderateScale(328, .25),
          height: moderateScale(85, .25),
          justifyContent: "flex-end",
          position: "absolute",
          right: moderateScale(-68, .25),
          bottom:0
        }}
      />
      <HStack>
        <Box style={{flex:1}}>
          <Text
            style={{
              fontSize: 10,
              color: "#000",
              letterSpacing: 0.2,
              marginBottom: 8,
              fontFamily: "Montserrat-Regular",
            }}
          >
            {`${moment(startTime).format("HH:mm")} - ${moment(endTime).format("HH:mm")}`}
          </Text>
          <Text
            style={{
              fontWeight: "bold",
              fontSize: 12,
              letterSpacing: 0.2,
              marginBottom: 8,
              fontFamily: "Montserrat-Bold",
            }}
          >
            {title}
          </Text>
          <Text
            style={{
              fontSize: 8,
              color: "#009174",
              letterSpacing: 0.2,
              fontFamily: "Montserrat-Bold",
            }}
            numberOfLines={1}
          >
            {_.upperCase(status)}
          </Text>
        </Box>
        <Box style={{flex:1, alignItems:"flex-end"}}>
          <Box style={{backgroundColor:"#000", width: 60, height:14, borderRadius:2, alignItems:"center", justifyContent:"center"}}>
            <Text
              style={{
                fontSize: 8,
                color: "#FFF",
                fontFamily: "Montserrat-Bold",
              }}
            >
              {dataActivity.activity_court.activity.activity_type_id == rent_court_activity_type_id && "FACILITIES"}
              {dataActivity.activity_court.activity.activity_type_id == class_activity_type_id && "CLASS"}
              {dataActivity.activity_court.activity.activity_type_id == pt_activity_type_id && "TRAINING"}
              {dataActivity.activity_court.activity.activity_type_id == panorama_activity_type_id && "PANORAMA"}
              {dataActivity.activity_court.activity.activity_type_id == sport_category_id && "SPORT"}
              {dataActivity.activity_court.activity.activity_type_id == fitness_category_id && "FITNESS"}
              {dataActivity.activity_court.activity.activity_type_id == wellness_category_id && "WELLNESS"}
            </Text>
          </Box>
        </Box>
      </HStack>
    </VStack>
  </TouchableOpacity>
  )
}