import React, {
  useEffect,
  useRef,
  useState,
  useReducer,
  useCallback,
} from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Alert,
  Image,
  Dimensions,
  FlatList,
  StyleSheet,
  Platform,
} from "react-native";
import { getScreenWidth } from "utils/size";
import { moderateScale } from "react-native-size-matters";
import { useDispatch, useSelector } from "react-redux";
import Selectors from "selectors";
import Actions from "actions";
import { UserApi } from "../../utils/Api";
import {
  Actionsheet,
  Box,
  Center,
  KeyboardAvoidingView,
  Button,
  useTheme,
} from "native-base";
import { image } from "images";
import SearchBar from "./SearchBar";
import { widthPercentageToDP } from "react-native-responsive-screen";
import Carousel, { Pagination } from "react-native-snap-carousel";
import { useIsFocused, useRoute } from "@react-navigation/native";
import _forEach from "lodash/forEach";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

const user = UserApi.client;

const AddKidsCard = ({ navigation, route }) => {
  const { colors } = useTheme();
  const [isOpen, setIsOpen] = useState(false);
  const [openQuitFamsAS, setOpenQuitFamsAS] = useState(false);
  const [addFamilyLoading, setAddFamilyLoading] = useState(false);
  const [quitFamilyLoading, setQuitFamilyLoading] = useState(false);
  const [idSelectedForQuitting, setIdSelectedForQuitting] = useState(null);
  const [keyboardHeight, setKeyboardHeight] = useState(0);

  const fams = useSelector(Selectors.getAllFamilies);
  console.log("fams >>>>> ", fams);
  const [famsName, setFamsName] = useState("");
  const [families, setFamilies] = useState(
    useSelector(Selectors.getAllFamilies),
  );
  // const [lastItemForFamilyData, setLastItemForFamilyData] = useState(useSelector(Selectors.getAllFamilies))

  const [_refresh, forceUpdate] = useReducer(x => x + 1, 0);
  const isFocused = useIsFocused();

  const [currentIndex, setCurrentIndex] = useState(0);

  const familyRef = useRef(null);

  const dispatch = useDispatch();
  const kids = useSelector(Selectors.getAllFamilyDetails);
  const dummyObject = {
    id: String(Math.random()),
    name: "random",
    user_family_details: [
      {
        user: {
          id: String(Math.random()),
          name: "random2",
          sports_count: parseInt(Math.random()),
          phone_number: "",
        },
      },
    ],
  };

  const filterFamily = () => {
    let newFams = fams;
    console.log("[AddKidsCard] fams: ", fams);

    _forEach(newFams, (item, index) => {
      if (item.name === "random") {
        newFams.splice(index, 1);
      }
    });

    newFams.push(dummyObject);
    console.log("newFams: ", newFams);

    setTimeout(() => {
      setFamilies(newFams);
    }, 500);
  };

  const profile = useSelector(Selectors.getProfile);
  useEffect(() => {
    // dispatch(Actions.fetchAllFamilies());
  }, []);

  const onClose = () => {
    setIsOpen(false);
    setFamsName("");
  };

  const closeQuitAS = () => {
    setOpenQuitFamsAS(false);
    setIdSelectedForQuitting(null);
  };

  const openCreateFamSheet = () => {
    setIsOpen(true);
  };

  const handleTextChange = text => {
    setFamsName(text);
  };

  const createFamily = () => {
    setAddFamilyLoading(true);
    console.log("PARAMS FAMILY CREATE: ", famsName);
    user
      .post("family/create", {
        name: famsName,
      })
      .then(res => {
        console.log("RES FAMILY/CREATE: ", res);
        if (res.data.success === true) {
          // dispatch(Actions.fetchAllFamilies());
          filterFamily();
          setFamsName("");

          setTimeout(() => {
            setAddFamilyLoading(false);
            onClose();
            route.onRefresh({
              forceRefresh: true,
            });
            setTimeout(() => {
              route.onRefresh({
                forceRefresh: false,
              });
            }, 500);
          }, 4000);
        } else {
          Alert.alert("Failed to add family");
        }
      })
      .catch(err => {
        Alert.alert("Error", err.response.data.message, [
          {
            text: "OK",
            onPress: () => console.log("OK Pressed"),
            style: "cancel",
          },
        ]);
        setAddFamilyLoading(false);
      });
  };

  const quitFamily = () => {
    setQuitFamilyLoading(true);

    user
      .delete(`/family/${idSelectedForQuitting}/quit`)
      .then(res => {
        // dispatch(Actions.fetchAllFamilies());
        filterFamily();

        setTimeout(() => {
          setQuitFamilyLoading(false);
          closeQuitAS();
        }, 3000);
      })
      .catch(err => {
        setQuitFamilyLoading(false);
      });
  };

  useEffect(() => {
    // filterFamily();
    // families.push(dummyObject)

    console.log("[AddKidsCard] kids: ", kids);
    console.log("[AddKidsCard] families: ", families);
    console.log("[AddKidsCard] profile: ", profile);
    console.log("[AddKidsCard] route.params: ", route);
  }, []);

  useEffect(() => {
    // console.log("[AddKidsCard] isFocused: ", isFocused);
    console.log("[AddKidsCard] fams di bawah: ", fams);
    // filterFamily();
  }, [isFocused, addFamilyLoading, quitFamilyLoading]);

  return (
    <View
      style={{
        width: getScreenWidth(),
        marginBottom: moderateScale(29, 0.25),
        // backgroundColor: "red",
      }}>
      {kids.length > 0 && (
        <Carousel
          layout="default"
          data={families}
          // data={lastItemForFamilyData}
          extraData={families}
          ref={familyRef}
          horizontal
          // showsHorizontalScrollIndicator={false}
          renderItem={({ item, index }) => {
            console.log("the familyId: ", item);
            console.log("index: ", families.length);
            // if (index == families.length - 1) {
            //   return (
            //     <View
            //       style={{
            //         marginHorizontal: moderateScale(16, 0.25),
            //         height: moderateScale(92, 0.25),
            //         backgroundColor: "#F8F9FC",
            //         alignItems: "center",
            //         borderColor: "#EBEEF4",
            //         borderWidth: 1,
            //         width: widthPercentageToDP("92%"),
            //       }}>
            //       <Text
            //         style={{
            //           fontSize: moderateScale(12, 0.25),
            //           color: "#212121",
            //           fontFamily: "Montserrat-Regular",
            //           fontWeight: "600",
            //           marginBottom: moderateScale(12, 0.25),
            //           marginHorizontal: moderateScale(16, 0.25),
            //           marginTop: moderateScale(16, 0.25),
            //         }}>
            //         {"Add your family members to enjoy KYZN together!"}
            //       </Text>
            //       <TouchableOpacity
            //         onPress={() => {
            //           openCreateFamSheet();
            //         }}
            //         style={{
            //           backgroundColor: "#212121",
            //           width: moderateScale(115, 0.25),
            //           height: moderateScale(25, 0.25),
            //           justifyContent: "center",
            //           alignItems: "center",
            //         }}>
            //         <Text
            //           style={{
            //             color: "white",
            //             fontFamily: "Montserrat-Regular",
            //             fontWeight: "700",
            //             fontSize: moderateScale(12, 0.25),
            //           }}>
            //           + Create Family
            //         </Text>
            //       </TouchableOpacity>
            //     </View>
            //   );
            // } else 
            if (item.name !== "random") {
              return (
                <View
                  style={{
                    marginHorizontal: moderateScale(16, 0.25),
                    minHeight: moderateScale(92, 0.25),
                    backgroundColor: "#F8F9FC",
                    alignItems: "center",
                    borderColor: "#EBEEF4",
                    borderWidth: 1,
                    width: widthPercentageToDP("92%"),
                  }}>
                  <View
                    style={{
                      flexDirection: "row",
                      alignItems: "center",
                      marginTop: moderateScale(16, 0.25),
                      marginBottom: moderateScale(12, 0.25),
                      width: "100%",
                      justifyContent: "space-between",
                    }}>
                    <Text
                      style={{
                        fontSize: moderateScale(12, 0.25),
                        color: "#9E9E9E",
                        fontFamily: "Montserrat-Regular",
                        fontWeight: "700",
                        marginLeft: moderateScale(17, 0.25),
                      }}>
                      {`${item.name} (${
                        item.user_family_details.filter(
                          kid => kid.user.id !== profile.id,
                        ).length
                      })`}
                    </Text>
                    {item.user_id === profile.id ? ( // means that the family belongs to this user
                      <TouchableOpacity
                        onPress={() => {
                          navigation.navigate("AddFamily", {
                            title: "Edit Family",
                            // familyId: item.id,
                            familyId:
                              item.user_family_details[0].user_family_id,
                            familyBelongsTo: item.user_id,
                          });
                        }}>
                        <Text
                          style={{
                            color: "#212121",
                            fontSize: moderateScale(10, 0.25),
                            fontFamily: "Montserrat-Regular",
                            fontWeight: "700",
                            marginRight: moderateScale(17, 0.25),
                          }}>
                          + Edit Family
                        </Text>
                      </TouchableOpacity>
                    ) : (
                      <TouchableOpacity
                        onPress={() => {
                          setIdSelectedForQuitting(item.id);
                          setOpenQuitFamsAS(true);
                        }}>
                        <Text
                          style={{
                            color: "red",
                            fontSize: moderateScale(10, 0.25),
                            fontFamily: "Montserrat-Regular",
                            fontWeight: "700",
                            marginRight: moderateScale(17, 0.25),
                          }}>
                          x Quit Family
                        </Text>
                      </TouchableOpacity>
                    )}
                  </View>
                  {/* {kids */}
                  {item.user_family_details
                    .filter(kid => kid.user.id != profile.id)
                    .map(({ id, user, sports_count, role }) => {
                      console.log("ROLEEEEX: ", item);
                      return (
                        <View key={id} style={{ width: "100%" }}>
                          <TouchableOpacity
                            onPress={() => {
                              navigation.navigate("ProfileDetail", {
                                title: "Profile Detail",
                                user: user,
                              });
                            }}>
                            <View
                              style={{
                                flexDirection: "row",
                                borderTopWidth: 1,
                                borderTopColor: "#EBEEF4",
                                justifyContent: "center",
                                alignItems: "center",
                                marginHorizontal: moderateScale(17, 0.25),
                                paddingVertical: moderateScale(12, 0.25),
                              }}>
                              <View style={{ flex: 0.4 }}>
                                <View
                                  style={{
                                    height: 30,
                                    width: 30,
                                    borderRadius: 1000,
                                    backgroundColor:
                                      id % 2 == 0 ? "#3387D7" : "#6E75FF",
                                    justifyContent: "center",
                                    alignItems: "center",
                                  }}>
                                  <Text
                                    style={{
                                      color: "white",
                                      fontSize: moderateScale(10, 0.25),
                                      fontFamily: "Montserrat-Regular",
                                      fontWeight: "600",
                                    }}>
                                    {user.name || "-"}
                                  </Text>
                                </View>
                              </View>
                              <View style={{ flex: 2 }}>
                                <Text
                                  style={{
                                    color: "black",
                                    fontSize: moderateScale(12, 0.25),
                                    fontFamily: "Montserrat-Regular",
                                    fontWeight: "600",
                                    // marginVertical: moderateScale(15, 0.25),
                                  }}>
                                  {user.name}
                                </Text>
                              </View>
                              <View
                                style={{
                                  justifyContent: "center",
                                  alignItems: "flex-end",
                                  flex: 1,
                                }}>
                                <Text
                                  style={{
                                    color: "black",
                                    fontSize: moderateScale(10, 0.25),
                                    fontFamily: "Montserrat-Regular",
                                    fontWeight: "600",
                                    // marginVertical: moderateScale(15, 0.25),
                                  }}>
                                  {/* {sports_count || 0} Sports */}
                                  {/* {user.phone_number ? "Child" : "Adult"} */}
                                  {role}
                                </Text>
                              </View>
                            </View>
                          </TouchableOpacity>
                        </View>
                      );
                    })}
                </View>
              );
            } else return null;
          }}
          // sliderWidth={widthPercentageToDP(100)}
          sliderWidth={getScreenWidth()}
          onBeforeSnapToItem={index => setCurrentIndex(index)}
          itemWidth={getScreenWidth()}
          enableSnap
          snapToAlignment={"center"}
          // loop
        />
      ) 
      /* : (
        <View
          style={{
            marginHorizontal: moderateScale(16, 0.25),
            height: moderateScale(92, 0.25),
            backgroundColor: "#F8F9FC",
            alignItems: "center",
            borderColor: "#EBEEF4",
            borderWidth: 1,
            width: widthPercentageToDP("92%"),
          }}>
          <Text
            style={{
              fontSize: moderateScale(12, 0.25),
              color: "#212121",
              fontFamily: "Montserrat-Regular",
              fontWeight: "600",
              marginBottom: moderateScale(12, 0.25),
              marginHorizontal: moderateScale(16, 0.25),
              marginTop: moderateScale(16, 0.25),
            }}>
            {"Create a family to enjoy KYZN together!"}
          </Text>
          <TouchableOpacity
            onPress={() => {
              openCreateFamSheet();
            }}
            style={{
              backgroundColor: "#212121",
              width: moderateScale(115, 0.25),
              height: moderateScale(25, 0.25),
              justifyContent: "center",
              alignItems: "center",
            }}>
            <Text
              style={{
                color: "white",
                fontFamily: "Montserrat-Regular",
                fontWeight: "700",
                fontSize: moderateScale(12, 0.25),
              }}>
              + Create Family
            </Text>
          </TouchableOpacity>
        </View>
      ) */
    }

      <>
        {kids.length > 0 ? (
          <Pagination
            containerStyle={styles.pagination}
            dotsLength={families.length}
            activeDotIndex={currentIndex}
            dotStyle={styles.paginationDot}
            dotColor={colors.black}
            inactiveDotColor={colors.white}
            inactiveDotStyle={styles.paginationDot}
            inactiveDotOpacity={1}
            inactiveDotScale={1}
          />
        ) : null}
      </>

      <KeyboardAwareScrollView
        enableOnAndroid={true}
        onKeyboardDidShow={e => {
          const theKeyboardHeight = e.endCoordinates.height;
          setKeyboardHeight(theKeyboardHeight);
        }}
        onKeyboardDidHide={e => {
          setKeyboardHeight(0);
        }}>
        <Actionsheet isOpen={isOpen} onClose={onClose}>
          <Actionsheet.Content h={245 + keyboardHeight}>
            <Image
              source={image.bg_actionsSheet}
              style={{
                width: 300,
                height: 300,
                position: "absolute",
                right: 0,
              }}
              alt="image"
            />

            <KeyboardAvoidingView
              style={{
                width: 270,
                marginTop: 8,
                alignItems: "center",
                justifyContent: "center",
              }}>
              <Text
                style={{
                  fontSize: moderateScale(16, 0.25),
                  color: "#000",
                  fontWeight: "bold",
                  fontFamily: "Montserrat-Regular",
                  marginBottom: moderateScale(12, 0.25),
                }}
                //   alignItems="flex-start"
              >
                {"Name Your Family"}
              </Text>
              <SearchBar
                placeholder={"Please enter a name for your family"}
                contentContainerStyle={{
                  width: Dimensions.get("screen").width * 0.9,
                }}
                onChangeText={text => handleTextChange(text)}
              />
              <Button
                onPress={() => {
                  if (famsName.length > 0) {
                    createFamily();
                  }
                }}
                isLoading={addFamilyLoading}
                style={{
                  backgroundColor: "#1035BB",
                  marginVertical: moderateScale(16),
                  width: Dimensions.get("screen").width * 0.9,
                }}>
                <Text
                  style={{
                    fontSize: moderateScale(18, 0.25),
                    fontFamily: "Montserrat-Regular",
                    fontWeight: "700",
                    color: "#fff",
                  }}>
                  SAVE
                </Text>
              </Button>
              <TouchableOpacity onPress={() => onClose()}>
                <Text
                  style={{
                    fontSize: moderateScale(18, 0.25),
                    fontFamily: "Montserrat-Regular",
                    fontWeight: "700",
                    color: "#000",
                  }}>
                  {"Close"}
                </Text>
              </TouchableOpacity>
            </KeyboardAvoidingView>
          </Actionsheet.Content>
        </Actionsheet>
      </KeyboardAwareScrollView>

      <Actionsheet isOpen={openQuitFamsAS} onClose={closeQuitAS}>
        <Actionsheet.Content h={245}>
          <Image
            source={image.bg_actionsSheet}
            style={{ width: 300, height: 300, position: "absolute", right: 0 }}
            alt="image"
          />

          <KeyboardAvoidingView
            style={{
              width: 270,
              marginTop: 8,
              alignItems: "center",
              justifyContent: "center",
            }}>
            <Text
              style={{
                fontSize: moderateScale(16, 0.25),
                color: "#000",
                fontWeight: "bold",
                fontFamily: "Montserrat-Regular",
                marginBottom: moderateScale(12, 0.25),
              }}>
              {"Are you sure you want to QUIT from this family?"}
            </Text>
            <Button
              onPress={() => {
                quitFamily();
              }}
              isLoading={quitFamilyLoading}
              style={{
                backgroundColor: "#1035BB",
                marginVertical: moderateScale(16),
                width: Dimensions.get("screen").width * 0.9,
              }}>
              <Text
                style={{
                  fontSize: moderateScale(18, 0.25),
                  fontFamily: "Montserrat-Regular",
                  fontWeight: "700",
                  color: "#fff",
                }}>
                {"YES"}
              </Text>
            </Button>
            <TouchableOpacity onPress={() => closeQuitAS()}>
              <Text
                style={{
                  fontSize: moderateScale(18, 0.25),
                  fontFamily: "Montserrat-Regular",
                  fontWeight: "700",
                  color: "#000",
                }}>
                {"NO"}
              </Text>
            </TouchableOpacity>
          </KeyboardAvoidingView>
        </Actionsheet.Content>
      </Actionsheet>
    </View>
  );
};

const styles = StyleSheet?.create({
  paginationContainer: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    paddingLeft: Platform?.OS === "android" ? 10 : 1,
    paddingRight: Platform?.OS === "android" ? 10 : 1,
  },
  pagination: {
    flex: 1,
  },
  paginationDot: {
    width: 10,
    height: 10,
    borderRadius: 5,
    borderWidth: 1,
  },
  paginationButtons: {
    marginHorizontal: 15,
  },
});

export default AddKidsCard;
