import React, { useState } from "react";
import { View, Text, TouchableOpacity, Image } from "react-native";
import { getScreenWidth } from "utils/size";
import theme from "utils/theme";
import { moderateScale } from "react-native-size-matters";
import { ChevronRightIcon } from "native-base";
import { image } from "images";

const AccountsMenu = ({ navigation }) => {
  const [dataMenu, setDataMenu] = useState([
    {
      id: 1,
      label: "My Profile",
      image: image.edit_ic,
      actions: "ProfileDetail",
      title: "Profile",
    },
    {
      id: 2,
      label: "Loyalty Programs",
      image: image.loyalty_ic,
      actions: "",
      title: "Loyalty Programs",
    },
    {
      id: 3,
      label: "Change Password",
      image: image.change_pass_ic,
      actions: "ChangePassword",
      title: "Change Password",
    },
    {
      id: 4,
      label: "Show my QR",
      image: image.qr_ic,
      actions: "QRViewer",
      title: "Show QR",
    },
    {
      id: 5,
      label: "My Coupons",
      image: image.ic_coupon,
      actions: "Coupons",
      title: "My Coupons",
    },
    {
      id: 6,
      label: "Referral Code",
      image: image.ic_referral,
      actions: "ReferralCode",
      title: "Referral Code",
    },
    // {
    //   id: 7,
    //   label: "Set Preference",
    //   image: image.ic_set_preference,
    //   actions: "SetPreferences",
    //   title: "Set Preference",
    // },
  ]);

  const goToScreen = (name, title) => {
    navigation.navigate(name, { title });
  };
  return (
    <View
      style={{
        width: getScreenWidth(),
      }}>
      <Text
        style={{
          fontSize: moderateScale(14, 0.25),
          color: "black",
          fontFamily: "Montserrat-Regular",
          fontWeight: "700",
          marginBottom: moderateScale(16, 0.25),
          marginHorizontal: moderateScale(16, 0.25),
        }}>
        My Account
      </Text>
      {dataMenu.map(item => (
        <TouchableOpacity
          style={{
            marginHorizontal: moderateScale(16, 0.25),
            borderTopWidth: 1,
            borderTopColor: "#EBEEF4",
            flexDirection: "row",
            alignItems: "center",
          }}
          key={item.id}
          onPress={
            item.actions !== ""
              ? () => goToScreen(item.actions, item.title)
              : console.log("item actions", item.actions)
          }>
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              paddingVertical: moderateScale(15, 0.25),
              marginHorizontal: moderateScale(18, 0.25),
              flex: 2,
            }}>
            <Image
              source={item.image}
              resizeMode="contain"
              alt="image"
              // size="xl"
              style={{
                width: moderateScale(16, 0.25),
                height: moderateScale(16, 0.25),
                // marginLeft: moderateScale(16, 0.25),
                marginRight: moderateScale(8, 0.25),
              }}
            />
            <Text
              style={{
                fontSize: moderateScale(12, 0.25),
                color: "black",
                fontFamily: "Montserrat-Regular",
                fontWeight: "600",
              }}>
              {item.label}
            </Text>
          </View>
          <View>
            <ChevronRightIcon size={6} />
          </View>
        </TouchableOpacity>
      ))}
    </View>
  );
};

export default AccountsMenu;
