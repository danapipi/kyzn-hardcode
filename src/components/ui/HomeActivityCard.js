import React, { useState } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ImageBackground,
} from "react-native";
import { moderateScale } from "react-native-size-matters";
import Commons from "../../utils/common";
import { image as images } from "images";

const HomeActivityCard = ({
  label,
  data,
  navigation,
  onPressViewAll,
  image,
}) => {
  console.log("HomeActivityCard data", data, image);

  const [uriImage, setUriImage] = useState("");
  const [dataDummy, setDataDummy] = useState([
    {
      id: 1,
      xp: 800,
      zeni: 500,
      title: "Yoga For Beginner",
      price: 300000,
      rating: 4.7,
    },
    { id: 2, xp: 800, zeni: 500, title: "Boxing", price: 300000, rating: 4.7 },
    {
      id: 3,
      xp: 800,
      zeni: 500,
      title: "Yoga Advanced",
      price: 300000,
      rating: 4.7,
    },
  ]);

  let imageTemp = uriImage == "" ? { uri: image } : uriImage;
  return (
    <View
      style={{
        flex: 1,
        marginTop: moderateScale(32, 0.25),
        marginRight: moderateScale(16, 0.25),
      }}>
      {data !== undefined && (
        <View>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              marginBottom: moderateScale(16, 0.25),
              alignItems: "center",
            }}>
            <Text
              style={{
                color: "black",
                fontSize: moderateScale(14, 0.25),
                fontWeight: "700",
                fontFamily: "Rubik-Regular",
              }}>
              {label}
            </Text>
            <TouchableOpacity onPress={onPressViewAll}>
              <Text
                style={{
                  color: "black",
                  fontSize: moderateScale(10, 0.25),
                  fontWeight: "400",
                  fontFamily: "Rubik-Regular",
                }}>
                View All
              </Text>
            </TouchableOpacity>
          </View>
          <View style={{ flexDirection: "row" }}>
            {data.activities.map((item, index) => {
              console.log("item doang", item, index + (1 % 2));
              if (index < 3) {
                return (
                  <View
                    key={item.id}
                    style={{
                      height: moderateScale(173, 0.25),
                      width: moderateScale(108, 0.25),
                      borderWidth: 1,
                      borderColor: "#EEEEEE",
                      marginHorizontal:
                        (index + 1) % 2 === 0
                          ? moderateScale(10, 0.25)
                          : moderateScale(0, 0.25),
                      // flex: 1,
                    }}>
                    <TouchableOpacity
                      onPress={() =>
                        navigation.navigate("SportDetail", {
                          data: { item: item },
                          type: item.activity_type_id,
                        })
                      }>
                      <Image
                        // source={{ uri: "https://via.placeholder.com/103x104" }}
                        onError={e =>
                          setUriImage(
                            require("../../../assets/images/img_product_placeholder.png"),
                          )
                        }
                        source={
                          item?.image_thumbnail
                            ? { uri: item?.image_thumbnail }
                            : imageTemp
                        }
                        // source={item.image_thumbnail}
                        style={{
                          width: moderateScale(108, 0.25),
                          height: moderateScale(104, 0.25),
                        }} />
                      <View
                        style={{
                          flexDirection: "row",
                          marginHorizontal: moderateScale(8, 0.25),
                          // justifyContent: "center",
                          alignItems: "center",
                          marginTop: moderateScale(6, 0.25),
                        }}>
                        <Text
                          style={{
                            color: "#424242",
                            fontSize: moderateScale(6, 0.25),
                            fontWeight: "500",
                            fontFamily: "Rubik-Regular",
                          }}>
                          Earn
                        </Text>
                        <View
                          style={{
                            flexDirection: "row",
                            backgroundColor: "#6778D0",
                            borderRadius: 10,
                            marginHorizontal: moderateScale(4, 0.25),
                            width: moderateScale(28, 0.25),
                            height: moderateScale(10, 0.25),
                            justifyContent: "center",
                            alignItems: "center",
                          }}>
                          <Text
                            style={{
                              color: "white",
                              fontSize: moderateScale(5, 0.25),
                              fontWeight: "500",
                              fontFamily: "Rubik-Regular",
                              marginRight: moderateScale(1, 0.25),
                            }}>
                            XP
                          </Text>
                          <Text
                            style={{
                              color: "white",
                              fontSize: moderateScale(6, 0.25),
                              fontWeight: "500",
                              fontFamily: "Rubik-Regular",
                            }}>
                            {item.xp_reward}
                          </Text>
                        </View>
                        <View
                          style={{
                            flexDirection: "row",
                            backgroundColor: "#FFA800",
                            borderRadius: 10,
                            width: moderateScale(32, 0.25),
                            height: moderateScale(10, 0.25),
                            justifyContent: "center",
                            alignItems: "center",
                          }}>
                          <Text
                            style={{
                              color: "white",
                              fontSize: moderateScale(5, 0.25),
                              fontWeight: "500",
                              fontFamily: "Rubik-Regular",
                              marginRight: moderateScale(1, 0.25),
                            }}>
                            ZENI
                          </Text>
                          <Text
                            style={{
                              color: "white",
                              fontSize: moderateScale(6, 0.25),
                              fontWeight: "500",
                              fontFamily: "Rubik-Regular",
                            }}>
                            {item.coin_reward}
                          </Text>
                        </View>
                      </View>
                      <View
                        style={{
                          marginTop: moderateScale(11, 0.25),
                          marginHorizontal: moderateScale(8, 0.25),
                          flexDirection: "column",
                        }}>
                        <Text
                          style={{
                            color: "black",
                            fontSize: moderateScale(10, 0.25),
                            fontWeight: "500",
                            fontFamily: "Rubik-Regular",
                            marginBottom: moderateScale(12, 0.25),
                          }}
                          numberOfLines={1}>
                          {item.name}
                        </Text>
                        <View
                          style={{
                            flexDirection: "row",
                            justifyContent: "space-between",
                          }}>
                          {/* <Text
                            style={{
                              color: "black",
                              fontSize: moderateScale(8, 0.25),
                              fontWeight: "700",
                              fontFamily: "Rubik-Regular",
                            }}
                          >
                            {Commons.convertToRupiah(item.step_duration_price)}
                          </Text> */}

                          <View style={{ flexDirection: "row" }}>
                            <Image
                              source={images.star}
                              style={{
                                width: moderateScale(8, 0.25),
                                height: moderateScale(8, 0.25),
                                marginRight: moderateScale(2, 0.25),
                              }} />
                            <Text
                              style={{
                                color: "black",
                                fontSize: moderateScale(8, 0.25),
                                fontWeight: "500",
                                fontFamily: "Rubik-Regular",
                              }}>
                              {item.rating}
                              {/* {item.rating} */}
                            </Text>
                          </View>
                        </View>
                      </View>
                    </TouchableOpacity>
                  </View>
                );
              }
            })}
          </View>
        </View>
      )}
    </View>
  );
};

export default HomeActivityCard;
