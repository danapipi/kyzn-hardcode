import React, { useState } from "react";
import {
  View,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  Image,
} from "react-native";
import {
  Box,
  Text,
  AspectRatio,
  HStack,
  VStack,
  ScrollView,
  Button,
  Actionsheet,
  ChevronRightIcon,
  ChevronLeftIcon,
  ChevronDownIcon,
} from "native-base";
import { useRoute } from "@react-navigation/native";
import PropTypes from "prop-types";
import { useDispatch, useSelector } from "react-redux";
import Actions from "actions";
import Loading from "components/ui/Loading";
import moment from "moment";
import _ from "lodash";
import CalendarPicker from "react-native-calendar-picker";

import { moderateScale } from "react-native-size-matters";

const DatePicker = ({
  label,
  bgColorLabel = "#F8F9FC",
  onSelected,
  value,
  valueInput,
  disabled,
  maxDateProps = true,
  minDateProps = false,
  ...props
}) => {
  const [isFocused, setIsFocused] = useState(true);

  const [onSelectedDate, setSelectedDate] = useState(
    value
      ? moment(value).format("DD MMMM yyyy")
      // : moment().format("DD MMMM yyyy")
      : null
  );

  const [isOpen, setIsOpen] = useState(false);

  const changeDate = (date) => {
    console.log("cek changeDate", moment(date).format("DD MMMM yyyy"));
    let dateSelect = moment(date).format("DD MMMM yyyy");
    setSelectedDate(dateSelect);
    onSelected(date);
  };

  const onSubmitDate = () => {
    onClose();
    // displayDay()
  };

  const onClose = () => {
    setIsOpen(false);
  };

  const openDatePickerSheet = () => {
    if(!disabled){
      setSelectedDate(moment().format("DD MMMM yyyy"))
      setIsOpen(true);
    }
  };

  const onDone = () => {
    setIsOpen(false);
  };

  const onViewDetail = () => {
    setIsOpen(false);
  };

  const changeGender = (label, value) => {
    setGenderSelected(label);
    // onSelected(value);
  };
  const labelStyle = {
    position: "absolute",
    left: 0,
    top: !isFocused ? moderateScale(35) : moderateScale(10),
    fontSize: !isFocused ? moderateScale(14) : moderateScale(10, 0.25),
    color: !isFocused ? "#aaa" : "#757575",
    marginLeft: moderateScale(18),
    marginBottom: !isFocused ? moderateScale(20) : moderateScale(0),
    zIndex: !isFocused ? moderateScale(0) : moderateScale(9),
    paddingLeft: !isFocused ? moderateScale(0) : moderateScale(5),
    paddingRight: !isFocused ? moderateScale(0) : moderateScale(5),
    backgroundColor: !isFocused ? "transparent" : bgColorLabel,
  };

  const inputContainer = {
    marginTop: 20,
    //   marginLeft: 10,
    marginRight: 10,
    borderLeftWidth: 2,
    borderRightWidth: 2,
    borderBottomWidth: 2,
    borderRadius: 4,
    height: 50,
    width: "100%",
    borderColor: !isFocused ? "#aaa" : "#9E9E9E",
    borderTopWidth: !isFocused ? 2 : 2,
    zIndex: 1,
    overflow: "visible",
  };
  const parent = {
    marginHorizontal: moderateScale(16),
    marginBottom: moderateScale(12, 0.25),
  };
  return (
    <View style={parent}>
      <Text style={labelStyle}>{label}</Text>
      <View style={inputContainer}>
        <TouchableOpacity
          style={{
            //   backgroundColor: boxColor || "#45535F",
            height: moderateScale(42, 0.25),
            // justifyContent: "center",
            flexDirection: "row",
            borderRadius: 2,
            //   borderColor: error ? "#FF5252" : "#45535F",
            //   borderWidth: error ? 1 : 0,
            marginBottom: moderateScale(4),
            flex: 1,
          }}
          onPress={() => openDatePickerSheet()}
        >
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              flex: 1,
              justifyContent: "space-between",
            }}
          >
            <Text
              style={{
                fontSize: valueInput || moderateScale(14),
                fontFamily: "Montserrat-Regular",
                //   color: locationSelected === "Indonesia" ? "#FFFFFF" : "#838586"
                color: "black",
                marginLeft: moderateScale(16),
              }}
            >
              {onSelectedDate}
            </Text>
            <ChevronDownIcon
              size={6}
              style={{ marginRight: moderateScale(11, 0.25) }}
            />
          </View>
        </TouchableOpacity>
      </View>
      <Actionsheet isOpen={isOpen} onClose={onClose} hideDragIndicator={true}>
        <Actionsheet.Content h={450}>
          <CalendarPicker
            initialDate={onSelectedDate}
            weekdays={["S", "M", "T", "W", "T", "F", "S"]}
            dayLabelsWrapper={{ borderColor: "transparent" }}
            previousComponent={<ChevronLeftIcon />}
            nextComponent={<ChevronRightIcon />}
            dayShape={{ backgroundColor: "red" }}
            selectedDayStyle={{
              backgroundColor: "transparent",
              borderColor: "black",
              borderWidth: 1,
              borderRadius: 14,
              width: 40,
            }}
            onDateChange={changeDate}
            maxDate={maxDateProps ? moment(): ""}
            minDate={minDateProps ? moment(): ""}
          />
          <TouchableOpacity onPress={onSubmitDate}>
            <Box
              style={{
                backgroundColor: "#212121",
                padding: 10,
                width: 320,
                alignItems: "center",
              }}
            >
              <Text style={{ color: "#FFF" }}>SAVE</Text>
            </Box>
          </TouchableOpacity>
        </Actionsheet.Content>
      </Actionsheet>
    </View>
  );
};

export default DatePicker;

const styles = StyleSheet.create({
  gridContainer: {
    flex: 1,
    flexDirection: "column",
    // flexWrap: "wrap",
    // padding: 10,
    // marginBottom: moderateScale(20),
    // backgroundColor: "#1D1F21",
    backgroundColor: "#343E47",
  },
  gridButtonContainer: {
    // marginTop: moderateScale(18),
    justifyContent: "center",
    // alignItems: "center"
  },
  gridIcon: {
    fontSize: 30,
    color: "white",
  },
  gridLabel: {
    fontSize: moderateScale(14),
    fontFamily: "Montserrat-Regular",
    // marginLeft: moderateScale(8),
    // paddingTop: 10,
  },
});
