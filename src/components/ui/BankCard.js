import React, { useState } from "react";
import { TouchableOpacity } from "react-native";
import { Box, Text, Image, AspectRatio, HStack, VStack } from "native-base";
import PropTypes from "prop-types";
import Commons from "../../utils/common";
import _ from "lodash";
import { image } from "images";


const BankCard = ({ data, numCol, onPress }) => {
  const dataActivity = data.item;
  const [uriImage, setUriImage] = useState("");

  let price = _.get(dataActivity, "price", null);


  let imageTemp = uriImage == "" ? { uri: dataActivity.image_landscape } : {uri: uriImage}
  return (
    <Box
      flex={1 / numCol}
      alignItems="center"
      style={{
        marginLeft: (data.index + 1) % 2 == 0 ? 6 : 16,
        marginRight: (data.index + 1) % 2 == 0 ? 16 : 6,
      }}
    >
      <TouchableOpacity style={{ flex: 1, width: "100%" }} onPress={onPress}>
        <Box
          w={"100%"}
          borderWidth={1}
          borderColor={"#EEEEEE"}
          borderStyle={"solid"}
          marginBottom={13}
          backgroundColor="#FFF"
          style={{
            paddingBottom: 16,
          }}
        >
          {/* <AspectRatio ratio={16 / 9}> */}
          <Box style={{height: 158}}>
            <Image
              onError={(e) => setUriImage("https://via.placeholder.com/103x104")}
              source={{uri: dataActivity.bank.image_url}}
              h={158}
              resizeMode="contain"
              // alt="image"
            />
          </Box>
          {/* </AspectRatio> */}
          <VStack style={{ paddingHorizontal: 10, paddingTop:12 }}>
            <Text
              alignItems="flex-start"
              fontWeight="bold"
              numberOfLines={1}
              style={{ fontFamily: "Montserrat-Regular", fontWeight:"700" }}
            >
              {dataActivity.bank.name}
            </Text>
            <HStack justifyContent="space-between" style={{ marginTop: 8 }}>
              <Text
                alignItems="flex-start"
                fontWeight="bold"
                fontSize={10}
                style={{ fontFamily: "Montserrat-Regular", fontWeight:"700" }}
              >
                {dataActivity.bank.code}
              </Text>
              
            </HStack>
          </VStack>
        </Box>
      </TouchableOpacity>
    </Box>
  );
};

BankCard.propTypes = {
  data: PropTypes.object.isRequired,
};

export default BankCard;
