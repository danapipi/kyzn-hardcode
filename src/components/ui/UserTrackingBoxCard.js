import React from "react";
import {
  Box,
  Image,
  AspectRatio,
  HStack,
  VStack,
  Center,
  ZStack,
  Spacer,
  // Button,
} from "native-base";
import { TouchableOpacity, Text } from "react-native";
import { moderateScale } from "react-native-size-matters";
import * as Progress from "react-native-progress";

export default function UserTrackingBoxCard({
  navigation,
  label,
  dataLabel,
  color,
  dataCircular,
  dataDesc,
}) {
  return (
    <TouchableOpacity>
      <Box
        shadow={2}
        style={{
          //   marginLeft: moderateScale(19, 0.25),
          //   marginRight: moderateScale(8, 0.25),
          height: moderateScale(42, 0.25),
          width: moderateScale(105, 0.25),
          marginBottom: moderateScale(19),
          backgroundColor: "#ffffff",
          borderColor: "#EEEEEE",
          borderWidth: moderateScale(1, 0.25),
          alignItems:"center"
          //   paddingHorizontal: moderateScale(7, 0.25)
        }}
      >
        <HStack style={{alignItems:"center", alignSelf:"center", flex:1}}>
          <Box
            style={{
              marginHorizontal: moderateScale(5),
              justifyContent: "center",
              alignItems: "center",
              // backgroundColor: "red"
            }}
          >
            <Progress.Circle
              progress={dataCircular / 100}
              size={25}
              showsText={true}
              color={color}
              unfilledColor={"#F5F5F5"}
              thickness={2}
              textStyle={{fontSize: 6, color: "#000", marginLeft:3}}
              borderColor={"transparent"}
              // indeterminate={true}
            />
          </Box>

          <VStack style={{flex:1}}>
            <Box style={{justifyContent:"space-between"}}>
              <HStack style={{ alignItems:'center'}}>
                <Text
                  style={{
                    color: color,
                    fontSize: moderateScale(10, 0.25),
                    fontWeight: "700",
                    marginRight: moderateScale(2, 0.25),
                    fontFamily: "Montserrat-Regular",
                  }}
                >
                  {label}
                </Text>
                <Text
                  style={{
                    fontSize: moderateScale(8, 0.25),
                    fontWeight: "400",
                    color: "#6778D0",
                    marginRight: moderateScale(7, 0.25),
                    fontFamily: "Montserrat-Regular",
                  }}
                >
                  {dataLabel}%
                </Text>
              </HStack>
            </Box>
            <Box
              style={{
                marginTop: moderateScale(2, 0.25),
                alignItems: "flex-start",
              }}
            >
              <Text
                style={{
                  fontSize: moderateScale(8, 0.25),
                  fontWeight: "800",
                  fontFamily: "Montserrat-Regular",
                }}
              >
                {dataDesc}
              </Text>
            </Box>
          </VStack>
        </HStack>
      </Box>
    </TouchableOpacity>
  );
}
