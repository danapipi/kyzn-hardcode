import React from "react";
import { Text, ImageBackground, TouchableOpacity } from "react-native";
import { Box, HStack, VStack } from "native-base";
import { image } from "../../../assets/images";
import { moderateScale } from "react-native-size-matters";
import moment from "moment";
import { useSelector } from "react-redux";
import _ from "lodash";

export default function CardActivity({ data, onPress, userId = null }) {
  console.log("activi data card", data);
  const dataActivity = data.item;
  const participantsFirstUser = _.get(
    dataActivity,
    "participants[0].user.id",
    0,
  );
  const startTime = dataActivity.activity_schedule.start_time;
  const { invite_status } = dataActivity;
  const profile = useSelector(state => state.AUTH.profile.data);
  const paying_user_id = _.get(data, "paying_user_id", profile.id);
  const isToday =
    moment(startTime).format("dddd, DD MMMM YYYY") ==
    moment().format("dddd, DD MMMM YYYY");
  const isTommorow =
    moment(startTime).format("dddd, DD MMMM YYYY") ==
    moment().add(1, "day").format("dddd, DD MMMM YYYY");
  const title = _.get(dataActivity, "activity.name", "");

  const {
    class_activity_type_id,
    panorama_activity_type_id,
    pt_activity_type_id,
    rent_court_activity_type_id,
    sport_category_id,
    fitness_category_id,
    wellness_category_id,
  } = useSelector(state => state.PRODUCT.activityTypeId.activityTypeId);

  const getStatus = obj => {
    if (_.upperCase(obj.attendance_status) !== "CANCELLED") {
      if (_.upperCase(obj.booking_status) == "SUCCESS") {
        if (participantsFirstUser != userId) {
          return "INVITED";
        } else {
          return "BOOKED";
        }
      } else if (_.upperCase(obj.booking_status) == "WAITING LIST") {
        return "WAITING LIST";
      } else if (_.upperCase(obj.booking_status) == "CANCELLED") {
        return "CANCELLED";
      }
    } else if (_.upperCase(obj.attendance_status) == "CANCELLED") {
      return "CANCELLED";
    }
    //WAITING LIST
  };
  return (
    <TouchableOpacity onPress={onPress}>
      <VStack
        style={{
          borderColor: "#DFE3EC",
          borderWidth: 1,
          padding: 16,
          marginBottom: 16,
          width: moderateScale(328, 0.25),
          // height: moderateScale(85, 0.25),
        }}>
        <ImageBackground
          source={image.bgCard}
          resizeMode="contain"
          alt="imageProfile"
          style={{
            width: moderateScale(328, 0.25),
            height: moderateScale(85, 0.25),
            justifyContent: "flex-end",
            position: "absolute",
            right: moderateScale(-68, 0.25),
            bottom: 0,
          }}
        />
        <HStack>
          <Box style={{ flex: 1 }}>
            <Text
              style={{
                fontSize: 10,
                color: "#757575",
                letterSpacing: 0.2,
                marginBottom: 8,
                fontFamily: "Montserrat-Regular",
              }}>
              {isToday
                ? `Today ・ ${moment(startTime).format("HH:mm")}`
                : isTommorow
                ? `Tomorrow ・ ${moment(startTime).format("HH:mm")}`
                : moment(startTime).format("DD MMM YYYY ・ HH:mm")}
            </Text>
            <Text
              style={{
                fontWeight: "bold",
                fontSize: 12,
                color: "#000",
                letterSpacing: 0.2,
                marginBottom: 8,
                fontFamily: "Montserrat-Bold",
              }}>
              {title}
            </Text>
            <Text
              style={{
                fontSize: 8,
                // color: dataActivity.status == "Over" ? "#E2BB4B" : "#009174",
                color:
                  getStatus(dataActivity) == "BOOKED" ||
                  getStatus(dataActivity) == "INVITED"
                    ? "#009174"
                    : getStatus(dataActivity) == "CANCELLED"
                    ? "red"
                    : "#E2BB4B",
                letterSpacing: 0.2,
                fontFamily: "Montserrat-Bold",
              }}
              numberOfLines={1}>
              {getStatus(dataActivity)}
            </Text>
          </Box>
          <Box
            style={{
              flex: 1,
              alignItems: "flex-start",
              flexDirection: "row-reverse",
              justifyContent: "space-between",
            }}>
            <Box
              style={{
                backgroundColor: "#000",
                width: 60,
                height: 14,
                borderRadius: 2,
                alignItems: "center",
                justifyContent: "center",
              }}>
              <Text
                style={{
                  fontSize: 8,
                  color: "#FFF",
                  fontFamily: "Montserrat-Bold",
                }}>
                {dataActivity.activity.activity_type_id ==
                  rent_court_activity_type_id && "FACILITIES"}
                {dataActivity.activity.activity_type_id ==
                  class_activity_type_id && "CLASS"}
                {dataActivity.activity.activity_type_id ==
                  pt_activity_type_id && "TRAINING"}
                {dataActivity.activity.activity_type_id ==
                  panorama_activity_type_id && "PANORAMA"}
                {dataActivity.activity.activity_type_id == sport_category_id &&
                  "SPORT"}
                {dataActivity.activity.activity_type_id ==
                  fitness_category_id && "FITNESS"}
                {dataActivity.activity.activity_type_id ==
                  wellness_category_id && "WELLNESS"}
              </Text>
            </Box>
            {/* {
            paying_user_id != userId && (
              <Box style={{backgroundColor:"#000", width: 60, height:14, borderRadius:2, alignItems:"center", justifyContent:"center"}}>
                <Text
                  style={{
                    fontSize: 8,
                    color: "#FFF",
                    fontFamily: "Montserrat-Bold",
                  }}
                >
                  {invite_status.toUpperCase()}
                </Text>
              </Box>
            )
          } */}
          </Box>
        </HStack>
      </VStack>
    </TouchableOpacity>
  );
}
