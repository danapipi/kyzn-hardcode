import React from "react";
import { View, Text } from "react-native";
import { moderateScale } from "react-native-size-matters";
import DividerHorizontal from "components/ui/DividerHorizontal";
import Commons from "../../utils/common";
import moment from "moment";

export default function TopUpHistoryList({ data }) {
  console.log(
    "amount",
    data,
    moment(data.item.created_at).format("DD MMM YYYY")
  );
  return (
    <View style={{ flex: 1 }} key={data.item.id}>
      {/* <View
        style={{
          backgroundColor: "#F8F9FC",
          height: moderateScale(38, 0.25),
          justifyContent: "center",
        }}
      >
        <Text
          style={{
            color: "#757575",
            fontSize: moderateScale(10, 0.25),
            fontWeight: "600",
            marginHorizontal: moderateScale(16, 0.25),
            fontFamily: "Montserrat-Regular",
          }}
        >
          {data.item.date}
        </Text>
      </View> */}
      <View>
        {/* {data.item.details.map((item) => {
          return ( */}
        <View>
          <View style={{ flexDirection: "row" }}>
            <View
              style={{
                flexDirection: "column",
                flex: 1,
                marginLeft: moderateScale(20, 0.25),
                // backgroundColor: "red"
              }}
            >
              <Text
                style={{
                  fontSize: moderateScale(12, 0.25),
                  fontWeight: "700",
                  color: "black",
                  marginTop: moderateScale(16, 0.25),
                  fontFamily: "Montserrat-Regular",
                }}
                numberOfLines={1}
              >
                {/* {item.label} */} 
                {data.item.type.charAt(0).toUpperCase() + data.item.type.slice(1)}
              </Text>
              <Text
                style={{
                  color: "#757575",
                  fontWeight: "600",
                  fontSize: moderateScale(10, 0.25),
                  fontFamily: "Montserrat-Regular",
                }}
              >
                {/* {item.from} */}
                K-Money
              </Text>
              <Text
                style={{
                  color: "#757575",
                  fontWeight: "600",
                  fontSize: moderateScale(10, 0.25),
                  marginTop: moderateScale(11, 0.25),
                  marginBottom: moderateScale(16, 0.25),
                  fontFamily: "Montserrat-Regular",
                }}
              >
                {/* {item.detail_trx} */}
                {moment(data.item.created_at).format("DD MMM YYYY")}
              </Text>
            </View>
            <View
              style={{
                flexDirection: "column",
                marginRight: moderateScale(16, 0.25),
              }}
            >
              <Text
                style={{
                  fontSize: moderateScale(12, 0.25),
                  fontWeight: "700",
                  color: "black",
                  marginTop: moderateScale(16, 0.25),
                  fontFamily: "Montserrat-Regular",
                }}
              >
                {/* Rp. {item.price} */}
                {data.item.type == "purchase" && "-"} {Commons.convertToRupiah(data.item.amount)}
              </Text>
              <Text
                style={{
                  fontSize: moderateScale(8, 0.25),
                  fontWeight: "700",
                  color:
                    // item.status === "COMPLETED" ? "#009174" : "#F99004",
                    data.item.type !== "purchase" ? "#009174" : "#ff5252",
                    // "#009174",
                  marginTop: moderateScale(3, 0.25),
                  alignSelf: "flex-end",
                  fontFamily: "Montserrat-Regular",
                }}
              >
                {/* {item.status} */}
                COMPLETED
              </Text>
            </View>
          </View>
          <DividerHorizontal style={{ marginVertical: moderateScale(0) }} />
        </View>
        {/* ); */}
        {/* })} */}
      </View>
    </View>
  );
}
