import React, { useState } from "react";
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  Image,
  ScrollView,
} from "react-native";
import { ChevronDownIcon, Actionsheet, Box, Button } from "native-base";
import { image } from "images";

import { moderateScale } from "react-native-size-matters";
import { countryList } from "helper/CountryList";

const NationPicker = ({
  label,
  bgColorLabel = "#F8F9FC",
  onSelected,
  value,
  valueInput,
  ...props
}) => {
  const [isFocused, setIsFocused] = useState(true);

  const [nationSelected, setNationSelected] = useState(value || "Indonesia");
  const [isOpen, setIsOpen] = useState(false);

  const onClose = () => {
    setIsOpen(false);
  };

  const openNationSheet = () => {
    setIsOpen(true);
  };

  const onDone = () => {
    setIsOpen(false);
  };

  const onViewDetail = () => {
    setIsOpen(false);
  };

  const changeNation = (label, value) => {
    setNationSelected(label);
    onSelected(value);
  };
  const labelStyle = {
    position: "absolute",
    left: 0,
    top: !isFocused ? moderateScale(35) : moderateScale(10),
    fontSize: !isFocused ? moderateScale(14) : moderateScale(10, 0.25),
    color: !isFocused ? "#aaa" : "#757575",
    marginLeft: moderateScale(18),
    marginBottom: !isFocused ? moderateScale(20) : moderateScale(0),
    zIndex: !isFocused ? moderateScale(0) : moderateScale(9),
    paddingLeft: !isFocused ? moderateScale(0) : moderateScale(5),
    paddingRight: !isFocused ? moderateScale(0) : moderateScale(5),
    backgroundColor: !isFocused ? "transparent" : bgColorLabel,
  };

  const inputContainer = {
    marginTop: 20,
    //   marginLeft: 10,
    marginRight: 10,
    borderLeftWidth: 2,
    borderRightWidth: 2,
    borderBottomWidth: 2,
    borderRadius: 4,
    height: 50,
    width: "100%",
    borderColor: !isFocused ? "#aaa" : "#9E9E9E",
    borderTopWidth: !isFocused ? 2 : 2,
    zIndex: 1,
    overflow: "visible",
  };
  const parent = {
    marginHorizontal: moderateScale(16),
    marginBottom: moderateScale(12, 0.25),
  };
  return (
    <View style={parent}>
      <Text style={labelStyle}>{label}</Text>
      <View style={inputContainer}>
        <TouchableOpacity
          style={{
            //   backgroundColor: boxColor || "#45535F",
            height: moderateScale(42, 0.25),
            // justifyContent: "center",
            flexDirection: "row",
            borderRadius: 2,
            //   borderColor: error ? "#FF5252" : "#45535F",
            //   borderWidth: error ? 1 : 0,
            marginBottom: moderateScale(4),
            flex: 1,
          }}
          onPress={() => openNationSheet()}
        >
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              flex: 1,
              justifyContent: "space-between",
            }}
          >
            <Text
              style={{
                fontSize: valueInput || moderateScale(14),
                fontFamily: "Montserrat-Regular",
                //   color: locationSelected === "Indonesia" ? "#FFFFFF" : "#838586"
                color: "black",
                marginLeft: moderateScale(16),
              }}
            >
              {nationSelected}
            </Text>
            <ChevronDownIcon
              size={6}
              style={{ marginRight: moderateScale(11, 0.25) }}
            />
          </View>
        </TouchableOpacity>
      </View>
      <Actionsheet isOpen={isOpen} onClose={onClose}>
        <Actionsheet.Content h={385}>
          <Image
            source={image.bg_actionsSheet}
            style={{ width: 300, height: 300, position: "absolute", right: 0 }}
            alt="image"
          />

          <Box style={{ width: 270, marginTop: 8 }}>
            <Text
              style={{
                fontSize: moderateScale(16, 0.25),
                color: "#000",
                fontWeight: "bold",
                fontFamily: "Montserrat-Regular",
                marginBottom: moderateScale(12, 0.25),
              }}
              //   alignItems="flex-start"
            >
              Select Nationality
            </Text>
            <ScrollView
              showsVerticalScrollIndicator={false}
              style={{marginBottom: moderateScale(60, 0.25)}}
            >
              {countryList.map((item, index) => {
                // console.log("item", item);
                return (
                  <TouchableOpacity
                    key={index}
                    onPress={() => changeNation(item.label, item.value)}
                    style={styles.gridButtonContainer}
                  >
                    <View
                      style={{
                        flexDirection: "row",
                        marginVertical: moderateScale(12, 0.25),
                        justifyContent: "space-between",
                      }}
                    >
                      <Text
                        style={[
                          styles.gridLabel,
                          {
                            color: "black",
                            fontWeight:
                              nationSelected === item.label ? "bold" : "normal",
                          },
                        ]}
                      >
                        {item.label}
                      </Text>
                      {nationSelected === item.label && (
                        <View
                          style={{
                            justifyContent: "center",
                            marginHorizontal: moderateScale(16),
                          }}
                        >
                          <View
                            style={{
                              height: 8,
                              width: 8,
                              borderRadius: 1000,
                              backgroundColor: "#1035BB",
                              justifyContent: "center",
                              alignItems: "center",
                            }}
                          />
                        </View>
                      )}
                    </View>
                  </TouchableOpacity>
                );
              })}
            </ScrollView>
          </Box>
        </Actionsheet.Content>
      </Actionsheet>
    </View>
  );
};

export default NationPicker;

const styles = StyleSheet.create({
  gridContainer: {
    flex: 1,
    flexDirection: "column",
    // flexWrap: "wrap",
    // padding: 10,
    // marginBottom: moderateScale(20),
    // backgroundColor: "#1D1F21",
    backgroundColor: "#343E47",
  },
  gridButtonContainer: {
    // marginTop: moderateScale(18),
    justifyContent: "center",
    // alignItems: "center"
  },
  gridIcon: {
    fontSize: 30,
    color: "white",
  },
  gridLabel: {
    fontSize: moderateScale(14),
    fontFamily: "Montserrat-Regular",
    // marginLeft: moderateScale(8),
    // paddingTop: 10,
  },
});
