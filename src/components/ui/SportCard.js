import React, { useState } from "react";
import { TouchableOpacity } from "react-native";
import { Box, Text, Image, AspectRatio, HStack, VStack } from "native-base";
import PropTypes from "prop-types";
import Commons from "../../utils/common";
import _ from "lodash";
import ActivityType from "../../helper/ActivityType";
import { image } from "images";
import { useSelector } from "react-redux";

const SportCard = ({ data, type, numCol, onPress }) => {
  console.log('SPORT CARD: ', data);
  const dataActivity = data.item;
  const [uriImage, setUriImage] = useState("");
  const {
    class_activity_type_id,
    panorama_activity_type_id,
    pt_activity_type_id,
    rent_court_activity_type_id,
  } = useSelector(state => state.PRODUCT.activityTypeId.activityTypeId);

  let isDiscounted = _.get(dataActivity, "is_discounted", null);
  // let realPrice = _.get(dataActivity, "price", null);
  let realPrice = _.get(dataActivity, "price", null);
  let isCouponDetected = _.get(dataActivity, "has_coupon", false);
  let activityType = _.get(dataActivity, "activity_type_id", "");
  console.log("asssssssuuuu", dataActivity.step_duration_price);
  let price =
    // activityType == 1
    //   ? dataActivity.step_duration_price
    // :
    isDiscounted ? dataActivity.discounted_price : realPrice;
  // ? realPrice
  // : dataActivity.step_duration_price;

  console.log(
    "price price",
    price,
    dataActivity.discounted_price,
    dataActivity,
  );
  let imageTemp =
    uriImage === "" ? { uri: dataActivity.image_thumbnail } : { uri: uriImage };
  return (
    <Box
      flex={1 / numCol}
      alignItems="center"
      style={{
        marginLeft: (data.index + 1) % 2 == 0 ? 6 : 16,
        marginRight: (data.index + 1) % 2 == 0 ? 16 : 6,
      }}>
      <TouchableOpacity style={{ flex: 1, width: "100%" }} onPress={onPress}>
        <Box
          w={"100%"}
          borderWidth={1}
          borderColor={"#EEEEEE"}
          borderStyle={"solid"}
          marginBottom={13}
          backgroundColor="#FFF"
          style={{
            paddingBottom: 16,
          }}>
          {/* <AspectRatio ratio={16 / 9}> */}
          {/* <Box style={{height: type == ActivityType.RENT_COUNT ? 158 : 79}}> */}
          <Box
            style={{ height: type == rent_court_activity_type_id ? 158 : 79 }}>
            <Image
              onError={e => setUriImage("https://via.placeholder.com/103x104")}
              source={imageTemp}
              // h={type == ActivityType.RENT_COUNT ? 158 : 79}
              h={type == rent_court_activity_type_id ? 158 : 79}
              resizeMode="cover"
              // alt="image"
            />
          </Box>
          {/* </AspectRatio> */}
          <VStack style={{ paddingHorizontal: 10, paddingTop: 12 }}>
            <Text
              alignItems="flex-start"
              fontWeight="bold"
              numberOfLines={1}
              style={{ fontFamily: "Montserrat-Regular", fontWeight: "700" }}>
              {dataActivity.name}
            </Text>
            <HStack justifyContent="space-between" style={{ marginTop: 8 }}>
              <Text
                alignItems="flex-start"
                fontWeight="bold"
                fontSize={10}
                style={{ fontFamily: "Montserrat-Regular", fontWeight: "700" }}>
                {/* Rp. {dataActivity.is_discounted ? Commons.convertToRupiah(dataActivity.discounted_price) : Commons.convertToRupiah(dataActivity.price)} {type == 7 && "/hr"} */}
                {/* {type == 1 && "Starting from\n"}{Commons.convertToRupiah(price)}  */}
                {type == rent_court_activity_type_id && "Starting from\n"}
                {Commons.convertToRupiah(price)}
                {/* {type == ActivityType.RENT_COUNT && "/hr"} */}
              </Text>

              <HStack justifyContent="center" alignItems="center">
                {isCouponDetected == true && (
                  <Image
                    source={image.ic_coupon}
                    style={{ height: 12, width: 12, marginRight: 3 }}
                    alt={"couponImage"}
                  />
                )}
                <Image
                  // source={type == ActivityType.RENT_COUNT
                  source={
                    type == rent_court_activity_type_id
                      ? require("../../../assets/images/star.png")
                      : require("../../../assets/images/xp.png")
                  }
                  style={{ height: 12, width: 12, marginRight: 3 }}
                  resizeMode="cover"
                  alt="image"
                />
                <Text
                  alignItems="flex-end"
                  fontWeight="bold"
                  fontSize={10}
                  style={{ fontFamily: "Montserrat-Regular" }}>
                  {/* {type == ActivityType.RENT_COUNT */}
                  {type == rent_court_activity_type_id
                    ? dataActivity.rating
                    : dataActivity.xp_reward}
                </Text>
              </HStack>
            </HStack>
            {dataActivity.is_discounted && (
              <Text
                alignItems="flex-start"
                fontWeight="bold"
                fontSize={8}
                strikeThrough={true}
                color="#FF3F22"
                style={{ fontFamily: "Montserrat-Regular" }}>
                {/* Rp. {dataActivity.price} */}
                {Commons.convertToRupiah(Commons.convertToRupiah(price))}
              </Text>
            )}
          </VStack>
        </Box>
      </TouchableOpacity>
    </Box>
  );
};

SportCard.propTypes = {
  data: PropTypes.object.isRequired,
};

export default SportCard;
