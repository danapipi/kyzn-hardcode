
import React, { useState } from "react";
import { Text } from "react-native";
import { Box } from "native-base";
import PropTypes from "prop-types";
import _ from "lodash";
import { moderateScale } from "react-native-size-matters";

const ProgressLevel = ({ res }) => {
  let currentLvl = _.get(res, 'data.current_level', 1)
  let userExp = _.get(res, 'data.user_experiences', [])
  let userXP = _.get(res, 'data.xp', 0)
  let nextLvl = currentLvl + 1
  let defaultProgress = userXP / (currentLvl - 1) 
  let progressLvl = userExp.length !== 0 ? userExp[1].exp_needed : defaultProgress
  let nextLvlProgress = currentLvl * progressLvl
  let nowXP = (currentLvl - 1) * progressLvl
  let progressNow = (userXP + nextLvlProgress) - (nowXP + nextLvlProgress)

  console.log("data progress", (nowXP + nextLvlProgress), (userXP + nextLvlProgress), progressNow);

  return(
    <Box style={{height:50, flexDirection:"row", alignItems:"flex-start", marginBottom:20, marginTop:30}}>
      <Box style={{flex:1}}>
        {currentLvl !== 1 &&
          <Box style={{height:moderateScale(4, .25), backgroundColor:"#D4D7ED", width:"100%"}}>
            <Box style={{height:moderateScale(4, .25), backgroundColor:"#6778D0", width:"100%"}}/>
          </Box>
        }
      </Box>
      <Box style={{flex:7}}>
        <Box style={{position:"absolute", top:-40, left: progressNow == 0 || progressNow == 100 ? `${progressNow - 7.5}%` : `${progressNow - 10}%`, alignItems:"center"}}>
          <Box style={{marginTop:10, backgroundColor:"#A6B0E1", padding:2, borderRadius:100, paddingHorizontal:5}}>
            <Text style={{
              fontWeight: "700",
              fontSize: moderateScale(10, .25),
              letterSpacing: 0.2,
              fontFamily: "Montserrat-Regular",
              color: "#FFF"
            }}>
              XP {userXP} 
            </Text>
          </Box>
          <Box style={{
              width: 0,
              height: 0,
              borderLeftWidth: 4,
              borderRightWidth: 4,
              borderTopWidth: 3,
              borderStyle: 'solid',
              backgroundColor: 'transparent',
              borderLeftColor: 'transparent',
              borderRightColor: 'transparent',
              borderTopColor: '#A6B0E1'
          }}/>
        </Box>
        <Box style={{height:moderateScale(4, .25), backgroundColor:"#D4D7ED", width:"100%"}}>
          <Box style={{height:moderateScale(4, .25), backgroundColor:"#6778D0", width:`${progressNow}%`}}/>
        </Box>
        <Box style={{position:"absolute", top:-5, left:-2}}>
          <Box style={{height:moderateScale(15, .25), width:moderateScale(15, .25), backgroundColor:"#6778D0", borderRadius:30}}/>
          <Box style={{marginTop:10}}>
            <Text style={{
              fontWeight: "700",
              fontSize: moderateScale(12, .25),
              letterSpacing: 0.2,
              fontFamily: "Montserrat-Regular",
              color: "#000"
            }}>
              Level {currentLvl}
            </Text>
            <Text style={{
              fontWeight: "700",
              fontSize: moderateScale(10, .25),
              letterSpacing: 0.2,
              fontFamily: "Montserrat-Regular",
              color: "#757575"
            }}>
              {nowXP} XP
            </Text>
          </Box>
        </Box>
      </Box>
      <Box style={{flex:3}}>
        <Box style={{height:moderateScale(4, .25), backgroundColor:"#D4D7ED", width:"100%"}}>
          <Box style={{height:moderateScale(4, .25), backgroundColor:"#6778D0", width:"0%"}}/>
        </Box>
        <Box style={{position:"absolute", top:-5, left:-2}}>
          <Box style={{height:moderateScale(15, .25), width:moderateScale(15, .25), backgroundColor: progressNow == 100 ? "#6778D0" : "#D4D7ED", borderRadius:30}}/>
          <Box style={{marginTop:10}}>
            <Text style={{
              fontWeight: "700",
              fontSize: moderateScale(12, .25),
              letterSpacing: 0.2,
              fontFamily: "Montserrat-Regular",
              color: "#000"
            }}>
              Level {nextLvl}
            </Text>
            <Text style={{
              fontWeight: "700",
              fontSize: moderateScale(10, .25),
              letterSpacing: 0.2,
              fontFamily: "Montserrat-Regular",
              color: "#757575"
            }}>
              {nextLvlProgress} XP
            </Text>
          </Box>
        </Box>
      </Box>
    </Box>
  )
}

export default ProgressLevel;