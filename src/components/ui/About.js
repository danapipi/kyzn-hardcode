import React, { useState } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ActivityIndicator,
} from "react-native";
import { getScreenWidth } from "utils/size";
import theme from "utils/theme";
import { moderateScale } from "react-native-size-matters";
import { ChevronRightIcon } from "native-base";
import { image } from "images";
import Actions from "actions";
import RNStorage from "../../utils/storage";

import { useDispatch, useSelector } from "react-redux";
import ClearFcmToken from "../../utils/ClearFCMToken";

const About = ({ navigation }) => {
  const dispatch = useDispatch();
  const [isLoadingLogOut, setIsLoadingLogOut] = useState(false);
  const [dataMenu, setDataMenu] = useState([
    {
      id: 1,
      label: "FAQ",
      image: image.faq,
      action: () => {
        navigation.navigate("FAQ");
      },
    },
    {
      id: 2,
      label: "Terms and Conditions",
      image: image.ic_term,
      action: () => {
        navigation.navigate("Terms");
      },
    },
    {
      id: 3,
      label: "Privacy Policy",
      image: image.ic_privacy,
      action: () => {
        navigation.navigate("Privacy");
      },
    },
    {
      id: 4,
      label: "Logout",
      image: image.ic_logout,
      action: () => {
        try {
          setIsLoadingLogOut(true);
          // ClearFcmToken()
          setTimeout(() => {
            RNStorage.removeToken();
            RNStorage.removeFirstTime();
            dispatch(Actions.firstTime(false));
            dispatch(Actions.logout());
            setIsLoadingLogOut(false);
          }, 1500);
          // .then(res => {
          //   setTimeout(() => {
          //     RNStorage.removeToken();
          //     RNStorage.removeFirstTime();
          //     dispatch(Actions.firstTime(false));
          //     dispatch(Actions.logout());
          //     setIsLoadingLogOut(false);
          //   }, 1500);
          // })
          // .catch(error => {
          //   setTimeout(() => {
          //     RNStorage.removeToken();
          //     RNStorage.removeFirstTime();
          //     dispatch(Actions.firstTime(false));
          //     dispatch(Actions.logout());
          //     setIsLoadingLogOut(false);
          //   }, 1500);
          // });
        } catch (error) {
          setTimeout(() => {
            RNStorage.removeToken();
            RNStorage.removeFirstTime();
            dispatch(Actions.firstTime(false));
            dispatch(Actions.logout());
            setIsLoadingLogOut(false);
          }, 1500);
        }
      },
    },
  ]);
  return (
    <View
      style={{
        width: getScreenWidth(),
      }}>
      <Text
        style={{
          fontSize: moderateScale(14, 0.25),
          color: "black",
          fontFamily: "Montserrat-Regular",
          fontWeight: "700",
          marginBottom: moderateScale(16, 0.25),
          marginHorizontal: moderateScale(16, 0.25),
        }}>
        About
      </Text>
      {dataMenu.map(item => (
        <TouchableOpacity
          style={{
            marginHorizontal: moderateScale(16, 0.25),
            borderTopWidth: 1,
            borderTopColor: "#EBEEF4",
            flexDirection: "row",
            alignItems: "center",
          }}
          key={item.id}
          onPress={item.action}>
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              paddingVertical: moderateScale(15, 0.25),
              marginHorizontal: moderateScale(18, 0.25),
              flex: 2,
            }}>
            <Image
              source={item.image}
              resizeMode="contain"
              alt="image"
              // size="xl"
              style={{
                width: moderateScale(16, 0.25),
                height: moderateScale(16, 0.25),
                // marginLeft: moderateScale(16, 0.25),
                marginRight: moderateScale(8, 0.25),
              }}
            />
            <Text
              style={{
                fontSize: moderateScale(12, 0.25),
                color: item.label === "Logout" ? "#FF8064" : "black",
                fontFamily: "Montserrat-Regular",
                fontWeight: "600",
              }}>
              {item.label}
            </Text>
          </View>
          <View>
            {item?.label === "Logout" ? (
              isLoadingLogOut ? (
                <ActivityIndicator />
              ) : (
                <ChevronRightIcon size={6} />
              )
            ) : (
              <ChevronRightIcon size={6} />
            )}
          </View>
        </TouchableOpacity>
      ))}
    </View>
  );
};

export default About;
