import React, { useState } from "react";
import { ImageBackground, Dimensions, Image } from "react-native";
import {
  Box,
  Text,
  Button,
  FlatList,
  ScrollView,
  HStack,
  ZStack,
} from "native-base";
import PropTypes from "prop-types";
import { image } from "../../../assets/images";
import { moderateScale } from "react-native-size-matters";
import UserTrackingBoxCard from "./UserTrackingBoxCard";

const { width, height } = Dimensions.get("window");

export default function UserTracking({
  name,
  exp,
  coin,
  userphoto,
  disablePanorama,
}) {
  const userTrakingWithPanorama = () => {
    return (
      <Box
        style={{
          // flex: 1,
          flexDirection: "row",
          width: width,
          // backgroundColor: "red"
        }}
      >
        <Box
          style={{
            flex: 1,
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
            marginHorizontal: moderateScale(16, 0.25),
          }}
        >
          {/* <UserTrackingBoxCard
            label={"Move"}
            dataLabel={"113"}
            color={"#3643F3"}
            dataCircular={75}
            dataDesc={"625/550 Cal"}
          />
          <UserTrackingBoxCard
            label={"Stand"}
            dataLabel={"100"}
            color={"#C90000"}
            dataCircular={99}
            dataDesc={"12/12 HR"}
          /> */}
        </Box>
        <Box
          style={{
            marginBottom: moderateScale(29, 0.25),
            marginTop: moderateScale(25, 0.25),
            flex: 1,
            justifyContent: "center",
            alignItems: "center",
            // flexDirection: "row"
          }}
        >
          <Image
            source={
              userphoto
                ? {
                    uri: userphoto,
                  }
                : image.ic_profile_placeholder
            }
            resizeMode="cover"
            alt="imageProfile"
            style={{
              width: moderateScale(77, 0.25),
              height: moderateScale(75, 0.25),
              // position: "absolute",
            }}
          ></Image>
          <Box style={{ justifyContent: "flex-end", alignItems: "center" }}>
            <ImageBackground
              source={image.bg_level}
              resizeMode="contain"
              alt="imageProfile"
              style={{
                width: moderateScale(43, 0.25),
                // height: moderateScale(14, 0.25),
                justifyContent: "center",
                alignItems: "center",
                position: "absolute",
              }}
            >
              <Text
                style={{
                  fontSize: moderateScale(8, 0.25),
                  fontWeight: "700",
                  color: "#ffffff",
                  fontFamily: "Montserrat-Regular",
                }}
              >
                LEVEL 1
              </Text>
            </ImageBackground>
          </Box>
        </Box>
        <Box
          style={{
            flex: 1,
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
            marginHorizontal: moderateScale(16, 0.25),
          }}
        >
          {/* <UserTrackingBoxCard
            label={"Step"}
            dataLabel={"113"}
            color={"#E9BD4A"}
            dataCircular={75}
            dataDesc={"12/12 HR"}
          />
          <UserTrackingBoxCard
            label={"Exercise"}
            dataLabel={"113"}
            color={"#5B643A"}
            dataCircular={75}
            dataDesc={"50/25 Min"}
          /> */}
        </Box>
      </Box>
    );
  };

  const userTrackingWithoutPanorama = () => {
    return (
      <Box
        style={{
          // flex: 1,
          // flexDirection: "row",
          width: width,
          height: moderateScale(106),
          // backgroundColor: "red",
          // ZIndex: 100,
        }}
      >
        <ZStack style={{}}>
          <Image
            source={image.background}
            // size="xl"
            alt="background"
            resizeMode="cover"
            style={{
              width: "100%",
              height: moderateScale(150, 0.25),
              marginTop: moderateScale(-70, 0.25),
            }}
            position="absolute"
          />
          <Box style={{ flexDirection: "row" }}>
            <Image
              source={
                userphoto
                  ? {
                      uri: userphoto,
                    }
                  : image.ic_profile_placeholder
              }
              resizeMode="cover"
              alt="imageProfile"
              style={{
                width: moderateScale(77, 0.25),
                height: moderateScale(75, 0.25),
                marginLeft: moderateScale(16, 0.25),
                // position: "absolute",
              }}
            ></Image>
            <Box style={{marginLeft: moderateScale(10, 0.25)}}>
              <Text
                style={{
                  fontSize: moderateScale(14, 0.25),
                  fontWeight: "700",
                  color: "#0E0E0E",
                  lineHeight: moderateScale(18, 0.25),
                  marginBottom: moderateScale(9, 0.25),
                  fontFamily: "Rubik-Regular",
                }}
              >
                {name}
              </Text>
              <HStack
                style={{
                  // backgroundColor: "blue",
                  justifyContent: "center",
                  alignItems: "center",
                  // height: moderateScale(42, 0.25)
                }}
              >
                <Image
                  source={image.exp}
                  resizeMode="contain"
                  alt="imageExp"
                  // size="xl"
                  style={{
                    width: moderateScale(19, 0.25),
                    height: moderateScale(19, 0.25),
                    marginRight: moderateScale(6, 0.25),
                  }}
                />
                <Text
                  style={{
                    color: "#212121",
                    fontSize: moderateScale(12, 0.25),
                    fontWeight: "500",
                    lineHeight: moderateScale(14, 0.25),
                    fontFamily: "Rubik-Regular",
                  }}
                >
                  {exp} XP
                </Text>
                <Image
                  source={image.coin}
                  resizeMode="contain"
                  alt="imageCoin"
                  // size="xl"
                  style={{
                    width: moderateScale(19, 0.25),
                    height: moderateScale(19, 0.25),
                    marginRight: moderateScale(6, 0.25),
                    marginLeft: moderateScale(11, 0.25),
                  }}
                />
                <Text
                  style={{
                    color: "#212121",
                    fontSize: moderateScale(12, 0.25),
                    fontWeight: "500",
                    lineHeight: moderateScale(14, 0.25),
                    fontFamily: "Rubik-Regular",
                  }}
                >
                  {coin} Coin
                </Text>
              </HStack>
            </Box>
          </Box>
        </ZStack>
      </Box>
    );
  };
  return disablePanorama
    ? userTrackingWithoutPanorama()
    : userTrakingWithPanorama();
}
