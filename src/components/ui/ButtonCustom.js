import React from "react";
import { View, Text, TouchableOpacity, Image, ActivityIndicator } from "react-native";
import { moderateScale } from "react-native-size-matters";


const ButtonCustom = ({
  label,
  buttonColor,
  buttonHeight,
  buttonRadius,
  labelFont,
  labelSize,
  labelColor,
  labelWeight,
  onPressButton,
  buttonMarginTop,
  buttonMarginBottom,
  buttonMarginHorizontal,
  disableButton,
  appiumLabel,
  styleButton,
  styleLabel,
  includeLogo = false,
  styleLogo,
  logo,
  isLoading = false
}) => {
  return (
    <TouchableOpacity
      onPress={onPressButton}
      disabled={isLoading === true ? true : disableButton}
      testID={appiumLabel}
      accessibilityLabel={appiumLabel}
    >
      <View
        style={[
          {
            marginHorizontal:
              moderateScale(buttonMarginHorizontal, 0.25) ||
              moderateScale(16, 0.25),
            height:
              moderateScale(buttonHeight, 0.25) || moderateScale(40, 0.25),
            borderRadius: moderateScale(buttonRadius) || moderateScale(2),
            backgroundColor: buttonColor || "#1035BB",
            justifyContent: "center",
            alignItems: "center",
            flexDirection: "row",
            marginTop:
              moderateScale(buttonMarginTop, 0.25) || moderateScale(10, 0.25),
            marginBottom:
              moderateScale(buttonMarginBottom, 0.25) ||
              moderateScale(10, 0.25),
          },
          styleButton,
        ]}
      >
        {includeLogo ? (
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              alignSelf: "center",
              flexDirection: "row",
            }}
          >
            <Image
              style={[
                {
                  width: moderateScale(14, 0.25),
                  height: moderateScale(14, 0.25),
                  marginRight: moderateScale(6, 0.25),
                },
                styleLogo,
              ]}
              source={logo}
            />

            <Text
              style={[
                {
                  marginHorizontal: moderateScale(8, 0.25),
                  fontFamily: labelFont || "Montserrat-Regular",
                  fontWeight: labelWeight || "normal",
                  fontSize:
                    moderateScale(labelSize, 0.25) || moderateScale(14, 0.25),
                  color: labelColor || "#FFFFFF",
                },
                styleLabel,
              ]}
            >
              {label}
            </Text>
          </View>
        ) : isLoading ? (
          <ActivityIndicator
            size={"small"}
            color={"#FFFFFF"}
          />
        ) : (
          <Text
            style={[
              {
                marginHorizontal: moderateScale(8, 0.25),
                fontFamily: labelFont || "Montserrat-Regular",
                fontWeight: labelWeight || "normal",
                fontSize:
                  moderateScale(labelSize, 0.25) || moderateScale(14, 0.25),
                color: labelColor || "#FFFFFF",
              },
              styleLabel,
            ]}
          >
            {label}
          </Text>
        )}
      </View>
    </TouchableOpacity>
  );
};

export default ButtonCustom;
