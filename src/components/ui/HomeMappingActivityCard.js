import React from "react";
import { View, Text } from "react-native";
import HomeActivityCard from "components/ui/HomeActivityCard";
import _ from "lodash";

const HomeMappingActivityCard = ({ navigation, dataAllBanner }) => {
  console.log("HomeMappingActivityCard Props: ", dataAllBanner);
  return (
    <View>
      {dataAllBanner.map(item => {
        if (item.category_child !== null && item.category !== null) {
          return (
            <HomeActivityCard
              label={item.category_child.name}
              data={item}
              image={item.category_child.image}
              navigation={navigation}
              onPressViewAll={() => {
                navigation.navigate("SportList", {
                  screen: "Home",
                  title: item.category_child.name,
                  params: {
                    title: item.category_child.name,
                    activityId: item.activity_id,
                    categoryId: item.category.id,
                    categoryChildId: item.category_child.id,
                  },
                });
              }}
            />
          );
        } else if (item.category_child !== null) {
          return (
            <HomeActivityCard
              label={item.category_child.name}
              data={item}
              navigation={navigation}
              image={item.category_child.image}
              onPressViewAll={() => {
                navigation.navigate("SportList", {
                  screen: "Home",
                  title: item.category_child.name,
                  params: {
                    title: item.category_child.name,
                    activityId: item.activity_id,
                    categoryId: item.category.id,
                    categoryChildId: item.category_child.id,
                  },
                });
              }}
            />
          );
        } else {
          return (
            <HomeActivityCard
              label={item.category.name}
              data={item}
              image={item.category.image}
              navigation={navigation}
              onPressViewAll={() => {
                navigation.navigate("Category", {
                  categoryID: item.category.id,
                  title: item.category.name,
                });
                // navigation.navigate("SportList", {
                //   screen: "Home",
                //   title: item.category.name,
                //   params: {
                //     title: item.category.name,
                //     activityId: item.activity_id,
                //     categoryId: item.category.id,
                //     categoryChildId: item.category_child_id,
                //   },
                // });
              }}
            />
          );
        }
      })}
    </View>
  );
};

export default HomeMappingActivityCard;
