import React from "react";
import {Platform} from 'react-native';
import { createStackNavigator } from "@react-navigation/stack";
import MembershipList from "screens/Memberships/List";
import MembershipPackagesList from "screens/Memberships/PackagesList";
import MembershipView from "screens/Memberships/View";
import MembershipPayment from "screens/Memberships/Payment";
import MembershipViewGroup from "screens/Memberships/View/GroupMemberView";
import { ChevronLeftIcon } from "native-base";
import { Image, TouchableOpacity, View, Dimensions } from "react-native";

const Stack = createStackNavigator();
const { width, height } = Dimensions.get("window");

const MembershipsStack = () => (
  <Stack.Navigator initialRouteName="MembershipList">
    <Stack.Screen
      name="MembershipList"
      component={MembershipList}
      options={{
        headerShown: true,
        title: "Premium Memberships",
        headerBackTitle: "",
        headerBackTitleVisible: false,
        headerBackTitleStyle: {
          fontWeight: "bold",
          marginLeft: 10,
        },
        headerTitleStyle: {
          fontWeight: "bold",
          marginLeft: Platform?.OS === 'android' ? -15 : -120,
        },
        headerBackImage: () => <ChevronLeftIcon />
      }}
    />
    <Stack.Screen
      name="MembershipPackagesList"
      component={MembershipPackagesList}
      options={{
        headerShown: true,
        title: "Premium Memberships",
        headerBackTitle: "",
        headerBackTitleVisible: false,
        headerBackTitleStyle: {
          fontWeight: "bold",
          marginLeft: 10,
        },
        headerTitleStyle: {
          fontWeight: "bold",
          marginLeft: Platform?.OS === 'android' ? -15 : -120,
        },
        headerBackImage: () => <ChevronLeftIcon />
      }}
    />
    <Stack.Screen
      name="MembershipView"
      component={MembershipView}
      options={{
        headerShown: true,
        title: "Premium Memberships",
        headerBackTitle: "",
        headerBackTitleVisible: false,
        headerBackTitleStyle: {
          fontWeight: "bold",
          marginLeft: 10,
        },
        headerTitleStyle: {
          fontWeight: "bold",
          marginLeft: Platform?.OS === 'android' ? -15 : -120,
        },
        headerBackImage: () => <ChevronLeftIcon />
      }}
    />

    <Stack.Screen
      name="MembershipViewGroup"
      component={MembershipViewGroup}
      options={{
        headerShown: true,
        headerBackTitle: "",
        headerBackTitleVisible: false,
        headerBackTitleStyle: {
          fontWeight: "bold",
          marginLeft: 10,
        },
        headerTitleStyle: {
          fontWeight: "bold",
          marginLeft: Platform?.OS === 'android' ? -15 : -120,
        },
        headerBackImage: () => <ChevronLeftIcon />
      }}
    />

    <Stack.Screen
      name="MembershipPayment"
      component={MembershipPayment}
      options={{
        headerShown: true,
        title: " ",
        headerLeftContainerStyle: { left: 10 },
        headerBackTitle: "Payment",
        headerBackTitleVisible: true,
        headerTintColor: "#000",
        headerBackTitleStyle: {
          fontWeight: "bold",
          marginLeft: 10,
        },
        headerBackground: () => (
          <Image
            style={{ width: width, height: 60 }}
            source={require("../../../assets/images/payment_background.png")}
            alt="image"
            resizeMode="cover"
          />
        ),
      }}
    />
  </Stack.Navigator>
);

export default MembershipsStack;
