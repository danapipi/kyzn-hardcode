import React from "react";
import { StyleSheet, Image, View, TouchableOpacity, Text } from "react-native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import Home from "screens/Home";
import Activity from "screens/Activity";
import Inbox from "screens/Inbox";
import { Icon, QuestionIcon } from "native-base";
import Accounts from "screens/Accounts/Profile";
import { image } from "images";
import { useDispatch, useSelector } from "react-redux";
import _ from "lodash";

const styles = StyleSheet.create({
  icon: {
    width: 30,
    height: 30,
    marginVertical: 5,
  },
});

const Tab = createBottomTabNavigator();

// const renderIcon = ({ focused, color, size, route }) => {
//   let iconName;

//   if (route.name === "Home") {
//     iconName = focused ? Image.home_active : QuestionIcon;
//   } else if (route.name === "Activity") {
//     iconName = focused ? Image.activity_active : QuestionIcon;
//   } else if (route.name === "Inbox") {
//     iconName = focused ? QuestionIcon : Image.mail_inactive;
//   } else {
//     iconName = focused ? Image.profile_active : QuestionIcon;
//   }

//   return <Icon resizeMode="contain" source={iconName} style={styles.icon} />;
// };

const BottomTabs = ({ navigation }) => {
  const profile = useSelector(state => state.AUTH.profile.data);
  const isMember =
    profile.user_memberships &&
    [...profile.user_membership_packages, ...profile.user_memberships].filter(
      item => _.get(item, "membership.name") != "Non-Member",
    ).length > 0;
  // const isMember = false
  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        headerShown: true,
      })}>
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          headerShown: true,
          title: "Home",
          headerTitle: "",
          headerLeftContainerStyle: { left: 10 },
          headerTintColor: "#000",
          headerBackTitleStyle: {
            fontWeight: "bold",
            marginLeft: 10,
          },
          tabBarIcon: ({ tintColor, focused }) =>
            focused ? (
              <Image
                resizeMode="contain"
                source={image.home_active}
                style={styles.icon}
                alt="image"
              />
            ) : (
              <Image
                resizeMode="contain"
                source={image.home_inactive}
                style={{ width: 27, height: 27 }}
                alt="image"
              />
            ),
          headerLeft: () => (
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <TouchableOpacity>
                <Image
                  style={{
                    width: 56,
                    height: 22,
                    marginLeft: 21,
                    marginRight: 7,
                  }}
                  source={image.logo}
                  alt="image"
                />
              </TouchableOpacity>
              {isMember && (
                <TouchableOpacity
                  onPress={() => navigation.navigate("UserMembership")}>
                  <Image
                    style={{ width: 68, height: 16 }}
                    source={image.premium_ic}
                    alt="image"
                  />
                </TouchableOpacity>
              )}
            </View>
          ),
          headerRight: () => (
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <TouchableOpacity onPress={() => navigation.navigate("QRViewer")}>
                {/* <TouchableOpacity onPress={() => navigation.navigate("VerifyPin")}> */}
                <Image
                  style={{ width: 24, height: 24, marginHorizontal: 17 }}
                  source={image.scan_qr}
                  alt="image"
                />
              </TouchableOpacity>
              {/* <TouchableOpacity onPress={() => console.log("click search")}>
              <Image
                style={{ width: 24, height: 24, marginHorizontal: 17 }}
                source={image.search}
                alt="image"
              />
            </TouchableOpacity> */}
            </View>
          ),
        }}
      />
      <Tab.Screen
        name="Activity"
        component={Activity}
        options={{
          headerShown: false,
          tabBarIcon: ({ tintColor, focused }) =>
            focused ? (
              <Image
                resizeMode="contain"
                source={image.activity_active}
                style={styles.icon}
                alt="image"
              />
            ) : (
              <Image
                resizeMode="contain"
                source={image.activity_inactive}
                style={{ width: 27, height: 27 }}
                alt="image"
              />
            ),
          // tabBarIcon: ({ focused, color, size }) =>
          //   renderIcon({ focused, color, size }),
        }}
      />
      <Tab.Screen
        name="Inbox"
        component={Inbox}
        options={{
          headerShown: false,
          tabBarIcon: ({ tintColor, focused }) =>
            focused ? (
              <Image
                resizeMode="contain"
                source={image.mail_active}
                style={{ width: 27, height: 27 }}
                alt="image"
              />
            ) : (
              <Image
                resizeMode="contain"
                source={image.mail_inactive}
                style={styles.icon}
                alt="image"
              />
            ),
          // tabBarIcon: ({ focused, color, size }) =>
          //   renderIcon({ focused, color, size }),
        }}
      />
      <Tab.Screen
        name="Account"
        component={Accounts}
        options={{
          headerShown: false,
          tabBarIcon: ({ tintColor, focused }) =>
            focused ? (
              <Image
                resizeMode="contain"
                source={image.profile_active}
                style={styles.icon}
                alt="image"
              />
            ) : (
              <Image
                resizeMode="contain"
                source={image.profile_inactive}
                style={{ width: 24, height: 24 }}
                alt="image"
              />
            ),
          // tabBarIcon: ({ focused, color, size }) =>
          //   renderIcon({ focused, color, size }),
        }}
      />
    </Tab.Navigator>
  );
};

export default BottomTabs;
