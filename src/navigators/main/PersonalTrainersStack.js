import React from "react";
import {Platform} from 'react-native';
import { createStackNavigator } from "@react-navigation/stack";
import PersonalTrainersList from "screens/PersonalTrainers/List";
import PersonalTrainerView from "screens/PersonalTrainers/View";
import { ChevronLeftIcon } from "native-base";

const Stack = createStackNavigator();

const PersonalTrainersStack = () => (
  <Stack.Navigator initialRouteName="PersonalTrainersList">
    <Stack.Screen
      name="PersonalTrainersList"
      component={PersonalTrainersList}
      options={{
        headerShown: true,
        title: "Personal Trainer",
        headerBackTitle: "",
        headerBackTitleVisible: false,
        headerBackTitleStyle: {
          fontWeight: "bold",
          marginLeft: 10,
        },
        headerTitleStyle: {
          fontWeight: "bold",
          marginLeft: Platform?.OS === 'android' ? -15 : -120,
        },
        headerBackImage: () => <ChevronLeftIcon />,
      }}
    />
    <Stack.Screen
      name="PersonalTrainerView"
      component={PersonalTrainerView}
      options={{
        headerShown: true,
        title: "",
        headerBackTitle: "",
        headerBackTitleVisible: false,
        headerBackTitleStyle: {
          fontWeight: "bold",
          marginLeft: 10,
        },
        headerTitleStyle: {
          fontWeight: "bold",
          marginLeft: Platform?.OS === 'android' ? -15 : -120,
        },
        headerBackImage: () => <ChevronLeftIcon />,
      }}
    />
  </Stack.Navigator>
);

export default PersonalTrainersStack;
