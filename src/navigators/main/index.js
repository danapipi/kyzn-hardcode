import React, { useEffect, useState } from "react";
import {
  Image,
  TouchableOpacity,
  View,
  Dimensions,
  Platform,
} from "react-native";
import { createStackNavigator } from "@react-navigation/stack";
import { ChevronLeftIcon } from "native-base";
import Selectors from "selectors";

import Home from "screens/Home";
import BottomTabs from "./BottomTabs";
import BottomTabsChild from "./BottomTabsChild";
import BottomTabsFacility from "./BottomTabsFacility";
import CategoryScreen from "screens/Category";
import EducationScreen from "screens/Education";
import SportList from "screens/Sports/SportList";
import SportDetail from "screens/Sports/SportDetail";
import TopUp from "screens/TopUp";
import TopUpDetails from "screens/TopUpDetails";
import VirtualAccountList from "screens/TopUpDetails/VirtualAccountList";
import Payment from "screens/Payment";
import MembershipsStack from "./MembershipsStack";
import UserMembership from "screens/UserMembership";
import DetailActivity from "screens/Activity/DetailActivity";
import DetailOrder from "screens/Activity/DetailOrder";
import DetailViewAll from "screens/Activity/DetailViewAll";
import DetailSchedule from "screens/Sports/DetailSchedule";
import ProfileDetail from "screens/Accounts/ProfileDetail";
import EditProfile from "screens/Accounts/EditProfile";
import AddFamily from "screens/Family/Add";
import QRViewer from "screens/QRViewer";
import ScanQR from "screens/ScanQR";
import AddKids from "screens/Kids/Add";
import WebViewDisplay from "screens/WebView";
import FAQ from "screens/Misc/FAQ";
import Terms from "screens/Misc/Terms";
import Privacy from "screens/Misc/Privacy";
import PersonalTrainersStack from "./PersonalTrainersStack";
import AssignMembership from "screens/UserMembership/AssignMembership";
import CouponList from "screens/Coupon";
import Additional from "screens/Sports/Additional";
import Participant from "screens/Sports/Participant";
import InboxDetail from "../../screens/InboxDetail";
import ReferralCode from "screens/ReferralCode";
import SetPreferences from "screens/SetPreferences";
import MembershipView from "screens/Memberships/View";
import MembershipList from "screens/Memberships/List";
import MembershipPackagesList from "screens/Memberships/PackagesList";
import MembershipViewGroup from "screens/Memberships/View/GroupMemberView";
import VerifyPin from "screens/VerifyPin";
import MembershipPayment from "screens/Memberships/Payment";
import ChangePassword from "screens/ChangePassword";
import FacilityList from "screens/Sports/FacilityList";
// import LoadingScreen from "screens/Home/LoadingScreen";
import { useSelector } from "react-redux";

const Stack = createStackNavigator();
const { width, height } = Dimensions.get("window");

const MainStack = () => {
  const [showOneTimeScreen, setShowOneTimeScreen] = useState(false);
  const data = useSelector(state => state.AUTH.firstTime);

  useEffect(() => {
    const firstTime = data.firstTime;
    console.log("action.data nav", firstTime);
    setShowOneTimeScreen(firstTime);
  }, []);

  return (
    <Stack.Navigator
      initialRouteName={data.firstTime ? "BottomTabs" : "SetPreferences"}>
      <Stack.Screen
        name="BottomTabs"
        component={BottomTabs}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Memberships"
        component={MembershipsStack}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="PersonalTrainers"
        component={PersonalTrainersStack}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="ProfileDetail"
        component={ProfileDetail}
        options={({ route }) => ({
          headerShown: true,
          title: " ",
          headerBackTitle: route.params.title,
          headerTitleAlign: "left",
          headerBackTitleStyle: {
            fontWeight: "bold",
            marginLeft: 10,
          },
          headerTintColor: "#000",
          headerLeftContainerStyle: { left: 10 },
        })}
      />
      <Stack.Screen
        name="InboxDetail"
        component={InboxDetail}
        options={({ route }) => ({
          headerShown: true,
          title: "",
          // headerBackTitle: route.params.title,
          headerBackTitle: "Message",
          headerTitleAlign: "left",
          headerBackTitleStyle: {
            fontWeight: "bold",
            marginLeft: 10,
          },
          headerTintColor: "#000",
          headerLeftContainerStyle: { left: 10 },
        })}
      />
      <Stack.Screen
        name="MembershipView"
        component={MembershipView}
        options={{
          headerShown: true,
          title: "Premium Memberships",
          headerBackTitle: "",
          headerBackTitleVisible: false,
          headerBackTitleStyle: {
            fontWeight: "bold",
            marginLeft: 10,
          },
          headerTitleStyle: {
            fontWeight: "bold",
            marginLeft: 0,
          },
          headerBackImage: () => <ChevronLeftIcon />,
        }}
      />
      <Stack.Screen
        name="MembershipList"
        component={MembershipList}
        options={{
          headerShown: true,
          title: "Premium Memberships",
          headerBackTitle: "",
          headerBackTitleVisible: false,
          headerBackTitleStyle: {
            fontWeight: "bold",
            marginLeft: 10,
          },
          headerTitleStyle: {
            fontWeight: "bold",
            marginLeft: -130,
          },
          headerBackImage: () => <ChevronLeftIcon />,
        }}
      />
      <Stack.Screen
        name="MembershipPackagesList"
        component={MembershipPackagesList}
        options={{
          headerShown: true,
          title: "Premium Memberships",
          headerBackTitle: "",
          headerBackTitleVisible: false,
          headerBackTitleStyle: {
            fontWeight: "bold",
            marginLeft: 10,
          },
          headerTitleStyle: {
            fontWeight: "bold",
            marginLeft: -130,
          },
          headerBackImage: () => <ChevronLeftIcon />,
        }}
      />
      <Stack.Screen
        name="MembershipViewGroup"
        component={MembershipViewGroup}
        options={{
          headerShown: true,
          headerBackTitle: "",
          headerBackTitleVisible: false,
          headerBackTitleStyle: {
            fontWeight: "bold",
            marginLeft: 10,
          },
          headerTitleStyle: {
            fontWeight: "bold",
            marginLeft: -130,
          },
          headerBackImage: () => <ChevronLeftIcon />,
        }}
      />

      <Stack.Screen
        name="EditProfile"
        component={EditProfile}
        options={({ route }) => ({
          headerShown: true,
          title: " ",
          headerBackTitle: route.params.title,
          headerTitleAlign: "left",
          headerBackTitleStyle: {
            fontWeight: "bold",
            marginLeft: 10,
          },
          headerTintColor: "#000",
          headerLeftContainerStyle: { left: 10 },
        })}
      />
      <Stack.Screen
        name="UserMembership"
        component={UserMembership}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="AssignMembership"
        component={AssignMembership}
        options={({ route }) => ({
          headerShown: true,
          title: "Assign Memberships",
          headerBackTitle: "",
          headerBackTitleVisible: false,
          headerBackTitleStyle: {
            fontWeight: "bold",
            marginLeft: 10,
          },
          headerTitleStyle: {
            fontWeight: "bold",
            marginLeft: Platform?.OS === "ios" ? -130 : 10,
          },
          headerTintColor: "#000",
          headerLeftContainerStyle: { left: 10 },
        })}
      />
      <Stack.Screen
        name="Category"
        component={CategoryScreen}
        options={({ route }) => ({
          headerShown: true,
          title: " ",
          headerBackTitle: route.params.title,
          headerTitleAlign: "left",
          headerBackTitleVisible: true,
          headerBackTitleStyle: {
            fontWeight: "bold",
            marginLeft: 10,
          },
          headerTintColor: "#000",
          headerLeftContainerStyle: { left: 10 },
        })}
      />
      <Stack.Screen
        name="Education"
        component={EducationScreen}
        options={({ route }) => ({
          headerShown: true,
          title: " ",
          headerBackTitle: route.params.title,
          headerTitleAlign: "left",
          headerBackTitleStyle: {
            fontWeight: "bold",
            marginLeft: 10,
          },
          headerTintColor: "#000",
          headerLeftContainerStyle: { left: 10 },
        })}
      />
      <Stack.Screen
        name="SportList"
        component={SportList}
        options={({ route }) => ({
          headerShown: true,
          title: " ",
          headerBackTitle: route.params.title,
          headerTitleAlign: "left",
          headerBackTitleStyle: {
            fontWeight: "bold",
            marginLeft: 10,
          },
          headerTintColor: "#000",
          headerLeftContainerStyle: { left: 10 },
        })}
      />
      <Stack.Screen
        name="FacilityList"
        component={FacilityList}
        options={({ route }) => ({
          headerShown: true,
          title: " ",
          headerBackTitle: route.params.title,
          headerTitleAlign: "left",
          headerBackTitleVisible: true,
          headerBackTitleStyle: {
            fontWeight: "bold",
            marginLeft: 10,
          },
          headerTintColor: "#000",
          headerLeftContainerStyle: { left: 10 },
        })}
      />
      <Stack.Screen
        name="SportDetail"
        component={SportDetail}
        options={{
          headerShown: true,
          title: " ",
          headerLeftContainerStyle: { left: 10 },
          headerBackTitle: "Back",
          headerTintColor: "#000",
          headerBackTitleStyle: {
            fontWeight: "bold",
            marginLeft: 10,
          },
          // headerRight: () => (
          //   <TouchableOpacity onPress={() => console.log("click share")}>
          //     <Image
          //       style={{ width: 24, height: 24, margin: 16 }}
          //       source={require("../../../assets/images/ic_share.png")}
          //       alt="image"
          //     />
          //   </TouchableOpacity>
          // ),
        }}
      />
      <Stack.Screen
        name="Payment"
        component={Payment}
        options={{
          headerShown: true,
          title: " ",
          headerLeftContainerStyle: { left: 10 },
          headerBackTitle: "Payment",
          headerBackTitleVisible: true,
          headerTintColor: "#000",
          headerBackTitleStyle: {
            fontWeight: "bold",
            marginLeft: 10,
          },
          headerBackground: () => (
            <Image
              style={{ width: width, height: 60 }}
              source={require("../../../assets/images/payment_background.png")}
              alt="image"
              resizeMode="cover"
            />
          ),
        }}
      />
      <Stack.Screen
        name="TopUp"
        component={TopUp}
        options={{
          headerShown: true,
          title: " ",
          headerLeftContainerStyle: { left: 10 },
          headerBackTitle: "My Credits",
          headerTintColor: "#000",
          headerBackTitleStyle: {
            fontWeight: "bold",
            marginLeft: 10,
          },
        }}
      />
      <Stack.Screen
        name="TopUpDetails"
        component={TopUpDetails}
        options={{
          headerShown: true,
          title: " ",
          headerLeftContainerStyle: { left: 10 },
          headerBackTitle: "Topup",
          headerTintColor: "#000",
          headerBackTitleStyle: {
            fontWeight: "bold",
            marginLeft: 10,
          },
          headerBackground: () => (
            <Image
              style={{ width: width, height: 90 }}
              source={require("../../../assets/images/payment_background.png")}
              alt="image"
              resizeMode="cover"
            />
          ),
        }}
      />
      <Stack.Screen
        name="VirtualAccountList"
        component={VirtualAccountList}
        options={{
          headerShown: true,
          title: " ",
          headerLeftContainerStyle: { left: 10 },
          headerBackTitle: "Topup",
          headerTintColor: "#000",
          headerBackTitleStyle: {
            fontWeight: "bold",
            marginLeft: 10,
          },
          headerBackground: () => (
            <Image
              style={{ width: width, height: 90 }}
              source={require("../../../assets/images/payment_background.png")}
              alt="image"
              resizeMode="cover"
            />
          ),
        }}
      />
      <Stack.Screen
        name="DetailActivity"
        component={DetailActivity}
        options={({ route }) => ({
          headerShown: true,
          title: " ",
          headerBackTitle: route.params.title,
          headerTitleAlign: "left",
          headerBackTitleStyle: {
            fontWeight: "bold",
            marginLeft: 10,
          },
          headerTintColor: "#000",
          headerLeftContainerStyle: { left: 10 },
        })}
      />
      <Stack.Screen
        name="DetailOrder"
        component={DetailOrder}
        options={({ route }) => ({
          headerShown: true,
          title: " ",
          headerBackTitle: route.params.title,
          headerTitleAlign: "left",
          headerBackTitleStyle: {
            fontWeight: "bold",
            marginLeft: 10,
          },
          headerTintColor: "#000",
          headerLeftContainerStyle: { left: 10 },
        })}
      />
      <Stack.Screen
        name="DetailViewAll"
        component={DetailViewAll}
        options={({ route }) => ({
          headerShown: true,
          title: " ",
          headerBackTitle: route.params.title,
          headerTitleAlign: "left",
          headerBackTitleStyle: {
            fontWeight: "bold",
            marginLeft: 10,
          },
          headerTintColor: "#000",
          headerLeftContainerStyle: { left: 10 },
        })}
      />
      <Stack.Screen
        name="DetailSchedule"
        component={DetailSchedule}
        options={({ route }) => ({
          headerShown: true,
          title: "Available schedule",
          headerBackTitle: "",
          headerBackTitleVisible: false,
          headerBackTitleStyle: {
            fontWeight: "bold",
            marginLeft: 10,
          },
          headerTitleStyle: {
            fontWeight: "bold",
            marginLeft: Platform?.OS === "ios" ? -130 : -5,
          },
          // headerRight: () => (
          //   <TouchableOpacity onPress={() => console.log("click share")}>
          //     <Image
          //       style={{ width: 24, height: 24, margin: 16 }}
          //       source={require("../../../assets/images/ic_search.png")}
          //       alt="image"
          //     />
          //   </TouchableOpacity>
          // ),
          headerTintColor: "#000",
          headerLeftContainerStyle: { left: 10 },
        })}
      />
      <Stack.Screen
        name="QRViewer"
        component={QRViewer}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="WebViewer"
        component={WebViewDisplay}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="ScanQR"
        component={ScanQR}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="AddFamily"
        component={AddFamily}
        options={({ route }) => ({
          headerShown: true,
          title: " ",
          headerBackTitle: route.params.title,
          headerTitleAlign: "left",
          headerBackTitleStyle: {
            fontWeight: "bold",
            marginLeft: 10,
          },
          headerTintColor: "#000",
          headerLeftContainerStyle: { left: 10 },
        })}
      />
      <Stack.Screen
        name="Participant"
        component={Participant}
        options={({ route }) => ({
          headerShown: true,
          title: "Invite Participants",
          headerBackTitle: "",
          headerBackTitleVisible: false,
          headerBackTitleStyle: {
            fontWeight: "bold",
            marginLeft: 10,
          },
          headerTitleStyle: {
            fontWeight: "bold",
            marginLeft: -130,
          },
          headerTintColor: "#000",
          headerLeftContainerStyle: { left: 10 },
        })}
      />
      <Stack.Screen
        name="AddKids"
        component={AddKids}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="FAQ"
        component={FAQ}
        options={{
          headerBackTitle: "FAQ",
          headerTitleAlign: "left",
          title: " ",
          headerBackTitleStyle: {
            fontWeight: "bold",
            marginLeft: 10,
          },
          headerTintColor: "#000",
          headerLeftContainerStyle: { left: 10 },
        }}
      />
      <Stack.Screen
        name="Terms"
        component={Terms}
        options={{
          headerBackTitle: "Terms and Conditions",
          headerTitleAlign: "left",
          title: " ",
          headerBackTitleStyle: {
            fontWeight: "bold",
            marginLeft: 10,
          },
          headerTintColor: "#000",
          headerLeftContainerStyle: { left: 10 },
        }}
      />
      <Stack.Screen
        name="Privacy"
        component={Privacy}
        options={{
          headerBackTitle: "Privacy Policy",
          headerTitleAlign: "left",
          title: " ",
          headerBackTitleStyle: {
            fontWeight: "bold",
            marginLeft: 10,
          },
          headerTintColor: "#000",
          headerLeftContainerStyle: { left: 10 },
        }}
      />
      <Stack.Screen
        name="Coupons"
        component={CouponList}
        options={{
          headerBackTitle: "Available Coupons",
          headerTitleAlign: "left",
          title: " ",
          headerBackTitleStyle: {
            fontWeight: "bold",
            marginLeft: 10,
          },
          headerTintColor: "#000",
          headerLeftContainerStyle: { left: 10 },
        }}
      />
      <Stack.Screen
        name="Additional"
        component={Additional}
        options={{
          headerShown: true,
          title: " ",
          headerLeftContainerStyle: { left: 10 },
          headerBackTitle: "Additional Items",
          headerTintColor: "#000",
          headerBackTitleStyle: {
            fontWeight: "bold",
            marginLeft: 10,
          },
          headerBackground: () => (
            <Image
              style={{ width: width, height: 60 }}
              source={require("../../../assets/images/payment_background.png")}
              alt="image"
              resizeMode="cover"
            />
          ),
        }}
      />
      <Stack.Screen
        name="SetPreferences"
        component={SetPreferences}
        options={{
          headerBackTitle: "Set Preferences",
          headerTitleAlign: "left",
          title: " ",
          headerBackTitleStyle: {
            fontWeight: "bold",
            marginLeft: 10,
          },
          headerTintColor: "#000",
          headerLeftContainerStyle: { left: 10 },
        }}
      />
      <Stack.Screen
        name="ReferralCode"
        component={ReferralCode}
        options={{
          headerBackTitle: "Referral Code",
          headerTitleAlign: "left",
          title: " ",
          headerBackTitleStyle: {
            fontWeight: "bold",
            marginLeft: 10,
          },
          headerTintColor: "#000",
          headerLeftContainerStyle: { left: 10 },
        }}
      />
      <Stack.Screen
        name="VerifyPin"
        component={VerifyPin}
        options={{
          headerBackTitle: "PIN Verification",
          headerTitleAlign: "left",
          title: " ",
          headerBackTitleStyle: {
            fontWeight: "bold",
            marginLeft: 10,
          },
          headerTintColor: "#000",
          headerLeftContainerStyle: { left: 10 },
        }}
      />
      <Stack.Screen
        name="MembershipPayment"
        component={MembershipPayment}
        options={{
          headerShown: true,
          title: " ",
          headerLeftContainerStyle: { left: 10 },
          headerBackTitle: "Payment",
          headerTintColor: "#000",
          headerBackTitleStyle: {
            fontWeight: "bold",
            marginLeft: 10,
          },
          headerBackground: () => (
            <Image
              style={{ width: width, height: 60 }}
              source={require("../../../assets/images/payment_background.png")}
              alt="image"
              resizeMode="cover"
            />
          ),
        }}
      />
      <Stack.Screen
        name="ChangePassword"
        component={ChangePassword}
        options={{
          headerShown: true,
          title: " ",
          headerLeftContainerStyle: { left: 10 },
          headerBackTitle: "Change Password",
          headerTintColor: "#000",
          headerBackTitleStyle: {
            fontWeight: "bold",
            marginLeft: 10,
          },
          headerTintColor: "#000",
          headerLeftContainerStyle: { left: 10 },
        }}
      />
    </Stack.Navigator>
  );
};

export default MainStack;
