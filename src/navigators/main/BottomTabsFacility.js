import React from "react";
import { StyleSheet, Image, View, TouchableOpacity, Text } from "react-native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import Home from "screens/Home";
import Activity from "screens/Activity";
import FacilityList from "../../screens/Sports/FacilityList"
import Inbox from "screens/Inbox";
import { Icon, QuestionIcon } from "native-base";
import Accounts from "screens/Accounts/Profile";
import { image } from "images";
import { useDispatch, useSelector } from "react-redux";
import _ from "lodash";

const styles = StyleSheet.create({
  icon: {
    width: 30,
    height: 30,
    marginVertical: 5,
  },
});

const Tab = createBottomTabNavigator();

const renderIcon = () => (
  <Icon resizeMode="contain" source={QuestionIcon} style={styles.icon} />
);

const BottomTabs = () => (
  <Tab.Navigator
    screenOptions={{
      headerShown: false,
    }}>
    <Tab.Screen
      name="Home"
      component={FacilityList}
      options={{
        tabBarIcon: ({tintColor, focused}) =>
          focused ? (
            <Image
              resizeMode="contain"
              source={image.home_active}
              style={styles.icon}
            />
          ) : (
            <Image
              resizeMode="contain"
              source={image.home_inactive}
              style={{width: 27, height: 27}}
            />
          ),
      }}
    />
    <Tab.Screen
      name="Activity"
      component={Activity}
      options={{
        headerShown: false,
        tabBarIcon: ({ tintColor, focused }) =>
          focused ? (
            <Image
              resizeMode="contain"
              source={image.activity_active}
              style={styles.icon}
              alt="image"
            />
          ) : (
            <Image
              resizeMode="contain"
              source={image.activity_inactive}
              style={{ width: 27, height: 27 }}
              alt="image"
            />
          ),
        // tabBarIcon: ({ focused, color, size }) =>
        //   renderIcon({ focused, color, size }),
      }}
    />
    <Tab.Screen
      name="Inbox"
      component={Inbox}
      options={{
        headerShown: false,
        tabBarIcon: ({ tintColor, focused }) =>
          focused ? (
            <Image
              resizeMode="contain"
              source={image.mail_active}
              style={{ width: 27, height: 27 }}
              alt="image"
            />
          ) : (
            <Image
              resizeMode="contain"
              source={image.mail_inactive}
              style={styles.icon}
              alt="image"
            />
          ),
        // tabBarIcon: ({ focused, color, size }) =>
        //   renderIcon({ focused, color, size }),
      }}
    />
    <Tab.Screen
      name="Account"
      component={Accounts}
      options={{
        headerShown: false,
        tabBarIcon: ({ tintColor, focused }) =>
          focused ? (
            <Image
              resizeMode="contain"
              source={image.profile_active}
              style={styles.icon}
              alt="image"
            />
          ) : (
            <Image
              resizeMode="contain"
              source={image.profile_inactive}
              style={{ width: 24, height: 24 }}
              alt="image"
            />
          ),
        // tabBarIcon: ({ focused, color, size }) =>
        //   renderIcon({ focused, color, size }),
      }}
    />
  </Tab.Navigator>
);

export default BottomTabs;
