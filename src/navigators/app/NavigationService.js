import * as React from "react";
import { CommonActions } from "@react-navigation/native";

export const navigationRef = React.createRef();

export const navigate = (name, params) =>
  navigationRef.current?.navigate(name, params);

export const navigationAction = (name, params) => {
  try {
    setTimeout(() => {
      try {
        const routeListStack = navigationRef?.current
          ?.getRootState()
          ?.routeNames?.map(d => {
            if (d === name) {
              return true;
            } else {
              return false;
            }
          })
          .filter(Boolean);
        if (routeListStack) {
          if (routeListStack?.length > 0) {
            // in the stack there is the route as expected
            navigationRef?.current?.dispatch(
              CommonActions?.navigate({ ...params }),
            );
          } else {
            // no route in current stack
            throw new Error(`${name} route was in another stack`);
          }
        } else {
          throw new Error(`${name} route was in another stack`);
        }
      } catch (error) {
        console.log("Error: ", error);
      }
    }, 1000);
  } catch (error) {
    console.log("Error: ", error);
  }
};

export default {
  navigationRef,
  navigate,
  navigationAction,
};
