import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { navigationRef } from "./NavigationService";
import { Text, View, StatusBar, Platform } from "react-native";
import Auth from "../auth";
import Main from "../main";
import Selectors from "selectors";
import { useSelector } from "react-redux";

const AppNavigator = () => {
  const token = useSelector(Selectors.getToken);

  const linking = {
    prefixes: ["https://kyzn.page.link/", "kyzn://"],
  };

  return (
    <NavigationContainer
      linking={linking}
      fallback={
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
          <Text>Loading...</Text>
        </View>
      }
      ref={navigationRef}>
      <StatusBar
        barStyle="dark-content"
        backgroundColor={Platform?.OS === "android" ? "white" : "transparent"}
      />
      {!token && <Auth />}
      {token && <Main />}
    </NavigationContainer>
  );
};

export default AppNavigator;
