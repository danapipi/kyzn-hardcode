import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { StyleSheet, Image, View, TouchableOpacity, Text } from "react-native";
import Landing from "screens/Landing";
import Login from "screens/Login";
import SignUp from "screens/SignUp";
import SignUpDetail from "screens/SignUpDetail";
import SignUpConfirmation from "screens/SignUpConfirmation";
import SetPreferences from "screens/SetPreferences";

const Stack = createStackNavigator();

const Auth = () => (
  <Stack.Navigator
    screenOptions={{ headerShown: false }}
    initialRouteName="Landing"
  >
    <Stack.Screen name="Landing" component={Landing} />
    <Stack.Screen name="Login" component={Login} />
    <Stack.Screen
      name="SignUp"
      component={SignUp}
      options={({ route }) => ({
        headerShown: true,
        title: " ",
        headerBackTitle: route.params.title,
        headerTitleAlign: "left",
        headerBackTitleStyle: {
          fontWeight: "bold",
          marginLeft: 10,
        },
        headerTintColor: "#000",
        headerLeftContainerStyle: { left: 10 },
      })}
    />
    <Stack.Screen
      name="SignUpDetail"
      component={SignUpDetail}
      options={({ route }) => ({
        headerShown: true,
        title: "Personal Information",
        headerBackTitle: "",
        headerBackTitleVisible: false,
        headerTitleAlign: "left",
        headerBackTitleStyle: {
          fontWeight: "bold",
          marginLeft: 10,
        },
        headerTitleStyle: {
          fontWeight: "bold",
          marginLeft: -20,
        },
        headerTintColor: "#000",
        headerLeftContainerStyle: { left: 10 },
      })}
    />
    <Stack.Screen
      name="SignUpConfirmation"
      component={SignUpConfirmation}
      options={({ route }) => ({
        headerShown: true,
        title: "User Agreement",
        headerBackTitle: "",
        headerBackTitleVisible: false,
        headerTitleAlign: "left",
        headerBackTitleStyle: {
          fontWeight: "bold",
          marginLeft: 10,
        },
        headerTitleStyle: {
          fontWeight: "bold",
          marginLeft: -20,
        },
        headerTintColor: "#000",
        headerLeftContainerStyle: { left: 10 },
      })}
    />
  </Stack.Navigator>
);

export default Auth;
