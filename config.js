const state = process.env.NODE_ENV === "development";

const env = {
  local: "local",
  dev: "dev",
  stage: "stage",
  production: "production",
};

const API_URL = {
  local: "",
  dev: "",
  stage: "https://api.kyzn.life",
  production: "https://api.kyzn.life",
};

//https://kyzn-api-staging.tbtft.com

const XENDIT_API_URL = {
  local: "",
  dev: "",
  stage: "https://api.xendit.co",
  production: "https://api.xendit.co",
};

const currentEnv = state ? env.stage : env.production;
console.log("state", state, process.env.NODE_ENV);
export const BASE_URL = API_URL[currentEnv];
export const XENDIT_BASE_URL = API_URL[XENDIT_API_URL];
